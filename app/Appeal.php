<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appeal extends Model
{
    public function background(){
    	return $this->belongsTo('App\Background', 'background_id');
    }

    public function user(){
    	return $this->belongsTo('App\User', 'user_id');
    }
}
