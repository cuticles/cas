<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Collection extends Model
{
    public function transmits(){
    	return $this->hasMany('App\Transmit', 'collection_id')->where('is_forward', '=', '0');
    }

    public function transmitsForwards(){
        return $this->hasMany('App\Transmit', 'collection_id')->where('is_forward', '=', '1')->orderBy('created_at', 'desc');
    }

    public function transmitsAll(){
        return $this->hasMany('App\Transmit', 'collection_id');
    }

    public function type(){
        return $this->belongsTo('App\Type', 'type_id');
    }
    public function transmit(){
        return $this->belongsTo('App\Transmit', 'collection_id');
    }

    public function status(){
        return $this->belongsTo('App\Type', 'status_id');
    }

    public function remarks(){
        return $this->hasMany('App\Remark');
    }

    public function documents(){
    	return $this->hasMany('App\Document')->orderBy('created_at', 'desc');
    }

    public function documentsSortedById(){
        return $this->hasMany('App\Document')->orderBy('id', 'desc');
    }

    public function documentsAsc(){
        return $this->hasMany('App\Document')->orderBy('created_at', 'asc');
    }

    public function trackings(){
        return $this->hasMany('App\Tracking')->orderBy('created_at', 'desc');
    }
    public function trackingsAsc(){
        return $this->hasMany('App\Tracking')->orderBy('created_at', 'asc');
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code', 'title', 'description', 'current_status', 'type', 'tags', 'trashed'
    ];
}
