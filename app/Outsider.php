<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Outsider extends Model
{
    public function adder(){
    	return $this->belongsTo('App\User', 'user_id');
    }

    public function location(){
    	return $this->belongsTo('App\Location','location_id');
    }
}
