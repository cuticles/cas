<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    public function collection(){
        return $this->belongsTo('App\Collection', 'content_id');
    }
    public function appeal(){
        return $this->belongsTo('App\Appeal', 'content_id');
    }
    public function rootColl(){
        return $this->belongsTo('App\Collection', 'content_root');
    }
    public function tracking(){
        return $this->belongsTo('App\Transmit', 'content_root')->where('is_forward', '=', '1');
    }
    public function type(){
        return $this->belongsTo('App\Type', 'content_root');
    }
    public function background(){
        return $this->belongsTo('App\Background', 'content_root');
    }
    public function document(){
        return $this->belongsTo('App\Document', 'content_id');
    }
    public function personnel(){
        return $this->belongsTo('App\User', 'content_id');
    }
    public function adder(){
        return $this->belongsTo('App\User', 'user_id');
    }
    public function location(){
        return $this->belongsTo('App\Location','content_root');
    }
    public function credential(){
        return $this->belongsTo('App\Credential','content_id');
    }    
    public function updater(){
        return $this->belongsTo('App\Credential','content_root');
    }
    public function user(){
        return $this->belongsTo('App\User','content_id');
    }
    public function group(){
        return $this->belongsTo('App\Group','content_root');
    }
    public function membership(){
        return $this->belongsTo('App\Membership','content_root');
    }
    public function users(){
        return $this->belongsTo('App\User','content_root');
    }    
    public function logs(){
        return $this->hasMany('App\Log')->orderBy('created_at', 'desc');
    }
    public function addOutsider(){
        return $this->belongsTo('App\Outsider','content_id');
    }
    public function addLocation(){
        return $this->belongsTo('App\Location','content_id');
    }
    public function addType(){
        return $this->belongsTo('App\Type','content_id');
    }
    public function addPoint(){
        return $this->belongsTo('App\Point','content_id');
    }
    public function addPosition(){
        return $this->belongsTo('App\Position','content_id');
    }



    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'content',
    ];
}
