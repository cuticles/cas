<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Point extends Model
{
    public function adder(){
    	return $this->belongsTo('App\User', 'user_id');
    }

    protected $fillable = [
        'name', 'value', 'user_id',
    ];
}
