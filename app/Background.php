<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Background extends Model
{
    public function type(){
    	return $this->belongsTo('App\Type', 'type_id');
    }

    public function user(){
    	return $this->belongsTo('App\User', 'user_id');
    }

    public function appeal(){
    	return $this->hasOne('App\Appeal');
    }

    protected $fillable = [
        'user_id', 'type_id', 'school', 'year_start', 'year_end', 'degree_level', 'course'
    ];
}
