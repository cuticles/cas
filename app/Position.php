<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
    public function adder(){
    	return $this->belongsTo('App\User', 'user_id');
    }

    protected $fillable = [
        'name', 'category', 'user_id',
    ];
}
