<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transmit extends Model
{
	public function collection(){
		return $this->belongsTo('App\Collection', 'collection_id');
	}

	public function receiver(){
		return $this->belongsTo('App\User', 'to_user_id');
	}

	public function sender(){
		return $this->belongsTo('App\User', 'from_user_id');
	}

	public function fromOutsideUploader(){
		return $this->belongsTo('App\User', 'uploader');
	}

	public function userUploader(){
		return $this->belongsTo('App\User', 'uploader');
	}

	public function outsider(){
		return $this->belongsTo('App\Outsider', 'outsider_id');
	}
	public function from_outside(){
		return $this->belongsTo('App\Outsider', 'from_outside');
	}


    protected $fillable = [
        'collection_id', 'to_user_id', 'from_user_id', 'is_forward', 'outside_sender', 'outside_location', 'from_outside_uploader', 'from_outside','trashed_by_sender', 'trashed_by_receiver'
    ];
}
