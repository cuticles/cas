<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Remark extends Model
{
	public function collection(){
		return $this->belongsTo('App\Collection');
	}
	public function user(){
		return $this->belongsTo('App\User');
	}
    protected $fillable = [
        'user_id', 'collection_id', 'content'
    ];
}
