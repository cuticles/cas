<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    public function handler(){
    	return $this->hasOne('App\User', 'id', 'supervisor');
    }

    public function members(){
    	return $this->hasMany('App\Membership');
    }

    protected $fillable = [
        'supervisor', 'name', 'description', 'cover'
    ];
}
