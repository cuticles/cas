<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Credential extends Model
{
	public function user(){
		return $this->belongsTo('App\User', 'user_id');
	}

	public function userUpdater(){
		return $this->belongsTo('App\User', 'updater');
	}

	public function documents(){
		return $this->hasMany('App\Document');
	}

	public function point(){
		return $this->belongsTo('App\Point', 'point_id');
	}

    protected $fillable = [
        'user_id', 'updater', 'title', 'point_id', 'description', 'doc_count', 'credential_id'
    ];

}
