<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    //
    public function receiver(){
    	return $this->belongsTo('App\User','to_user_id');
    }

    public function sender(){
    	return $this->belongsTo('App\User', 'from_user_id');
    }

    public function remark(){
        return $this->belongsTo('App\Remark', 'object_id');
    }

    public function documents(){
    	return $this->belongsTo('App\Document', 'object_id');
    }

    public function transmit(){
        return $this->belongsTo('App\Transmit', 'object_id');
    }

    public function credential(){
        return $this->belongsTo('App\Credential', 'object_id');
    }
    public function background(){
        return $this->belongsTo('App\Background', 'object_id');
    }
    public function collection(){
        return $this->belongsTo('App\Collection', 'object_id');
    }
    public function group(){
        return $this->belongsTo('App\Group', 'object_id');
    }
    public function tracking(){
        return $this->belongsTo('App\Tracking', 'object_id');
    }
    public function notifs(){
        return $this->hasMany('App\Notification')->orderBy('created_at', 'asc');
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'from_user_id', 'to_user_id', 'type','object_id', 'is_seen'
    ];
}
