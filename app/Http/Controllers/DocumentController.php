<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Credential;
use App\Document;
use App\Log;
use Auth;

class DocumentController extends Controller
{

    public function addDocument(Request $request, $id) {
    	$document = $request->doc_name;
        $user = User::findOrFail($id);
    
	    $doc = new Document;
	    $name = $document->getClientOriginalName();
	    $extension = $document->getClientOriginalExtension();
	    $size = $document->getSize();

	    $fauxname = Str::random(10)."_".time().".".$extension;
	    $destination = public_path('uploads');
	    $document->move($destination, $fauxname);

	    $doc->credential_id = $request->cred_id;
	    $doc->file_name = $name;
	    $doc->file_size = $size;
	    $doc->file_extension = $extension;
	    $doc->file_path = $destination."\\".$fauxname;
	    $doc->is_credential_doc = '1';

	    $doc->save();

	    $log = new Log;
        $log->content = 'You added a credential, '.$doc->file_name.' of '.$user->firstname.' '.$user->middlename.' '.$user->surname.'</span>';
        $log->user_id = Auth::user()->id;
        // $log->action = '1';
        // $log->type = '3'; 
        // $log->content_root = $id;
        $log->save();

		return redirect('/personnelUtil/'.$id.'/Credentials');
	    
 	}

  	public function renameDocument(Request $request) {
    	$document = Document::findOrFail($request->id);

        $log = new Log;
        $log->content = 'You renamed the credential, '.$document->file_name.' to '.$request->title.'.';
        $log->user_id = Auth::user()->id;

    	$document->file_name = $request->title;
		$document->save();

    	

    	return json_encode($document); 
  	}

	public function deleteDocument(Request $request) {
    	$id = $request->id;
    	$document = Document::findOrFail($id);
    	$credential = $document->credential;
    	$credential->numDocs = count($credential->documents);

        $log = new Log;
        $log->content = 'You deleted the credential, '.$document->file_name.'.';
        $log->user_id = Auth::user()->id;
        $log->save();

        $document->delete();
    	return json_encode($credential);
  	}


}
