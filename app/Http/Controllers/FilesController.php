<?php
namespace App\Http\Controllers;
use App\User;
use Auth;
use App\Document;
use App\Collection;
use App\Transmit;
use App\Group;
use App\Outsider;
use App\Type;
use App\Location;
use App\Log;
use Illuminate\Support\Facades\DB;
use Carbon;
use Storage;
use Response;

use Illuminate\Http\Request;

class FilesController extends Controller
{
    public function sentFiles(){
    	$icons = array("pdf" => "file pdf outline icon", "docx" => "file word outline icon", "doc" => "file word outline icon", "xls" => "file excel outline icon", "xlsx" => "file excel outline icon", "zip" => "file archive outline icon", "rar" => "file archive outline icon", "pptx" => "file powerpoint outline icon", "ppt" => "file powerpoint outline icon", "txt" => "file alternate outline icon");

        function arrayfy($objs){
            $arr = [];
            foreach ($objs as $obj) {
                array_push($arr, $obj->id);
            }
            return $arr;
        }

        function extractObjects($ids){
            $objects = [];
            foreach ($ids as $id) {
                $doc = Document::findOrFail($id);
                array_push($objects, $doc);
            }
            return $objects;
        }

        $groups = Group::get();
    	$user = Auth::user();
        $outsiders = Outsider::all();
        $types = Type::where('category', '1')->get();
        $locs = Location::all();
        $partitions = [];
        $docs_chunks = [];
    	$sentDocs = [];

        $doc_ids = DB::table('transmits')->join('collections', 'transmits.collection_id', '=', 'collections.id')->join('documents', 'collections.id', '=', 'documents.collection_id')->where('transmits.from_user_id', '=', Auth::user()->id)->where('transmits.trashed_by_sender', '=', '0')->where('transmits.is_forward', '=', '0')->orderBy('documents.id', 'desc')->select('documents.id')->get();
        $doc_ids = arrayfy($doc_ids);
        $doc_ids = array_unique($doc_ids);

        $numItems = count($doc_ids);

        if(count($doc_ids) > 0){
            $docs_chunks = array_chunk($doc_ids, 10);

            for($i = 0; $i < count($docs_chunks); $i++){
                $end = $docs_chunks[$i][0];
                $start = $docs_chunks[$i][count($docs_chunks[$i])-1];

                $partitions[strval($i+1)] = $start.':'.$end;
            }

            $sentDocs = extractObjects($docs_chunks[0]);
        }

    	$users = DB::table('users')->where('id', '!=', Auth::user()->id)->select('users.firstname', 'users.middlename', 'users.surname', 'users.profile_pic', 'users.id')->get();

    	return view('admin.sent', compact('icons', 'sentDocs', 'users', 'groups', 'partitions', 'numItems', 'outsiders', 'types', 'locs'));
        /*print_r($partitions);*/
    }

    public function sentCollections(){
        function arrayfy($objs){
            $arr = [];
            foreach ($objs as $obj) {
                array_push($arr, $obj->id);
            }
            return $arr;
        }

        function extractObjects($ids){
            $objects = [];
            foreach ($ids as $id) {
                $coll = Collection::findOrFail($id);
                $transmit = $coll->transmits->first();
                $coll->receiver = $transmit->receiver;
                $coll->from_outside = $transmit->from_outside;
                $coll->to_outside = $transmit->to_outside;
                $coll->outsider = $transmit->outsider;
                array_push($objects, $coll);
            }
            return $objects;
        }

        $groups = Group::get();
    	$icons = array('report'=>'images/analysis.png', 'certificate' => 'images/diploma.png', 'request' => 'images/application.png');
    	$user = Auth::user();
        $outsiders = Outsider::all();
        $types = Type::where('category', '1')->get();
        $locs = Location::all();
    	$user_id = Auth::user()->id;
    	$users = DB::table('users')->where('id', '!=', Auth::user()->id)->select('users.firstname', 'users.middlename', 'users.surname', 'users.profile_pic', 'users.id')->get();
        $sentCollections = [];
        $partitions = [];

        $coll_ids = DB::table('transmits')->join('collections', 'transmits.collection_id', '=', 'collections.id')->where('transmits.from_user_id', '=', Auth::user()->id)->where('transmits.trashed_by_sender', '=', '0')->where('transmits.is_forward', '=', '0')->orderBy('collections.id', 'desc')->select('collections.id')->get();
        $coll_ids = arrayfy($coll_ids);
        $coll_ids = array_unique($coll_ids);

        $numItems = count($coll_ids);

        if(count($coll_ids) > 0){
            $colls = array_chunk($coll_ids, 10);

            for($i = 0; $i < count($colls); $i++){
                $end = $colls[$i][0];
                $start = $colls[$i][count($colls[$i])-1];

                $partitions[strval($i+1)] = $start.':'.$end;
            }

            $sentCollections = extractObjects($colls[0]);
        }

    	return view('admin.sent_collections', compact('users', 'sentCollections', 'icons', 'groups', 'partitions', 'outsiders', 'types', 'locs', 'numItems'));
    }

    public function receivedFiles(){
        $icons = array("pdf" => "file pdf outline icon", "docx" => "file word outline icon", "doc" => "file word outline icon", "xls" => "file excel outline icon", "xlsx" => "file excel outline icon", "zip" => "file archive outline icon", "rar" => "file archive outline icon", "pptx" => "file powerpoint outline icon", "ppt" => "file powerpoint outline icon", "txt" => "file alternate outline icon");
        function arrayfy($objs){
            $arr = [];
            foreach ($objs as $obj) {
                array_push($arr, $obj->id);
            }
            return $arr;
        }

        function extractObjects($ids){
            $objects = [];
            foreach ($ids as $id) {
                $doc = Document::findOrFail($id);
                array_push($objects, $doc);
            }
            return $objects;
        }

        $user = Auth::user();
        $groups = Group::get();
        $user_id = Auth::user()->id;
        $outsiders = Outsider::all();
        $types = Type::where('category', '1')->get();
        $locs = Location::all();
        $receivedDocs = [];
        $partitions = [];
        $doc_ids = DB::table('transmits')->join('collections', 'transmits.collection_id', '=', 'collections.id')->join('documents', 'collections.id', '=', 'documents.collection_id')->where('transmits.to_user_id', '=', Auth::user()->id)->where('transmits.trashed_by_receiver', '=', '0')->where('transmits.is_forward', '=', '0')->orderBy('documents.id', 'desc')->select('documents.id')->get();
        $doc_ids = arrayfy($doc_ids);
        $doc_ids = array_unique($doc_ids);

        $numItems = count($doc_ids);
        if(count($doc_ids) > 0){
            $docs_chunks = array_chunk($doc_ids, 10);

            for($i = 0; $i < count($docs_chunks); $i++){
                $end = $docs_chunks[$i][0];
                $start = $docs_chunks[$i][count($docs_chunks[$i])-1];

                $partitions[strval($i+1)] = $start.':'.$end;
            }

            $receivedDocs = extractObjects($docs_chunks[0]);
        }
        
        $users = DB::table('users')->where('id', '!=', Auth::user()->id)->select('users.firstname', 'users.middlename', 'users.surname', 'users.profile_pic', 'users.id')->get();

        return view('admin.received', compact('icons', 'receivedDocs', 'users', 'groups', 'partitions', 'numItems', 'outsiders', 'types', 'locs'));
        // print_r($partitions);
    }

    public function receivedCollections(){
        function extractObjects($ids){
            $objects = [];
            foreach ($ids as $id) {
                $coll = Collection::findOrFail($id);
                $transmit = $coll->transmits->first();
                $coll->sender = $transmit->sender;
                $coll->from_outside = $transmit->from_outside;
                $coll->outsider = $transmit->outsider;
                $coll->uploader = $transmit->fromOutsideUploader;
                
                array_push($objects, $coll);
            }
            return $objects;
        }

        function arrayfy($objs){
            $arr = [];
            foreach ($objs as $obj) {
                array_push($arr, $obj->id);
            }
            return $arr;
        }

        $groups = Group::get();
        $icons = array('report'=>'images/analysis.png', 'certificate' => 'images/diploma.png', 'request' => 'images/application.png');
        $user = Auth::user();
        $user_id = Auth::user()->id;
        $users = DB::table('users')->where('id', '!=', Auth::user()->id)->select('users.firstname', 'users.middlename', 'users.surname', 'users.profile_pic', 'users.id')->get();
        /*$transmits = $user->incomingTransmits;*/
        $receivedCollections = [];
        $outsiders = Outsider::all();
        $types = Type::where('category', '1')->get();
        $locs = Location::all();
        $partitions = [];
        $coll_ids = DB::table('transmits')->join('collections', 'transmits.collection_id', '=', 'collections.id')->where('transmits.to_user_id', '=', Auth::user()->id)->where('transmits.trashed_by_receiver', '=', '0')->where('transmits.is_forward', '=', '0')->orderBy('collections.id', 'desc')->select('collections.id')->get();
        $numItems = count($coll_ids);
        $coll_ids = arrayfy($coll_ids);
        $coll_ids = array_unique($coll_ids);

        if(count($coll_ids) > 0){
            $colls = array_chunk($coll_ids, 10);

            for($i = 0; $i < count($colls); $i++){
                $end = $colls[$i][0];
                $start = $colls[$i][count($colls[$i])-1];

                $partitions[strval($i+1)] = $start.':'.$end;
            }

            $receivedCollections = extractObjects($colls[0]);
        }

        return view('admin.received_collections', compact('users', 'receivedCollections', 'icons', 'groups', 'partitions', 'outsiders', 'types', 'locs', 'numItems'));
    }

    public function filterSentDocs(Request $request){
        function uniquifyDocs($docs){
            $doc_ids = [];
            $documents = [];

            foreach ($docs as $doc){
                if(!(in_array($doc->id, $doc_ids))){
                    array_push($documents, $doc);
                    array_push($doc_ids, $doc->id);
                }
            }
            return $documents;
        }

        $type = $request->fileType;
        $sortBy = $request->sortBy;
        $sortDir = $request->sortDir;
        $start = $request->start;
        $end = $request->end;

        $user = Auth::user();
        $docs = "";
        if($type == 'all'){
            $docs = DB::table('transmits')->join('collections', 'transmits.collection_id', '=', 'collections.id')->join('documents', 'collections.id', '=', 'documents.collection_id')->where('documents.id', '>=', $start)->where('documents.id', '<=', $end)->where('transmits.from_user_id', '=', Auth::user()->id)->where('transmits.trashed_by_sender', '=', '0')->where('transmits.is_forward', '=', '0')->orderBy('documents.'.$sortBy, $sortDir)->select('documents.*')->get();
        }
        else{
            $docs = DB::table('transmits')->join('collections', 'transmits.collection_id', '=', 'collections.id')->join('documents', 'collections.id', '=', 'documents.collection_id')->where('documents.id', '>=', $start)->where('documents.id', '<=', $end)->where('transmits.from_user_id', '=', Auth::user()->id)->where('transmits.trashed_by_sender', '=', '0')->where('transmits.is_forward', '=', '0')->where('documents.file_extension', '=', $type)->orderBy('documents.'.$sortBy, $sortDir)->select('documents.*')->get();
        }

        $docs = uniquifyDocs($docs);

        foreach($docs as $doc){
            $doc->date_created = Carbon\Carbon::parse($doc->created_at)->format('M d');
        }

        return json_encode($docs);
    }

    public function filterReceivedDocs(Request $request){
        $type = $request->fileType;
        $sortBy = $request->sortBy;
        $sortDir = $request->sortDir;
        $start = $request->start;
        $end = $request->end;

        $received = '';
        if($type == 'all'){
            $received = DB::table('transmits')->join('collections', 'transmits.collection_id', '=', 'collections.id')->join('documents', 'collections.id', '=', 'documents.collection_id')->where('documents.id', '>=', $start)->where('documents.id', '<=', $end)->where('transmits.to_user_id', '=', Auth::user()->id)->where('transmits.trashed_by_receiver', '=', '0')->where('transmits.is_forward', '=', '0')->orderBy('documents.'.$sortBy, $sortDir)->select('documents.*')->get();
        }
        else{
            $received = DB::table('transmits')->join('collections', 'transmits.collection_id', '=', 'collections.id')->join('documents', 'collections.id', '=', 'documents.collection_id')->where('documents.id', '>=', $start)->where('documents.id', '<=', $end)->where('transmits.to_user_id', '=', Auth::user()->id)->where('transmits.trashed_by_receiver', '=', '0')->where('transmits.is_forward', '=', '0')->where('documents.file_extension', '=', $type)->orderBy('documents.'.$sortBy, $sortDir)->select('documents.*')->get();
        }

        foreach($received as $doc){
            $doc->date_created = Carbon\Carbon::parse($doc->created_at)->format('M d');
        }

        return json_encode($received);
    }

    public function filterSentCollections(Request $request){
        function uniquifyColls($colls){
            $coll_ids = [];
            $collections = [];

            foreach ($colls as $coll){
                if(!(in_array($coll->id, $coll_ids))){
                    $transmit = Transmit::where('collection_id', '=', $coll->id)->first();
                    $collection = Collection::findOrFail($coll->id);
                    $collection->receiver = $transmit->receiver;
                    $collection->to_outside = $transmit->to_outside;
                    $collection->outsider = $transmit->outsider;
                    array_push($collections, $collection);
                    array_push($coll_ids, $coll->id);
                }
            }
            return $collections;
        }

        $icons = array('report'=>'images/analysis.png', 'certificate' => 'images/diploma.png', 'request' => 'images/application.png');
        $type = $request->type;
        $sortBy = $request->sortBy;
        $sortDir = $request->sortDir;
        $user_id = Auth::user()->id;
        $start = $request->start;
        $end = $request->end;

        $collections = '';

        if($type == 'all'){
            if($sortBy != 'current_status'){
                $collections = DB::table('transmits')->join('collections', 'transmits.collection_id', '=', 'collections.id')->where('collections.id', '>=', $start)->where('collections.id', '<=', $end)->where('transmits.from_user_id', '=', Auth::user()->id)->where('transmits.trashed_by_sender', '=', '0')->where('transmits.is_forward', '=', '0')->orderBy('collections.'.$sortBy, $sortDir)->select('collections.id')->get();
            }
            else{
                $collections = DB::table('transmits')->join('collections', 'transmits.collection_id', '=', 'collections.id')->join('types', 'collections.status_id', '=', 'types.id')->where('collections.id', '>=', $start)->where('collections.id', '<=', $end)->where('transmits.from_user_id', '=', Auth::user()->id)->where('transmits.trashed_by_sender', '=', '0')->where('transmits.is_forward', '=', '0')->orderBy('types.name', $sortDir)->select('collections.id')->get();
            }    
        }
        else{
            if($sortBy != 'current_status'){
                $collections = DB::table('transmits')->join('collections', 'transmits.collection_id', '=', 'collections.id')->join('types', 'collections.type_id', '=', 'types.id')->where('collections.id', '>=', $start)->where('collections.id', '<=', $end)->where('transmits.from_user_id', '=', Auth::user()->id)->where('transmits.trashed_by_sender', '=', '0')->where('transmits.is_forward', '=', '0')->where('types.name', $type)->orderBy('collections.'.$sortBy, $sortDir)->select('collections.id')->get();
            }
            else{
                $collections = DB::table('transmits')->join('collections', 'transmits.collection_id', '=', 'collections.id')->join('types', 'collections.type_id', '=', 'types.id')->where('collections.id', '>=', $start)->where('collections.id', '<=', $end)->where('transmits.from_user_id', '=', Auth::user()->id)->where('transmits.trashed_by_sender', '=', '0')->where('transmits.is_forward', '=', '0')->where('types.name', $type)->orderBy('types.name', $sortDir)->select('collections.id')->get();
            }
        }

        $collections = uniquifyColls($collections);

        return view('loaders.sent-collections-loader', compact('collections', 'icons'));
    }

    public function filterReceivedCollections(Request $request){
        function retrieveColls($colls){
            $collections = [];

            foreach ($colls as $coll){
                $transmit = Transmit::where('collection_id', '=', $coll->id)->first();
                $collection = Collection::findOrFail($coll->id);
                $collection->sender = $transmit->sender;
                $collection->from_outside = $transmit->from_outside;
                $collection->outside_sender = $transmit->outside_sender;
                $collection->uploader = $transmit->fromOutsideUploader;
                array_push($collections, $collection);
            }
            return $collections;
        }

        $icons = array('report'=>'images/analysis.png', 'certificate' => 'images/diploma.png', 'request' => 'images/application.png');
        $type = $request->type;
        $sortBy = $request->sortBy;
        $sortDir = $request->sortDir;
        $user_id = Auth::user()->id;
        $start = $request->start;
        $end = $request->end;

        $collections = '';

        if($type == 'all'){
            if($sortBy != 'current_status'){
                $collections = DB::table('transmits')->join('collections', 'transmits.collection_id', '=', 'collections.id')->where('collections.id', '>=', $start)->where('collections.id', '<=', $end)->where('transmits.to_user_id', '=', Auth::user()->id)->where('transmits.trashed_by_receiver', '=', '0')->where('transmits.is_forward', '=', '0')->orderBy('collections.'.$sortBy, $sortDir)->select('collections.id')->get();
            }
            else{
                $collections = DB::table('transmits')->join('collections', 'transmits.collection_id', '=', 'collections.id')->join('types', 'collections.status_id', '=', 'types.id')->where('collections.id', '>=', $start)->where('collections.id', '<=', $end)->where('transmits.to_user_id', '=', Auth::user()->id)->where('transmits.trashed_by_receiver', '=', '0')->where('transmits.is_forward', '=', '0')->orderBy('types.name', $sortDir)->select('collections.id')->get();
            }
        }
        else{
            if($sortBy != 'current_status'){
                $collections = DB::table('transmits')->join('collections', 'transmits.collection_id', '=', 'collections.id')->join('types', 'collections.type_id', '=', 'types.id')->where('collections.id', '>=', $start)->where('collections.id', '<=', $end)->where('transmits.to_user_id', '=', Auth::user()->id)->where('transmits.trashed_by_receiver', '=', '0')->where('transmits.is_forward', '=', '0')->where('types.name', $type)->orderBy('collections.'.$sortBy, $sortDir)->select('collections.id')->get();
            }
            else{
                $collections = DB::table('transmits')->join('collections', 'transmits.collection_id', '=', 'collections.id')->join('types', 'collections.type_id', '=', 'types.id')->where('collections.id', '>=', $start)->where('collections.id', '<=', $end)->where('transmits.to_user_id', '=', Auth::user()->id)->where('transmits.trashed_by_receiver', '=', '0')->where('transmits.is_forward', '=', '0')->where('types.name', $type)->orderBy('types.name', $sortDir)->select('collections.id')->get();
            }
        }

        $collections = retrieveColls($collections);

        return view('loaders.received-collections-loader', compact('collections', 'icons'));
    }

    public function renameDoc(Request $request){
        $doc = Document::findOrFail($request->id);
        $coll = DB::table('collections')->join('documents','documents.collection_id','=','collections.id')->where('documents.id','=',$request->id)->select('collections.id')->get();
        $log = new Log;
        $log->user_id = Auth::user()->id;
        $log->content = 'You renamed the document, '.$doc->file_name.' to '.$request->newFileName.'.';
        // $log->action = '1';
        // $log->type = '3';
        // $log->content_root = $credential->user_id;
        $log->save();
        $fileName = $request->newFileName;
        $prev_file_name = $doc->file_name;

        $doc->file_name = $fileName;
        $doc->update();


        return $doc->file_name;
    }

    public function searchReceivedDocs(Request $request){
        function uniquifyDocs($docs){
            $doc_ids = [];
            $documents = [];

            foreach ($docs as $doc){
                if(!(in_array($doc->id, $doc_ids))){
                    array_push($documents, $doc);
                    array_push($doc_ids, $doc->id);
                }
            }
            return $documents;
        }

        $doc = $request->searchValue;
        $table = '';
        $receivedOut = [];
        $searchBy = $request->searchByValue;
        if($searchBy == 'file_name'){
            $table = 'documents';
        }
        else if($searchBy == 'code'){
            $table = 'collections';
        }

        if($searchBy == 'sender'){
            $receivedDocs = DB::table('documents')->leftJoin('collections','documents.collection_id','=','collections.id')->leftJoin('transmits','collections.id','=','transmits.collection_id')->leftJoin('users','transmits.from_user_id','=','users.id')->where('transmits.to_user_id','=',Auth::user()->id)->where('transmits.trashed_by_receiver','=','0')->where(DB::raw("CONCAT(`users`.`firstname`, `users`.`middlename`, `users`.`surname`)"),'LIKE', '%' . $doc . '%')->select('documents.*')->get();

            $receivedOut = DB::table('documents')->leftJoin('collections','documents.collection_id','=','collections.id')->leftJoin('transmits','collections.id','=','transmits.collection_id')->leftJoin('outsiders', 'transmits.outsider_id','=','outsiders.id')->where('transmits.to_user_id','=',Auth::user()->id)->where('transmits.trashed_by_sender','=','0')->where('transmits.trashed_by_receiver','=','0')->where('transmits.to_outside','1')->where('outsiders.name','LIKE', '%' . $doc . '%')->select('documents.*')->get();
        }
        else{
            $receivedDocs = DB::table('documents')->leftJoin('collections','documents.collection_id','=','collections.id')->leftJoin('transmits','collections.id','=','transmits.collection_id')->leftJoin('users','transmits.from_user_id','=','users.id')->where('transmits.to_user_id','=',Auth::user()->id)->where('transmits.trashed_by_receiver','=','0')->where($table.'.'.$searchBy, 'LIKE', '%' . $doc . '%')->select('documents.*')->get();
        }
        $receivedDocs = uniquifyDocs($receivedDocs);
        $receivedOut = uniquifyDocs($receivedOut);

        $receivedDocs = array_merge($receivedDocs, $receivedOut); 


        return json_encode($receivedDocs);
    }

    public function searchSentDocs(Request $request){
        function uniquifyDocs($docs){
            $doc_ids = [];
            $documents = [];

            foreach ($docs as $doc){
                if(!(in_array($doc->id, $doc_ids))){
                    array_push($documents, $doc);
                    array_push($doc_ids, $doc->id);
                }
            }
            return $documents;
        }

        $doc = $request->search;
        $table = '';
        $sentOut = [];
        $searchBy = $request->searchBy;
        if($searchBy == 'file_name'){
            $table = 'documents';
        }
        else if($searchBy == 'code'){
            $table = 'collections';
        }

        if($searchBy == 'receiver'){

            $sentDocs = DB::table('documents')->leftJoin('collections','documents.collection_id','=','collections.id')->leftJoin('transmits','collections.id','=','transmits.collection_id')->leftJoin('users','transmits.to_user_id','=','users.id')->where('transmits.from_user_id','=',Auth::user()->id)->where('transmits.trashed_by_sender','=','0')->where(DB::raw("CONCAT(`users`.`firstname`, `users`.`middlename`, `users`.`surname`)"),'LIKE', '%' . $doc . '%')->select('documents.*')->get();

            $sentOut = DB::table('documents')->leftJoin('collections','documents.collection_id','=','collections.id')->leftJoin('transmits','collections.id','=','transmits.collection_id')->leftJoin('outsiders', 'transmits.outsider_id','=','outsiders.id')->where('transmits.from_user_id','=',Auth::user()->id)->where('transmits.trashed_by_sender','=','0')->where('transmits.to_outside','1')->where('outsiders.name','LIKE', '%' . $doc . '%')->select('documents.*')->get();

        }
        else{
            $sentDocs = DB::table('documents')->leftJoin('collections','documents.collection_id','=','collections.id')->leftJoin('transmits','collections.id','=','transmits.collection_id')->leftJoin('users','transmits.to_user_id','=','users.id')->where('transmits.from_user_id', '=', Auth::user()->id)->where('transmits.trashed_by_sender','=','0')->where('transmits.is_forward', '=', '0')->where($table.'.'.$searchBy, 'LIKE', '%' . $doc . '%')->select('documents.*')->get();
        }
        $sentDocs = uniquifyDocs($sentDocs);
        $sentOut = uniquifyDocs($sentOut);

        $sentDocs = array_merge($sentDocs, $sentOut); 


        return json_encode($sentDocs);
    }

    public function viewDetails(Request $request){
        $id = $request->id;
        $type = $request->type;

        $doc = Document::findOrFail($id);
        $coll = $doc->collection;
        $transmits = $coll->transmits;

        $users = [];
        if($type == 'received'){
            foreach ($transmits as $transmit) {
                $user = $transmit->receiver;
                if($transmit->from_outside == '1'){
                    $user->from_outside = '1';
                    $user->outside_sender = $transmit->outsider->name;
                }
                else{
                    $user->from_outside = '0';
                    $user = $transmit->sender;
                    $user->name = $user->firstname.' '.$user->middlename.' '.$user->surname;
                }
                array_push($users, $user);
                break;
            }
        }
        else{
            
            foreach ($transmits as $transmit) {
                $user = $transmit->sender;
                if($transmit->to_outside == '1'){
                    $user->to_outside = '1';
                    $user->outside_sender = $transmit->outsider->name;
                }
                else{
                    $user->to_outside = '0';
                    $user = $transmit->receiver;
                    $user->name = $user->firstname.' '.$user->middlename.' '.$user->surname;
                }
                array_push($users, $user);
            }
        }

        return view('loaders.viewdetails-loader', compact('doc', 'coll', 'users', 'type'));
    }

    public function validateFileUploads(Request $request){
        return json_encode($request->title);
    }

    public function getDocument($id)
    {   
        $pdf = DB::table('documents')->where('id', $id)->first();
        $filename = $pdf->file_name;
        $path = $pdf->file_path;
        // $path = storage_path($filename);

        return Response::make(file_get_contents($path), 200, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; filename="'.$filename.'"',
        ]);
    }
}