<?php

namespace App\Http\Controllers;
use App\Document;
use App\Log;
use Auth;

use Illuminate\Http\Request;

class DownloadsController extends Controller
{
  public function download(Request $request) {
  	// $document = $request->id;
  	$file= $request->file_path;
  	$doc = Document::where('file_path', $file)->first();

    $headers = [
	              'Content-Type' => 'application/pdf',
	              'Content-Disposition' => 'inline; filename="'.$doc->file_name.'"'
	           ];

  // //save logs
  // $log = new Log;
  // $log->user_id = Auth::user()->id;
  // $log->content_id = $doc->id;
  // $log->action = '6';
  // $log->type = '2'; 
  // $log->content_root=$doc->id;
  // $log->save();
  $log = new Log;
  $log->user_id = Auth::user()->id;
  $log->content = 'You downloaded a document, '.$doc->file_name.'.';
  // $log->action = '1';
  // $log->type = '3';
  // $log->content_root = $credential->user_id;
  $log->save();

	return response()->download($file, $doc->file_name, $headers);

    // $pathToFile = 
    // return response()->download($pathToFile);
  }
}


