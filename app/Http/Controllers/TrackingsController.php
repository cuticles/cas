<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tracking;
use Auth;
use App\Collection;
use App\Location;
use App\Type;
use App\Outsider;
use App\Document;
use App\Notification;
use App\Remark;
use App\Log;
use App\Point;
use Illuminate\Support\Str;

class TrackingsController extends Controller
{
    public function addTracking(Request $request){
    	$collection = Collection::findOrFail($request->collection_id);
    	$type = Type::findOrFail($request->new_status);

    	$log = new Log;
        $log->user_id = Auth::user()->id;
        $log->content = 'You updated the tracking of the collection, '.$collection->title.' from '.$collection->status->name.' to '.$type->name.'.';
        $log->save();

    	/*Status tracking*/
    	$update_track = new Tracking;
    	$update_track->collection_id = $request->collection_id;
    	$update_track->user_updater = Auth::user()->id;
    	$update_track->type_id = Type::where('name', 'update')->first()->id;
    	$update_track->status_id = $request->new_status;
    	$update_track->location_id = $request->location;
    	$update_track->is_step = 1;

    	if($request->update_method == 'outside'){
    		$update_track->from_outside = '1';
    		$update_track->outsider_id = $request->outsider_id;
    	}
    	
    	$update_track->save();

    	$collection->status_id = $request->new_status;;
    	$collection->update();
    	$transmit = $collection->transmits->first();

	    /*adding of notif*/
    	$notification = new Notification;
        $notification->from_user_id = Auth::user()->id;
        if($transmit->from_outside == '0'){
		    $notification->to_user_id = $transmit->sender->id;
		}
		else{
			$notification->to_user_id = $transmit->userUploader->id;
		}
        $notification->type = '6'; 
        $notification->object_id = $update_track->id;
        $notification->save();

        

    	/*Attachment tracking*/
    	if($request->attachments != null){
    		$attach_track = new Tracking;
	    	$attach_track->collection_id = $request->collection_id;
	    	$attach_track->user_updater = Auth::user()->id;
	    	$attach_track->type_id = Type::where('name', 'attachment')->first()->id;
	    	$attach_track->status_id = $request->new_status;
	    	$attach_track->is_step = 0;
	    	$attach_track->location_id = $request->location;
	    	if($request->update_method == 'outside'){
	    		$attach_track->from_outside = '1';
	    		$attach_track->outsider_id = $request->outsider_id;
	    	}
	    	$attach_track->save();

	    	/*Saving attachment as Document in database*/
	    	$documents = $request->attachments;
	    	foreach($documents as $document){
		    	$attachment = new Document;
				$name = $document->getClientOriginalName();
				$extension = $document->getClientOriginalExtension();
				$size = $document->getSize();

		        $fauxname = Str::random(10)."_".time().".".$extension;
		        $destination = public_path('uploads');
		        $document->move($destination, $fauxname);

		        $attachment->collection_id = $request->collection_id;
		        $attachment->tracking_id = $update_track->id;
		        $attachment->file_name = '('.Type::findOrFail($request->new_status)->name.') '.$name;
		        $attachment->file_size = $size;
		        $attachment->file_extension = $extension;
		        $attachment->file_path = $destination."\\".$fauxname;
		        $attachment->status_id = $request->new_status;
		        $attachment->save();
	    	}

    	}

    	/*Adding of remark*/
    	if($request->remark != null){
    		$remark = new Remark;
	    	$remark->user_id = Auth::user()->id;
	    	$remark->collection_id = $request->collection_id;
	    	$remark->content = $request->remark;
	    	$remark->save();

    	/*adding of notif*/
	    	$notification = new Notification;
	        $notification->from_user_id = Auth::user()->id;
	        $notification->to_user_id = $collection->transmits->first()->from_user_id;
	        $notification->type = '7'; 
	        $notification->object_id = $remark->id;
	        $notification->save();
    	}
		
    	return redirect('/collection/'.$collection->id);
    }
}
