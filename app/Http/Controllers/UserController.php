<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use App\Personnel;
use App\Credential;
use App\Location;
use App\Log;
use App\Type;
use Auth;
use Hash;
use App\Notification;
use App\Point;
use App\Position;

class UserController extends Controller
{
    public function organizePersonnel(Request $request){
        function extractObjects($data){
            $objs = [];
            foreach ($data as $datum){
                $obj = User::findOrFail($datum->id);
                array_push($objs, $obj);
            }
            return $objs;
        }

        $sortBy = $request->sortBy;
        $sortDir = $request->sortDir;
        $start = $request->start;
        $end = $request->end;

        if($sortBy == 'name'){
            $users = DB::table('users')->where('users.id', '>=', $start)->where('users.id', '<=', $end)->orderBy(DB::raw("CONCAT(`users`.`firstname`, `users`.`middlename`, `users`.`surname`)"), $sortDir)->select('users.id')->get();
            $users = extractObjects($users);
        }
        elseif($sortBy == 'position'){
            $users = DB::table('users')->join('positions', 'positions.id', '=', 'users.position_id')->where('users.id', '>=', $start)->where('users.id', '<=', $end)->orderBy('positions.name', $sortDir)->select('users.id')->get();
            $users = extractObjects($users);
        }
        else{
            $users = User::where('id', '>=', $start)->where('id', '<=', $end)->orderBy($sortBy, $sortDir)->get();
        }

        foreach($users as $user){
            $user->points = DB::table('credentials')
            ->select('credentials.id as credID', 'credentials.user_id as userID', 'credentials.point_id as pointID', 'points.value as value')
            ->where('is_deleted', '=', '0')->where('credentials.user_id', $user->id)
            ->join('points', 'points.id', '=', 'credentials.point_id')
            ->groupBy('credentials.user_id')
            ->sum('value');
        }

        return view('loaders.search-personnel-loader', compact('users'));
    }

    public function listPersonnel(){
        $users = [];
        $users_ids = [];

        $label = Position::all()->pluck('name')->unique()->toArray();
        $position_id = Position::all()->pluck('id')->unique()->toArray();
        $labels = array_values($label);
        $position_ids = array_values($position_id);
        $data = [];
        $partitions = [];
        
        foreach($position_ids as $position_id){
            $personnel = User::where('position_id', '=', $position_id)->where('is_deleted','0');

            array_push($data, $personnel->count());
        }

        $loc = Location::all()->pluck('name')->unique()->toArray();
        $locs = array_values($loc);
        $locs_key = array_keys($loc);

        $user_count_loc = [];

        foreach($locs_key as $loc) {
            $users_loc = User::where('division', '=', $loc)->where('is_deleted','0');


            array_push($user_count_loc, $users_loc->count());
        } 

        $credentials = Credential::all()->where('is_deleted','0')->pluck('point_id')->toArray();


        foreach (DB::table('users')->orderBy('id', 'desc')->where('is_deleted','0')->select('users.id')->get() as $user_id){
            array_push($users_ids, $user_id->id);
        }

        $numUsers = count($users_ids);

        if(count($users_ids) > 0){
            $docs_chunks = array_chunk($users_ids, 10);

            for($i = 0; $i < count($docs_chunks); $i++){
                $end = $docs_chunks[$i][0];
                $start = $docs_chunks[$i][count($docs_chunks[$i])-1];

                $partitions[strval($i+1)] = $start.':'.$end;
            }

            foreach($docs_chunks[0] as $id){
                $single_user = User::findOrFail($id);
                array_push($users, $single_user);
            }
        }

        foreach($users as $user){
             $user->points = DB::table('credentials')
            ->select('credentials.id as credID', 'credentials.user_id as userID', 'credentials.point_id as pointID', 'points.value as value')
            ->where('is_deleted', '=', '0')->where('credentials.user_id', $user->id)
            ->join('points', 'points.id', '=', 'credentials.point_id')
            ->groupBy('credentials.user_id')
            ->sum('value');
        }

        return view('personnel.managePersonnel', compact('users', 'labels', 'data', 'total_users', 'user_count_loc', 'locs', 'partitions', 'numUsers'));
    }

    public function addPersonnel(Request $request) {
        $user = new User;
        $user->username = $request->username;
        $encrypted = Hash::make($request->password);
        $user->password = $encrypted;
        $user->email = $request->email;
        $user->firstname = $request->firstname;
        $user->middlename = $request->middlename;
        $user->surname = $request->surname;
        $user->position_id = $request->designation;
        $user->division = $request->division;
        $user->is_admin = 0; 

        $profpic = $request->myimage;
        $name = $profpic->getClientOriginalName();
        $extension = $profpic->getClientOriginalExtension();
        $size = $profpic->getSize();

        $fauxname = "images"."/".Str::random(10)."_".time().".".$extension;
        $destination = public_path('images');
        $profpic->move($destination, $fauxname);
        $user->profile_pic = $fauxname; 
       
        $user->save();

        $log = new Log;
        $log->user_id = Auth::user()->id;
        $log->content_id = $user->id;
        $log->action = '1';
        $log->type = '5'; 
        $log->content_root = $user->division;
        $log->save();
        return redirect('/managePersonnel');
    }

    public function addPersonnelForm() {
        $locations = Location::all();
        $positions = Position::all();
        return view('personnel.addPersonnel', compact('locations', 'positions'));
    }

    public function searchUser(Request $request){
        $user = $request->search;
        $users = User::where(DB::raw("CONCAT(`users`.`firstname`,' ', `users`.`middlename`,' ', `users`.`surname`)"),'LIKE', '%' . $user . '%')->get();

        return view('loaders.search-personnel-loader',compact('users'));
    }
    public function resetUsers(Request $request){
        $users = User::all();

        return view('personnel.managePersonnel',compact('users'));
    }

    public function viewPersonnel($id) {
        $user = User::findOrFail($id);
        $levels = Type::where('category', '4')->get();
        $admins = User::where('is_admin', '1')->get();
        $courses = file('text/courses.txt');
        $credentials = [];

        $points = Point::all();


        $credPoints = DB::table('credentials')
            ->select('credentials.id as credID', 'credentials.user_id as userID', 'credentials.point_id as pointID', 'points.value as value')
            ->where('is_deleted', '=', '0')->where('credentials.user_id', $user->id)
            ->join('points', 'points.id', '=', 'credentials.point_id')
            ->groupBy('credentials.user_id')
            ->sum('value');        

        foreach ($user->credentials as $cred){
           array_push($credentials, $cred);
        }

        $numItems = count($credentials);

        if(count($credentials) > 0){
            $creds_chunks = array_chunk($credentials, 10);

            for($i = 0; $i < count($creds_chunks); $i++){
                $end = $creds_chunks[$i][0]->id;
                $start = $creds_chunks[$i][count($creds_chunks[$i])-1]->id;

                $partitions[strval($i+1)] = $start.':'.$end;
            }

            $credentials = $creds_chunks[0];
        }

        $degLevels = ['1' => 'Associative', '2' => 'Bachelor\'s', '3' => 'Master\'s', '4' => 'Doctoral'];
        $backgrounds = $user->backgrounds;
        return view('personnel.personnelUtil', compact('user', 'credentials', 'backgrounds', 'levels', 'courses', 'admins', 'degLevels', 'points', 'partitions', 'numItems', 'credPoints'));

    }

    public function editPersonnelForm($id) {
        $user = User::findOrFail($id);
        $locations = Location::all();
        $positions = Position::all();
        return view('personnel.editPersonnelForm', compact('user', 'locations', 'positions'));
    }

    public function updatePersonnel(Request $request, $id) {
        $user = User::findOrFail($id);
       
        $user->firstname = $request->firstname;
        $user->middlename = $request->middlename;
        $user->surname = $request->surname;
        $user->birthdate = $request->birthdate;
        $user->address = $request->address;
        $user->sex = $request->sex;
        $user->civil_status = $request->civil_status;
        $user->mobile_num = $request->mobile_num;
        $user->telephone_num = $request->telephone_num;
        $user->email = $request->email;
        $user->position_id = $request->designation;
        $user->division = $request->division;

        $profpic = $request->myimage;

        if ($profpic == NULL) {
            $user->profile_pic = $user->profile_pic;
        }
        else {
            $name = $profpic->getClientOriginalName();
            $extension = $profpic->getClientOriginalExtension();
            $size = $profpic->getSize();
            $fauxname = "images"."/".Str::random(10)."_".time().".".$extension;
            $destination = public_path('images');
            $profpic->move($destination, $fauxname);
            $user->profile_pic = $fauxname; 
        }


        $user->update();

        //Save notification
        $notification = new Notification;
        $notification->from_user_id = Auth::user()->id;
        $notification->to_user_id = $user->id;
        $notification->type = '3'; 
        $notification->object_id = $user->id;
        if($notification->from_user_id != $notification->to_user_id){
            $notification->save();
        }

        $log = new Log;
        $log->user_id = Auth::user()->id;
        $log->content_id = $user->id;
        $log->action = '2';
        $log->type = '5'; 
        $log->content_root = $user->division;
        $log->save();

        return redirect('/personnelUtil/'.$id)->with('success', 'Data updated');
    }
    
    public function deletePersonnel(Request $request) {
        $id = $request->id;
        $user = User::findOrFail($id);
        $user->is_deleted = '1';
        $user->save();


        $log = new Log;
        $log->user_id = Auth::user()->id;
        $log->content_id = $id;
        $log->type = '5';
        $log->action= '4';
        $log->content_root = $user->division;
        $log->save();
        
        return $user->id;
    }
}
