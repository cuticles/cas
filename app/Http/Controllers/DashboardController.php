<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Transmit;
use App\Group;
use App\Collection;
use App\Type;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function dashboardGroup($group_id){
        $stat_icons = ['documents' => 'file outline', 'collections' => 'folder outline', 'forwards' => 'paper plane outline'];

        function uniquifyColls($colls){
            $coll_ids = [];
            $collections = [];

            foreach($colls as $collection){
                if(!(in_array($collection->id, $coll_ids))){
                    array_push($collections, $collection);
                    array_push($coll_ids, $collection->id);
                }
            }

            return $collections;
        }

        function extractCollections($transmits){
            $coll_ids = [];
            $collections = [];

            foreach($transmits as $transmit){
                if(!(in_array($transmit->collection->id, $coll_ids))){
                    array_push($collections, $transmit->collection);
                    array_push($coll_ids, $transmit->collection->id);
                }
            }

            return $collections;
        }

    	function countDocuments($colls){
    		$count = 0;

    		foreach($colls as $coll){
    			$count += count($coll->documents);
    		}

    		return $count;
    	}

        function retrieveCollStatusStats($colls){
            $stats = [];

            foreach ($colls as $coll) {
                if($coll != null){
                    $status = DB::table('types')->where('types.id', $coll->status_id)->select('types.name')->first();
                    if(array_key_exists($status->name, $stats)){
                        $stats[$status->name] += 1;
                    }
                    else{
                        $stats[$status->name] = 0;
                        $stats[$status->name] += 1;
                    }
                }
            }

            return $stats;
        }

        function retrieveCollStats($colls){
            $stats = [];

            foreach ($colls as $coll) {
                if($coll != null){
                    $type = DB::table('types')->where('types.id', $coll->type_id)->select('types.name')->first();
                    if(array_key_exists($type->name, $stats)){
                        $stats[$type->name] += 1;
                    }
                    else{
                        $stats[$type->name] = 0;
                        $stats[$type->name] += 1;
                    }
                }
                
            }

            return $stats;
        }

    	function extractMembers($members){
    		$member_objects = [];
    		foreach($members as $member){
    			array_push($member_objects, $member->user);
    		}
    		return $member_objects;
    	}
    	$user = Auth::user();
        $group = 'none';
    	$group_stat = ['documents' => [0, 0], 'collections' => [0, 0], 'forwards' => [0, 0]];
    	$user_stat = ['documents' => [], 'collections' => [], 'forwards' => []];
        $coll_status_stats = '';
        $coll_stats = '';
    	$members_stats = [];
    	$members = '';
        $labels = [];
        $data = [];
        $status_labels = [];
        $status_data = [];

        if($group_id != 'none'){
            $group = Group::findOrFail($group_id);
            $members = extractMembers($group->members);

            $colls = DB::table('groups')->join('memberships', 'groups.id', '=', 'memberships.group_id')->join('users', 'memberships.user_id', '=', 'users.id')->join('transmits', 'users.id', '=', 'transmits.from_user_id')->join('collections', 'transmits.collection_id', '=', 'collections.id')->where('groups.id', '=', $group->id)->select('collections.status_id', 'collections.type_id', 'collections.id')->get();

            $colls = uniquifyColls($colls);

            $coll_stats = retrieveCollStats($colls);
            $coll_status_stats = retrieveCollStatusStats($colls);

            foreach($members as $member){
                $group_stat['documents'][0] += countDocuments(extractCollections($member->createdTransmits));
                $group_stat['documents'][1] += countDocuments(extractCollections($member->incomingTransmits));
                $group_stat['collections'][0] += count(extractCollections($member->createdTransmits));
                $group_stat['collections'][1] += count($member->incomingTransmits);
                $group_stat['forwards'][0] += count($member->myForwards);
                $group_stat['forwards'][1] += count($member->receivedForwards);

                $member->documents_stat = countDocuments(extractCollections($member->createdTransmits)).','.countDocuments(extractCollections($member->incomingTransmits));

                $member->collections_stat = count(extractCollections($member->createdTransmits)).','.count($member->incomingTransmits);

                $member->forwards_stat = count($member->myForwards).','.count($member->receivedForwards);

                array_push($members_stats, $member);
            }

            $user_stat['documents'][0] = countDocuments(extractCollections($user->createdTransmits));
            $user_stat['documents'][1] = countDocuments(extractCollections($user->incomingTransmits));
            $user_stat['collections'][0] = count(extractCollections($user->createdTransmits));
            $user_stat['collections'][1] = count($user->incomingTransmits);
            $user_stat['forwards'][0] = count($user->myForwards);
            $user_stat['forwards'][1] = count($user->receivedForwards);

            foreach ($coll_stats as $type => $count){
                array_push($labels, $type);
                array_push($data, $count);
            }

            foreach ($coll_status_stats as $type => $count){
                array_push($status_labels, $type);
                array_push($status_data, $count);
            }
        }

    	return view('admin.dashboard', compact('group', 'members_stats', 'user_stat', 'group_stat', 'stat_icons', 'data', 'labels', 'status_data', 'status_labels'));
    }

    public function index(){
        $user = Auth::user();
        $memberships = $user->myGroups;

        if(count($memberships) == 1){
            $membership = $user->myGroups->first();
            return redirect('/dashboard/'.$membership->group->id);
        }
        else if(count($memberships) == 0){
            return redirect('/dashboard/none');
        }
        else{
            $groups = [];
            foreach ($memberships as $mem) {
                array_push($groups, $mem->group);
            }
            return view('admin.display-groups-dashboard', compact('groups'));
        }
    }

    public function searchItems(Request $request){
        $icons = array("pdf" => "file pdf outline icon", "docx" => "file word outline icon", "doc" => "file word outline icon", "xls" => "file excel outline icon", "xlsx" => "file excel outline icon", "zip" => "file archive outline icon", "rar" => "file archive outline icon", "pptx" => "file powerpoint outline icon", "ppt" => "file powerpoint outline icon", "txt" => "file alternate outline icon");
        $coll_icons = array('report'=>'images/analysis.png', 'certificate' => 'images/diploma.png', 'request' => 'images/application.png');

        function fetchDocs($docs){
            $doc_ids = [];
            $documents = [];

            foreach($docs as $doc){
                if(!(in_array($doc->id, $doc_ids))){
                    array_push($doc_ids, $doc->id);
                    $doc->owner_profile_pic = asset($doc->profile_pic);
                    $doc->sender_name = $doc->firstname.' '.$doc->middlename.' '.$doc->surname;
                    array_push($documents, $doc);
                }
            }
            return $documents;
        }

        function fetchColls($colls){
            $coll_ids = [];
            $collections = [];

            foreach($colls as $coll){
                if(!(in_array($coll->id, $coll_ids))){
                    array_push($coll_ids, $coll->id);
                    $coll->owner_profile_pic = asset($coll->profile_pic);
                    $coll->type_name = Type::findOrFail($coll->type_id)->name;
                    $coll->status = Type::findOrFail($coll->status_id);
                    $coll->sender_name = $coll->firstname.' '.$coll->middlename.' '.$coll->surname;
                    array_push($collections, $coll);
                }
            }
            return $collections;
        }

        $search = $request->search;
        $searchBy = $request->searchBy;
        $id = $request->id;
        $items = '';

        if($searchBy == 'documents'){
            $items = DB::table('groups')->join('memberships', 'groups.id', '=', 'memberships.group_id')->join('users', 'memberships.user_id', '=', 'users.id')->join('transmits', 'users.id', '=', 'transmits.from_user_id')->join('collections', 'transmits.collection_id', '=', 'collections.id')->join('documents', 'collections.id', '=', 'documents.collection_id')->where('groups.id', '=', $id)->where('transmits.is_forward', '=', '0')->where('documents.file_name', 'LIKE', '%'.$search.'%')->select('documents.*', 'users.firstname', 'collections.title as coll_title', 'users.middlename', 'users.surname', 'users.profile_pic')->get();
            $items = fetchDocs($items);
        }
        else{
            $items = DB::table('groups')->join('memberships', 'groups.id', '=', 'memberships.group_id')->join('users', 'memberships.user_id', '=', 'users.id')->join('transmits', 'users.id', '=', 'transmits.from_user_id')->join('collections', 'transmits.collection_id', '=', 'collections.id')->where('groups.id', '=', $id)->where('transmits.is_forward', '=', '0')->where('collections.title', 'LIKE', '%'.$search.'%')->select('collections.*', 'users.firstname', 'users.middlename', 'users.surname', 'users.profile_pic')->get();
            $items = fetchColls($items);
        }

        return view('loaders.dashboard-search-loader', compact('items', 'searchBy', 'icons', 'coll_icons'));
    }
}
