<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Group;
use App\Collection;
use App\Transmit;
use App\Type;
use App\Notification;
use App\log;
use Auth;
use Illuminate\Support\Facades\DB;

class ForwardsController extends Controller
{
    public function organizeReceivedForwards(Request $request){
        function uniquifyForwards($forwards){
            $coll_ids = [];
            $forwards_arr = [];

            foreach ($forwards as $forward) {
                if(!(in_array($forward->collection_id, $coll_ids))){
                    $transForward = Transmit::findOrFail($forward->id);
                    array_push($coll_ids, $forward->collection_id);
                    array_push($forwards_arr, $transForward);
                }
            }

            return $forwards_arr;
        }

        $sortBy = $request->sortBy;
        $sortDir = $request->sortDir;
        $status = $request->status;
        $type = 'received';
        $start = $request->start;
        $end = $request->end;

        $forwards = DB::table('transmits')->join('collections', 'collections.id', '=', 'transmits.collection_id')->where('transmits.is_forward', '1')->where('transmits.to_user_id', Auth::user()->id)->where('transmits.id', '>=', $start)->where('transmits.id', '<=', $end);
        if($status != 'all'){
            $forwards = $forwards->where('collections.status_id', $status);   
        }

        if($sortBy == 'created_at'){
            $forwards = $forwards->orderBy('transmits.created_at', $sortDir);
        }
        else{
            $forwards = $forwards->orderBy('collections.title', $sortDir);
        }

        $forwards = $forwards->select('transmits.id', 'transmits.collection_id')->get();

        $forwards = uniquifyForwards($forwards);

        return view('loaders.forwards-loader', compact('forwards', 'type'));
    }
    public function organizeMyForwards(Request $request){
        function uniquifyForwards($forwards){
            $coll_ids = [];
            $forwards_arr = [];

            foreach ($forwards as $forward) {
                if(!(in_array($forward->collection_id, $coll_ids))){
                    $transForward = Transmit::findOrFail($forward->id);
                    array_push($coll_ids, $forward->collection_id);
                    array_push($forwards_arr, $transForward);
                }
            }

            return $forwards_arr;
        }

        $sortBy = $request->sortBy;
        $sortDir = $request->sortDir;
        $status = $request->status;
        $type = 'sent';
        $start = $request->start;
        $end = $request->end;

        $forwards = DB::table('transmits')->join('collections', 'collections.id', '=', 'transmits.collection_id')->where('transmits.is_forward', '1')->where('transmits.from_user_id', Auth::user()->id)->where('transmits.id', '>=', $start)->where('transmits.id', '<=', $end);
        if($status != 'all'){
            $forwards = $forwards->where('collections.status_id', $status);   
        }

        if($sortBy == 'created_at'){
            $forwards = $forwards->orderBy('transmits.created_at', $sortDir);
        }
        else{
            $forwards = $forwards->orderBy('collections.title', $sortDir);
        }

        $forwards = $forwards->select('transmits.id', 'transmits.collection_id')->get();

        $forwards = uniquifyForwards($forwards);

        return view('loaders.forwards-loader', compact('forwards', 'type'));
    }

    public function forwardCollection(Request $request){
        $method = $request->forward_method;
        $authorized_users = [];
        $collection_id = $request->collection_id;
        $collection = Collection::findOrFail($collection_id);
        $transmitOne = $collection->transmits->first();
        foreach($collection->transmitsAll as $transmit){
            if($transmit->to_outside == '0'){
                array_push($authorized_users, $transmit->receiver->id);
            }
            if($transmit->from_outside == '0'){
                array_push($authorized_users, $transmit->sender->id);
            }
        }


        if($method == 'group'){
            $id = $request->group_id;
            $group = Group::findOrFail($id);
            $members = $group->members;

            foreach ($members as $member) {
                if(!(in_array($member->user->id, $authorized_users))){
                    $forward = new Transmit;
                    $forward->to_user_id = $member->user->id;
                    $forward->from_user_id = Auth::user()->id;
                    $forward->collection_id = $collection_id;
                    $forward->is_forward = '1';
                    $forward->save();
               
                //Save notification
                $notification = new Notification;
                $notification->from_user_id = Auth::user()->id;
                $notification->to_user_id = $member->user->id;
                $notification->type = '4'; 
                $notification->object_id = $collection->id;
                $notification->save();
                }
                
            }
            $log = new Log;
            $log->content = 'You forwarded the collection, '.$collection->title.' to '.$method;
            $log->user_id = Auth::user()->id;
            $log->save();

        }
        if($method == 'one'){
            $forward = new Transmit;
            $forward->to_user_id = $request->recipient;
            $forward->from_user_id = Auth::user()->id;
            $forward->collection_id = $collection_id;
            $forward->is_forward = '1';
            $forward->save();
            
            //Save notification
            $notification = new Notification;
            $notification->from_user_id = Auth::user()->id;
            $notification->to_user_id = $request->recipient;
            $notification->type = '4'; 
            $notification->object_id = $collection->id;
            $notification->save();
        }
        else{
            $recipients = explode(',', $request->recipients);
            foreach ($recipients as $id) {
                $forward = new Transmit;
                $forward->to_user_id = $id;
                $forward->from_user_id = Auth::user()->id;
                $forward->collection_id = $collection_id;
                $forward->is_forward = '1';
                $forward->save();
            
            //Save notification
            $notification = new Notification;
            $notification->from_user_id = Auth::user()->id;
            $notification->to_user_id = $id;
            $notification->type = '4'; 
            $notification->object_id = $collection->id;
            $notification->save();
        }

        $log = new Log;
        $log->content = 'You forwarded the collection, '.$collection->title.' to '.$method;
        $log->user_id = Auth::user()->id;
        $log->save();

        }


    	return redirect('/collection/'.$collection_id);
    }

    public function receivedForwards(){
        $initForwards = Auth::user()->receivedForwards;
        $numItems = count($initForwards);
        $forwards = [];

        foreach($initForwards as $forward){
            array_push($forwards, $forward);
        }

        $partitions = [];

        if(count($forwards) > 0){
            $forwards_chunks = array_chunk($forwards, 20);

            for($i = 0; $i < count($forwards_chunks); $i++){
                $end = $forwards_chunks[$i][0]->id;
                $start = $forwards_chunks[$i][count($forwards_chunks[$i])-1]->id;

                $partitions[strval($i+1)] = $start.':'.$end;
            }

            $forwards = $forwards_chunks[0];
        }
        $types = Type::where('category', '3')->get();

        return view('admin.received_forwards', compact('forwards', 'partitions', 'numItems', 'types'));
    }
    
    public function index(){
        function uniquifyForwards($forwards){
            $coll_ids = [];
            $forwards_arr = [];

            foreach ($forwards as $forward) {
                if(!(in_array($forward->collection->id, $coll_ids))){
                    array_push($coll_ids, $forward->collection->id);
                    array_push($forwards_arr, $forward);
                }
            }

            return $forwards_arr;
        }
        
    	$forwards = Auth::user()->myForwards;
        $forwards = uniquifyForwards($forwards);
        $partitions = [];

        $numItems = count($forwards);

        if(count($forwards) > 0){
            $forwards_chunks = array_chunk($forwards, 20);

            for($i = 0; $i < count($forwards_chunks); $i++){
                $end = $forwards_chunks[$i][0]->id;
                $start = $forwards_chunks[$i][count($forwards_chunks[$i])-1]->id;

                $partitions[strval($i+1)] = $start.':'.$end;
            }

            $forwards = $forwards_chunks[0];
        }

        $types = Type::where('category', '3')->get();


    	return view('admin.forward', compact('forwards', 'types', 'partitions', 'numItems'));
    }

    public function checkGroup(Request $request){
        $group_id = $request->id;
        $coll_id = $request->coll_id;

        $coll = Collection::findOrFail($coll_id);
        $authorized_users = [];
        foreach($coll->transmitsAll as $transmit){
            array_push($authorized_users, $transmit->receiver->id);
            array_push($authorized_users, $transmit->sender->id);
        }
        $group = Group::findOrFail($group_id);
        $users = [];

        foreach ($group->members as $member){
            if(!(in_array($member->user->id, $authorized_users))){
                array_push($users, $member->user);
            }
        }

        return count($users);
    }

    public function viewReceivers(Request $request){
        $transmits = Transmit::where('collection_id', $request->id)->where('from_user_id', Auth::user()->id)->where('is_forward', '1')->get();
        $users = [];
        foreach ($transmits as $transmit) {
            $user = $transmit->receiver;
            $user->name = $user->firstname.' '.$user->middlename.' '.$user->surname;
            array_push($users, $user);
        }

        return view('loaders.forward-receivers-loader', compact('users'));
    }

    public function trashForward(Request $request){
        function uniquifyForwards($forwards){
            $coll_ids = [];
            $forwards_arr = [];

            foreach ($forwards as $forward) {
                if(!(in_array($forward->collection->id, $coll_ids))){
                    array_push($coll_ids, $forward->collection->id);
                    array_push($forwards_arr, $forward);
                }
            }

            return $forwards_arr;
        }

        $coll_id = $request->id;
        $collection = Collection::where('id',$coll_id)->get();
        $type = $request->type;
        $forwards = '';

        if($type == 'sent'){
            $transmits = Transmit::where('collection_id', $coll_id)->where('from_user_id', Auth::user()->id)->where('is_forward', '1')->where('trashed_by_sender','0')->get();
            foreach($transmits as $transmit){
                $transmit->trashed_by_sender = '1';
                $transmit->update();
            }

            $forwards = Auth::user()->myForwards;
            $forwards = uniquifyForwards($forwards);
        }
        else if($type == 'received'){
            $transmits = Transmit::where('collection_id', $coll_id)->where('to_user_id', Auth::user()->id)->where('is_forward', '1')->where('trashed_by_sender','0')->get();
            foreach($transmits as $transmit){
                $transmit->trashed_by_receiver = '1';
                $transmit->update();
            }
            $forwards = Auth::user()->receivedForwards;
        }

        //save logs
        $log = new Log;
        $log->user_id = Auth::user()->id;
        $log->content = 'You trashed a forwarded collection, '.$collection->title;
        // $log->action = '3';
        // $log->type = '4'; 
        // $log->content_root = $coll_id;
        $log->save();

        return view('loaders.forwards-loader', compact('forwards', 'type'));
    }
    public function searchForwards(Request $request){
        function retrieveColls($forwards){
            $coll_ids = [];
            $forward_objs = [];

            foreach ($forwards as $forward){
                if (!(in_array($forward->collection_id, $coll_ids))){
                    $forward = Transmit::findOrFail($forward->id);
                    array_push($forward_objs, $forward);
                    array_push( $coll_ids, $forward->collection_id);
                }
                
            }
            return $forward_objs;
        }

        $cols = $request->search;
        $searchBy = $request->searchBy;
        $user_id = Auth::user()->id;
        $forwards = [];
        $collOut = [];
        $type = 'sent';

        //now get all user and services in one go without looping using eager loading
        //In your foreach() loop, if you have 1000 users you will make 1000 queries

        if($searchBy == 'receiver'){
            $forwards = DB::table('transmits')->join('users', 'transmits.to_user_id', '=', 'users.id')->join('collections', 'transmits.collection_id', '=', 'collections.id')->where('transmits.is_forward', '=', '1')->where('transmits.from_user_id', '=', Auth::user()->id)->where('transmits.trashed_by_sender', '=', '0')->where(DB::raw("CONCAT(`users`.`firstname`, `users`.`middlename`, `users`.`surname`)"), 'LIKE', '%' . $cols . '%')->select('transmits.id', 'transmits.collection_id')->get();
        }
        else{
            $forwards = DB::table('transmits')->join('collections', 'transmits.collection_id', '=', 'collections.id')->where('transmits.from_user_id', '=', Auth::user()->id)->where('transmits.is_forward','=','1')->where('collections.'.$searchBy, 'LIKE', '%' . $cols . '%')->select('transmits.id', 'transmits.collection_id')->get();
        }
        $forwards = retrieveColls($forwards);

        return view('loaders.forwards-loader', compact('forwards', 'type'));
    }
    public function searchForwardsToMe(Request $request){
        function retrieveColls($forwards){
            $coll_ids = [];
            $forward_objs = [];

            foreach ($forwards as $forward){
                if (!(in_array($forward->collection_id, $coll_ids))){
                    $forward = Transmit::findOrFail($forward->id);
                    array_push($forward_objs, $forward);
                    array_push( $coll_ids, $forward->collection_id);
                }
                
            }
            return $forward_objs;
        }

        $cols = $request->search;
        $searchBy = $request->searchBy;
        $user_id = Auth::user()->id;
        $forwards = [];
        $collOut = [];
        $type = 'received';

        //now get all user and services in one go without looping using eager loading
        //In your foreach() loop, if you have 1000 users you will make 1000 queries

        if($searchBy == 'sender'){
            $forwards = DB::table('transmits')->join('users', 'transmits.to_user_id', '=', 'users.id')->join('collections', 'transmits.collection_id', '=', 'collections.id')->where('transmits.is_forward', '=', '1')->where('transmits.to_user_id', '=', Auth::user()->id)->where('transmits.trashed_by_receiver', '=', '0')->where(DB::raw("CONCAT(`users`.`firstname`, `users`.`middlename`, `users`.`surname`)"), 'LIKE', '%' . $cols . '%')->select('transmits.id', 'transmits.collection_id')->get();
        }
        else{
            $forwards = DB::table('transmits')->join('collections', 'transmits.collection_id', '=', 'collections.id')->where('transmits.to_user_id', '=', Auth::user()->id)->where('transmits.is_forward','=','1')->where('collections.'.$searchBy, 'LIKE', '%' . $cols . '%')->select('transmits.id', 'transmits.collection_id')->get();
        }
        $forwards = retrieveColls($forwards);

        return view('loaders.forwards-loader', compact('forwards', 'type'));
    }
}
