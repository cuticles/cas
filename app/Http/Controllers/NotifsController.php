<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Controllers;
use App\Collection;
use App\User;
use App\Document;
use App\Transmit;
use App\Notification;
use Auth;

class NotifsController extends Controller
{
    public function saveNotif(){
 		$user = Auth::user()->id;
 		$transmits = Transmits::where('to_user_id',$user)->get();
		$users = User::where('id', '!=', $user)->get();
 		
    	return redirect('/sent');
    }
    public function viewNotif(){
    	$user = Auth::user();
        $numItems = 10;
        $notifications = $user->notifications->take($numItems);
        $notifCount = count($user->notifications);
        

    	return view('admin.notification',compact('notifications','numItems','notifCount'));
    }
    public function moreNotif(Request $request){
        $user = Auth::user();
        $numItems = $request->numItems + '10';
        $notifications = $user->notifications->take($numItems);
        $notifCount = count($user->notifications);
        

        return view('loaders.notifications-loader',compact('notifications','numItems','notifCount'));
    }

    public function seenNotif(Request $request){
        $id = $request->id;
        $notif = Notification::findOrFail($id);
        $notif->is_seen = 'true';
        $notif->save();
        
        return redirect('/notification');
    }
	
}
