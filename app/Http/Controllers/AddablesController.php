<?php
namespace App\Http\Controllers;
use App\Outsider;
use App\Location;
use App\Type;
use App\User;
use App\Point;
use App\Position;
use App\Log;
use Illuminate\Support\Facades\DB;
use Auth;
use Illuminate\Http\Request;

class AddablesController extends Controller
{
    public function getPositions(){
        function chunkAddables($objects){
            $data = ['first_chunk' => [], 'partitions' => []];
            if(count($objects) > 0){
                $partitions = [];
                $addable_chunks = array_chunk($objects, 10);

                for($i = 0; $i < count($addable_chunks); $i++){
                    $end = $addable_chunks[$i][0]->id;
                    $start = $addable_chunks[$i][count($addable_chunks[$i])-1]->id;

                    $partitions[strval($i+1)] = $start.':'.$end;
                }

                $data['first_chunk'] = $addable_chunks[0];
                $data['partitions'] = $partitions;
            }
            return $data;
        }

        function arrayfy($objects){
            $array = [];
            foreach($objects as $obj){
                array_push($array, $obj);
            }
            return $array;
        }
        $category = ['1' => 'Faculty', '2' => 'Staff'];
        $positions = Position::orderBy('id', 'desc')->get();
        $numItems = count($positions);
        
        //For Outsiders
        $data = chunkAddables(arrayfy($positions));

        $positions = $data['first_chunk'];
        $partitions_positions = $data['partitions'];

        return view('addables.positions', compact('positions', 'numItems', 'partitions_positions', 'category'));
    }

    public function getPoints(){
        function chunkAddables($objects){
            $data = ['first_chunk' => [], 'partitions' => []];
            if(count($objects) > 0){
                $partitions = [];
                $addable_chunks = array_chunk($objects, 10);

                for($i = 0; $i < count($addable_chunks); $i++){
                    $end = $addable_chunks[$i][0]->id;
                    $start = $addable_chunks[$i][count($addable_chunks[$i])-1]->id;

                    $partitions[strval($i+1)] = $start.':'.$end;
                }

                $data['first_chunk'] = $addable_chunks[0];
                $data['partitions'] = $partitions;
            }
            return $data;
        }

        function arrayfy($objects){
            $array = [];
            foreach($objects as $obj){
                array_push($array, $obj);
            }
            return $array;
        }
        $points = Point::orderBy('id', 'desc')->get();
        $numItems = count($points);
        
        //For Outsiders
        $data = chunkAddables(arrayfy($points));

        $points = $data['first_chunk'];
        $partitions_points = $data['partitions'];

        return view('addables.points', compact('points', 'numItems', 'partitions_points'));
    }

    public function getTypes(){
        function chunkAddables($objects){
            $data = ['first_chunk' => [], 'partitions' => []];
            if(count($objects) > 0){
                $partitions = [];
                $addable_chunks = array_chunk($objects, 10);

                for($i = 0; $i < count($addable_chunks); $i++){
                    $end = $addable_chunks[$i][0]->id;
                    $start = $addable_chunks[$i][count($addable_chunks[$i])-1]->id;

                    $partitions[strval($i+1)] = $start.':'.$end;
                }

                $data['first_chunk'] = $addable_chunks[0];
                $data['partitions'] = $partitions;
            }
            return $data;
        }

        function arrayfy($objects){
            $array = [];
            foreach($objects as $obj){
                array_push($array, $obj);
            }
            return $array;
        }
        $category = ['1' => 'Collection', '2' => 'Tracking', '3' => 'Status', '4' => 'Background'];
        $types = Type::orderBy('id', 'desc')->get();
        $numItems = count($types);
        
        //For Outsiders
        $data = chunkAddables(arrayfy($types));

        $types = $data['first_chunk'];
        $partitions_types = $data['partitions'];

        return view('addables.types', compact('types', 'numItems', 'partitions_types', 'category'));
    }

    public function getLocations(){
        function chunkAddables($objects){
            $data = ['first_chunk' => [], 'partitions' => []];
            if(count($objects) > 0){
                $partitions = [];
                $addable_chunks = array_chunk($objects, 10);

                for($i = 0; $i < count($addable_chunks); $i++){
                    $end = $addable_chunks[$i][0]->id;
                    $start = $addable_chunks[$i][count($addable_chunks[$i])-1]->id;

                    $partitions[strval($i+1)] = $start.':'.$end;
                }

                $data['first_chunk'] = $addable_chunks[0];
                $data['partitions'] = $partitions;
            }
            return $data;
        }

        function arrayfy($objects){
            $array = [];
            foreach($objects as $obj){
                array_push($array, $obj);
            }
            return $array;
        }

        $locations = Location::orderBy('id', 'desc')->get();
        $numItems = count($locations);
        
        //For Outsiders
        $data = chunkAddables(arrayfy($locations));

        $locations = $data['first_chunk'];
        $partitions_locations = $data['partitions'];

        return view('addables.locations', compact('locations', 'numItems', 'partitions_locations'));
    }

    public function getOutsiders(){
        function chunkAddables($objects){
            $data = ['first_chunk' => [], 'partitions' => []];
            if(count($objects) > 0){
                $partitions = [];
                $addable_chunks = array_chunk($objects, 10);

                for($i = 0; $i < count($addable_chunks); $i++){
                    $end = $addable_chunks[$i][0]->id;
                    $start = $addable_chunks[$i][count($addable_chunks[$i])-1]->id;

                    $partitions[strval($i+1)] = $start.':'.$end;
                }

                $data['first_chunk'] = $addable_chunks[0];
                $data['partitions'] = $partitions;
            }
            return $data;
        }

        function arrayfy($objects){
            $array = [];
            foreach($objects as $obj){
                array_push($array, $obj);
            }
            return $array;
        }

        $outsiders = Outsider::orderBy('id', 'desc')->get();
        $locations = Location::orderBy('id', 'desc')->get();
        $numItems = count($outsiders);
        
        //For Outsiders
        $data = chunkAddables(arrayfy($outsiders));

        $outsiders = $data['first_chunk'];
        $partitions_outsiders = $data['partitions'];

        return view('addables.outsiders', compact('outsiders', 'locations', 'numItems', 'partitions_outsiders'));
    }

    public function deleteAddables(Request $request){
        $categ = $request->category;
        if($categ == 'outsiders'){
            $outsider = Outsider::findOrFail($request->id);
            

            //save logs
            $log = new Log;
            $log->user_id = Auth::user()->id;
            $log->content = 'You deleted the outsider, '.$outsider->name.'.';
            $log->save();

            $outsider->delete();
        }
        elseif($categ == 'locations'){
            $location = Location::findOrFail($request->id);
            

            //save logs
            $log = new Log;
            $log->user_id = Auth::user()->id;
            $log->content = 'You deleted the location, '.$location->name.'.';
            $log->save();

            $location->delete();
        }
        elseif($categ == 'types'){
            $type = Type::findOrFail($request->id);
           

            //save logs
            $log = new Log;
            $log->user_id = Auth::user()->id;
            $log->content = 'You deleted the type, '.$type->name.'.';
            $log->save();
            $type->delete();
        }
        elseif($categ == 'points'){
            $point = Point::findOrFail($request->id);
        
            //save logs
            $log = new Log;
            $log->user_id = Auth::user()->id;
            $log->content = 'You deleted the point, '.$point->name.'.';
            $log->save();

            $point->delete();
        }
        elseif($categ == 'positions'){
            $position = Position::findOrFail($request->id);
            
            //save logs
            $log = new Log;
            $log->user_id = Auth::user()->id;
            $log->content = 'You deleted the position, '.$position->name.'.';
            $log->save();

            $position->delete();
        }
        return $request->id;
    }

    public function editAddables(Request $request){
        $categ = $request->category;

        if($categ == 'outsiders'){
            $obj = Outsider::findOrFail($request->id);
            $obj->name = $request->name;
            $obj->location_id = $request->locId;
            $obj->update();

            $obj->location_name = $obj->location->name;
        }
        elseif($categ == 'locations'){
            $obj = Location::findOrFail($request->id);
            $obj->name = $request->name;
            $obj->update();
        }
        elseif($categ == 'types'){
            $obj = Type::findOrFail($request->id);
            $obj->name = $request->name;
            $obj->category = $request->kind;
            $obj->update();
        }
        elseif($categ == 'points'){
            $obj = Point::findOrFail($request->id);
            $obj->name = $request->name;
            $obj->value = $request->value;
            $obj->update();
        }
        elseif($categ == 'positions'){
            $obj = Position::findOrFail($request->id);
            $obj->name = $request->name;
            $obj->category = $request->kind;
            $obj->update();
        }
        //save logs
        $log = new Log;
        $log->user_id = Auth::user()->id;
        $log->content = 'You updated the'.$categ.', '.$request->name.'.';
        $log->save();


        return json_encode($obj);
    }

    public function addAddables(Request $request){
        $categ = $request->category;
        $category = [];

        if($categ == 'outsiders'){
            $obj = new Outsider;
            $obj->name = $request->name;
            $obj->location_id = $request->locId;
            $obj->user_id = Auth::user()->id;
            $obj->save();
        }
        elseif($categ == 'locations'){
            $obj = new Location;
            $obj->name = $request->name;
            $obj->user_id = Auth::user()->id;
            $obj->save(); 
        }
        elseif($categ == 'types'){
            $obj = new Type;
            $obj->name = $request->name;
            $obj->category = $request->kind;
            $obj->user_id = Auth::user()->id;
            $obj->save();
            $category = ['1' => 'Collection', '2' => 'Tracking', '3' => 'Status', '4' => 'Background'];
        }
        elseif($categ == 'points'){
            $obj = new Point;
            $obj->name = $request->name;
            $obj->value = $request->value;
            $obj->user_id = Auth::user()->id;
            $obj->save();
        }
        elseif($categ == 'positions'){
            $obj = new Position;
            $obj->name = $request->name;
            $obj->category = $request->kind;
            $obj->user_id = Auth::user()->id;
            $obj->save();
            $category = ['1' => 'Faculty', '2' => 'Staff'];
        }
        //save logs
        $log = new Log;
        $log->user_id = Auth::user()->id;
        $log->content = 'You updated the '.$categ.', '.$request->name.'.';
        $log->save();

        return view('loaders.append-addable-loader', compact('obj', 'categ', 'category'));
    }

    public function organizeAddables(Request $request){
        function extractObjects($data, $categ){
            $objs = [];
            if($categ == 'outsiders'){
                foreach ($data as $datum){
                    $obj = Outsider::findOrFail($datum->id);
                    array_push($objs, $obj);
                }
            }
            return $objs;
        }

        $sortBy = $request->sortBy;
        $sortDir = $request->sortDir;
        $categ = $request->category;
        $start = $request->start;
        $end = $request->end;
        $category = [];

        if($categ == 'outsiders'){
            if($sortBy == 'location'){
                $data = DB::table('outsiders')->join('locations', 'outsiders.location_id', '=', 'locations.id')->where('outsiders.id', '>=', $start)->where('outsiders.id', '<=', $end)->orderBy('locations.name', $sortDir)->select('outsiders.id')->get();
                $data = extractObjects($data, $categ); 
            }
            else{
                $data = Outsider::where('id', '>=', $start)->where('id', '<=', $end)->orderBy($sortBy, $sortDir)->get();
            }
        }
        elseif($categ == 'locations'){
            $data = Location::where('id', '>=', $start)->where('id', '<=', $end)->orderBy($sortBy, $sortDir)->get();
        }
        elseif($categ == 'types'){
            $data = Type::where('id', '>=', $start)->where('id', '<=', $end)->orderBy($sortBy, $sortDir)->get();
            $category = ['1' => 'Collection', '2' => 'Tracking', '3' => 'Status', '4' => 'Background'];
        }
        elseif($categ == 'points'){
            $data = Point::where('id', '>=', $start)->where('id', '<=', $end)->orderBy($sortBy, $sortDir)->get();
        }
        elseif($categ == 'positions'){
            $data = Position::where('id', '>=', $start)->where('id', '<=', $end)->orderBy($sortBy, $sortDir)->get();
            $category = ['1' => 'Faculty', '2' => 'Staff'];
        }
        return view('loaders.addables-loader', compact('data', 'categ', 'category'));
    }

    public function index(){
        function chunkAddables($objects){
            $data = ['first_chunk' => [], 'partitions' => []];
            if(count($objects) > 0){
                $partitions = [];
                $addable_chunks = array_chunk($objects, 10);

                for($i = 0; $i < count($addable_chunks); $i++){
                    $end = $addable_chunks[$i][0]->id;
                    $start = $addable_chunks[$i][count($addable_chunks[$i])-1]->id;

                    $partitions[strval($i+1)] = $start.':'.$end;
                }

                $data['first_chunk'] = $addable_chunks[0];
                $data['partitions'] = $partitions;
            }
            return $data;
        }

        function arrayfy($objects){
            $array = [];
            foreach($objects as $obj){
                array_push($array, $obj);
            }
            return $array;
        }

        $locations = Location::orderBy('id', 'desc')->get();
        $outsiders = Outsider::orderBy('id', 'desc')->get();
        $types = Type::orderBy('id', 'desc')->get();

        $numItems = ['outsiders' => count($outsiders), 'locations' => count($locations), 'types' => count($types)];

        //For Locations
        $data1 = chunkAddables(arrayfy($locations));
        $locations = $data1['first_chunk'];
        $partitions_locations = $data1['partitions'];

        //For Outsiders
        $data2 = chunkAddables(arrayfy($outsiders));
        $outsiders = $data2['first_chunk'];
        $partitions_outsiders = $data2['partitions'];

        //For Types
        $data3 = chunkAddables(arrayfy($types));
        $types = $data3['first_chunk'];
        $partitions_types = $data3['partitions'];

        return view('addables.manage-addables', compact('locations', 'outsiders', 'types', 'numItems', 'partitions_types', 'partitions_locations', 'partitions_outsiders'));
    }

    public function addOutsider(Request $request){
        $loc = $request->loc;
        $name = $request->name;
        $newLoc = $request->newLoc;

        $outsider = new Outsider;
        $outsider->user_id = Auth::user()->id;
        $outsider->name = $name;
        if($loc == 'others'){
            $new = new Location;
            $new->user_id = Auth::user()->id;
            $new->name = $newLoc;
            $new->save();

            $outsider->location_id = $new->id;
            $outsider->save();
        }
        else{
            $outsider->location_id = $loc;
            $outsider->save();
        }

        return json_encode($outsider);
    }
    public function searchAddables(Request $request){
        function extractObjects($data, $categ){
            $objs = [];
            if($categ == 'outsiders'){
                foreach ($data as $datum){
                    $obj = Outsider::findOrFail($datum->id);
                    array_push($objs, $obj);
                }
            }
            elseif($categ == 'locations'){
                foreach ($data as $datum){
                    $obj = Location::findOrFail($datum->id);
                    array_push($objs, $obj);
                }
            }
            elseif($categ == 'types'){
                foreach ($data as $datum){
                    $obj = Type::findOrFail($datum->id);
                    array_push($objs, $obj);
                }
            }
            elseif($categ == 'points'){
                foreach ($data as $datum){
                    $obj = Point::findOrFail($datum->id);
                    array_push($objs, $obj);
                }
            }
            elseif($categ == 'positions'){
                foreach ($data as $datum){
                    $obj = Position::findOrFail($datum->id);
                    array_push($objs, $obj);
                }
            }
            return $objs;
        }
        $addable = $request->searchValue;
        $searchBy = $request->searchType;
        $categ = $request->categ;
        $data = '';
        if($categ == 'outsiders' ){
            if($searchBy == 'adder'){
                $data = DB::table('outsiders')->join('users','outsiders.user_id', '=', 'users.id')->where(DB::raw("CONCAT(`users`.`firstname`, `users`.`middlename`, `users`.`surname`)"),'LIKE', '%' . $addable . '%')->select('outsiders.id')->get();
            }
            elseif($searchBy == 'locations'){
                $data = DB::table('outsiders')->join('locations','outsiders.location_id','=', 'locations.id')->where('locations.name','LIKE', '%' . $addable . '%')->select('outsiders.id')->get();
            }
            else{
                $data = DB::table('outsiders')->where('outsiders.name', 'LIKE','%'. $addable.'%')->select('outsiders.id')->get();
            }
            $data = extractObjects($data, $categ); 
        }
        elseif($categ == 'locations' ){
            if($searchBy == 'adder'){
                $data = DB::table('locations')->join('users','locations.user_id', '=', 'users.id')->where(DB::raw("CONCAT(`users`.`firstname`, `users`.`middlename`, `users`.`surname`)"),'LIKE', '%' . $addable . '%')->select('locations.id')->get();
            }
            elseif($searchBy == 'name'){
                
                $data = DB::table('locations')->where('locations.name', 'LIKE','%'. $addable.'%')->select('locations.id')->get();
            }
            $data = extractObjects($data, $categ); 
        }
        elseif($categ == 'points' ){
            if($searchBy == 'adder'){
                $data = DB::table('points')->join('users','points.user_id', '=', 'users.id')->where(DB::raw("CONCAT(`users`.`firstname`, `users`.`middlename`, `users`.`surname`)"),'LIKE', '%' . $addable . '%')->select('points.id')->get();
            }
            elseif($searchBy == 'name'){
                
                $data = Point::where('name', 'LIKE','%'. $addable.'%')->select('id')->get();
            }
            $data = extractObjects($data, $categ); 
        }
        elseif($categ == 'positions' ){
            $category = ['1' => 'Faculty', '2' => 'Staff'];
            if($searchBy == 'adder'){
                $data = DB::table('positions')->join('users','positions.user_id', '=', 'users.id')->where(DB::raw("CONCAT(`users`.`firstname`, `users`.`middlename`, `users`.`surname`)"),'LIKE', '%' . $addable . '%')->select('positions.id')->get();

            }
            elseif($searchBy == 'name'){
                
                $data = DB::table('positions')->where('positions.name', 'LIKE','%'. $addable.'%')->select('positions.id')->get();
            }
            $data = extractObjects($data, $categ); 
        }
        else{
            $category = ['1' => 'Collection', '2' => 'Tracking', '3' => 'Status', '4' => 'Background'];
            if($searchBy == 'adder'){
                $data = DB::table('types')->join('users','types.user_id', '=', 'users.id')->where(DB::raw("CONCAT(`users`.`firstname`, `users`.`middlename`, `users`.`surname`)"),'LIKE', '%' . $addable . '%')->select('types.id')->get();
            }
            elseif($searchBy == 'name'){
                $data = DB::table('types')->where('types.name', 'LIKE','%'. $addable.'%')->select('types.id')->get();
            }

            $data = extractObjects($data, $categ); 
        }
        return view('loaders.addables-loader', compact('data', 'categ','category'));
    }
}
