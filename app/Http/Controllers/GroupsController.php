<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Support\Str;
use App\User;
use App\Membership;
use App\Group;
use App\Log;
use App\Notification;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class GroupsController extends Controller
{
    public function createGroup(Request $request){
        $group = new Group;
        $group->name = $request->name;
        $group->description = $request->description;
        $group->supervisor = Auth::user()->id;

        //Saving of cover photo
        $cover_photo = $request->cover_photo;

        $extension = $cover_photo->getClientOriginalExtension();
        $fauxname = Str::random(10)."_".time().".".$extension;
        $destination = public_path('uploads');
        $cover_photo->move($destination, $fauxname);

        $group->cover = 'uploads/'.$fauxname;
        $group->save();

        //Saving of members
        $members = explode(',', $request->members);
        array_push($members, Auth::user()->id);

        foreach ($members as $member_id){
            $member = new Membership;
            $member->user_id = $member_id;
            $member->group_id = $group->id;
            $member->save();
        }
        $log = new Log;
        $log->content = 'You created a group , '.$request->name.'.';
        $log->user_id = Auth::user()->id;
        $log->save();


        return redirect('/groups');
    }

    public function index(){
    	$user = Auth::user();
    	$memberships = $user->myGroups;
        $users = DB::table('users')->where('id', '!=', Auth::user()->id)->select('users.firstname', 'users.middlename', 'users.surname', 'users.profile_pic', 'users.id')->get();

    	if(count($memberships) == 1){
            $membership = $user->myGroups->first();
            return redirect('/group/'.$membership->group->id);
    	}
        else if(count($memberships) == 0){
            return redirect('/group/none');
        }
        else{
            $groups = [];
            foreach ($memberships as $mem) {
                array_push($groups, $mem->group);
            }
            return view('group.display-groups', compact('groups', 'users'));
        }
        
    }

    public function goToGroup($group_id){
        $members = [];
        $add_members = [];
        $group = 'none';
        if($group_id != 'none'){
            $group = Group::findOrFail($group_id);
            foreach($group->members as $member){
                array_push($members, $member->user->id);
            }

            $users = User::get();
            foreach($users as $user){
                if(!(in_array($user->id, $members))){
                    array_push($add_members, $user);
                }
            }
        }
        
        return view('group.group', compact('group', 'add_members'));
    }

    public function addMembers(Request $request){
        $members = $request->members;
        $members = explode(',', $members);
        
        foreach($members as $id){
            $member = new Membership;
            $member->group_id = $request->group_id;
            $member->user_id = $id;
            $member->save();

            $user = User::findOrFail($member->user_id);
            $group = Group::findOrFail($member->group_id);
            

            //Save notification
            $notification = new Notification;
            $notification->from_user_id = Auth::user()->id;
            $notification->to_user_id = $id;
            $notification->type = '5'; 
            $notification->object_id = $member->group_id;
            $notification->save();


            $log = new Log;
            $log->content = 'You added '.$user->firstname.' '.$user->surname.' as member to the group, '.$group->name.'.';
            $log->user_id = Auth::user()->id;
            $log->save();
        }

        
        return redirect('/group/'.$request->group_id);
    }

    public function removeMember(Request $request){
        $id = $request->id;
        $membership = Membership::findOrFail($id);
        $user = User::findOrFail($membership->user_id);
        $group = Group::findOrFail($membership->group_id);

        $log = new Log;
        $log->user_id = Auth::user()->id;
        $log->content = 'You removed '.$user->firstname.' '.$user->surname.' as memeber to the group, '.$group->name.'.';
        $log->save();
        
        $membership->delete();
        return $id;
    }

    public function searchMember(Request $request){
        $search = $request->search;
        $id = $request->id;

        $members = DB::table('groups')->join('memberships', 'groups.id', '=', 'memberships.group_id')->join('users', 'memberships.user_id', '=', 'users.id')->join('positions', 'users.division', '=', 'positions.id')->where('groups.id', '=', $id)->where(DB::raw("CONCAT(`users`.`firstname`, `users`.`middlename`, `users`.`surname`)"), 'LIKE', '%' . $search . '%')->select('users.firstname', 'users.middlename', 'users.surname', 'users.profile_pic', 'users.email', 'positions.name as position_name', 'memberships.id as membership_id')->get();

        foreach ($members as $member) {
            $member->name = $member->firstname.' '.$member->middlename.' '.$member->surname;
            $member->profile_pic_path = asset($member->profile_pic);
        }
        return json_encode($members);
    }
}
