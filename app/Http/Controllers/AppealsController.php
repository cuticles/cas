<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Appeal;
use App\Log;

class AppealsController extends Controller
{
	public function respondToAppeal(Request $request){
		$id = $request->id;
		$mode = $request->mode;

		$appeal = Appeal::findOrFail($id);
		if($mode == 'verify'){
			$appeal->is_verified = '1';
			//save logs
            $log = new Log;
            $log->user_id = Auth::user()->id;
            $log->content = 'You verified the appeal, '.$appeal->name.'.';
            $log->save();
		}
		else if($mode == 'undo'){
			$appeal->is_verified = '0';
			//save logs
            $log = new Log;
            $log->user_id = Auth::user()->id;
            $log->content = 'You undo verifying the appeal, '.$appeal->name.'.';
            $log->save();
		}

		$appeal->update();

		return $id;
	}

    public function verifyRequest(){
    	$appeals = Auth::user()->appeals;
    	$degLevels = ['1' => 'Associative', '2' => 'Bachelor\'s', '3' => 'Master\'s', '4' => 'Doctoral'];


    	return view('personnel.verifyRequests', compact('appeals', 'degLevels'));
    }
}
