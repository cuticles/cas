<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use App\Document;
use App\Collection;
use App\Tracking;
use App\User;
use App\Outsider;
use App\Group;
use App\Location;
use App\Notification;
use App\Transmit;
use App\Type;
use App\log;
use Auth;
use Carbon;

class CollectionsController extends Controller
{
    public function viewReceivers(Request $request){
        $transmits = Transmit::where('collection_id', $request->id)->where('from_user_id', Auth::user()->id)->get();
        $users = [];
        foreach ($transmits as $transmit) {
            $user = $transmit->receiver;
            $user->name = $user->firstname.' '.$user->middlename.' '.$user->surname;
            array_push($users, $user);
        }

        return view('loaders.forward-receivers-loader', compact('users'));
    }

    public function addDocuments(Request $request){
        $documents = $request->docs;
        $coll_id = $request->coll_id;
        $status = $request->status;
        $track = Tracking::where('status_id', Type::where('name', $status)->where('category', '3')->first()->id)->where('collection_id', $coll_id)->first();
        $coll = Collection::findOrFail($coll_id);

        foreach($documents as $document){
            $doc = new Document;
            $name = '('.ucfirst($status).')'.$document->getClientOriginalName();
            $extension = $document->getClientOriginalExtension();
            $size = $document->getSize();

            $fauxname = Str::random(10)."_".time().".".$extension;
            $destination = public_path('uploads');
            $document->move($destination, $fauxname);

            $doc->collection_id = $coll_id;
            $doc->tracking_id = $track->id;
            $doc->file_name = $name;
            $doc->file_size = $size;
            $doc->file_extension = $extension;
            $doc->file_path = $destination."\\".$fauxname;
            $doc->status_id = Type::where('name', $status)->where('category', '3')->first()->id;
            $doc->save();
        }

        //Save notification
        foreach($coll->transmits as $transmit){
            $notification = new Notification;
            $notification->from_user_id = Auth::user()->id;
            $notification->to_user_id = $transmit->sender->id;
            $notification->type = '2'; 
            $notification->object_id = $coll_id;
            $notification->save();
        }
        
        $log = new Log;
        $log->content = 'You added a document, '.$doc->file_name.'to the collection, '.$coll->title.'.';
        $log->user_id = Auth::user()->id;
        // $log->action = '1';
        // $log->type = '2'; 
        // $log->content_root = $coll_id;
        $log->save();

        return redirect('/collection/'.$coll_id);
    }

    public function deleteDocument(Request $request){
        function arrayStatuses($docs){
            $statuses_arr = [];
            foreach ($docs as $doc){
                array_push($statuses_arr, $doc->status->name);
            }
            return $statuses_arr;
        }

        $id = $request->id;
        $coll_id = $request->coll_id;
        $case = $request->case;
        $doc = Document::findOrFail($id);

        if($case == 'manyPending' || $case == 'manyDocRemain'){
            $doc->delete();
        }
        else{
            $statuses = arrayStatuses($doc->collection->documents);
            $doc->collection->status_id = Type::where('name', $statuses[1])->where('category', '3')->first()->id;
            $doc->collection->update();
            $trackings = Tracking::where('status_id', $doc->status_id)->where('collection_id', $doc->collection->id)->get();
            foreach($trackings as $track){
                $track->delete();
            }
            $doc->delete();
        }


        return redirect('/collection/'.$coll_id);
    }
    public function deleteDocumentCheck(Request $request){
        function reverseArray($docs){
            $reversed_docs = [];
            for($i = (count($docs)-1); $i>=0; $i--){
                array_push($reversed_docs, $docs[$i]);
            }

            return $reversed_docs;
        }
        function createArrangedArrayOfDocuments($docs){
            $docs_assoc = [];

            foreach ($docs as $doc) {
                if(array_key_exists($doc->status->name, $docs_assoc)){
                    $docs_assoc[$doc->status->name][] = $doc;
                }
                else{
                    $docs_assoc[$doc->status->name] = array();
                    $docs_assoc[$doc->status->name][] = $doc;
                }
            }

            $temp_arr = [];

            foreach($docs_assoc as $docs){
                foreach ($docs as $doc) {
                    array_push($temp_arr, $doc);
                }
            }

            $final = reverseArray($temp_arr);

            return $final;
        }

        function recordStatusNumbers($docs){
            $statuses = [];
            foreach($docs as $doc){
                if(array_key_exists($doc->status->name, $statuses)){
                    $statuses[$doc->status->name] += 1;
                }
                else{
                    $statuses[$doc->status->name] = 0;
                    $statuses[$doc->status->name] += 1;
                }
            }
            return $statuses;
        }

        function arrayStatuses($docs){
            $statuses_arr = [];
            foreach ($docs as $doc){
                array_push($statuses_arr, $doc->status->name);
            }
            return $statuses_arr;
        }

        $id = $request->id;
        $response = ['case' => null, 'documentId' => $id, 'message' => null, 'status' => null];
        $doc = Document::findOrFail($id);
        $docs = createArrangedArrayOfDocuments($doc->collection->documentsAsc);
        $statuses = recordStatusNumbers($docs);
        $statuses_arr = arrayStatuses($docs);

        if($doc->status->name == 'pending'){
            if($statuses['pending'] == 1){
                $response['message'] = 'There is only one <span class="semibold">Pending</span> document left and you can\'t delete this due to either of the following reasons: A collection should never be empty, or there should always be atleast one <span class="semibold">Pending</span> document in a collection.';
                $response['status'] = 'fail';
                $response['case'] = 'solePending'; 
            }
            else{
                $response['message'] = 'Are you sure you want to delete <span class="semibold">'.$doc->file_name.'</span> with status <span class="semibold">Pending</span>?';
                $response['status'] = 'success';
                $response['case'] = 'manyPending'; 
            }
        }
        else{
            if($statuses[$doc->status->name] == 1 && array_search($doc->status->name, $statuses_arr) == 0){
                $response['message'] = 'Are you sure you want to delete <span class="semibold">'.$doc->file_name.'</span> with status '.$doc->status->name.' and revert back to status <span class="semibold">'.ucfirst($statuses_arr[1]).'</span>?';
                $response['status'] = 'success';
                $response['case'] = 'soleDocHead';
            }
            else if($statuses[$doc->status->name] == 1 && array_search($doc->status->name, $statuses_arr) != 0){
                $response['message'] = 'You can\'t remove this document because it is in the middle of the tracking and it is the only document left with this status. You may delete some documents after this document with status <span class="semibold">'.$doc->status->name.'</span>.';
                $response['status'] = 'fail';
                $response['case'] = 'soleDocMiddle';
            }
            else if($statuses[$doc->status->name] > 1){
                $response['message'] = 'Are you sure you want to delete <span class="semibold">'.$doc->file_name.'</span>?';
                $response['status'] = 'success';
                $response['case'] = 'manyDocRemain';
            }
        }
        return json_encode($response);
    }

    public function saveCollection(Request $request){
        $create_type = $request->create_coll_type;
    	$collection = new Collection;
    	$collection->code = Str::random(10);
    	$collection->title = $request->title;
    	$collection->description = $request->description;
    	$collection->type_id = $request->type_id;
        $collection->status_id = Type::where('name', 'pending')->where('category', '3')->first()->id;
    	$collection->tags = $request->tags;
    	$collection->save();
        
        if($create_type == "receive"){
            $method = $request->receive_by_method;
            $ids = [];
            if($method == 'many'){
                $ids = explode(',', $request->receivers); 
            }
            else if($method == 'group'){
                $group = Group::findOrFail($request->group_id);

                foreach ($group->members as $member){
                    if($member->user->id != Auth::user()->id){
                        array_push($ids, $member->user->id);
                    }
                }
            }
            else if($method == 'all'){
                $users = DB::tables('users')->where('users.id', '!=', Auth::user()->id)->select('users.id')->get();

                foreach ($users as $user){
                    array_push($ids, $user->id);
                }
            }
            
            array_push($ids, Auth::user()->id);

            foreach ($ids as $id){
                $transmit = new Transmit;
                $transmit->collection_id = $collection->id;
                $transmit->to_user_id = $id;
                $transmit->uploader = Auth::user()->id;
                $transmit->from_outside = '1';
                $transmit->outsider_id = $request->outsider_id;
                $transmit->save();

                //Save notification
                $notification = new Notification;
                $notification->from_user_id = Auth::user()->id;
                $notification->to_user_id = $transmit->to_user_id;
                $notification->type = '1'; 
                $notification->object_id = $collection->id;
                $notification->save();
            }
        }
        else{
            $method = $request->receive_by_method;
            $ids = [];
            $outsiders = explode(',', $request->outsider_ids);


            if($method == 'many'){
                $ids = explode(',', $request->receivers); 
            }
            else if($method == 'group'){
                $group = Group::findOrFail($request->group_id);

                foreach ($group->members as $member){
                    if($member->user->id != Auth::user()->id){
                        array_push($ids, $member->user->id);
                    }
                }
            }
            else if($method == 'all'){
                $users = DB::tables('users')->where('users.id', '!=', Auth::user()->id)->select('users.id')->get();

                foreach ($users as $user){
                    array_push($ids, $user->id);
                }
            }

            if($request->send_to == 'inside'){
                foreach ($ids as $id){
                    $transmit = new Transmit;
                    $transmit->collection_id = $collection->id;
                    $transmit->from_user_id = Auth::user()->id;
                    $transmit->to_user_id = $id;
                    $transmit->save();

                    //Save notification
                    $notification = new Notification;
                    $notification->from_user_id = Auth::user()->id;
                    $notification->to_user_id = $transmit->to_user_id;
                    $notification->type = '1'; 
                    $notification->object_id = $collection->id;
                    $notification->save();

                    //save Log
                    $log = new Log;
                    $log->user_id = Auth::user()->id;
                    $log->content = 'You sent collection, '.$collection->title.' to '.$request->receive_by_method.'.';
                    // $log->action = '1';
                    // $log->type = '1';
                    // $log->content_root = $transmit->to_user_id; 
                    $log->save();
                }
                
            }
            else{
                foreach ($outsiders as $id){
                    $transmit = new Transmit;
                    $transmit->collection_id = $collection->id;
                    $transmit->from_user_id = Auth::user()->id;
                    $transmit->outsider_id = $id;
                    $transmit->to_outside = '1';
                    $transmit->save();

                    //Save notification
                    $notification = new Notification;
                    $notification->from_user_id = Auth::user()->id;
                    $notification->to_user_id = $transmit->to_user_id;
                    $notification->type = '1'; 
                    $notification->object_id = $collection->id;
                    $notification->save();

                 //save Log

                    $log = new Log;
                    $log->user_id = Auth::user()->id;
                    $log->content = 'You sent collection, '.$collection->title.' to '.$request->receive_by_method.'.';
                    // $log->action = '1';
                    // $log->type = '1';
                    // $log->content_root = $transmit->to_user_id; 
                    $log->save();
                }                
            }
        }

        //Creation of tracking for initial status of collection
        $sole_transmit = $collection->transmits->first();
        $type_init = Type::where('name', 'initial')->first();
        $update_track = new Tracking;
        $update_track->collection_id = $collection->id;
        $update_track->user_updater = Auth::user()->id;
        $update_track->type_id = $type_init->id;
        $update_track->status_id = Type::where('name', 'pending')->where('category', '3')->first()->id;
        $update_track->is_step = 0;

        if($create_type == 'receive'){
            $update_track->from_outside = '1';
            $update_track->outsider_id = $request->outsider_id;
            $update_track->location_id = $sole_transmit->receiver->division;
        }
        else{
            $outsiders = explode(',', $request->outsider_ids);

            if($request->send_to == 'outside'){
                $update_track->to_outside = '1';
                $update_track->outsider_id = $outsiders[0];
                $update_track->location_id = Outsider::findOrFail($outsiders[0])->location_id;
            }
            else{
                $update_track->location_id = $sole_transmit->receiver->division;
            }
        }

        $update_track->save();

        //Moving of documents to public path and saving of documents to the database
		$documents = $request->documents;
		foreach($documents as $document){
			$doc = new Document;
    		$name = $document->getClientOriginalName();
    		$extension = $document->getClientOriginalExtension();
    		$size = $document->getSize();

            $fauxname = Str::random(10)."_".time().".".$extension;
	        $destination = public_path('uploads');
	        $document->move($destination, $fauxname);

	        $doc->collection_id = $collection->id;
            $doc->tracking_id = $update_track->id;
	        $doc->file_name = $name;
	        $doc->file_size = $size;
	        $doc->file_extension = $extension;
	        $doc->file_path = $destination."\\".$fauxname;
            $doc->status_id = Type::where('name', 'pending')->where('category', '3')->first()->id;
	        $doc->save();
		}

        return redirect('/sent');
    }

    public function viewCollection($id){
        function reverseArray($docs){
            $reversed_docs = [];
            for($i = (count($docs)-1); $i>=0; $i--){
                array_push($reversed_docs, $docs[$i]);
            }

            return $reversed_docs;
        }

        function createArrangedArrayOfDocuments($docs){
            $docs_assoc = createDocSegments($docs);

            $temp_arr = [];

            foreach($docs_assoc as $status => $docs){
                foreach ($docs as $doc) {
                    array_push($temp_arr, $doc);
                }
            }

            $final = reverseArray($temp_arr);

            return $final;
        }

        function createDocSegments($docs){
            $segments = [];

            foreach ($docs as $doc) {
                if(array_key_exists($doc->status->name, $segments)){
                    $doc->sizeInBytes = formatBytes($doc->file_size);
                    $segments[$doc->status->name][] = $doc;
                }
                else{
                    $segments[$doc->status->name] = array();
                    $doc->sizeInBytes = formatBytes($doc->file_size);
                    $segments[$doc->status->name][] = $doc;
                }
            }
            return $segments;
        }

        function formatBytes($size, $precision = 2){
            $base = log($size, 1024);
            $suffixes = array('', 'KB', 'MB', 'GB', 'TB');   

            return round(pow(1024, $base - floor($base)), $precision) .' '. $suffixes[floor($base)];
        }

        $collection = Collection::findOrFail($id);
        $user = Auth::user();
        $user_id = Auth::user()->id;
        $users = DB::table('users')->select('users.id', 'users.firstname', 'users.middlename', 'users.surname', 'users.profile_pic')->get();
        $authorized_users = [];
        $authorized = [];
        $forward_users = [];
        $icons = array("pdf" => "file pdf outline icon", "docx" => "file word outline icon", "doc" => "file word outline icon", "xls" => "file excel outline icon", "xlsx" => "file excel outline icon", "zip" => "file archive outline icon", "rar" => "file archive outline icon", "pptx" => "file powerpoint outline icon", "ppt" => "file powerpoint outline icon", "txt" => "file alternate outline icon");
        $types = Type::where('category', '1')->get();
        $statuses = Type::where('category', '3')->where('name', '!=', 'pending')->get();
        $documents = [];
        $docs_segments = [];
        
        $groups = Group::all();
        $outsiders = Outsider::all();
        $locations = Location::all();
        $trackings_statuses = Tracking::where('collection_id', $id)->pluck('status_id')->unique()->toArray();
        $progress_trackings = [];
        $trackings = array();
        $trackings_status_names = [];

        if($collection != null){
            //Initialize other details from the transmits
            $transmit = Transmit::where('collection_id', '=', $collection->id)->first();
            $collection->receiver = $transmit->receiver;
            $collection->sender = $transmit->sender;
            $collection->from_outside = $transmit->from_outside;
            $collection->to_outside = $transmit->to_outside;
            $collection->outsider = $transmit->outsider;

            $documents = createArrangedArrayOfDocuments($collection->documentsAsc);
            foreach ($documents as $document){
                $document->sizeInBytes = formatBytes($document->file_size);
            }
            $docs_segments = createDocSegments($collection->documents);

            $progress_trackings = $collection->trackings->where('is_step', '1');
            $trackingObjects = $collection->trackings->take(3);

            //For tracking status names
            foreach($trackings_statuses as $track_type){
                array_push($trackings_status_names, Type::findOrFail($track_type)->name);
            }

            //For latest 3 trackings shown on the right side
            foreach($trackingObjects as $tracking){
                if(array_key_exists(Carbon\Carbon::parse($tracking->created_at)->format('F j, Y'), $trackings)){
                    $trackings[Carbon\Carbon::parse($tracking->created_at)->format('F j, Y')][] = $tracking;
                }
                else{
                    $trackings[Carbon\Carbon::parse($tracking->created_at)->format('F j, Y')] = array();
                    $trackings[Carbon\Carbon::parse($tracking->created_at)->format('F j, Y')][] = $tracking;
                }
            }

            //Determine users/admins who are authorized to update/view
            foreach($collection->transmitsAll as $transmit){
                if($transmit->to_outside == '0'){
                    array_push($authorized_users, $transmit->receiver->id);
                    array_push($authorized, $transmit->receiver->id);
                    if($transmit->from_outside == '0'){
                        array_push($authorized, $transmit->sender->id);
                    }
                }
                if($transmit->to_outside == '1'){
                    array_push($authorized_users, $transmit->sender->id);
                    array_push($authorized, $transmit->sender->id);
                }
            }

            foreach ($users as $user){
                if(!(in_array($user->id, $authorized))){
                    array_push($forward_users, $user);
                }
            }
        }

        return view('collections.view_collection', compact('collection', 'icons', 'trackings', 'forward_users', 'documents', 'authorized_users', 'groups', 'docs_segments', 'outsiders', 'locations', 'types', 'statuses', 'progress_trackings', 'trackings_status_names', 'authorized'));
    }

    public function editCollection(Request $request){
        $icons = array('report' => asset('images/analysis.png'), 'certificate' => asset('images/diploma.png'), 'request' => asset('images/application.png'));

        $id = $request->id;
        $coll = Collection::findOrFail($id);

        $log = new Log;
        $log->user_id = Auth::user()->id;
        $log->content = 'You edited the collection, '.$coll->title.' to '.$request->titl.'.';
        // $log->action = '2';
        // $log->type = '1';
        // $log->content_root = $id; 
        $log->save();

        $coll->title = $request->title;
        $coll->description = $request->description;
        $coll->type_id = $request->type;
        $coll->tags = $request->tags;

        $coll->update();
        $coll->type_icon = $icons[$coll->type->name];
        $coll->type_name = ucfirst($coll->type->name);

        

        return json_encode($coll);
    }

    public function searchReceivedCols(Request $request){
        function retrieveColls($colls){
            $collections = [];

            foreach ($colls as $coll){
                $transmit = Transmit::where('collection_id', '=', $coll->id)->first();
                $collection = Collection::findOrFail($coll->id);
                // $collection->type = Type::where('id','=',$collection->type_id);
                $collection->sender = $transmit->sender;
                $collection->from_outside = $transmit->from_outside;
                $collection->outsider = $transmit->outsider;
                array_push($collections, $collection);
            }
            return $collections;
        }

        $icons = array('report'=>'images/analysis.png', 'certificate' => 'images/diploma.png', 'request' => 'images/application.png');
        $cols = $request->search;
        $searchBy = $request->searchBy;
        $user_id = Auth::user()->id;
        $collections = '';
        $receivedOut = [];

        if($searchBy == 'sender'){
            $collections = DB::table('transmits')->join('users', 'transmits.from_user_id','=','users.id')->join('collections', 'transmits.collection_id', '=', 'collections.id')->where('transmits.to_user_id', '=', Auth::user()->id)->where('transmits.trashed_by_receiver', '=', '0')->where(DB::raw("CONCAT(`users`.`firstname`, `users`.`middlename`, `users`.`surname`)"),'LIKE', '%' . $cols . '%')->select('collections.*')->get();

            $receivedOut = DB::table('transmits')->join('outsiders', 'transmits.outsider_id','=','outsiders.id')->join('collections', 'transmits.collection_id', '=', 'collections.id')->where('transmits.to_user_id', '=', Auth::user()->id)->where('transmits.trashed_by_receiver', '=', '0')->where('transmits.trashed_by_sender','=','0')->where('transmits.to_outside','1')->where('outsiders.name','LIKE', '%' . $cols . '%')->select('collections.*')->get();
        }
        else{
            $collections = DB::table('transmits')->join('collections', 'transmits.collection_id', '=', 'collections.id')->where('transmits.to_user_id', '=', Auth::user()->id)->where('transmits.trashed_by_receiver', '=', '0')->where('collections.'.$searchBy, 'LIKE', '%' . $cols . '%')->select('collections.*')->get();
        }

        $collections = retrieveColls($collections);
        $receivedOut = retrieveColls($receivedOut);

        $collections = array_merge($collections, $receivedOut);

        return view('loaders.received-collections-loader', compact('collections', 'icons'));
    }

    public function searchSentCols(Request $request){
        function retrieveColls($colls){
            $coll_ids = [];
            $collections = [];

            foreach ($colls as $coll){
                if (!(in_array($coll->id, $coll_ids))){
                    $transmit = Transmit::where('collection_id', '=', $coll->id)->first();
                    $collection = Collection::findOrFail($coll->id);
                    $collection->receiver = $transmit->receiver;
                    $collection->to_outside = $transmit->to_outside;
                    $collection->outsider = $transmit->outsider;
                    array_push($collections, $collection);
                    array_push( $coll_ids,$coll->id);
                }
                
            }
            return $collections;
        }
        $icons = array('report'=>'images/analysis.png', 'certificate' => 'images/diploma.png', 'request' => 'images/application.png');
        $cols = $request->search;
        $searchBy = $request->searchBy;
        $user_id = Auth::user()->id;
        $collections = [];
        $collOut = [];

        //now get all user and services in one go without looping using eager loading
        //In your foreach() loop, if you have 1000 users you will make 1000 queries

        if($searchBy == 'receiver'){
            $collections = DB::table('transmits')->join('users', 'transmits.to_user_id','=','users.id')->join('collections', 'transmits.collection_id', '=', 'collections.id')->where('transmits.from_user_id', '=', Auth::user()->id)->where('transmits.trashed_by_sender', '=', '0')->where(DB::raw("CONCAT(`users`.`firstname`, `users`.`middlename`, `users`.`surname`)"),'LIKE', '%' . $cols . '%')->select('collections.*')->get();

            $collOut = DB::table('transmits')->join('outsiders', 'transmits.outsider_id','=','outsiders.id')->join('collections', 'transmits.collection_id', '=', 'collections.id')->where('transmits.to_outside','1')->where('transmits.from_user_id', '=', Auth::user()->id)->where('transmits.trashed_by_sender', '=', '0')->where('outsiders.name','LIKE', '%' . $cols . '%')->select('collections.*')->get();
        }
        else{
            $collections = DB::table('transmits')->join('collections', 'transmits.collection_id', '=', 'collections.id')->where('transmits.from_user_id', '=', Auth::user()->id)->where('transmits.trashed_by_sender', '=', '0')->where('collections.'.$searchBy, 'LIKE', '%' . $cols . '%')->select('collections.*')->get();
        }


        $collections = retrieveColls($collections);
        $collOut = retrieveColls($collOut);

        $collections = array_merge($collections, $collOut);   

        return view('loaders.sent-collections-loader', compact('collections', 'icons'));
    }

    public function trashCollection(Request $request){
        $coll_id = $request->id;
        $type = $request->type;
        $coll = Collection::findOrFail($coll_id);

        if($type == 'sent'){
            $transmits = Transmit::where('collection_id', $coll_id)->where('from_user_id', Auth::user()->id)->where('is_forward', '0')->where('trashed_by_sender','0')->get();
            foreach($transmits as $transmit){
                $transmit->trashed_by_sender = '1';
                $transmit->update();
            }
        }
        else{
            $transmits = Transmit::where('collection_id', $coll_id)->where('to_user_id', Auth::user()->id)->where('is_forward', '0')->where('trashed_by_receiver','0')->get();
            foreach($transmits as $transmit){
                $transmit->trashed_by_receiver = '1';
                $transmit->update();
            }
        }
        //save Log
        $log = new Log;
        $log->user_id = Auth::user()->id;
        $log->content = 'You trashed the collection, '.$coll->title.'.';
        // $log->action = '3';
        // $log->type = '1'; 
        // $log->content_root= $coll_id;
        $log->save();
        return json_encode($coll_id);
    }

    public function trashReceivedColls(){
        function uniquifyColls($colls){
            $coll_ids = [];
            $collections = [];

            foreach ($colls as $coll){
                if (!(in_array($coll->id, $coll_ids))){
                    $transmit = Transmit::where('collection_id', '=', $coll->id)->first();
                    $collection = Collection::findOrFail($coll->id);
                    // $collection->receiver = $transmit->receiver;
                    array_push($collections, $collection);
                    array_push( $coll_ids,$coll->id);

                }  
            }
            return $collections;
        }
        $collections = '';
        $icons = array('report'=>'images/analysis.png', 'certificate' => 'images/diploma.png', 'request' => 'images/application.png');
        $collections = DB::table('collections')->join('transmits','collections.id','=','transmits.collection_id')->where('transmits.from_user_id','=',Auth::user()->id)->where('is_forward','0')->where('trashed_by_sender','1')->orWhere('trashed_by_receiver','1')->select('collections.*')->get();

        $collections = uniquifyColls($collections);

        return view('admin.recycle-bin',compact('collections','icons'));


    }
    public function deleteTrashColls(Request $request){
        $coll_id = $request->id;  
        $type = $request->type;
        $transmits = Transmit::findOrFail($coll_id);
        if($transmits->from_user_id == Auth::user()->id){
            $transmits = Transmit::where('collection_id', $coll_id)->where('from_user_id', Auth::user()->id)->where('is_forward', '0')->where('trashed_by_sender','1')->get();
            foreach($transmits as $transmit){
                $transmit->delete();
            }
            $notifs = Notification::where('object_id', $coll_id)->where('from_user_id', Auth::user()->id)->where('type','1')->get();
            foreach($notifs as $notif){
                $notif->delete();
            }
        }
        else{
            $transmits = Transmit::where('collection_id', $coll_id)->where('to_user_id', Auth::user()->id)->where('is_forward', '0')->where('trashed_by_receiver','1')->get();
            foreach($transmits as $transmit){
                $transmit->delete();
            }
            $notifs = Notification::where('object_id', $coll_id)->where('to_user_id', Auth::user()->id)->where('type','1')->get();
            foreach($notifs as $notif){
                $notif->delete();
            }
        }
        $collection = Collection::findOrFail($coll_id);

        //save Log
        $log = new Log;
        $log->user_id = Auth::user()->id;
        $log->content = 'You deleted the collection, '.$collection->title.'.';
        $log->save();

        $collection->delete();


        return $coll_id;
    }
    public function restoreTrashColls(Request $request){
        $coll_id = $request->id;
        $coll = Collection::findOrFail($coll_id);
        
        $transmits = Transmit::where('collection_id', $coll_id)->where('from_user_id',Auth::user()->id)->where('trashed_by_sender','1')->get();
        foreach($transmits as $transmit){
            $transmit->trashed_by_sender = '0';
            $transmit->save();
        }  
 
        $transmits = Transmit::where('collection_id', $coll_id)->where('to_user_id',Auth::user()->id)->where('trashed_by_receiver','1')->get();
            foreach($transmits as $transmit){
                $transmit->trashed_by_receiver = '0';
                $transmit->save();
            }  

       
        // //save Log
        // $log = new Log;
        // $log->user_id = Auth::user()->id;
        // $log->content_id = $coll_id;
        // $log->action = '5';
        // $log->type = '1'; 
        // $log->content_root = $coll_id;
        // $log->save();
        //save Log
        $log = new Log;
        $log->user_id = Auth::user()->id;
        $log->content = 'You restored the collection, '.$coll->title.'.';
        // $log->action = '1';
        // $log->type = '1';
        // $log->content_root = $transmit->to_user_id; 
        $log->save();

        return json_encode($coll_id);
    }
} 