<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Location;
use App\Outsider;
use App\Type;
use App\Point;
use App\Position;
use App\User;
use Auth;

class PaginationController extends Controller
{
    public function resetPagesUsers(Request $request){
        $numItems = $request->num_items;
        $users_ids = [];
        $partitions = [];

        foreach (DB::table('users')->orderBy('id', 'desc')->where('is_deleted','0')->select('users.id')->get() as $user_id){
            array_push($users_ids, $user_id->id);
        }

        if(count($users_ids) > 0){
            $user_chunks = array_chunk($users_ids, $numItems);

            for($i = 0; $i < count($user_chunks); $i++){
                $end = $user_chunks[$i][0];
                $start = $user_chunks[$i][count($user_chunks[$i])-1];

                $partitions[strval($i+1)] = $start.':'.$end;
            }
        }

        return json_encode($partitions);
    }

    public function resetCredentialPages(Request $request){
        $numItems = $request->num_items;
        $user_id = $request->user_id;
        $user = User::findOrFail($user_id);
        $partitions = [];

        $credentials = [];
        foreach ($user->credentials as $cred){
            array_push($credentials, $cred);
        }

        if(count($credentials) > 0){
            $docs_chunks = array_chunk($credentials, $numItems);

            for($i = 0; $i < count($docs_chunks); $i++){
                $end = $docs_chunks[$i][0]->id;
                $start = $docs_chunks[$i][count($docs_chunks[$i])-1]->id;

                $partitions[strval($i+1)] = $start.':'.$end;
            }
        }

        return json_encode($partitions);
    }

    public function resetPagesAddables(Request $request){
        function partitionAddables($objects, $numItems){
            $partitions = [];
            if(count($objects) > 0){
                $addable_chunks = array_chunk($objects, $numItems);

                for($i = 0; $i < count($addable_chunks); $i++){
                    $end = $addable_chunks[$i][0]->id;
                    $start = $addable_chunks[$i][count($addable_chunks[$i])-1]->id;

                    $partitions[strval($i+1)] = $start.':'.$end;
                }
            }
            return $partitions;
        }

        function arrayfy($objects){
            $array = [];
            foreach($objects as $obj){
                array_push($array, $obj);
            }
            return $array;
        }

        $num_items = $request->num_items;
        $categ = $request->category;

        if($categ == 'outsiders'){
            $objects = Outsider::orderBy('id', 'desc')->get();
            $partitions = partitionAddables(arrayfy($objects), $num_items);
        }
        elseif($categ == 'locations'){
            $objects = Location::orderBy('id', 'desc')->get();
            $partitions = partitionAddables(arrayfy($objects), $num_items);
        }
        elseif($categ == 'types'){
            $objects = Type::orderBy('id', 'desc')->get();
            $partitions = partitionAddables(arrayfy($objects), $num_items);
        }
        elseif($categ == 'points'){
            $objects = Point::orderBy('id', 'desc')->get();
            $partitions = partitionAddables(arrayfy($objects), $num_items);
        }
        elseif($categ == 'positions'){
            $objects = Position::orderBy('id', 'desc')->get();
            $partitions = partitionAddables(arrayfy($objects), $num_items);
        }

        $data = ['partitions' => $partitions, 'count' => count($objects)];

        return json_encode($partitions);
    }
    public function resetReceivedForwards(Request $request){
        $initForwards = Auth::user()->receivedForwards;
        $num_items = $request->num_items;
        $forwards = [];

        foreach($initForwards as $forward){
            array_push($forwards, $forward);
        }

        $partitions = [];

        if(count($forwards) > 0){
            $forwards_chunks = array_chunk($forwards, $num_items);

            for($i = 0; $i < count($forwards_chunks); $i++){
                $end = $forwards_chunks[$i][0]->id;
                $start = $forwards_chunks[$i][count($forwards_chunks[$i])-1]->id;

                $partitions[strval($i+1)] = $start.':'.$end;
            }

            $forwards = $forwards_chunks[0];
        }

        return json_encode($partitions);
    }

    public function resetPagesMyForwards(Request $request){
        function uniquifyForwards($forwards){
            $coll_ids = [];
            $forwards_arr = [];

            foreach ($forwards as $forward) {
                if(!(in_array($forward->collection->id, $coll_ids))){
                    array_push($coll_ids, $forward->collection->id);
                    array_push($forwards_arr, $forward);
                }
            }

            return $forwards_arr;
        }
        
        $num_items = $request->num_items;
        $forwards = Auth::user()->myForwards;
        $forwards = uniquifyForwards($forwards);
        $partitions = [];

        $numItems = count($forwards);

        $forwards_chunks = array_chunk($forwards, $num_items);

        for($i = 0; $i < count($forwards_chunks); $i++){
            $end = $forwards_chunks[$i][0]->id;
            $start = $forwards_chunks[$i][count($forwards_chunks[$i])-1]->id;

            $partitions[strval($i+1)] = $start.':'.$end;
        }

        $forwards = $forwards_chunks[0];

        return json_encode($partitions);
    }

    public function resetPagesSentDocs(Request $request){
    	function arrayfy($objs){
            $arr = [];
            foreach ($objs as $obj) {
                array_push($arr, $obj->id);
            }
            return $arr;
        }

        $num_items = $request->num_items;
        $user = Auth::user();
        $partitions = [];
    	$doc_ids = DB::table('transmits')->join('collections', 'transmits.collection_id', '=', 'collections.id')->join('documents', 'collections.id', '=', 'documents.collection_id')->where('transmits.from_user_id', '=', Auth::user()->id)->where('transmits.trashed_by_sender', '=', '0')->where('transmits.is_forward', '=', '0')->orderBy('documents.id', 'desc')->select('documents.id')->get();
        $doc_ids = arrayfy($doc_ids);
        $doc_ids = array_unique($doc_ids);

        if(count($doc_ids) > 0){
            $docs_chunks = array_chunk($doc_ids, $num_items);

            for($i = 0; $i < count($docs_chunks); $i++){
                $end = $docs_chunks[$i][0];
                $start = $docs_chunks[$i][count($docs_chunks[$i])-1];

                $partitions[strval($i+1)] = $start.':'.$end;
            }
        }

        return json_encode($partitions);
    }

    public function resetPagesSentColls(Request $request){
    	function arrayfy($objs){
            $arr = [];
            foreach ($objs as $obj) {
                array_push($arr, $obj->id);
            }
            return $arr;
        }

        $num_items = $request->num_items;
        $partitions = [];

        $coll_ids = DB::table('transmits')->join('collections', 'transmits.collection_id', '=', 'collections.id')->where('transmits.from_user_id', '=', Auth::user()->id)->where('transmits.trashed_by_sender', '=', '0')->where('transmits.is_forward', '=', '0')->orderBy('collections.id', 'desc')->select('collections.id')->get();
        $coll_ids = arrayfy($coll_ids);
        $coll_ids = array_unique($coll_ids);

        if(count($coll_ids) > 0){
            $colls = array_chunk($coll_ids, $num_items);

            for($i = 0; $i < count($colls); $i++){
                $end = $colls[$i][0];
                $start = $colls[$i][count($colls[$i])-1];

                $partitions[strval($i+1)] = $start.':'.$end;
            }
        }

        $data = ['partitions' => $partitions, 'count' => count($coll_ids)];

        return json_encode($data);
    }

    public function resetPagesReceivedDocs(Request $request){
    	function arrayfy($objs){
            $arr = [];
            foreach ($objs as $obj) {
                array_push($arr, $obj->id);
            }
            return $arr;
        }
        
        $partitions = [];
        $num_items = $request->num_items;
        $doc_ids = DB::table('transmits')->join('collections', 'transmits.collection_id', '=', 'collections.id')->join('documents', 'collections.id', '=', 'documents.collection_id')->where('transmits.to_user_id', '=', Auth::user()->id)->where('transmits.trashed_by_receiver', '=', '0')->where('transmits.is_forward', '=', '0')->orderBy('documents.id', 'desc')->select('documents.id')->get();
        $doc_ids = arrayfy($doc_ids);
        $doc_ids = array_unique($doc_ids);

        if(count($doc_ids) > 0){
            $docs_chunks = array_chunk($doc_ids, $num_items);

            for($i = 0; $i < count($docs_chunks); $i++){
                $end = $docs_chunks[$i][0];
                $start = $docs_chunks[$i][count($docs_chunks[$i])-1];

                $partitions[strval($i+1)] = $start.':'.$end;
            }
        }

        return json_encode($partitions);
    }

    public function resetPagesReceivedColls(Request $request){
        function arrayfy($objs){
            $arr = [];
            foreach ($objs as $obj) {
                array_push($arr, $obj->id);
            }
            return $arr;
        }

    	$num_items = $request->num_items;
    	$partitions = [];

        $coll_ids = DB::table('transmits')->join('collections', 'transmits.collection_id', '=', 'collections.id')->where('transmits.to_user_id', '=', Auth::user()->id)->where('transmits.trashed_by_receiver', '=', '0')->where('transmits.is_forward', '=', '0')->orderBy('collections.id', 'desc')->select('collections.id')->get();
        $coll_ids = arrayfy($coll_ids);
        $coll_ids = array_unique($coll_ids);

        if(count($coll_ids) > 0){
            $colls = array_chunk($coll_ids, $num_items);

            for($i = 0; $i < count($colls); $i++){
                $end = $colls[$i][0];
                $start = $colls[$i][count($colls[$i])-1];

                $partitions[strval($i+1)] = $start.':'.$end;
            }
        }

        $data = ['partitions' => $partitions, 'count' => count($coll_ids)];

        return json_encode($data);
    }
}
