<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Controllers;
use App\Collection;
use App\User;
use App\Document;
use App\Remark;
use App\Log;
use App\Type;
use App\Location;
use App\Transmit;
use App\Notification;
use App\Tracking;
use App\Point;
use App\Position;
use App\Credential;
use App\Outsider;
use App\Group;
use App\Membership;
use App\Appeal;
use App\Background;
use App\Personnel;


use Auth;
use Carbon;

class LogsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function viewLogs(){
        $user = Auth::user()->id;
        $logs = Log::where('user_id','=',$user)->orderBy('id','desc')->get();  
      
        $logCount = count($logs); 

        $dates = array();
        foreach($logs as  $date){
            if(array_key_exists(Carbon\Carbon::parse($date->created_at)->format('Y-m-d'), $dates)){
                $dates[Carbon\Carbon::parse($date->created_at)->format('Y-m-d')][] = $date;
            }
            else{
                $dates[Carbon\Carbon::parse($date->created_at)->format('Y-m-d')] = array();
                $dates[Carbon\Carbon::parse($date->created_at)->format('Y-m-d')][] = $date;
            }
        }
        
        return view('admin.activity-log',compact('logs','dates','numItems','logCount'));
    }
    public function moreLogs(Request $request){
        $user = Auth::user()->id;
        $logs = Log::where('user_id','=',$user)->orderBy('id','desc')->get(); 
        $numItems =  $request->numItems + 10 ; 
        $logCount = count($logs); 

        $dates = array();
        foreach($logs->take($numItems) as  $date){
            if(array_key_exists(Carbon\Carbon::parse($date->created_at)->format('Y-m-d'), $dates)){
                $dates[Carbon\Carbon::parse($date->created_at)->format('Y-m-d')][] = $date;
            }
            else{
                $dates[Carbon\Carbon::parse($date->created_at)->format('Y-m-d')] = array();
                $dates[Carbon\Carbon::parse($date->created_at)->format('Y-m-d')][] = $date;
            }
        }
        return view('loaders.logs-loader',compact('logs','dates','numItems','logCount'));

    }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
