<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Credential;
use App\Document;
use Auth;
use App\Notification;
use App\Log;
use App\Point;
use Illuminate\Support\Facades\DB;
use App\User;

class CredentialsController extends Controller
{
    public function organizeCredentials(Request $request){
        function extractObjects($data){
            $objs = [];
            foreach ($data as $datum){
                $obj = Credential::findOrFail($datum->id);
                array_push($objs, $obj);
            }

            return $objs;
        }

        $user_id = $request->user_id;
        $sortBy = $request->sortBy;
        $sortDir = $request->sortDir;
        $start = $request->start;
        $end = $request->end;

        if($sortBy == 'category'){
            $credentials = DB::table('credentials')->join('points', 'credentials.point_id', '=', 'points.id')->where('credentials.user_id', $user_id)->where('credentials.id', '>=', $start)->where('credentials.id', '<=', $end)->orderBy('points.name', $sortDir)->select('credentials.id')->get();
            $credentials = extractObjects($credentials);
        }
        else{
            $credentials = Credential::where('user_id', $user_id)->where('id', '>=', $start)->where('id', '<=', $end)->orderBy($sortBy, $sortDir)->get();
        }

        return view('loaders.credentials-loader', compact('credentials'));
    }

  public function addCredentials(Request $request, $id) {

    $credential = new Credential;

    $credential->title = $request->cred_name;
    $credential->description = $request->cred_desc;
    $credential->point_id = $request->cred_category;
    $credential->updater = Auth::user()->id;
    $credential->user_id = $id;

    $documents = $request->support_docs;

    $credential->is_deleted ='0';
    $credential->save();
	foreach($documents as $document){

        $doc = new Document;
        $name = $document->getClientOriginalName();
        $extension = $document->getClientOriginalExtension();
        $size = $document->getSize();

        $fauxname = Str::random(10)."_".time().".".$extension;
        $destination = public_path('uploads');
        $document->move($destination, $fauxname);

        $doc->credential_id = $credential->id;
        $doc->file_name = $name;
        $doc->file_size = $size;
        $doc->file_extension = $extension;
        $doc->file_path = $destination."\\".$fauxname;
        $doc->is_credential_doc = '1';

        $doc->save();
    }
    //Save notification
   
        $notification = new Notification;
        $notification->from_user_id = Auth::user()->id;
        $notification->to_user_id = $credential->user_id;
        $notification->type = '3'; 
        $notification->object_id = $credential->id;
        if($notification->from_user_id != $notification->to_user_id){
            $notification->save();
        }
        
     
        //save logs
        $user = User::findOrFail($id); 
        $log = new Log;
        $log->user_id = Auth::user()->id;
        $log->content = 'You added a credential, '.$doc->file_name.' of '.$user->firsname.' '.$user->surname;
        $log->save();
    
    return redirect('/personnelUtil/'.$id.'/Credentials');
  }

  public function deleteCredentials(Request $request) {
    $id = $request->id;
    $credential = Credential::findOrFail($id);
    $credential->is_deleted = '1';
    $credential->save();




    //Save notification

    $notification = new Notification;
    $notification->from_user_id = Auth::user()->id;
    $notification->to_user_id = $credential->user_id;
    $notification->type = '3'; 
    $notification->object_id = $id;
    $notification->save();

    return $credential->id;
  }

  public function editCredentials(Request $request) {
    $id = $request->id;
    $credential = Credential::findOrFail($id);
    $credential->title = $request->title;
    $credential->description = $request->description;
    $credential->point_id = $request->category_point;

    $credential->update();
    $credential->category_name = $credential->point->name;


    return json_encode($credential);  
  }

  public function retrieveDocs(Request $request) {
    $icons = array("pdf" => "file pdf outline icon", "docx" => "file word outline icon", "xls" => "file excel outline icon", "xlsx" => "file excel outline icon", "zip" => "file archive outline icon", "rar" => "file archive outline icon", "pptx" => "file powerpoint outline icon", "ppt" => "file powerpoint outline icon", "txt" => "file alternate outline icon");
    $credential = Credential::findOrFail($request->id);
    $documents = $credential->documents;

    // foreach($documents as $doc){
    //     $doc->icon_ext = $icons[$doc->file_extension];
    // }

    return json_encode($documents);
    // return $documents;
  }

  public function listCredentials(Request $request) { 
    $credentials = User::all()->sortByDesc("created_at");
    return view('personnel.personnelUtil', compact('credentials'));
  }
  public function searchCredentials(Request $request){
    $searchBy = $request->searchValue;
    $id = $request->id;

    $credentials = DB::table('credentials')->join('users','users.id','=','credentials.user_id')->where('title','LIKE','%'.$searchBy.'%')->where('credentials.is_deleted','=','0')->where('credentials.user_id','=',$id)->get();

    return view('loaders.credentials-loader', compact('credentials'));
  }


}
