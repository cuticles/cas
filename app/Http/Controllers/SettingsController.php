<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use Carbon\Carbon;

class SettingsController extends Controller
{
    public function index() {
    	$user = Auth::user();
    	return view('settings.settings', compact('user'));
    }

    public function saveContractExpiry(Request $request) {
        $user = Auth::user();
    	$user->contractAlert = $request->contractAlert;

        $log = new Log;
        $log->user_id = Auth::user()->id;
        $log->content = 'You set the contract expiry to, '.$user->contractAlert.'.';
        $log->save();

    	$user->save();
        return redirect('/settings')->with('alert', 'Contract Expiration Date saved!');
    }

}
