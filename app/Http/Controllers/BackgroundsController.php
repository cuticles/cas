<?php

namespace App\Http\Controllers;
use App\Background;
use App\Appeal;
use App\Type;
use App\Log;
use App\Notification;
use Auth;

use Illuminate\Http\Request;

class BackgroundsController extends Controller
{
	public function deleteBackground(Request $request){
		$background = Background::findOrFail($request->id);
		$background->appeal->delete();

		//save logs
        $log = new Log;
        $log->user_id = Auth::user()->id;
        $log->content = 'You deleted the background, '.$background->name.'.';
        $log->save();

		$background->delete();

		return $request->id;
	}

    public function addBackground(Request $request){
    	$degLevels = ['1' => 'Associative', '2' => 'Bachelor\'s', '3' => 'Master\'s', '4' => 'Doctoral'];
    	$background = new Background;
    	$background->user_id = $request->user_id;
		$background->school = $request->school;
		$background->type_id = $request->type_id;
		$background->degree_level = $request->degree_level;
		$background->course = $request->course;
		$background->year_start = $request->year_start;
		$background->year_end = $request->year_end;
		$background->save();


		$notification = new Notification;
        $notification->from_user_id = Auth::user()->id;
        $notification->to_user_id = $background->user_id;
        $notification->type = '10'; 
        $notification->object_id = $background->id;
        $notification->save();

		//save logs
		$type = Type::findOrFail($request->type_id);
		$user = User::findOrFail($request->user_id);

        $log = new Log;
        $log->user_id = Auth::user()->id;
        $log->content = 'You added a background, '.$type->name.' of '.$user->firstname.' '.$user->surname.'.';
        $log->save();

    	$appeal = new Appeal;
    	$appeal->user_id = $request->supervisor;
    	$appeal->background_id = $background->id;
    	$appeal->save();

    	return view('loaders.background-loader', compact('background', 'degLevels'));
    }

    public function editBackground(Request $request){
    	$degLevels = ['1' => 'Associative', '2' => 'Bachelor\'s', '3' => 'Master\'s', '4' => 'Doctoral'];
    	$background = Background::findOrFail($request->id);
		$background->school = $request->school;
		$background->type_id = $request->type_id;

		if(Type::findOrFail($request->type_id)->name != 'tertiary'){
			$background->degree_level = null;
			$background->course = null;
		}
		else{
			$background->degree_level = $request->degree_level;
			$background->course = $request->course;
		}
		
		$background->year_start = $request->year_start;
		$background->year_end = $request->year_end;
		$background->update();

		$background->type_name = ucfirst(Type::findOrFail($request->type_id)->name);
		if($background->degree_level != null){
			$background->degree_level_name = $degLevels[$request->degree_level];
		}

		return json_encode($background);
    }
}
