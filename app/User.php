<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    public function incomingTransmits(){
        return $this->hasMany('App\Transmit', 'to_user_id')->where('is_forward', '=', '0')->orderBy('created_at', 'desc');
    }

    public function createdTransmits(){
        return $this->hasMany('App\Transmit', 'from_user_id')->where('is_forward', '=', '0')->orderBy('created_at', 'desc');
    }

    public function myForwards(){
        return $this->hasMany('App\Transmit', 'from_user_id')->where('is_forward', '=', '1')->where('from_outside', '=', '0')->where('trashed_by_sender', '=', '0')->orderBy('created_at', 'desc');
    }

    public function location(){
        return $this->belongsTo('App\Location', 'division');
    }

    public function receivedForwards(){
        return $this->hasMany('App\Transmit', 'to_user_id')->where('is_forward', '=', '1')->where('trashed_by_receiver', '=', '0')->orderBy('created_at', 'desc');
    }

    public function notifications(){
        return $this->hasMany('App\Notification', 'to_user_id')->orderBy('created_at','desc');
    }

    public function logs(){
        return $this->hasMany('App\Log', 'user_id');
    }

    public function credentials(){
        return $this->hasMany('App\Credential')->where('is_deleted', '=', '0')->orderBy('id', 'desc');
    }

    public function myGroups(){
        return $this->hasMany('App\Membership', 'user_id');
    }

    public function backgrounds(){
        return $this->hasMany('App\Background', 'user_id');
    }

    public function appeals(){
        return $this->hasMany('App\Appeal');
    }

    public function position(){
        return $this->belongsTo('App\Position', 'position_id');
    }

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'sex', 'birthdate', 'address', 'firstname', 'middlename', 'surname', 'mobile_num', 'telephone_num', 'email', 'password', 'salary', 'civil_status', 'division', 'designation', 'is_admin',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $table = 'users';
}
