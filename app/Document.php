<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
	public function collection(){
		return $this->belongsTo('App\Collection', 'collection_id');
	}

	public function credential(){
		return $this->belongsTo('App\Credential', 'credential_id');
	}

	public function status(){
		return $this->belongsTo('App\Type', 'status_id');
	}

	public function tracking(){
		return $this->belongsTo('App\Tracking', 'tracking_id');
	}

    protected $fillable = [
        'collection_id', 'credential_id', 'tracking_id', 'user_id', 'file_name', 'file_size', 'file_extension', 'file_path', 'status', 'is_credential_doc', 
    ];

}