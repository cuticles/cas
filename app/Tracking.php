<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tracking extends Model
{
    public function updater(){
    	return $this->belongsTo('App\User', 'user_updater');
    }

    public function collection(){
    	return $this->belongsTo('App\Collection');
    }

    public function type(){
    	return $this->belongsTo('App\Type', 'type_id');
    }

    public function status(){
        return $this->belongsTo('App\Type', 'status_id');
    }

    public function location(){
    	return $this->belongsTo('App\Location', 'location_id');
    }

    public function outsider(){
    	return $this->belongsTo('App\Outsider', 'outsider_id');
    }

    public function documents(){
        return $this->hasMany('App\Document');
    }

    protected $fillable = [
        'collection_id', 'user_updater', 'type', 'new_status', 'location', 'end_person', 'is_step'
    ];
}
