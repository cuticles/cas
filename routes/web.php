<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

/*Document Management System*/
Route::get('/home', 'HomeController@index');
Route::get('/test', 'HomeController@test');

Route::group(['middleware' => 'auth'], function () {
	Route::post('/create/coll', 'CollectionsController@saveCollection');
	Route::get('/sent', 'FilesController@sentFiles');
	Route::get('/sent/collections', 'FilesController@sentCollections');
	Route::get('/collection/{id}', 'CollectionsController@viewCollection');
	Route::post('/collection/update', 'TrackingsController@addTracking');
	Route::get('/received', 'FilesController@receivedFiles');
	Route::get('/received/collections', 'FilesController@receivedCollections');
	Route::post('/forward/collection', 'ForwardsController@forwardCollection');
	Route::get('/myforwards', 'ForwardsController@index');
	Route::get('/forwards/tome', 'ForwardsController@receivedForwards');
	Route::get('/collection/remarkview/{id}', 'CollectionsController@viewCollection');
	Route::post('/sentdocs/filter', 'FilesController@filterSentDocs');
	Route::post('/sentdocs-list/filter', 'FilesController@filterSentDocsList');
	Route::post('/receiveddocs/filter', 'FilesController@filterReceivedDocs');
	Route::post('/sentcollections/filter', 'FilesController@filterSentCollections');
	Route::post('/receivedcollections/filter', 'FilesController@filterReceivedCollections');
	Route::post('/group/addmembers', 'GroupsController@addmembers');
	Route::get('/dashboard', 'DashboardController@index');
	Route::get('/dashboard/{group_id}', 'DashboardController@dashboardGroup');
	Route::post('/doc/rename', 'FilesController@renameDoc');
	Route::post('/collection/edit', 'CollectionsController@editCollection');
	Route::post('/collection/trash', 'CollectionsController@trashCollection');
	Route::post('/doc/viewdetails', 'FilesController@viewDetails');
	Route::get('/test', 'FilesController@testing');
	Route::post('/group/removemember', 'GroupsController@removeMember');
	Route::post('/group/searchmember', 'GroupsController@searchMember');
	Route::post('/forward/checkgroup', 'ForwardsController@checkGroup');
	Route::post('/dashboard/search', 'DashboardController@searchItems');
	Route::post('/forwards/receivers', 'ForwardsController@viewReceivers');
	Route::post('/forwards/trash', 'ForwardsController@trashForward');
	Route::get('/group/{group_id}', 'GroupsController@goToGroup');
	Route::get('/groups', 'GroupsController@index');
	Route::post('/validate/files', 'FilesController@validateFileUploads');
	Route::post('/delete/doc/check', 'CollectionsController@deleteDocumentCheck');
	Route::post('/delete/doc', 'CollectionsController@deleteDocument');
	Route::post('/add/docs', 'CollectionsController@addDocuments');
	Route::post('/reset/pages/sent-docs', 'PaginationController@resetPagesSentDocs');
	Route::post('/reset/pages/sent-colls', 'PaginationController@resetPagesSentColls');
	Route::post('/reset/pages/received-docs', 'PaginationController@resetPagesReceivedDocs');
	Route::post('/reset/pages/received-colls', 'PaginationController@resetPagesReceivedColls');
	Route::post('/outsider/add', 'AddablesController@addOutsider');
	Route::post('/view/receivers/sent-colls', 'CollectionsController@viewReceivers');
	Route::post('/forward/organize', 'ForwardsController@organizeMyForwards');
	Route::post('/reset/pages/my-forwards', 'PaginationController@resetPagesMyForwards');
	Route::get('/addables/outsiders', 'AddablesController@getOutsiders');
	Route::get('/addables/locations', 'AddablesController@getLocations');
	Route::get('/addables/types', 'AddablesController@getTypes');
	Route::get('/addables/points', 'AddablesController@getPoints');
	Route::get('/addables/positions', 'AddablesController@getPositions');
	Route::post('/background/add', 'BackgroundsController@addBackground');
	Route::post('/background/edit', 'BackgroundsController@editBackground');
	Route::post('/background/delete', 'BackgroundsController@deleteBackground');
	Route::post('/credentials/organize', 'CredentialsController@organizeCredentials');
	Route::post('/reset/pages/credentials', 'PaginationController@resetCredentialPages');
	Route::post('/personnel/organize', 'UserController@organizePersonnel');
	Route::post('/reset/pages/users', 'PaginationController@resetPagesUsers');
	Route::post('/verify/appeal', 'AppealsController@respondToAppeal');
	Route::post('/create/group', 'GroupsController@createGroup');

	Route::get('/settings', 'SettingsController@index');
	Route::post('/saveContractExpiry', 'SettingsController@saveContractExpiry');


	Route::post('/received-forward/organize', 'ForwardsController@organizeReceivedForwards');
	Route::post('/reset/pages/received-forwards', 'PaginationController@resetReceivedForwards');
	Route::get('/manage/addables', 'AddablesController@index');
	Route::post('/organize/addables', 'AddablesController@organizeAddables');
	Route::post('/add/addables', 'AddablesController@addAddables');
	Route::post('/edit/addables', 'AddablesController@editAddables');
	Route::post('/delete/addables', 'AddablesController@deleteAddables');
	Route::post('/reset/pages/addables', 'PaginationController@resetPagesAddables');
	Route::post('/addables/search','AddablesController@searchAddables');
	Route::post('/credentials/search','CredentialsController@searchCredentials');



	Route::post('/sent/notif', 'NotifsController@saveNotif');
	Route::post('/notification/num', 'NotifsController@seenNotif');
	Route::post('/notification/more', 'NotifsController@moreNotif');
	Route::get('/notifications', 'NotifsController@viewNotif');
	Route::get('/logs', 'LogsController@viewLogs');
	Route::post('/logs/more', 'LogsController@moreLogs');
	Route::post('/resetUsers', 'UserController@resetUsers');

	/*Personnel Recording System*/

	Route::get('/managePersonnel', 'UserController@listPersonnel');
	Route::post('/addPersonnel', 'UserController@addPersonnel');
	Route::get('/addPersonnelForm', 'UserController@addPersonnelForm');
	Route::post('/deletePersonnel','UserController@deletePersonnel');
	Route::get('/editPersonnelForm/{id}', 'UserController@editPersonnelForm');
	Route::post('/editPersonnel/{id}', 'UserController@updatePersonnel');
	Route::get('/personnelUtil/{id}', 'UserController@viewPersonnel');
	Route::post('/addCredentials/{id}', 'CredentialsController@addCredentials');
	Route::post('/deleteCredential','CredentialsController@deleteCredentials');
	Route::post('/editCredential','CredentialsController@editCredentials');

	Route::post('/addDocument/{id}', 'DocumentController@addDocument');
	Route::post('/renameDocument', 'DocumentController@renameDocument');
	Route::post('/deleteDocument', 'DocumentController@deleteDocument');

	Route::post('/users/search','UserController@searchUser');

	Route::post('/retrieveDocs', 'CredentialsController@retrieveDocs');
	Route::get('/appeals', 'AppealsController@verifyRequest');


	/*for profile tabs; where credentials will show*/
	Route::get('/personnelUtil/{id}/Credentials', 'UserController@viewPersonnel');
	Route::get('/personnelUtil/{id}/Profile', 'UserController@viewPersonnel');


	/*Searching for Documents and Collection*/
	Route::post('/received/search', 'FilesController@searchReceivedDocs');
	Route::post('/sent/search', 'FilesController@searchSentDocs');
	Route::post('/received/collections/search', 'CollectionsController@searchReceivedCols');
	Route::post('/sent/collections/search', 'CollectionsController@searchSentCols');
	Route::post('/forwards/search','ForwardsController@searchForwards');
	Route::post('/forwardstome/search','ForwardsController@searchForwardsToMe');
	Route::get('/trash','CollectionsController@trashReceivedColls');
	Route::post('/collection/trash','CollectionsController@trashCollection');
	Route::post('/collection/trash/delete','CollectionsController@deleteTrashColls');
	Route::post('/collection/trash/restore','CollectionsController@restoreTrashColls');


	Route::get('/documents/pdf-document/{id}', 'FilesController@getDocument');
	Route::post('/download/doc', 'DownloadsController@download');
});