<div id="renameFileModal" class="ui small modal">
    <div class="header">
        Rename Document
    </div>
    <div class="content">
        <div class="ui fluid right labeled left icon input">
            <i class="pencil icon"></i>
            <input type="text" name="rename" id="fileNameInput" old-name="">
            <div class="ui label" id="fileExtension">
            </div>
        </div>
        <div id="errorRename" class="error-msg"></div>
    </div>
    <div class="actions">
        <button class="ui red right labeled icon button" id="renameBtn" document-id="">
            <i class="check icon"></i>
            Rename
        </button>
    </div>
</div>

<script type="text/javascript">
    function extractFileName(array){
        var fileName = '';
        for(var i = 0; i < (array.length - 1); i++){
            if(i === (array.length - 2))
                fileName += array[i];
            else
                fileName += array[i] + '.';
        }
        return fileName;
    }

    function getFileNameSubstr(string){
        if(string.length > 20){
            return string.substring(0, 20)+'...';
        }
        return string;
    }
    $(document).ready(function(){
        $('#renameBtn').click(function(){
            var docId = $('#renameBtn').attr('document-id');
            var extension = $('#fileExtension').html();
            var newName = $('#fileNameInput').val();
            var oldName = $('#fileNameInput').attr('old-name');

            if(oldName == (newName+extension)){
                $('#errorRename').html('You should enter a new name!');
                $('#errorRename').transition('bounce');
            }
            else if(newName == ''){
                $('#errorRename').html('Please enter a new name for this file.');
                $('#errorRename').transition('bounce');
            }
            else{
                $('#errorRename').empty();
                rename(docId, newName+extension);
            }
        });

        function rename(docId, newFileName){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "/doc/rename",
                data: {
                    id: docId,
                    newFileName: newFileName
                },
                success: function(name){
                    var renameItem = $('#renameItem'+docId);
                    renameItem.attr('file-name', '');
                    renameItem.attr('file-name', name);
                    $('#gridFileName'+docId).html(getFileNameSubstr(name));
                    $('#listFileName'+docId).html(name);
                    $('#renameFileModal').modal('hide');
                    $('#gridFileName'+docId).transition('pulse');
                    $('#gridFileName'+docId).transition('pulse');
                    initiateDynamicFunctions();
                },
                error: function(err){
                    console.log(err.responseText);
                }
            });
        }
    });
</script>