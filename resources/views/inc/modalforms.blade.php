<div class="ui modal" id="receiveModal">
    <div class="header">
        Receive Collection
    </div>
    <div class="scrolling content">
        <div class="ui inverted dimmer" id="receiveModalLoader">
            <div class="ui text loader">Adding outsider</div>
        </div>
        <div id="receiveFormCont">
            <form class="ui form" id="receiveCollForm" action="/create/coll" enctype="multipart/form-data" method="POST">
                {{ csrf_field() }}
                <input type="hidden" name="create_coll_type" value="receive">
                <div class="field">
                    <label>Outside Sender</label>
                    <div class="ui stackable grid">
                        <div class="fifteen wide column">
                            <div id="outsiderSelect" class="ui fluid search selection dropdown receive-dropdown">
                                <input type="hidden" name="outsider_id" id="outsiderInput">
                                <i class="dropdown icon"></i>
                                <div class="default text">Select Outsider</div>
                                <div class="menu" id="receiveOutsidersMenu">
                                    <div class="disabled item" data-value="">Select Outsider</div>
                                    @foreach($outsiders as $outsider)
                                        <div class="item" data-value="{{ $outsider->id }}">
                                            {{ $outsider->name }}
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="one wide column">
                            <button type="button" class="circular ui red icon button" id="showAddOutCont" data-tooltip="Add new outsiders" data-position="bottom right">
                                <i class="plus icon"></i>
                            </button>
                        </div>
                    </div>
                    <div class="status-msg status-check-receive" id="outsiderSelectMsgReceived" status="wrong">
                    </div>
                </div>
                <div class="field">
                    <label>Recipient(s)</label>
                    <div class="ui grid">
                        <div class="four wide column">
                            <div id="receiveRecipientType" class="ui fluid selection dropdown receive-dropdown">
                                <input type="hidden" name="receive_by_method" id="receiveRecipientTypeInput" value="many">
                                <i class="dropdown icon"></i>
                                <div class="default text">Many</div>
                                <div class="menu">
                                    <div class="disabled item" data-value="">Receive by</div>
                                    <div class="item" data-value="many">Many</div>
                                    <div class="item" data-value="group">Group</div>
                                    <div class="item" data-value="all">All</div>
                                </div>
                            </div>
                        </div>
                        <div class="twelve wide column">
                            <div id="receiveManyContainer">
                                <div id="receiveMany" class="ui fluid multiple search selection dropdown receive-dropdown">
                                    <input type="hidden" name="receivers" id="receiveManyInput">
                                    <i class="dropdown icon"></i>
                                    <div class="default text">Select recipient</div>
                                    <div class="menu">
                                        @foreach($users as $user)
                                            <div class="item" data-value="{{ $user->id }}">
                                                <img class="ui avatar image" src="{{ asset($user->profile_pic) }}">
                                                {{ $user->firstname." ".$user->middlename." ".$user->surname }}
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="hidden" id="receiveGroupContainer">
                                <div id="receiveGroup" class="hidden ui fluid search selection dropdown receive-dropdown">
                                    <input type="hidden" name="group_id" id="receiveGroupInput">
                                    <i class="dropdown icon"></i>
                                    <div class="default text">Select Group</div>
                                    <div class="menu">
                                        @foreach($groups as $group)
                                            <div class="item" data-value="{{ $group->id }}">
                                                {{ $group->name }}
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="status-msg status-check-receive" id="receiveRecipientsMsg" status="wrong">
                    </div>
                </div>
                <div class="field">
                    <label>Title</label>
                    <input type="text" name="title" placeholder="Enter title" id="receiveTitleInput">
                    <div class="status-msg status-check-receive" id="receiveTitlesMsg" status="wrong">
                    </div>
                </div>
                <div class="field">
                    <label>Type</label>
                    <div class="ui fluid selection dropdown receive-dropdown">
                        <input type="hidden" name="type_id" id="receiveTypeInput">
                        <i class="dropdown icon"></i>
                        <div class="default text">Select collection type</div>
                        <div class="menu">
                            <div class="disabled item" data-value="" >Select collection type</div>
                            @foreach($types as $type)
                                <div class="item" data-value="{{ $type->id }}">
                                    {{ ucfirst($type->name) }}
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="status-msg status-check-receive" id="receiveTypeMsg" status="wrong">
                    </div>
                </div>
                <div class="field">
                    <label>Description</label>
                    <textarea rows="2" form="receiveCollForm" name="description" id="descriptionInput" placeholder="Enter description"></textarea>
                </div>
                <div class="field" data-tooltip="Uploaded documents should not exceed 1 MB and should belong to document file types (e.g. pdf, docx)" data-position="top left">
                    <label>Upload files</label>
                    <div class="ui labeled left icon input">
                        <i class="paperclip icon"></i>
                        <input id="docFiles" type="file" name="documents[]" placeholder="Attach files" multiple="">
                    </div>
                </div>
                <div class="field">
                    <label>Tags</label>
                    <div class="ui right labeled left icon input">
                        <i class="tags icon"></i>
                        <input type="text" name="tags" placeholder="Enter tags" id="tagsInput">
                        <div class="ui tag label">
                            Add tags
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="hidden" id="receiveAddOutsiderCont">
            <div class="hidden ui segment" id="receiveSuccessMsg">
                <i class="check circle red big icon"></i>
                Outsider successfully added! It's already in the Outside Sender options.
            </div>
            <div class="ui form">
                <div class="ui stackable grid">
                    <div class="eleven wide column">
                        <div class="field">
                            <div class="ui fluid input">
                                <input type="text" class="outsider-name" name="new_outsider" id="outsiderName" placeholder="Enter outsider's name" mode="receive">
                            </div>
                            <div class="status-msg status-check-outsider-name-receive status-check-include-new-loc-receive" id="outsiderName-receive" status="wrong"></div>
                        </div>
                        <div class="field">
                            <div class="ui fluid search selection dropdown receive-dropdown">
                                <input type="hidden" name="new_outsider" class="outsider-location" id="outsiderLocation" mode="receive">
                                <i class="dropdown icon"></i>
                                <div class="default text" id="receiveDefaultText">Select outsider's location</div>
                                <div class="menu">
                                    <div class="disabled item" data-value="">Select outsider's location</div>
                                    <div class="item" data-value="others">Others, please specify</div>
                                    @foreach($locs as $loc)
                                        <div class="item" data-value="{{ $loc->id }}">
                                            {{ $loc->name }}
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="status-msg status-check-outsider-name-receive status-check-include-new-loc-receive" id="outsiderLocation-receive" status="wrong"></div>
                        </div>
                        <div class="hidden field" id="receiveNewLocField">
                            <div class="ui fluid input">
                                <input type="text" name="new_outsider" class="outsider-new-location" id="receiveNewLocation" placeholder="Enter new location" mode="receive">
                            </div>
                            <div class="status-msg status-check-outsider-name-receive status-check-include-new-loc-receive" id="outsiderNewLoc-receive" status="wrong"></div>
                        </div>
                    </div>
                    <div class="four wide column">
                        <div class="field">
                            <button type="button" class="ui disabled fluid red button" id="addOutsider">
                                <i class="plus icon"></i>
                                Add outsider
                            </button>
                        </div>
                        
                        <div class="field">
                            <button type="button" class="ui fluid button" id="cancelAddOutsider"> 
                                <i class="left arrow icon"></i>
                                Go Back
                            </button>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
    </div>
    <div class="actions">
        <button class="ui button deny">Cancel</button>
        <button type="submit" form="receiveCollForm" class="ui approve red right labeled icon button" id="receiveCollBtn">
            <i class="paper plane outline icon"></i>
            Receive
        </button>
    </div>
</div>

<div class="ui modal" id="sendModal">
    <div class="header">
        Send Collection
    </div>
    <div class="scrolling content">
        <div class="ui inverted dimmer" id="sendModalLoader">
            <div class="ui text loader">Adding outsider</div>
        </div>
        <div id="sendFormCont">
            <form class="ui form" id="sendDocForm" action="/create/coll" enctype="multipart/form-data" method="POST">
                {{ csrf_field() }}
                <input type="hidden" name="create_coll_type" value="send">
                <div class="field" id="sendFromField">
                    <label>Send to</label>
                    <div id="sendToDropdown" class="ui fluid selection dropdown receive-dropdown">
                        <input type="hidden" name="send_to" id="sendToInput" value="inside">
                        <i class="dropdown icon"></i>
                        <div class="default text">Users within system</div>
                        <div class="menu">
                            <div class="disabled item" data-value="">Send to</div>
                            <div class="item" data-value="inside">User(s) within system</div>
                            <div class="item" data-value="outside">Outsider(s)</div>
                        </div>
                    </div>
                </div>
                <div class="disabled field" id="sendOutsideField">
                    <label>Receiver from Outside</label>
                    <div class="ui stackable grid">
                        <div class="fifteen wide column">
                            <div id="sendOutsideDrpdwn" class="ui fluid search selection dropdown receive-dropdown">
                                <input type="hidden" name="outsider_ids" id="sendOutInput">
                                <i class="dropdown icon"></i>
                                <div class="default text">Select Outsider</div>
                                <div class="menu" id="sendOutsidersMenu">
                                    <div class="disabled item" data-value="">Select Outsider</div>
                                    @foreach($outsiders as $outsider)
                                        <div class="item" data-value="{{ $outsider->id }}">
                                            {{ $outsider->name }}
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="one wide column">
                            <button type="button" class="circular ui red icon button" id="sendShowAddOutCont" data-tooltip="Add new outsiders" data-position="bottom right">
                                <i class="plus icon"></i>
                            </button>
                        </div>
                    </div>
                    <div class="status-msg status-check-inc-outsider-send" id="outsiderSelectMsg" status="wrong">
                    </div>
                </div>
                <div class="field" id="sendInsideField">
                    <label>Recipient</label>
                    <div class="ui grid">
                        <div class="four wide column">
                            <div id="recipientType" class="ui fluid selection dropdown receive-dropdown">
                                <input type="hidden" name="receive_by_method" id="recipientTypeInput" value="many">
                                <i class="dropdown icon"></i>
                                <div class="default text">Many</div>
                                <div class="menu">
                                    <div class="disabled item" data-value="">Receive by</div>
                                    <div class="item" data-value="many">Many</div>
                                    <div class="item" data-value="group">Group</div>
                                    <div class="item" data-value="all">All</div>
                                </div>
                            </div>
                        </div>
                        <div class="twelve wide column">
                            <div id="manyContainer">
                                <div id="sendMany" class="ui fluid multiple search selection dropdown receive-dropdown">
                                    <input type="hidden" name="receivers" id="manyInput">
                                    <i class="dropdown icon"></i>
                                    <div class="default text">Select recipient</div>
                                    <div class="menu">
                                        @foreach($users as $user)
                                            <div class="item" data-value="{{ $user->id }}">
                                                <img class="ui avatar image" src="{{ asset($user->profile_pic) }}">
                                                {{ $user->firstname." ".$user->middlename." ".$user->surname }}
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="hidden" id="groupContainer">
                                <div id="sendGroup" class="hidden ui fluid search selection dropdown receive-dropdown">
                                    <input type="hidden" name="group_id" id="groupInput">
                                    <i class="dropdown icon"></i>
                                    <div class="default text">Select Group</div>
                                    <div class="menu">
                                        @foreach($groups as $group)
                                            <div class="item" data-value="{{ $group->id }}">
                                                {{ $group->name }}
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="status-msg status-check-send status-check-inc-outsider-send" id="recipientsMsg" status="wrong">
                    </div>
                </div>
                <div class="field">
                    <label>Title</label>
                    <input type="text" name="title" placeholder="Enter title" id="titleInput">
                    <div class="status-msg status-check-send status-check-inc-outsider-send" id="titlesMsg" status="wrong">
                    </div>
                </div>
                <div class="field">
                    <label>Type</label>
                    <div class="ui fluid selection dropdown receive-dropdown">
                        <input id="typeInput" type="hidden" name="type_id">
                        <i class="dropdown icon"></i>
                        <div class="default text">Select collection type</div>
                        <div class="menu">
                            <div class="disabled item" data-value="" >Select collection type</div>
                            @foreach($types as $type)
                                <div class="item" data-value="{{ $type->id }}">
                                    {{ ucfirst($type->name) }}
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div id="typeMsg" class="status-msg status-check-send status-check-inc-outsider-send" status="wrong">
                    </div>
                </div>
                <div class="field">
                    <label>Description</label>
                    <textarea rows="2" form="sendDocForm" name="description" id="descriptionInput" placeholder="Enter description"></textarea>
                </div>
                <div class="field" data-tooltip="Uploaded documents should not exceed 1 MB and should belong to document file types (e.g. pdf, docx)" data-position="top left">
                    <label>Upload files</label>
                    <div class="ui labeled left icon input">
                        <i class="paperclip icon"></i>
                        <input id="docFiles" type="file" name="documents[]" placeholder="Attach files" multiple="" required="">
                    </div>
                </div>
                <div class="field">
                    <label>Tags</label>
                    <div class="ui right labeled left icon input">
                        <i class="tags icon"></i>
                        <input type="text" name="tags" placeholder="Enter tags" id="tagsInput">
                        <div class="ui tag label">
                            Add tags
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="hidden" id="sendAddOutsiderCont">
            <div class="hidden ui segment" id="sendSuccessMsg">
                <i class="check circle red big icon"></i>
                Outsider successfully added! It's already in the Receiver from Outside options.
            </div>
            <div class="ui form">
                <div class="ui stackable grid">
                    <div class="eleven wide column">
                        <div class="field">
                            <div class="ui fluid input">
                                <input type="text" name="new_outsider" class="outsider-name" id="sendOutsiderName" placeholder="Enter outsider's name" mode="send">
                            </div>
                            <div class="status-msg status-check-outsider-name-send status-check-include-new-loc-send" id="outsiderName-send" status="wrong"></div>
                        </div>
                        <div class="field">
                            <div class="ui fluid search selection dropdown send-dropdown">
                                <input type="hidden" class="outsider-location" name="new_outsider" id="sendOutsiderLocation" mode="send">
                                <i class="dropdown icon"></i>
                                <div class="default text" id="sendDefaultText">Select outsider's location</div>
                                <div class="menu">
                                    <div class="disabled item" data-value="">Select outsider's location</div>
                                    <div class="item" data-value="others">Others, please specify</div>
                                    @foreach($locs as $loc)
                                        <div class="item" data-value="{{ $loc->id }}">
                                            {{ $loc->name }}
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="status-msg status-check-outsider-name-send status-check-include-new-loc-send" id="outsiderLocation-send" status="wrong"></div>
                        </div>
                        <div class="hidden field" id="sendNewLocField">
                            <div class="ui fluid input">
                                <input type="text" name="new_outsider" class="outsider-new-location" id="sendNewLocation" placeholder="Enter new location" mode="send">
                            </div>
                            <div class="status-msg status-check-outsider-name-send status-check-include-new-loc-send" id="outsiderNewLoc-send" status="wrong"></div>
                        </div>
                    </div>
                    <div class="four wide column">
                        <div class="field">
                            <button type="button" class="ui disabled fluid red button" id="sendAddOutsider">
                                <i class="plus icon"></i>
                                Add outsider
                            </button>
                        </div>
                        
                        <div class="field">
                            <button type="button" class="ui fluid button" id="sendCancelAddOutsider"> 
                                <i class="left arrow icon"></i>
                                Go Back
                            </button>
                        </div> 
                    </div>
                </div>
            </div>  
        </div>
    </div>
    <div class="actions">
        <button type="submit" form="sendDocForm" class="ui disabled red right labeled icon button" id="sendCollBtn">
            <i class="paper plane outline icon"></i>
            Send
        </button>
        <button class="ui button deny">Cancel</button>
    </div>
</div>

<script type="text/javascript" src="{{ asset('js/validations/send-receive-forms.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(){
        function addOutsider(name, loc, newLoc, type){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "/outsider/add",
                data: {
                    name: name,
                    loc: loc,
                    newLoc: newLoc
                },
                success: function(outsider){
                    outsider = JSON.parse(outsider);
                    $('#sendOutsidersMenu').append(
                        $('<div>').addClass('item').attr('data-value', outsider.id).append(outsider.name).append(
                            $('<span>').addClass('semibold new-out').html('(NEW)')
                        )
                    );
                    $('#receiveOutsidersMenu').append(
                        $('<div>').addClass('item').attr('data-value', outsider.id).append(outsider.name).append(
                            $('<span>').addClass('semibold new-out').html('(NEW)')
                        )
                    );

                    if(type == 'send'){
                        $('#sendOutsiderName').val('');
                        $('#sendOutsiderLocation').val('');
                        $('#sendNewLocation').val('');
                        $('#sendDefaultText').html('Select outsider\'s location');
                        $('#sendSuccessMsg').removeClass('hidden');
                        $('#sendModalLoader').removeClass('active');
                    }
                    else{
                        $('#outsiderName').val('');
                        $('#outsiderLocation').val('');
                        $('#receiveNewLocation').val('');
                        $('#receiveDefaultText').html('Select outsider\'s location');
                        $('#receiveSuccessMsg').removeClass('hidden');
                        $('#receiveModalLoader').removeClass('active');
                    }
                    
                },
                error: function(err){
                    console.log(err.responseText);
                }
            });
        }
        $('.send-dropdown').dropdown();
        $('.receive-dropdown').dropdown();
        //If add outsider button is clicked for Send Collection Form
        $('#sendAddOutsider').click(function(){
            var name = $('#sendOutsiderName').val();
            var loc = $('#sendOutsiderLocation').val();
            var newLoc = $('#sendNewLocation').val();

            $('#sendModalLoader').addClass('active');

            addOutsider(name, loc, newLoc, 'send');
        });

        //If add outsider button is clicked for Receive Collection Form
        $('#addOutsider').click(function(){
            var name = $('#outsiderName').val();
            var loc = $('#outsiderLocation').val();
            var newLoc = $('#receiveNewLocation').val();

            $('#receiveModalLoader').addClass('active');

            addOutsider(name, loc, newLoc, 'receive');
        });

        $('#sendOutsiderLocation').change(function(){
            if($(this).val() == 'others'){
                $('#sendNewLocField').removeClass('hidden');
            }
            else{
                $('#sendNewLocField').addClass('hidden');
            }

            var mode = $(this).attr('mode');
            console.log(mode);
            var msgElement = $('#outsiderLocation-'+mode);
            var status = '';
            var message = '';

            if($(this).val() == ''){
                status = 'wrong';
                message = 'This is a required field. You need to choose a location.';
            }
            else{
                status = 'okay';
                message = "This is a valid location.";
            }
            
            assignValidationMessageOutsider(msgElement, message, status, mode);
        });

        $('#outsiderLocation').change(function(){
            if($(this).val() == 'others'){
                $('#receiveNewLocField').removeClass('hidden');
            }
            else{
                $('#receiveNewLocField').addClass('hidden');
            }

            var mode = $(this).attr('mode');
            var msgElement = $('#outsiderLocation-'+mode);
            var status = '';
            var message = '';

            if($(this).val() == ''){
                status = 'wrong';
                message = 'This is a required field. You need to choose a location.';
            }
            else{
                status = 'okay';
                message = "This is a valid location.";
            }
            
            assignValidationMessageOutsider(msgElement, message, status, mode);
        });

        $('#showAddOutCont').click(function(){
            $('#receiveFormCont').transition('slide left');
            $('#receiveAddOutsiderCont').removeClass('hidden');
        });

        $('#cancelAddOutsider').click(function(){
            $('#receiveFormCont').transition('slide left');
            $('#receiveAddOutsiderCont').addClass('hidden');
        });

        //Send Form Add Outsider
        $('#sendShowAddOutCont').click(function(){
            $('#sendFormCont').transition('slide left');
            $('#sendAddOutsiderCont').removeClass('hidden');
        });

        $('#sendCancelAddOutsider').click(function(){
            $('#sendFormCont').transition('slide left');
            $('#sendAddOutsiderCont').addClass('hidden');
        });

        $('#sendOutsideDrpdwn').dropdown({
            clearable: true
        });

        $('#sendToInput').change(function(){
            if($(this).val() == 'inside'){
                $('#sendInsideField').removeClass('disabled');
                $('#sendOutsideField').addClass('disabled');
            }
            else{
                $('#sendInsideField').addClass('disabled');
                $('#sendOutsideField').removeClass('disabled');
            }
            validateSendForm();
        });

        $('#confirmAddOutsider').click(function(){
            $(this).addClass('loading');
        });
    });
</script>