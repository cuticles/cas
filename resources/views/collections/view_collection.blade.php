@extends('layouts.admin')

@section('content')
@if($collection != null)
@if(in_array(Auth::user()->id, $authorized))
	<style type="text/css">
		body{
			background-color: #F5F5F5 !important;
		}
		.top-segment{
			background-image: url('{{ asset('images/office_bg_1.png') }}') !important;
			background-size: cover !important;
		}
	</style>
	@include('inc.rename-form')
	<!-- Edit Collection Modal -->
	<div class="ui modal" id="editCollectionModal">
        <div class="header">
            Edit: <span id="editCollectionTitle"></span>
        </div>
        <div class="content">
            <form class="ui form" id="editCollForm" action="/send/doc" enctype="multipart/form-data" method="POST">
                {{ csrf_field() }}
                <div class="field">
                    <label>Title</label>
                    <input type="text" name="title" id="editCollTitle" placeholder="Enter title" old-value="">
                    <div class="status-msg status-check" id="titleMsg" status="wrong"></div>
                </div>
                <div class="field">
                    <label>Type</label>
                    <div class="ui fluid selection dropdown receive-dropdown">
                        <input id="editCollType" type="hidden" name="type" old-value="">
                        <i class="dropdown icon"></i>
                        <div class="default text" id="collTypeText">Select collection type</div>
                        <div class="menu">
                            <div class="disabled item" data-value="" >Select collection type</div>
                            @foreach($types as $type)
                                <div class="item" data-value="{{ $type->id }}">
                                    {{ ucfirst($type->name) }}
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="status-msg status-check" id="editTypeMsg" status="wrong"></div>
                </div>
                <div class="field">
                    <label>Description</label>
                    <textarea rows="2" form="sendDocForm" name="description" placeholder="Enter description" id="editCollDescription" old-value=""></textarea>
                    <div class="status-msg status-check" id="descriptionMsgEdit" status="okay" changed="false"></div>
                </div>
                <div class="field">
                    <label>Tags</label>
                    <div class="ui right labeled left icon input">
                        <i class="tags icon"></i>
                        <input type="text" name="tags" placeholder="Enter tags" id="editCollTags" old-value="">
                        <div class="ui tag label">
                            Add tags
                        </div>
                    </div>
                    <div class="status-msg status-check" id="tagsMsgEdit" status="okay" changed="false"></div>
                </div>
            </form>
			<div class="error-msg" id="changesStatus"></div>
        </div>
        <div class="actions">
        	<button class="ui red right labeled icon button" coll-id="" id="editCollBtn">
                <i class="check icon"></i>
                Save Changes
            </button>
            <button class="ui button deny" id="cancelEditColl">Cancel</button>
        </div>
    </div> <!-- End Edit Collection Modal -->

	<div class="ui modal" id="addDocsModal">
		<div class="header">
			Add Documents
		</div>
		<div class="content">
			<div id="statusListCont">
				<div class="ui vertically divided stackable grid">
					@foreach($docs_segments as $status => $docs)
						<div class="row">
							<div class="three wide centered column">
								<div class="ui red statistic">
								    <div class="value">
								    	{{ count($docs) }}
								    </div>
									<div class="label">
								    	{{ $status }}
								    </div>
								</div>
							</div>
							<div class="ten wide column">
								<div class="ui relaxed divided list">
									@foreach($docs as $doc)
										<div class="item">
											<i class="file-icon {{ $icons[$doc->file_extension] }}"></i>
											<div class="content">
												<div class="header">{{ $doc->file_name }}</div>
											</div>
											<div class="description">
												{{ $doc->sizeInBytes }}
											</div>
										</div>
									@endforeach
								</div>
							</div>
							<div class="three wide column">
								<button class="ui right labeled icon button add-docs-btn" status="{{ $status }}">
					                <i class="plus icon"></i>
					                Add
					            </button>
							</div>
						</div>
					@endforeach
				</div>
			</div>
			<div class="hidden file-upload-form" id="addDocsFormCont">
				<button id="backBtn" class="ui red right labeled medium icon button">
	                <i class="arrow left icon icon"></i>
	                Back
	            </button>
				<form class="ui form margin-top-20" method="POST" action="/add/docs" enctype="multipart/form-data" id="addDocsForm">
					{{ csrf_field() }}
					<input type="hidden" name="coll_id" value="{{ $collection->id }}">
					<input type="hidden" name="status" value="" id="addDocStatusInput">
					<div class="field">
						<label>Upload documents</label>
						<div class="ui action fluid input">
							<input type="file" name="docs[]" required="" multiple="">
							<button type="submit" form="addDocsForm" class="ui red button">Upload</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

	<div class="ui fullscreen modal" id="viewDocModal">
        <i class="close icon"></i>
        <div class="content">
            <embed src="" style="width:1200px; height:550px;" id="viewDocEmbed">
        </div>
    </div>

	<div class="ui small modal" id="deleteDocModal" case="">
		<div class="header">
			Delete document
		</div>
		<div class="content" id="deleteDocContent">
			
		</div>
		<form method="POST" action="/delete/doc" id="deleteDocForm">
			{{ csrf_field() }}
			<input type="hidden" name="coll_id" value="{{ $collection->id }}">
			<input type="hidden" name="id" value="" id="idInput">
			<input type="hidden" name="case" value="" id="caseInput">
		</form>
		<div class="actions">
			<button class="ui button deny">Cancel</button>
            <button type="submit" form="deleteDocForm" id="deleteDocBtn" class="ui red right labeled icon button" doc-id="">
                <i class="trash icon"></i>
                Delete
            </button>
		</div>
	</div>

	<div id="trackingModal" class="ui scrolling small modal">
		<div class="header">
			Tracking
		</div>
		<div class="content">
			<div class="ui grid tracking-grid">
				@foreach($collection->trackings as $tracking)
					<div class="row custom-row">
						<div class="circle-tracking"></div>
						<div class="eight wide column custom-column">
							{{ Carbon\Carbon::parse($tracking->created_at)->format('F j, Y') }}
							<div class="tracking-time">
								{{ Carbon\Carbon::parse($tracking->created_at)->format('h:i A') }}
							</div>
						</div>
						<div class="eight wide column bordered-column custom-column">
							@if($tracking->type->name == 'attachment')
								<div class="small-text grey-color">
									<i class="paperclip icon"></i> 
									Additional document version
								</div>
								<div class="tracking-content">
									<span class="semibold">
										{{ $tracking->updater->firstname." ".$tracking->updater->middlename." ".$tracking->updater->surname }}
									</span> attached a file for <span class="semibold">{{ $tracking->status->name }}</span>.
								</div>
							@elseif($tracking->type->name == 'update')
								<div class="small-text grey-color">
									<i class="building outline icon"></i>
									From: 
									<span class="semibold">
										@if($tracking->from_outside == '1')
											{{ $tracking->outsider->location->name }}
										@else	
											{{ $tracking->updater->location->name }}
										@endif
									</span>
								</div>
								<div class="tracking-content">
									<span class="semibold">
										@if($tracking->from_outside == '1')
											{{ $tracking->outsider->name }}
										@else
											{{ $tracking->updater->firstname." ".$tracking->updater->middlename." ".$tracking->updater->surname }}
										@endif
									</span> 
										marked your request as 
										<span class="semibold">{{ $tracking->status->name }}</span>.
									@if($tracking->from_outside == '1')
										<div>
											<span class="small-text grey-color">
												Managed by <span class="semibold">{{ $tracking->updater->firstname." ".$tracking->updater->middlename." ".$tracking->updater->surname }}</span>
											</span>
										</div>
									@endif
								</div>
								<div class="small-text grey-color">
									<i class="map marker alternate icon"></i>
									Current location: 
									<span class="semibold">
										{{ $tracking->location->name }}
									</span>
								</div>
							@elseif($tracking->type->name == 'initial')
								<div class="small-text grey-color">
									<i class="building outline icon"></i>
									From: 
									<span class="semibold">
										@if($tracking->from_outside == '1')
											{{ $tracking->outsider->location->name }}
										@else	
											{{ $tracking->updater->location->name }}
										@endif
									</span>
								</div>
								<div class="tracking-content">
									<span class="semibold">
										@if($tracking->from_outside == '1')
											{{ $tracking->outsider->name }}
										@else
											{{ $tracking->updater->firstname." ".$tracking->updater->middlename." ".$tracking->updater->surname }}
										@endif
									</span> 
										sent this collection to 
										<span class="semibold">
											{{ count($tracking->collection->transmits) }} 
											@if(count($tracking->collection->transmits) == 1)
												user
											@else
												users
											@endif
										</span>.
									@if($tracking->from_outside == '1')
										<div>
											<span class="small-text grey-color">
												Managed by <span class="semibold">{{ $tracking->updater->firstname." ".$tracking->updater->middlename." ".$tracking->updater->surname }}</span>
											</span>
										</div>
									@endif
								</div>
								<div class="small-text grey-color">
									<i class="map marker alternate icon"></i>
									Current location: 
									<span class="semibold">
										{{ $tracking->location->name }}
									</span>
								</div>
							@endif
						</div>
					</div>
				@endforeach
			</div>
		</div>
	</div>

	<div id="forwardModal" class="ui small modal">
		<div class="header">
			Forward to
		</div>
		<div class="content">
			<form id="forwardForm" class="ui form" action="/forward/collection" method="POST">
				{{ csrf_field() }}
				<input type="hidden" name="collection_id" value="{{ $collection->id }}" id="collId">
				<div class="field">
                    <label>Forward to</label>
                    <div id="forwardMethodDropdown" class="ui fluid selection dropdown">
                        <input type="hidden" name="forward_method" value="many" id="forwardMethod">
                        <i class="dropdown icon"></i>
                        <div class="default text">Many</div>
                        <div class="menu">
                            <div class="item" data-value="many">Many</div>
                            <div class="item" data-value="one">One</div>
                            <div class="item" data-value="group">Group</div>
                        </div>
                    </div>
                </div>
                <div class="field hidden" id="forwardToGroup">
                    <label>Group</label>
                    <div id="forwardGroup" class="ui fluid search selection dropdown">
                        <input type="hidden" name="group_id" id="forwardGroupInput">
                        <i class="dropdown icon"></i>
                        <div class="default text">Select group</div>
                        <div class="menu">
                        	@foreach($groups as $group)
                            	<div class="item" data-value="{{ $group->id }}">{{ $group->name }}</div>
                            @endforeach
                        </div>
                    </div>
                    <div class="error-msg" id="forwardGroupMessage" status="wrong"></div>
                </div>
                <div class="field" id="forwardToMany">
                    <label>Recipients</label>
                    <div id="many-search" class="ui fluid multiple search selection dropdown">
                        <input type="hidden" name="recipients" id="forwardManyInput">
                        <i class="dropdown icon"></i>
                        <div class="default text" id="defTextForwardUsers">Select recipient</div>
                        <div class="menu">
                            @foreach($forward_users as $user)
                                <div class="item" data-value="{{ $user->id }}">
                                	<img class="ui avatar image" src="{{ asset($user->profile_pic) }}">
                                    {{ $user->firstname." ".$user->middlename." ".$user->surname }}
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="error-msg" id="forwardManyMsg" status="wrong"></div>
                </div>
                <div class="field hidden" id="forwardToOne">
                    <label>Recipient</label>
                    <div id="one-search" class="ui fluid search selection dropdown">
                        <input type="hidden" name="recipient" id="forwardOneInput">
                        <i class="dropdown icon"></i>
                        <div class="default text">Select recipient</div>
                        <div class="menu">
                            @foreach($forward_users as $user)
                                <div class="item" data-value="{{ $user->id }}">
                                	<img class="ui avatar image" src="{{ asset($user->profile_pic) }}">
                                    {{ $user->firstname." ".$user->middlename." ".$user->surname }}
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="error-msg" id="forwardOneMsg" status="wrong"></div>
                </div>
			</form>
		</div>
		<div class="actions">
			<button class="ui button deny">Cancel</button>
            <button type="submit" form="forwardForm" id="forwardBtn" class="ui disabled approve red right labeled icon button">
                <i class="paper plane outline icon"></i>
                Forward
            </button>
		</div>
	</div>

	<div id="updateModal" class="ui long small modal">
		<div class="header">
			Update Status of Collection
		</div>
		<div class="content">
			<div class="ui inverted dimmer" id="modalLoader">
	            <div class="ui text loader">Adding outsider</div>
	        </div>
			<div id="updateFormCont">
				<form id="updateStatusForm" class="ui form" action="/collection/update" enctype="multipart/form-data" method="POST">
					{{ csrf_field() }}
					<input type="hidden" name="collection_id" value="{{ $collection->id }}">
					<div class="field">
	                    <label>Mark as</label>
	                    <div class="ui fluid selection dropdown update-dropdown">
	                        <input type="hidden" name="new_status" value="" id="newStatusInput">
	                        <i class="dropdown icon"></i>
	                        <div class="default text" id="defaultTextNewStatus">Select status</div>
	                        <div class="menu">
	                        	@if(!in_array('received', $trackings_status_names))
			                        @foreach($statuses as $status)
			                        	@if(!in_array($status->name, $trackings_status_names) && $status->name != 'received')
				                        	<div class="item disabled" data-value="{{ $status->id }}">
				                        		{{ ucfirst($status->name) }}
				                        	</div>
				                        @elseif($status->name == 'received')
				                        	<div class="item" data-value="{{ $status->id }}">
				                        		{{ ucfirst($status->name) }}
				                        	</div>
			                        	@endif
			                        @endforeach
			                    @else
			                    	@foreach($statuses as $status)
			                        	@if(!in_array($status->name, $trackings_status_names))
				                        	<div class="item" data-value="{{ $status->id }}">
				                        		{{ ucfirst($status->name) }}
				                        	</div>
			                        	@endif
			                        @endforeach
		                        @endif
	                        </div>
	                    </div>
	                    <div class="status-msg status-check-update status-inc-outsiders" id="statusMsg" status="wrong"></div>
	                </div>
					<div class="field toggle-field">
						<label>Update method</label>
						<div class="ui fluid selection dropdown update-dropdown">
	                        <input type="hidden" name="update_method" value="" id="updateMethod">
	                        <i class="dropdown icon"></i>
	                        <div class="default text">Select updater</div>
	                        <div class="menu">
	                            <div class="item" data-value="me">
	                            	<img class="ui avatar image" src="{{ asset(Auth::user()->profile_pic) }}">
	                            	{{ Auth::user()->firstname.' '.Auth::user()->middlename.' '.Auth::user()->surnname }} (Myself)
	                        	</div>
	                            <div class="item" data-value="outside">Person from outside</div>
	                        </div>
	                    </div>
	                    <div class="status-msg status-check-update status-inc-outsiders" id="updateMethodMsg" status="wrong"></div>
					</div>
	            	<div class="field toggle-field" id="actingPersonnel">
	            		<label>Outsider</label>
				    	<div class="ui stackable grid">
	                        <div class="fourteen wide column">
	                            <div id="outsiderSelect" class="ui fluid search selection dropdown receive-dropdown">
	                                <input type="hidden" name="outsider_id" id="outsiderInput">
	                                <i class="dropdown icon"></i>
	                                <div class="default text">Select outsider</div>
	                                <div class="menu" id="outsidersMenu">
	                                    <div class="disabled item" data-value="">Select outsider</div>
	                                    @foreach($outsiders as $outsider)
	                                        <div class="item" data-value="{{ $outsider->id }}">
	                                            {{ $outsider->name }}
	                                        </div>
	                                    @endforeach
	                                </div>
	                            </div>
	                        </div>
	                        <div class="two wide column">
	                            <button type="button" class="circular ui red icon button" id="showAddOutCont" data-tooltip="Add new outsiders" data-position="bottom right">
	                                <i class="plus icon"></i>
	                            </button>
	                        </div>
	                    </div>
	                    <div class="status-msg status-inc-outsiders" id="updateOutsiderMsg" status="wrong"></div>
				  	</div>
	                <div class="field" data-tooltip="You should select the next or current location after you updated this collection's status" data-position="top left">
	                	<label>Location</label>
	                    <div class="ui fluid selection dropdown update-dropdown">
	                        <input type="hidden" name="location" value="" id="locationInput">
	                        <i class="dropdown icon"></i>
	                        <div class="default text">Select location</div>
	                        <div class="menu">
	                        	<div class="disabled item" data-value="">Select location</div>
		                        @foreach($locations as $location)
		                        	<div class="item" data-value="{{ $location->id }}">
	                                    {{ $location->name }}
	                                </div>
		                        @endforeach
	                        </div>
	                    </div>
	                    <div class="status-msg status-check-update status-inc-outsiders" id="updateLocationMsg" status="wrong"></div>
	                </div>
	                <div class="field toggle-field" id="attachmentField">
	                	<label>Attachment</label>
	                	<input class="toggle-require" id="attachmentInput" multiple="true" type="file" name="attachments[]" placeholder="Please select a document file" required="true">
	                </div>
	                <div class="field">
	                    <label>Remark</label>
	                    <textarea rows="2" form="updateStatusForm" name="remark" placeholder="Enter remark"></textarea>
	                </div>
				</form>
			</div>
			<div class="hidden" id="updateAddOutsiderCont">
	            <div class="hidden ui segment" id="updateSuccessMsg">
	                <i class="check circle red big icon"></i>
	                Outsider successfully added! It's already in the Outside Sender options.
	            </div>
	            <div class="ui form">
	                <div class="ui stackable grid">
	                    <div class="eleven wide column">
	                        <div class="field">
	                            <div class="ui fluid input">
	                                <input type="text" name="new_outsider" id="outsiderName" placeholder="Enter outsider's name">
	                            </div>
	                            <div class="status-msg status-check-include-new-loc status-check-outsider-add" id="outsiderNameMsg" status="wrong"></div>
	                        </div>
	                        <div class="field">
	                            <div class="ui fluid search selection dropdown receive-dropdown">
	                                <input type="hidden" name="new_outsider" id="outsiderLocation">
	                                <i class="dropdown icon"></i>
	                                <div class="default text" id="updateDefaultText">Select outsider's location</div>
	                                <div class="menu">
	                                    <div class="disabled item" data-value="">Select outsider's location</div>
	                                    <div class="item" data-value="others">Others, please specify</div>
	                                    @foreach($locations as $loc)
	                                        <div class="item" data-value="{{ $loc->id }}">
	                                            {{ $loc->name }}
	                                        </div>
	                                    @endforeach
	                                </div>
	                            </div>
	                            <div class="status-msg status-check-include-new-loc status-check-outsider-add" id="outsiderLocationMsg" status="wrong"></div>
	                        </div>
	                        <div class="hidden field" id="newLocField">
	                            <div class="ui fluid input">
	                                <input type="text" name="new_outsider" id="newLocation" placeholder="Enter new location">
	                            </div>
	                            <div class="status-msg status-check-include-new-loc" id="outsiderNewLocMsg" status="wrong"></div>
	                        </div>
	                    </div>
	                    <div class="four wide column">
	                        <div class="field">
	                            <button type="button" class="ui disabled fluid red button" id="addOutsider">
	                                <i class="plus icon"></i>
	                                Add outsider
	                            </button>
	                        </div>
	                        
	                        <div class="field">
	                            <button type="button" class="ui fluid button" id="cancelAddOutsider"> 
	                                <i class="left arrow icon"></i>
	                                Go Back
	                            </button>
	                        </div>
	                    </div>
	                </div>
	            </div>  
	        </div>
		</div>
		<div class="actions">
			<button type="submit" form="updateStatusForm" class="ui disabled red right labeled icon button" id="updateStatusModalBtn">
                <i class="pencil alternate icon"></i>
                Save
            </button>
			<button class="ui button deny">Cancel</button>
            
		</div>
	</div>

	<div class="ui container">
		<div class="ui stackable grid">
			<div class="twelve wide column collection-main">
				<div class="ui segments">
					<div class="ui segment top-segment">
						<div class="opt-btn-cont">
							<div class="ui right pointing dropdown icon" id="opt-circle-btn">
								<i class="icon ellipsis vertical larger-ellipsis"></i>
								<div class="menu">
							  		<div class="item" id="forwardLink">Forward</div>
							  		<div class="item">Send Remark</div>
							  		<div class="item edit-coll" coll-title="{{ $collection->title }}" coll-description="{{ $collection->description }}" coll-type="{{ $collection->type->id }}" coll-type-name="{{ ucfirst($collection->type->name) }}" coll-tags="{{ $collection->tags }}" coll-id="{{ $collection->id }}" id="editItem{{ $collection->id }}">Edit</div>
							  		<div class="item">Delete</div>
								</div>
							</div>
						</div>
						<div class="top-coll-details-segment">
							<div class="ui large header coll-title-view">
								<span id="title-coll">{{ $collection->title }}</span>
								<div class="ui horizontal basic white large label margin-label" id="type-coll"> {{ ucfirst($collection->type->name) }}</div>
							</div>
							<div class="user-meta-details">
								<a class="ui image label margin-r-15" href="@if($collection->from_outside == '0') /personnelUtil/{{ $collection->sender->id}} @endif">
									@if($collection->from_outside == '0')
										<img class="ui avatar image" src="{{ asset($collection->sender->profile_pic) }}">
										From: {{ $collection->sender->firstname.' '.$collection->sender->middlename.' '.$collection->sender->surname }}
									@else
										From: {{ $collection->outsider->name }}
									@endif
								</a>
								<a class="ui image label" href="@if($collection->to_outside == '0') /personnelUtil/{{ $collection->receiver->id}} @endif">
									@if($collection->to_outside == '0')
										<img class="ui avatar image" src="{{ asset($collection->receiver->profile_pic) }}">
										To: {{ $collection->receiver->firstname.' '.$collection->receiver->middlename.' '.$collection->receiver->surname }}
									@else
										To: {{ $collection->outsider->name }}
									@endif
								</a>
								@if(count($collection->transmits) > 1)
									<span class="small-white-text"> and </span>
									<a class="ui label" href="#user">{{ count($collection->transmits)-1 }} more</a>
								@endif
							</div>
						</div>
					</div>
					<div class="ui segment">
						<div class="timestamp-div">
							<div class="ui horizontal label margin-r-15"> {{ ucfirst($collection->status->name) }}</div>
							<span class="small-grey-text">
								<i class="clock outline icon"></i>
								{{ Carbon\Carbon::parse($collection->created_at)->format('F j, Y h:i A') }}
							</span>
						</div>
						<div class="tags-cont" id="tags-coll">
							<span class="tag-design"><i class="tags icon"></i></span>
							@foreach(explode(',', $collection->tags) as $tag)
	                        	<a href="" class="ui tag label">{{ $tag }}</a>
	                        @endforeach
						</div>
					</div>
				</div>

				<div class="collection-code-cont viewcol-group">
					<div class="ui medium header">Collection code</div>
					<div class="ui centered card code-content">
						<div class="blurring dimmable collection-code">
							<div class="ui dimmer">
								<div class="content">
									<div class="center">
										<button class="ui inverted toggle button" id="copyCode">Copy Code</button>
									</div>
								</div>
							</div>
							<div class="content">
								<div class="ui large header" id="codeText">
									{{ $collection->code }}
								</div>
								<p class="meta">
									Use this code to uniquely identify your documents.
								</p>
							</div>
						</div>
					</div>
				</div>

				<div class="progress-details viewcol-group">
					<div class="ui medium header">Progress of request</div>
					<div class="ui stackable grid">
						<div class="eleven wide column">
							@if(count($progress_trackings) == 0)
								<img class="ui small @if(in_array(Auth::user()->id, $authorized_users)) centered @endif image" src="{{ asset('svg/Empty.png') }}"> 
							@else
								<div class="ui fluid vertical steps">
									@foreach($progress_trackings as $tracking)
										@if($tracking->is_step == 1)
											<div class="step complete">
												<i class="check circle red icon"></i>
												<div class="content">
													<div class="title">{{ ucfirst($tracking->status->name) }}</div>
													<div class="description">
														<span class="semibold">{{ $tracking->updater->firstname." ".$tracking->updater->middlename." ".$tracking->updater->surname}}</span> have marked this as {{ $tracking->status->name }}.
													</div>
												</div>
											</div>
										@endif
									@endforeach
								</div>
							@endif
						</div>
						<div class="five wide column">
							@if(in_array(Auth::user()->id, $authorized_users))
								<button id="updateBtn" class="ui red labeled icon button" data-tooltip="Assign status to this collection" data-position="bottom center">
				                    <i class="pencil alternate icon"></i>
				                    Update status
				                </button>
			                @endif
						</div>
					</div>
					
				</div>

				<div class="more-details viewcol-group">
					<div class="ui medium header">Content</div>
					<div class="ui top attached tabular menu">
						<a class="item active" data-tab="files">Files</a>
						<a class="item" data-tab="details">Details</a>
						<a class="item" data-tab="remarks">Remarks</a>
					</div>
					<div class="ui bottom attached tab segment active files-tab" data-tab="files">
						@php
			                function formatBytes($size, $precision = 2)
			                {
			                    $base = log($size, 1024);
			                    $suffixes = array('', 'KB', 'MB', 'GB', 'TB');   

			                    return round(pow(1024, $base - floor($base)), $precision) .' '. $suffixes[floor($base)];
			                }
			            @endphp

			            @foreach($documents as $receivedDoc)
			                <form id="download{{ $receivedDoc->id }}" method="POST" action="/download/doc">
			                    {{ csrf_field() }}
			                    <input type="hidden" name="file_path" value="{{ $receivedDoc->file_path }}">
			                </form>
			            @endforeach
			            <button id="addDocsTrigger" class="ui red right labeled icon button">
			                <i class="plus icon"></i>
			                Add Documents
			            </button>
			            <div class="ui three column stackable grid margin-top-20">
			                @foreach($documents as $sentDoc)
			                    <div class="column">
			                        <div class="ui fluid card file-cont">
			                            <div class="content">
			                            	<a class="ui red right ribbon label">{{ ucfirst($sentDoc->status->name) }}</a>
			                                <div id="{{ $sentDoc->id }}" class="ui floating dropdown icon options-dropdown">
			                                    <i class="ellipsis vertical icon options-icon" document-id="{{ $sentDoc->id }}"></i>
			                                    <div class="menu">
			                                    	@if($collection->from_outside == '0')
				                                    	@if(($sentDoc->tracking->updater->id == Auth::user()->id) || (Auth::user()->id == $sentDoc->collection->transmits->first()->sender->id))
				                                        <div class="item trash-doc" doc-id="{{ $sentDoc->id }}">
				                                            <i class="trash alternate icon"></i>
				                                            Delete
				                                        </div>
				                                        <div class="item rename-file" id="renameItem{{ $sentDoc->id }}" document-id="{{ $sentDoc->id }}" file-name="{{ $sentDoc->file_name }}">
				                                            <i class="edit icon"></i>
				                                            Rename
				                                        </div>
				                                        @endif
				                                    @else
				                                    	@if(($sentDoc->tracking->updater->id == Auth::user()->id) || (Auth::user()->id == $sentDoc->collection->transmits->first()->userUploader->id))
				                                        <div class="item trash-doc" doc-id="{{ $sentDoc->id }}">
				                                            <i class="trash alternate icon"></i>
				                                            Delete
				                                        </div>
				                                        <div class="item rename-file" id="renameItem{{ $sentDoc->id }}" document-id="{{ $sentDoc->id }}" file-name="{{ $sentDoc->file_name }}">
				                                            <i class="edit icon"></i>
				                                            Rename
				                                        </div>
				                                        @endif
			                                        @endif
			                                        
			                                        <a class="item download-doc" href="#download" path="dow" document-id="{{ $sentDoc->id }}">
			                                            <i class="download icon"></i>
			                                            Download
			                                        </a>


			                                        <div class="item view-document" path="/documents/pdf-document/{{$doc->id}}">
                                   	 				<i class="eye icon"></i>
				                                        View Document
				                            		</div>
			                                    </div>
			                                </div>
			                                <div class="icon-container">
			                                    <i class="{{ $icons[$sentDoc->file_extension]}} extension-icon"></i>
			                                </div>
			                            </div>
			                            <div class="extra content">
			                                <div class="ui small header file-header" id="gridFileName{{ $sentDoc->id }}">
			                                	@if(strlen($sentDoc->file_name)>26)
													{{ substr($sentDoc->file_name, 0, 23) }}...
												@else
													{{ $sentDoc->file_name }}
												@endif
			                                </div>
			                                <div class="meta"> {{ $sentDoc->sizeInBytes }} | {{ $sentDoc->file_extension}} </div>
			                            </div>
			                        </div>
			                    </div>
			                @endforeach
			            </div>
					</div>
					<div class="ui bottom attached tab segment" data-tab="details">
						<div class="ui grid">
							<div class="row">
								<div class="three wide column">
									Owner
								</div>
								<div class="thirteen wide column">
									@if($collection->from_outside == '1')
										<span class="semibold">{{ $collection->outside_sender }}</span>
									@else
										<span class="semibold">{{ $collection->sender->firstname." ".$collection->sender->middlename." ".$collection->sender->surname }}</span>
									@endif
								</div>
							</div>
							@if($collection->from_outside == '1')
								<div class="row">
									<div class="three wide column">
										Location
									</div>
									<div class="thirteen wide column">
										<span class="semibold">{{ $collection->outside_location }}</span>
									</div>
								</div>
							@endif
							<div class="row">
								<div class="three wide column">
									Sent to
								</div>
								<div class="thirteen wide column">
									@if($collection->to_outside)
										<span class="semibold">
											{{ $collection->outsider->name }}
										</span>
									@else
										<span class="semibold">
											{{ $collection->receiver->firstname." ".$collection->receiver->middlename." ".$collection->receiver->surname }}
										</span>										
									@endif
									
									@if(count($collection->transmits) > 1)
										and 
										<a class="ui label" href="#user">{{ count($collection->transmits)-1 }} more</a>
									@endif
								</div>
							</div>
							<div class="row">
								<div class="three wide column">
									Date created
								</div>
								<div class="thirteen wide column">
									{{ Carbon\Carbon::parse($collection->created_at)->format('F j, Y') }}
								</div>
							</div>
							<div class="row">
								<div class="three wide column">
									Date modified
								</div>
								<div class="thirteen wide column">
									{{ Carbon\Carbon::parse($collection->updated_at)->format('F j, Y') }}
								</div>
							</div>
						</div>
						<div class="description-cont">
							Description
							<p class="description-body" id="description-coll">
								{{ $collection->description }}
							</p>
						</div>
					</div>
					<div class="ui bottom attached tab segment remarks-container" data-tab="remarks">
						<div class="ui divided items">
							@forelse($collection->remarks as $remark)
								<div class="item">
									<div class="content">
										<div class="ui small header">
											<img class="ui avatar image" src="{{ asset($remark->user->profile_pic) }}">
											{{ $remark->user->firstname." ".$remark->user->middlename." ".$remark->user->surname }}
										</div>
										<div class="meta">
											{{ $remark->created_at->diffForHumans() }}
										</div>
										<div class="description remark-content">
											{{ $remark->content }}
										</div>
									</div>
								</div>
							@empty
								<img class="ui medium centered image" src="{{ asset('svg/Empty.png') }}">
							@endforelse
						</div>
					</div>
				</div>
			</div>

			<div class="four wide column">
				<div class="ui segment">
					<div class="ui small header">Tracking</div>
					<div>
						@forelse($trackings as $date => $tracks)
							<div class="trackings-group">
								<div class="date-tracking semibold">
									{{ $date }}
								</div>
								@foreach($tracks as $tracking)
								<div class="tracking-row">
									@if($tracking->type->name == 'attachment')
										<div class="small-text grey-color">
											<i class="paperclip icon"></i> 
											Additional document version
										</div>
										<div class="tracking-content">
											<span class="semibold">
												{{ $tracking->updater->firstname." ".$tracking->updater->middlename." ".$tracking->updater->surname }}
											</span> attached a file for <span class="semibold">{{ $tracking->status->name }}</span>.
										</div>
									@elseif($tracking->type->name == 'update')
										<div class="small-text grey-color">
											<i class="building outline icon"></i>
											From: 
											<span class="semibold">
												@if($tracking->from_outside == '1')
													{{ $tracking->outsider->location->name }}
												@else	
													{{ $tracking->updater->location->name }}
												@endif
											</span>
										</div>
										<div class="tracking-content">
											<span class="semibold">
												@if($tracking->from_outside == '1')
													{{ $tracking->outsider->name }}
												@else
													{{ $tracking->updater->firstname." ".$tracking->updater->middlename." ".$tracking->updater->surname }}
												@endif
											</span> 
												marked your request as 
												<span class="semibold">{{ $tracking->status->name }}</span>.
											@if($tracking->from_outside == '1')
												<div>
													<span class="small-text grey-color">
														Managed by <span class="semibold">{{ $tracking->updater->firstname." ".$tracking->updater->middlename." ".$tracking->updater->surname }}</span>
													</span>
												</div>
											@endif
										</div>
										<div class="small-text grey-color">
											<i class="map marker alternate icon"></i>
											Current location: 
											<span class="semibold">
												{{ $tracking->location->name }}
											</span>
										</div>
									@elseif($tracking->type->name == 'initial')
										<div class="small-text grey-color">
											<i class="building outline icon"></i>
											From: 
											<span class="semibold">
												@if($tracking->from_outside == '1')
													{{ $tracking->outsider->location->name }}
												@else	
													{{ $tracking->updater->location->name }}
												@endif
											</span>
										</div>
										<div class="tracking-content">
											<span class="semibold">
												@if($tracking->from_outside == '1')
													{{ $tracking->outsider->name }}
												@else
													{{ $tracking->updater->firstname." ".$tracking->updater->middlename." ".$tracking->updater->surname }}
												@endif
											</span> 
												sent this collection to 
												<span class="semibold">
													{{ count($tracking->collection->transmits) }} 
													@if(count($tracking->collection->transmits) == 1)
														user
													@else
														users
													@endif
												</span>.
											@if($tracking->from_outside == '1')
												<div>
													<span class="small-text grey-color">
														Managed by <span class="semibold">{{ $tracking->updater->firstname." ".$tracking->updater->middlename." ".$tracking->updater->surname }}</span>
													</span>
												</div>
											@endif
										</div>
										<div class="small-text grey-color">
											<i class="map marker alternate icon"></i>
											Current location: 
											<span class="semibold">
												{{ $tracking->location->name }}
											</span>
										</div>
									@endif
								</div>
								@endforeach
							</div>
						@empty
							No tracking yet.
						@endforelse
					</div>
					<button class="ui red button margin-top-10" id="viewTrackingBtn">View Tracking</button>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="{{ asset('js/validations/update-status-validation.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/validations/edit-coll-validations.js') }}"></script>
	<script type="text/javascript">
		function initiateDynamicFunctions(){
			$('#addOutsider').click(function(){
	            var name = $('#outsiderName').val();
	            var loc = $('#outsiderLocation').val();
	            var newLoc = $('#newLocation').val();

	            $('#modalLoader').addClass('active');

	            addOutsider(name, loc, newLoc, 'receive');
	        });

	        $('#outsiderLocation').change(function(){
	            if($(this).val() == 'others'){
	                $('#newLocField').removeClass('hidden');
	            }
	            else{
	                $('#newLocField').addClass('hidden');
	            }
	        });

			$('#showAddOutCont').click(function(){
	            $('#updateFormCont').transition('slide left');
	            $('#updateAddOutsiderCont').removeClass('hidden');
	        });

	        $('#cancelAddOutsider').click(function(){
	            $('#updateFormCont').transition('slide left');
	            $('#updateAddOutsiderCont').addClass('hidden');
	        });

			//This function adds outsiders via Ajax
			function addOutsider(name, loc, newLoc, type){
	            $.ajaxSetup({
	                headers: {
	                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	                }
	            });
	            $.ajax({
	                type: "POST",
	                url: "/outsider/add",
	                data: {
	                    name: name,
	                    loc: loc,
	                    newLoc: newLoc
	                },
	                success: function(outsider){
	                    outsider = JSON.parse(outsider);
	                    $('#outsidersMenu').append(
	                        $('<div>').addClass('item').attr('data-value', outsider.id).append(outsider.name).append(
	                            $('<span>').addClass('semibold new-out').html('(NEW)')
	                        )
	                    );

                        $('#outsiderName').val('');
                        $('#outsiderLocation').val('');
                        $('#newLocation').val('');
                        $('#updateDefaultText').html('Select outsider\'s location');
                        $('#updateSuccessMsg').removeClass('hidden');
                        $('#modalLoader').removeClass('active'); 
	                },
	                error: function(err){
	                    console.log(err.responseText);
	                }
	            });
	        }


			//Renaming of a document

	        $('.rename-file').each(function(){
	            var docId = $(this).attr('document-id');
	            var oldName = $(this).attr('file-name');
	            var namesArr = oldName.split('.');
	            var fileName = extractFileName(namesArr);
	            var extension = namesArr[namesArr.length-1];

	            $(this).click(function(){
	                $('#errorRename').empty();
	                $('#renameBtn').attr('document-id', docId);
	                $('#fileNameInput').val(fileName);
	                $('#fileNameInput').attr('old-name', fileName+'.'+extension);
	                $('#fileExtension').html('.'+extension);
	                $('#renameFileModal').modal('show');
	            });
	        });

	        //When edit item is clicked, show modal
	        $('.edit-coll').each(function(){
                var title = $(this).attr('coll-title');
                var type = $(this).attr('coll-type');
                var typeText = $(this).attr('coll-type-name');
                var description = $(this).attr('coll-description');
                var tags = $(this).attr('coll-tags');
                var id = $(this).attr('coll-id');

                $(this).click(function(){
                    $('#editCollectionTitle').html(title);
                    $('#editCollBtn').attr('coll-id', id);
                    $('#editCollTitle').val(title).attr('old-value', title);
                    $('#editCollDescription').html(description).attr('old-value', description);
                    $('#editCollTags').val(tags).attr('old-value', tags);
                    $('#editCollType').val(type).attr('old-value', type);
                    $('#collTypeText').html(typeText);

                    $('.status-check').each(function(){
                        $(this).empty().attr('status', 'okay').attr('changed', 'false');
                    });
                    $('#changesStatus').empty();
                    $('#editCollBtn').addClass('disabled');

                    $('#editCollectionModal').modal('show');
                });
            });
		}
		
		$(document).ready(function(){
			initiateDynamicFunctions();
			$('.add-docs-btn').click(function(){
				var status = $(this).attr('status');

				$('#addDocStatusInput').val(status);
				$('#statusListCont').addClass('hidden');
				$('#addDocsFormCont').removeClass('hidden');
			});

			//Viewing of document
			$('.view-document').click(function(){
                var path = $(this).attr('path');
                $('#viewDocEmbed').attr('src', path);
                $('#viewDocModal').modal('show');
            });

			//Downloading of document
			$('.download-doc').click(function(){
	            var id = $(this).attr('document-id');
	            var name = $(this).attr('filename');
	            $('#download'+id).submit();
	            console.log(id);
	        });

			//When edit collection button is clicked
	        $('#editCollBtn').click(function(){
                var id = $('#editCollBtn').attr('coll-id');
                var title = $('#editCollTitle').val();
                var type = $('#editCollType').val();
                var description = $('#editCollDescription').val();
                var tags = $('#editCollTags').val();

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: "/collection/edit",
                    data: {
                        id: id,
                        type: type,
                        title: title,
                        tags: tags,
                        description: description
                    },
                    success: function(collection){
                        var coll = JSON.parse(collection);
                        var editItem = $('#editItem'+id);
                        editItem.attr('coll-title', coll.title);
                        editItem.attr('coll-description', coll.description);
                        editItem.attr('coll-type', coll.type);
                        editItem.attr('coll-tags', coll.tags);

                        $('#title-coll').html(coll.title);
                        $('#type-coll').html(coll.type_name);
                        $('#description-coll').html(coll.description);
                        $('#editCollectionModal').modal('hide');

                        $('#tags-coll').empty();
                        $('#tags-coll').append(
                        	$('<span>').addClass('tag-design').append(
                        		$('<i>').addClass('tags icon')
                        	)
                        );
                        var tags = coll.tags.split(',');
                        for(var i = 0; i < tags.length; i++){
                        	$('#tags-coll').append(
                        		$('<a>').addClass('ui tag label').append(
                        			tags[i]
                        		)
                        	);
                        }
                        
                        initiateDynamicFunctions();
                    },
                    error: function(err){
                        console.log(err.responseText);
                    }
                });
            });

			$('#backBtn').click(function(){
				$('#statusListCont').removeClass('hidden');
				$('#addDocsFormCont').addClass('hidden');
			});

			/*$('#deleteDocBtn').click(function(){
				var id= $(this).attr('doc-id');
				var case = $('#deleteDocModal').attr('case');

				$.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: "/delete/doc",
                    data: {
                        id: id,
                        case: case
                    },
                    success: function(data){
                    },
                    error: function(err){
                        console.log(err.responseText);
                    }
                });
			});*/

			$('#addDocsTrigger').click(function(){
				$('#addDocsModal').modal('show');
			});

			$('.trash-doc').click(function(){
				var id = $(this).attr('doc-id');
				
				$.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: "/delete/doc/check",
                    data: {
                        id: id
                    },
                    success: function(data){
                        var response = JSON.parse(data);
                        if(response.status == 'success'){
                        	$('#deleteDocBtn').removeClass('disabled');
                        	$('#deleteDocModal').attr('case', response.case);
                        	$('#deleteDocBtn').attr('doc-id', id);

                        	$('#idInput').val(id);
                        	$('#caseInput').val(response.case);
                        }
                        else{
                        	$('#deleteDocBtn').addClass('disabled');
                        }
                        $('#deleteDocContent').html(response.message);
                        $('#deleteDocModal').modal('show');
                    },
                    error: function(err){
                        console.log(err.responseText);
                    }
                });
			});
			$('.menu .item').tab();

			$('#viewTrackingBtn').click(function(){
				$('#trackingModal').modal('show');
			});
			
			function validateInputs(){
				if($('#forwardMethod').val() === 'group'){
					if($('#forwardGroupMessage').attr('status') === 'okay'){
						$('#forwardBtn').removeClass('disabled');
					}
					else{
						$('#forwardBtn').addClass('disabled');
					}
				}
				else if($('#forwardMethod').val() === 'one'){
					if($('#forwardOneMsg').attr('status') === 'okay'){
						$('#forwardBtn').removeClass('disabled');
					}
					else{
						$('#forwardBtn').addClass('disabled');
					}
				}
				else{
					if($('#forwardManyMsg').attr('status') == 'okay'){
						$('#forwardBtn').removeClass('disabled');
					}
					else{
						$('#forwardBtn').addClass('disabled');
					}
				}
			}

			$('#forwardManyInput').change(function(){
				if($(this).val() == ''){
					$('#forwardManyMsg').attr('status', 'wrong').html('This is a required field. You need to send this to at least one user.').transition('bounce');
				}
				else{
					$('#forwardManyMsg').attr('status', 'okay').empty();
				}
				validateInputs();
			});

			$('#forwardOneInput').change(function(){
				if($(this).val() == ''){
					$('#forwardOneMsg').attr('status', 'wrong').html('This is a required field. You need to send this to a user.').transition('bounce');
				}
				else{
					$('#forwardOneMsg').attr('status', 'okay').empty();
				}
				validateInputs();
			});

			$('#forwardGroupInput').on('change', function(){
				if($(this).val() == ''){
					$('#forwardGroupMessage').attr('status', 'wrong').removeClass('success-msg').addClass('error-msg').html('This is a required field. You need to send this to a group.').transition('bounce');
					validateInputs();
				}
				else{
					var groupId = $(this).val();
					var collId = $('#collId').val();
					$.ajaxSetup({
	                    headers: {
	                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	                    }
	                });
	                $.ajax({
	                    type: "POST",
	                    url: "/forward/checkgroup",
	                    data: {
	                        id: groupId,
	                        coll_id: collId
	                    },
	                    success: function(data){
	                        if(data == 0){
	                        	$('#forwardGroupMessage').html("You can't forward to this group anymore. It may be that they already received the collection.").removeClass('success-ms').addClass('error-msg').attr('status', 'wrong');
	                        	$('#forwardGroupMessage').transition('bounce');
	                        }
	                        else{
	                        	if(data == 1){
	                        		$('#forwardGroupMessage').html("You can send to "+data+" person in this group.").removeClass('error-msg').addClass('success-msg');
	                        		$('#forwardGroupMessage').attr('status', 'okay');
	                        	}
	                        	else if(data > 1){
	                        		$('#forwardGroupMessage').html("You can send to "+data+" people in this group.").removeClass('error-msg').addClass('success-msg');
	                        		$('#forwardGroupMessage').attr('status', 'okay');
	                        	}
	                        	$('#forwardGroupMessage').transition('bounce');
	                        }
	                        validateInputs();
	                    },
	                    error: function(err){
	                        console.log(err.responseText);
	                    }
	                });
				}
				validateInputs();	
			});

			$('#forwardMethod').on('change', function(){
				var method = $(this).val();

				if(method == "group"){
					$('#forwardToGroup').removeClass('hidden');
					$('#forwardToOne').addClass('hidden');
					$('#forwardToMany').addClass('hidden');
					$('#forwardToGroup').transition('pulse');
				}
				else if(method == 'many'){
					$('#forwardToMany').removeClass('hidden');
					$('#forwardToOne').addClass('hidden');
					$('#forwardToGroup').addClass('hidden');
					$('#forwardToMany').transition('pulse');
				}
				else if(method == 'one'){
					$('#forwardToMany').addClass('hidden');
					$('#forwardToOne').removeClass('hidden');
					$('#forwardToGroup').addClass('hidden');
					$('#forwardToOne').transition('pulse');
				}
				validateInputs();
			});

			$('.options-icon').each(function(){
                var id = "#"+$(this).attr('document-id');
                $(id).dropdown({
                    transition: 'horizontal flip'
                });
            });

            $('#forwardMethodDropdown').dropdown();

            $('#one-search').dropdown();

            $('#forwardGroup').dropdown({
            	clearable: true
            });

            $('#opt-circle-btn').dropdown();

            $('#updateBtn').click(function(){
            	$('#updateModal').modal('show');
            });

            $('#forwardLink').click(function(){
            	$('#forwardModal').modal('show');
            });

            $('.collection-code').dimmer({
				on: 'hover'
			});

			$("#copyCode").click(function(){
				var code = $("#codeText");

				code.select();

				document.execCommand('copy');
			});

        	$("#many-search").dropdown({
        		clearable: true
        	});

        	$('.update-dropdown').dropdown({
        		clearable: true
        	});

        	$('.receive-dropdown').dropdown({
        		clearable: true
        	});
		});
	</script>
@else
	<div class="ui placeholder segment">
		<div class="ui icon header">
			<i class="hand paper outline icon"></i>
			You are not allowed to view this collection.
		</div>
	</div>
@endif
@else($collection == null)
	<div class="ui placeholder segment">
		<div class="ui icon header">
			<i class="times circle outline icon"></i>
			This collection does not exist.
		</div>
	</div>
@endif
@endsection