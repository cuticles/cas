<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>CAS Management System</title>

    <!-- Scripts -->
    
    <!-- Semantic UI -->
    <link rel="stylesheet" type="text/css" href="{{ asset('semantic/dist/semantic.min.css') }}">
    <script type="text/javascript" src="{{ asset('js/jquery.js') }}"></script>
    <script
      src="https://code.jquery.com/jquery-3.1.1.min.js"
      integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
      crossorigin="anonymous"></script>
    <script type="text/javascript" src="{{ asset('semantic/dist/semantic.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/sent.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/tablesort.js') }}"></script>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('fontawesome/css/all.css') }}" rel="stylesheet">
    <link rel="shortcut icon" href="{{ asset('svg/favicon.ico') }}" type="image/x-icon">
    <link rel="icon" href="{{ asset('svg/favicon.ico') }}" type="image/x-icon">
    

</head>
<body>
    @if(Auth::user()->is_admin == 1)
    <div class="ui red left vertical inverted sidebar labeled icon menu" id="sidebar-addables">
        <a class="item @if(Request::is('addables/outsiders')) active @endif" href="/addables/outsiders">
            <i class="users icon"></i>
            Outsiders
        </a>
        <a class="item @if(Request::is('addables/locations')) active @endif" href="/addables/locations">
            <i class="map marker alternate icon"></i>
            Locations
        </a>
        <a class="item @if(Request::is('addables/types')) active @endif" href="/addables/types">
            <i class="sitemap icon"></i>
            Types
        </a>
        <a class="item @if(Request::is('addables/points')) active @endif" href="/addables/points">
            <i class="calculator icon"></i>
            Points
        </a>
        <a class="item @if(Request::is('addables/positions')) active @endif" href="/addables/positions">
            <i class="id badge outline icon"></i>
            Positions
        </a>
    </div>
    @endif
    <div id="app">
        
        <div class="app-sidebar" id="sidebar-app">
            @if(Auth::user()->is_admin == 1)
            <span id="bar-icon-sb" >
                <i class="bars icon large" id="barsMenuShow"></i>
            </span>
            @endif
            <div class="logo-container">
                <img src="{{ asset('svg/CAS-DMS-PRS.svg') }}" width="33%" height="33%">
            </div>
            <div class="sb-group">
                <a class="sb-head" href="/dashboard">
                    <i class="fa fa-chart-bar sb-icon"></i>
                    Dashboard
                </a>
                <a class="sb-head mynotif" href="/notifications">
                    <i class="fa fa-bell sb-icon"></i>
                    Notifications 
                        @php
                            {{ $total = count(DB::table('notifications')->where('to_user_id',Auth::user()->id)->where('is_seen','false')->get());}}
                        @endphp
                        @if($total > 0)
                            <div class="ui icon-indicator mini circular label">
                                {{$total}}</div>
                        
                        @endif       
                        
                </a> 
            </div>

            <div class="sb-group">
                <div class="@if(Request::is('received') || Request::is('received/collections')) darker @endif">
                    <div class="sb-head slide-group" id="receivedBar" state="@if(Request::is('received') || Request::is('received/collections')) up @else down @endif" name="received">
                        <i class="fa fa-file-import sb-icon"></i>
                        Received
                        <i class="fa fa-angle-down icon-indicator" id="receivedAngle"></i>
                    </div>
                    <div class="@if(!(Request::is('received') || Request::is('received/collections'))) hidden @endif" id="receivedCont">
                        <a class="sb-hidden-item" href="/received">
                            Documents
                        </a>
                        <a class="sb-hidden-item" href="/received/collections">
                            Collections
                        </a>
                    </div>
                </div>
                
                <div class="@if(Request::is('sent') || Request::is('sent/collections')) darker @endif">
                    <div class="sb-head slide-group" id="sentBar" state="@if(Request::is('sent') || Request::is('sent/collections')) up @else down @endif" name="sent">
                        <i class="fa fa-file-export sb-icon"></i>
                        Sent
                        <i class="fa fa-angle-down icon-indicator" id="sentAngle"></i>
                    </div>
                    <div class="@if(!(Request::is('sent') || Request::is('sent/collections'))) hidden @endif" id="sentCont">
                        <a class="sb-hidden-item" href="/sent">
                            Documents
                        </a>
                        <a class="sb-hidden-item" href="/sent/collections">
                            Collections
                        </a>
                    </div>
                </div>

                <div class="@if(Request::is('myforwards') || Request::is('forwards/tome')) darker @endif">
                    <div class="sb-head slide-group" id="forwardsBar" state="@if(Request::is('myforwards') || Request::is('forwards/tome')) up @else down @endif" name="forwards">
                        <i class="fa fa-paper-plane sb-icon"></i>
                        Forward
                        <i class="fa fa-angle-down icon-indicator" id="forwardsAngle"></i>
                    </div>
                    <div class="@if(!(Request::is('myforwards') || Request::is('forwards/tome'))) hidden @endif" id="forwardsCont">
                        <a class="sb-hidden-item" href="/myforwards">
                            My Forwards
                        </a>
                        <a class="sb-hidden-item" href="/forwards/tome">
                            Forwarded To Me
                        </a>
                    </div>
                </div>
                <div class=" @if(Request::is('logs') ) darker @endif">
                    <div class="sb-head" id = "personnel-side">
                       <a  href="/logs">
                        <i class="fa fa-history sb-icon"></i>
                                Activity Log
                            </a>
                    </div>
               
            </div>
                

            </div>

            @if (Auth::user()->is_admin == 1)
            <div class="sb-group @if(Request::is('addPersonnelForm') || Request::is('managePersonnel') ) darker @endif">
           
                <div class="sb-head" id = "personnel-side">
                    <i class="fa fa-users-cog sb-icon"></i>
                    Personnel
                </div>
               
                <a class="sb-item" href="/managePersonnel">
                    <i class="fa fa-id-card sb-icon"></i>
                    View Personnel
                    @if(Request::is('managePersonnel'))
                        <i class="fa fa-angle-right icon-indicator"></i>
                    @endif
                    
                </a>
                <a class="sb-item" href="/addPersonnelForm">
                    <i class="fa fa-user-plus sb-icon"></i>
                    Add New Personnel
                    @if(Request::is('addPersonnelForm'))
                        <i class="fa fa-angle-right icon-indicator"></i>
                    @endif
                </a>
            </div>
            @endif

            <div class="floating-image">
                <div id="userDrop" class="ui right pointing dropdown">
                    <img class="ui image circular prof-image" src="{{ asset(Auth::user()->profile_pic) }}">
                    <div class="menu">
                        <a href="/personnelUtil/{{Auth::user()->id}}" class="item semibold">
                            {{ Auth::user()->firstname.' '.Auth::user()->middlename.' '.Auth::user()->surname }}
                        </a>
                        <a class="item" href="/trash">
                            Recycle Bin
                        </a>
                        <a href="/groups" class="item">
                            Group
                        </a>
                        @if (Auth::user()->is_admin == 1)
                         <a href="/appeals" class="item">
                            Requests
                        @endif
                        </a>

                        <div class="header">Account</div>
                        <a href="/editPersonnelForm/{{Auth::user()->id}}" class="item">
                            Manage account
                        </a>
                        <a href="/settings" class="item">
                            Settings
                        </a>
                        <a href="{{ route('logout') }}" class="item" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <main class="py-4 app-content">
            @yield('content')
        </main>
    </div>
    <script type="text/javascript">
        $('#userDrop').dropdown();

       function showDiv(div, main) {
            var x = document.getElementById(div);
            var y = document.getElementById(main);
            if (x.style.display === "none") {
                y.innerHTML = x.innerHTML;
            } 
        }
        $('#barsMenuShow').click(function(){
            $('#sidebar-addables').sidebar('setting', 'transition', 'overlay').sidebar('toggle');
        });

        $(document).ready(function(){
            $('deleteBtn').click(function(){
                $('delete-modal').modal('show');
            });

            // $('.mynotif').click(function(){
            //     var seen = 'true'
            //     $.ajaxSetup({
            //         headers: {
            //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            //         }
            //     });
            //     $.ajax({
            //         type: "POST",
            //         url: "/notification/num",
            //         data: {
            //             seen: seen,
            //         },
            //         success: function(coll){
            //             // $('#restoreCollectionModal').modal('hide');
            //             console.log('yey');
            //         },
            //         error: function(err){
            //             console.log(err.responseText);
            //         }
            //     });
            // });

            $('.slide-group').each(function(){
                $(this).click(function(){
                    var name = $(this).attr('name');
                    if($('#'+name+'Bar').attr('state').trim() == 'down'){
                        $('#'+name+'Cont').slideDown();
                        $('#'+name+'Angle').removeClass('fa-angle-down').addClass('fa-angle-up');
                        $('#'+name+'Bar').attr('state', 'up')
                    }
                    else{
                        $('#'+name+'Cont').slideUp();
                        $('#'+name+'Angle').removeClass('fa-angle-up').addClass('fa-angle-down');
                        $('#'+name+'Bar').attr('state', 'down')
                    }
                });
            });
        });
    </script>
</body>
</html>
