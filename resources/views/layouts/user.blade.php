<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>CAS Management System</title>

    <!-- Scripts -->
    
    <!-- Semantic UI -->
    <link rel="stylesheet" type="text/css" href="{{ asset('semantic/dist/semantic.min.css') }}">
    <script
      src="https://code.jquery.com/jquery-3.1.1.min.js"
      integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
      crossorigin="anonymous"></script>
    <script type="text/javascript" src="{{ asset('semantic/dist/semantic.min.js') }}"></script>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('fontawesome/css/all.css') }}" rel="stylesheet">
</head>
<body>

    <div id="app">
        <div class="app-sidebar">
                <div class="ui right aligned category search">
  <div class="ui icon input">
    <input class="prompt" type="text" placeholder="Search animals...">
    <i class="search icon"></i>
  </div>
  <div class="results"></div>
</div>
            <div class="sb-group">
                <a class="sb-head" href="#ref">
                    <i class="fa fa-bell sb-icon"></i>
                    Notifications
                </a>

                <div class="sb-group">
                    <a class="sb-head" href="#ref">
                        <i class="fa fa-users-cog sb-icon"></i>
                        Personnel
                    </a>
                    <a class="sb-item" href="#ref">
                        <i class="fa fa-id-card sb-icon"></i>
                        View Personnel
                    </a>
                    <a class="sb-item" href="#ref">
                        <i class="fa fa-id-card sb-icon"></i>
                        Add New Personnel
                    </a>
                    <a class="sb-item" href="#ref">
                        <i class="fa fa-users sb-icon"></i>
                        Groups
                    </a>
                </div>

                <a class="sb-head" href="#ref">
                    <i class="fa fa-users-cog sb-icon"></i>
                    My Records
                </a>

                <a class="sb-head" href="#ref">
                    <i class="fa fa-users-cog sb-icon"></i>
                    My Account
                </a>

                <a class="sb-head" href="#ref">
                    <i class="fa fa-users-cog sb-icon"></i>
                    Activity Log
                </a>
            </div>
            <div class="sb-bottom">
                <div id="userDrop" class="ui top pointing dropdown">
                    <i class="fa fa-user-circle sb-icon"></i>
                    {{ Auth::user()->firstname." ".Auth::user()->middlename." ".Auth::user()->surname}}
                    <div class="menu">
                        <div class="item">
                            Profile
                        </div>
                        <div class="item">
                            Settings
                        </div>
                        <div class="item">
                            Logout
                        </div>
                    </div>
                </div>
                <div class="logout-grp">
                    <a href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                        <i class="fa fa-door-open sb-icon"></i>
                        {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </div>
        </div>

        <main class="py-4 app-content">
            @yield('content')
        </main>
    </div>
    <script type="text/javascript">
        $('#userDrop').dropdown();
    </script>
</body>
</html>
