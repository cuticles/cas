@extends('layouts.admin')

@section('content')

<h3> Settings </h3>
<span id = "alert_contract_msg"> </span> 
<div class = "ui compact segment" id = "contract_segment"> 
	<h4> Contract Expiration Date </h4> 
	<br>
	<span>
		@if (Auth::user()->contractAlert != NULL)
			The date you save for your contract alert is {{ Carbon\Carbon::parse(Auth::user()->contractAlert)->toFormattedDateString() }} 
		@else
			You haven't saved a date yet. Save the date for your contract alert below.
		@endif
	</span>
	<form action = "/saveContractExpiry" method = "post" id = "contractAlertInput" class = "ui form">
		@csrf
		<br>
		<div class = "ui grid">
			<div class = "row">
				<div class = "ten wide column">
					<input type = "datetime-local" id = "contractAlert" name = "contractAlert" user-id = "{{Auth::user()->id}}" min = "<?php echo date('Y-m-d').'T'.date('H:i'); ?>"> 
				</div>

				<div class = "two column" id = "contractAlertBtn">
	  				<button type = "submit" class = "ui red button" id = "saveAlert"> Save </button>
	  			</div>
	  		</div> 
	  	</div>
  	</form>

</div>

<script type = "text/javascript">

	var msg = '{{Session::get('alert')}}';
    var exist = '{{Session::has('alert')}}';

    if(exist){
      $('#alert_contract_msg').html(msg);
    }

	$("#contractAlertInput").form({
		fields: ({
				contractAlert: {
					identifier: 'contractAlert',
					rules: [
					{
						type: 'empty',
						prompt : 'Please enter you contract expiration date',
					}]
				},
		}),
		inline : true,
    	on : 'submit',
	});

	



</script>

@endsection