
@foreach($sentDocs as $sentDoc)
    <div class="ui fluid card">
        <div class="content">
            <div id="{{ $sentDoc->id }}" class="ui floating dropdown icon options-dropdown">
                <i class="ellipsis vertical icon options-icon" document-id="{{ $sentDoc->id }}"></i>
                <div class="menu">
                    <div class="item rename-file" id="rename{{ $sentDoc->id }}" document-id="{{ $sentDoc->id }}" file-name="{{ $sentDoc->file_name }}">
                        <i class="edit icon"></i>
                        Rename
                    </div>
                    <div class="item view-details" document-id="{{ $sentDoc->id }}">
                        <i class="history icon"></i>
                        Details
                    </div>
                </div>
            </div>
            <div class="icon-container">
                <i class="{{ $icons[$sentDoc->file_extension]}} extension-icon"></i>
            </div>
        </div>
        <div class="extra content">
            <div class="ui small header file-header" id="gridFileName{{ $sentDoc->id }}">{{ $sentDoc->file_name}}</div>
            <div class="meta"> {{ formatBytes($sentDoc->file_size) }} | {{ $sentDoc->file_extension}} </div>
        </div>
    </div>
@endforeach
    </div>
</div>

<div class="table-cont hidden" id="documentsTable">
    <table class="ui striped very basic table">
        <thead>
            <th class="one wide"></th>
            <th class="eight wide">File Name</th>
            <th class="four wide">Size</th>
            <th class="two wide">Date Created</th>
            <th class="one wide"></th>
        </thead>
        <tbody id="documentsTableBody">
            @foreach($sentDocs as $doc)
                <tr>
                    <td>
                        <i class="{{ $icons[$doc->file_extension]}} red table-icon"></i>
                    </td>
                    <td>
                        <span class="semibold" id="listFileName{{ $doc->id }}">{{ $doc->file_name }}</span>
                    </td>
                    <td>
                        {{ formatBytes($doc->file_size) }}
                    </td>
                    <td>
                        {{ Carbon\Carbon::parse($doc->created_at)->format('M d') }}
                    </td>
                    <td>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>