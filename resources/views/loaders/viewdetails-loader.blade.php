<div class="ui secondary pointing menu" id="viewDetailsTab">
	<a class="active item" data-tab="basic">Details</a>
	<a class="item" data-tab="receivers">
		@if($type == 'received')
			Sender
		@else
			Receivers
		@endif
	</a>
</div>
<div class="ui bottom attached active tab" data-tab="basic">
	<div class="vd-top-content">
		<table class="ui single line table">
			<thead>
				<tr>
					<th class="five wide">Property</th>
					<th class="eleven wide">Detail</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>Collection</td>
					<td>{{ $coll->title }}</td>
				</tr>
				<tr>
					<td>File Name</td>
					<td>{{ $doc->file_name }}</td>
				</tr>
				<tr>
					<td>File Extension</td>
					<td>{{ $doc->file_extension }}</td>
				</tr>
				<tr>
					<td>File Size</td>
					<td>{{ $doc->file_size }}</td>
				</tr>
				<tr>
					<td>Date Uploaded</td>
					<td>{{ Carbon\Carbon::parse($doc->created_at)->format('F j, Y') }}</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
<div class="ui bottom attached tab" data-tab="receivers">
	@foreach($users as $user)
		<div class="padding-5">
			@if($user->from_outside == '1' || $user->to_outside == '1')
				<span class="semibold">
				{{ $user->outside_sender }}
				</span>
			@else
				<img class="ui avatar image" src="{{ asset($user->profile_pic) }}">
				<span class="semibold">
					{{ $user->name }}
				</span>
			@endif
		</div>
		<div class="ui divider"></div>
	@endforeach
</div>