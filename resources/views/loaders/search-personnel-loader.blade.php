<div class = "row">
    <div class = "five wide column">
        <h3> Name </h3>
    </div>
     <div class = "four wide column">
        <h3> Position </h3>
    </div>
     <div class = "three wide column">
        <h3> Credential Points </h3>
    </div>
    <div class = "four wide column user-opt">
        <h3> </h3>
    </div>
</div>
@foreach ($users as $user)
    <div class = "row" id="users-table">
        <div class = "five wide column" id = "{{$user->id}}">
            <img class="ui avatar image circular" src="{{$user['profile_pic']}}">
            <a href="/personnelUtil/{{$user->id}}" title = "View Profile" id = "fullname">
                {{ $user->firstname }} {{ $user->surname }} 
            </a>
        </div>

        <div class = "four wide column">
            @if ($user->position_id != NULL)
                 {{ $user->position->name }}
            @endif
        </div>
         <div class = "three wide column">
           {{ $user->points }}
        </div>
        <div class = "four wide column user-opt">
            @if (Auth::user()->is_admin == 1)
                <a href = "/editPersonnelForm/{{$user->id}}" class = "fas fa-user-edit fa-lg" aria-hidden = "true" title = "Edit Profile"> </a> 
                <a class = "item delete fa fa-trash fa-lg" user-id = "{{$user->id}}" aria-hidden = "true" title = "Delete">   </a>
            @endif
        </div>
    </div>
@endforeach