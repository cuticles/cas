@extends('layouts.admin')

@section('content')
    <div class="ui items">
        
        @foreach($collections as $collection)<div class="ui small modal" id="trashCollectionModal">
            <div class="header">
                Delete Collection
            </div>
            <div class="content">
                Are you sure you want to delete <span class="semibold" id="trashCollTitle">{{$collection->title}}</span> permanently?
            </div>
            <div class="actions">
                <button class="ui black right labeled icon button" coll-id="" id="trashCollBtn">
                    <i class="trash alternate outline icon"></i>
                    Yes
                </button>
                <button class="ui deny button">No</button>
            </div>
        </div>
            <div class="item coll-item" id="collection{{ $collection->id }}">
                <div id="{{ $collection->id }}" class="ui floating dropdown options-dropdown-coll">
                    <i class="ellipsis vertical icon options-icon" document-id="{{ $collection->id }}"></i>
                    <div class="menu">
                        <div class="item restore-coll" coll-id="{{ $collection->id }}">
                            <i class="restore icon"></i>
                            Restore
                        </div>
                        <div class="item trash-coll" coll-id="{{ $collection->id }}">
                            <i class="trash alternate icon"></i>
                            Delete
                        </div>
                    </div>
                </div>
                <div class="ui tiny image">
                    <img src="{{ asset($icons[$collection->type]) }}" id="type{{ $collection->id }}">
                </div>
                <div class="content">
                    <div class="mg-bottom-12">
                        <div class="ui basic horizontal label status-label">
                            {{ ucfirst($collection->current_status)}}
                        </div>
                        <span class="meta">
                            {{ $collection->created_at->diffForHumans() }}
                        </span>
                    </div>
                    <a href="/collection/{{ $collection->id }}" class="header title-collection" id="title{{ $collection->id }}">
                        {{ $collection->title }}
                    </a>
                </div>
            </div>
        @endforeach
    </div>

    <script type="text/javascript">

        $(document).ready(function(){
            $('.options-dropdown-coll').dropdown();
        });
        $('.trash-coll').each(function(){
            var id = $(this).attr('coll-id');
            var title = $(this).attr('coll-title');
            $(this).click(function(){
                $('#trashCollTitle').html(title);
                $('#trashCollBtn').attr('coll-id', id);
                $('#trashCollectionModal').modal('show');
            });
        });
         $('#trashCollBtn').click(function(){
                var id = $('#trashCollBtn').attr('coll-id');
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: "/collection/trash/delete",
                    data: {
                        id: id,
                        type: 'sent'
                    },
                    success: function(coll){
                        $('#trashCollectionModal').modal('hide');
                        console.log('rerer');
                        filter();
                    },
                    error: function(err){
                        console.log(err.responseText);
                    }
                });
            });
    </script>

@endsection
