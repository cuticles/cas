@foreach($dates as $date => $logs)
	<div class="ui segments logs" id="{{$date}}">
		<div class="ui segment">
			{{Carbon\Carbon::parse($date)->format('F j, Y , l')}}
		</div>
  	@foreach($logs as $log)
	    <div class="ui segment singleLog" id="{{$log->id}}">
					@if( $log->action == '1')
					@if( $log->type == '1')
					 	You sent a collection,  <a href="/collection/{{ $log->collection->id }}" class="semibold " id="title{{ $log->id }}">
                            {{ $log->collection->title }}</a> to <a href="/personnelUtil/{{ $log->users->id }}" class="semibold" id="title{{ $log->id }}">{{$log->users->firstname." ".$log->users->middlename." ".$log->users->surname}}</a>.
                    @elseif($log->type == '3')
   						You added the credential, <a href="/personnelUtil/{{ $log->users->id }}" class="semibold" id="title{{ $log->id }}"> {{$log->credential->title}} </a> of <a href="/personnelUtil/{{ $log->users->id }}" class="semibold" id="title{{ $log->id }}">{{ $log->users->firstname." ".$log->users->middlename." ".$log->users->surname}}</a> from <a href="/personnelUtil/{{ $log->users->id }}" class="semibold" id="title{{ $log->id }}">{{$log->users->location->name}}</a>.   
                    @elseif( $log->type =='5')
					    You added new personnel to the  <a href="/group/{{$log->content_root}}" class="semibold" id="title{{ $log->id }}">{{$log->location->name}}</a>.
					@elseif( $log->type =='4')
   						You forwarded the collection <a href="/collection/{{ $log->collection->id }}" class="semibold" id="title{{ $log->id }}">{{$log->collection->title}}</a> to . 
   							<span class="semibold">{{ $log->users->firstname." ".$log->users->middlename." ".$log->users->surname}}</span> from <span class="semibold">{{ $log->users->location->name}}</span>.
   					@elseif( $log->type =='6')
   						You forwarded the collection <a href="/collection/{{ $log->collection->id }}" class="semibold" id="title{{ $log->id }}">{{$log->collection->title}}</a> to a group.

   					@elseif($log->type == '8')
   						You added an educational background, <a href="/personnelUtil/{{ $log->users->id }}/Profile" class="semibold" id="title{{ $log->id }}"> {{$log->background->type->name}} level</a> of <a href="/personnelUtil/{{ $log->user->id }}" class="semibold" id="title{{ $log->id }}">{{ $log->user->firstname." ".$log->user->middlename." ".$log->user->surname}}</a>.				   										   					
                    @else
   						You added the document <a href="/collection/{{ $log->rootColl->id }}" class="semibold" id="title{{ $log->id }}">{{$log->document->file_name}}</a>,  to the collection
	   					<a href="/collection/{{ $log->rootColl->id }}" class="semibold" id="title{{ $log->id }}">
                            {{ $log->rootColl->title }}</a>. 
					
					 @endif
				@elseif( $log->action =='2')
					@if( $log->type == '1')
   						You updated the collection, <a href="/collection/{{ $log->collection->id }}" class="semibold" id="title{{ $log->id }}">{{$log->collection->title}}</a>.
					@elseif( $log->type == '2')
   						You renamed the document<span class="semibold">{{$log->prev_file_name}}</span>, <a href="/collection/{{ $log->document->id }}" class="semibold" id="title{{ $log->id }}"> {{$log->document->file_name}}</a> from the collection <a href="/collection/{{ $log->rootColl->id }}" class="semibold" id="title{{ $log->id }}">{{$log->collection->title}}</a>. 
   					@elseif ($log->type == '3')
   						You updated the credential, <a href="/personnelUtil/{{ $log->content_id }}" class="semibold" id="title{{ $log->id }}">{{$log->credential->title}}</a> of <a href="/personnelUtil/{{ $log->user->id }}" class="semibold" id="title{{ $log->id }}">{{ $log->users->firstname." ".$log->users->middlename." ".$log->users->surname}}</a> from {{$log->users->location->name}}.
   					@elseif( $log->type == '5')
   						You updated the information of personnel, <a href="/personnelUtil/{{ $log->user->id }}" class="semibold" id="title{{ $log->id }}" data-tooltip="see info">{{ $log->user->firstname." ".$log->user->middlename." ".$log->user->surname}}</a> from <a href="/personnelUtil/{{ $log->user->id }}" class="semibold" id="title{{ $log->id }}">{{$log->user->location->name}}</a>.
					
   					@elseif( $log->type == '8')
   						You updated the tracking status of collection, <a href="/collection/{{ $log->collection->id }}" class="semibold" id="title{{ $log->id }}">{{$log->collection->title}}</a> to <span class="semibold">{{$log->collection->status->name}}</span>.
   					@elseif($log->type == '9')
   						@if( $log->content_root == '0')
   							You verified the appeal <a href="/appeals/{{ $log->content_id }}" class="semibold" id="title{{ $log->id }}">{{ $log->appeal->background->type->name}} </a>
   							of <a href="/personnelUtil/{{ $log->user->id }}" class="semibold" id="title{{ $log->id }}" data-tooltip="see info">{{ $log->appeal->background->user->firstname." ".$log->appeal->background->user->middlename." ".$log->appeal->background->user->surname}}</a>.
   						@elseif($log->content_root == '1')
   							You undo the verifying of the appeal <a href="/appeals/{{ $log->content_id }}" class="semibold" id="title{{ $log->id }}">{{ $log->appeal->background->type->name}} </a>
   							of <a href="/personnelUtil/{{ $log->user->id }}" class="semibold" id="title{{ $log->id }}" data-tooltip="see info">{{ $log->appeal->background->user->firstname." ".$log->appeal->background->user->middlename." ".$log->appeal->background->user->surname}}</a>.
   						@endif
					 
					@endif
				@elseif($log->action =='3')
					@if( $log->type == '1')
   						You trashed the collection, <a href="/trash" class="semibold" id="title{{ $log->id }}">{{$log->collection->title}}</a>. 
   					@else
   						You trashed <span class="semibold">{{ $log->personnel->firstname." ".$log->personnel->middlename." ".$log->personnel->surname}}</span> from {{$log->location->name}}.
					@endif
				@elseif($log->action =='4')
   					@if($log->type == '6')
   						You deleted 
   						@if($log->content_root == '1')
   							an Outsider.
   						@elseif($log->content_root == '2')
   							a Location.
   						@elseif($log->content_root == '3')
   							a Type.
   						@elseif($log->content_root == '4')
   							a Point.
   						@elseif($log->content_root == '5')
   							a Position.
   						@endif
   					@elseif($log->type == '3')
   						You deleted the credential <a href="/personnelUtil/{{ $log->content_id }}/Credential" class="semibold" id="title{{ $log->id }}">{{ $log->credential }} </a> of <a href="/personnelUtil/{{ $log->content_id }}/Credential" class="semibold" id="title{{ $log->id }}">{{ $log->user->firstname." ".$log->user->middlename." ".$log->user->surname}}</a>.
   					@elseif( $log->type =='5')
   						You removed <span class="semibold">{{ $log->user->firstname." ".$log->user->middlename." ".$log->user->surname}}</span> as a member of  <a href="/group/{{ $log->content_root}}" class="semibold" id="title{{ $log->id }}"> {{ $log->group['name']}}.</a>


   					@endif
   				@elseif($log->action =='5')
   						You restored the collection,<span class="semibold"> {{$log->collection->title}}</span>. 
   				@elseif($log->action =='6')
   						You downloaded the document,<a class="semibold" href="/documents/pdf-document/{{$log->content_id}}" > {{$log->document->file_name}}</a>. 	
				@endif
				
			 <div class="meta">
				<span>{{Carbon\Carbon::parse($log->created_at)->format('g:ia')}}
			 	 {{$log->created_at->diffForHumans()}}</span>
			 </div>
   		</div>
	@endforeach

</div>
@endforeach

@if($logCount >= $numItems)
	<div class="seemore" id="seemore" numItems="{{ $numItems }}" count="{{$logCount}}"><span data-tooltip="See more" data-inverted=""><img src="{{ asset('/images/arrow.png')}}" id="arrow_scrolldown" ></span> </div>

@endif
</div>
<script type="text/javascript">
	$('.seemore').click(function(){
		var numItems = $(this).attr('numItems');
		var num = $(this).attr('count');
		var id = $('.logs').attr('id');
		var log =  $('.singleLog').attr('id');
	        $.ajaxSetup({
	            headers: {
	                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	            }
	        });
	        $.ajax({
	            type: "POST",
	            url: "/logs/more",
	            data:{
	            	numItems: numItems ,
	            },
	            success: function(logs){
        			$('#logs-grid').empty();
        			$('#logs-grid').append(logs);
					$('#logs-grid').transition('pulse');
					
	                console.log(num,numItems);
	                
	            },
	            error: function(err){
	                console.log(err.responseText);
	            }
	        });
    });

</script>