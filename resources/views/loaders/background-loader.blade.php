<div class="ui segment" id="background{{ $background->id }}">
	<div class="ui stackable horizontally divided grid">
		<div class="nine wide column">
			<div class="ui basic label" id="bgTypeName{{ $background->id }}">
				{{ ucfirst($background->type->name) }}
			</div>
			<div class="marginalized big">
				Started school at <span class="semibold" id="bgSchool{{ $background->id }}"> {{ $background->school }}</span>
			</div>
			<div id="courseCont{{ $background->id }}">
					@if($background->type->name == 'tertiary')
						<div class="marginalized">
							<span id="bgDegLevel{{ $background->id }}">{{ $degLevels[$background->degree_level] }}</span> Degree in 
							<span class="semibold" id="bgCourse{{ $background->id }}">
								{{ $background->course }}
							</span>
						</div>
					@endif
			</div>
			<div class="small-grey-margin marginalized">
				<i class="graduation cap icon"></i>
				<span id="bgYearStart{{ $background->id }}">{{ $background->year_start}}</span> - 
				<span id="bgYearEnd{{ $background->id }}">
					@if($background->year_end == null)
						Present
					@else
						{{ $background->year_end}}
					@endif
				</span>
			</div>
		</div>
		<div class="seven wide column bg-cont-relative">
			<div class="ui floating dropdown background-drpdwn" id="dropdownBg{{ $background->id }}">
                <i class="ellipsis vertical icon options-icon" background-id="{{ $background->id }}"></i>
                <div class="menu">
                    <div class="item edit-background" background-school="{{ $background->school }}" background-type-id="{{ $background->type_id }}" background-type-name="{{ ucfirst($background->type->name) }}" background-year-start="{{ $background->year_start }}" background-year-end="{{ $background->year_end }}" background-degree-level="{{ $background->degree_level }}" background-course="{{ $background->course }}" background-degree-level-name="@if($background->type->name == 'tertiary') {{ $degLevels[$background->degree_level] }} @endif" background-id="{{ $background->id }}" id="editBackground{{ $background->id }}">
                        <i class="edit icon"></i>
                        Edit
                    </div>
                    <div class="item delete-background" background-id="{{ $background->id }}">
                        <i class="trash alternate icon"></i>
                        Delete
                    </div>
                </div>
            </div>
			<div class="marginalized">
				Managed by
				<img src="{{ asset($background->appeal->user->profile_pic) }}" class="ui avatar image">
				<span class="semibold">
					{{ $background->appeal->user->firstname.' '.$background->appeal->user->middlename.' '.$background->appeal->user->surname }}
				</span>
			</div>
			<div class="marginalized">
				<div class="ui basic label">
					@if($background->appeal->is_verified == '1')
						<i class="plus icon"></i>
						Verified
					@else
						<i class="times icon"></i>
						Not yet verified
					@endif
				</div>
			</div>
		</div>
	</div>
</div>