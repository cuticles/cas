@foreach($users as $user)
	<div class="padding-5">
		<img class="ui avatar image" src="{{ asset($user->profile_pic) }}">
		<span class="semibold">
			{{ $user->name }}
		</span>
	</div>
	<div class="ui divider"></div>
@endforeach