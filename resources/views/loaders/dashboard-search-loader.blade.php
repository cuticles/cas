@php
    function formatBytes($size, $precision = 2)
    {
        $base = log($size, 1024);
        $suffixes = array('', 'KB', 'MB', 'GB', 'TB');   

        return round(pow(1024, $base - floor($base)), $precision) .' '. $suffixes[floor($base)];
    }
@endphp

@if($searchBy == 'documents')
	@foreach($items as $doc)
		<div class="row">
			<div class="two wide column dashboard-icon-cont">
				<i class="{{ $icons[$doc->file_extension] }} dashboard-icon"></i>
			</div>
			<div class="seven wide column">
				<h3 class="ui header">{{ $doc->file_name }}</h3>
				<h4 class="ui header">
					Collection: {{ $doc->coll_title }}
				</h4>
				<div class="small-text grey-color">
					{{ ucfirst($doc->file_extension).' | '.formatBytes($doc->file_size) }}
				</div>
			</div>
			<div class="five wide column">
				<img class="ui avatar image" src="{{ $doc->owner_profile_pic }}">
				<span class="semibold">{{ $doc->sender_name }}</span>
			</div>
			<div class="two wide column">
				<div class="date-grey">
					<i class="calendar outline icon"></i>
					{{ Carbon\Carbon::parse($doc->created_at)->format('M d') }}
				</div>
			</div>
		</div>
	@endforeach
@else
	@foreach($items as $coll)
		<div class="row">
			<div class="two wide column">
				<img class="ui centered tiny image" src="{{ asset($coll_icons[$coll->type_name]) }}">
			</div>
			<div class="seven wide column">
				<h3 class="ui header">{{ $coll->title }}</h3>
				<div class="ui label">
					{{ ucfirst($coll->status->name) }}
				</div>
			</div>
			<div class="five wide column">
				<img class="ui avatar image" src="{{ $coll->owner_profile_pic }}">
				<span class="semibold">{{ $coll->sender_name }}</span>
			</div>
			<div class="two wide column">
				<div class="date-grey">
					<i class="calendar outline icon"></i>
					{{ Carbon\Carbon::parse($coll->created_at)->format('M d') }}
				</div>
			</div>
		</div>
	@endforeach
@endif