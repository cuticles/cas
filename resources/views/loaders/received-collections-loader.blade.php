@foreach($collections as $collection)
    <div class="row coll-item">
        <div class="two wide column">
            <img class="ui tiny image" src="{{ asset($icons[$collection->type->name]) }}" id="type{{ $collection->id }}">
        </div>
        <div class="seven wide column">
            <div class="mg-bottom-12">
                <div class="ui basic horizontal label status-label">
                    {{ ucfirst($collection->status->name)}}
                </div>
                <span class="meta">
                    {{ $collection->created_at->diffForHumans() }}
                </span>
            </div>
            <a href="/collection/{{ $collection->id }}" class="header title-collection" id="title{{ $collection->id }}">
                {{ $collection->title }}
            </a>
        </div>
        <div class="six wide column">
            <div>
                <i class="paperclip icon"></i>
                <span class="margin-r-10 semibold">{{ count($collection->documents) }}</span>
                Attached documents
            </div>
            <div class="margin-top-10 sender-info">
                <span class="received-from">received from</span> 
                @if($collection->from_outside == 0)
                    <img class="ui avatar image" src="{{ asset($collection->sender->profile_pic) }}">
                    <a href="#user" class="fullname-link">
                        {{$collection->sender->firstname." ".$collection->sender->middlename." ".$collection->sender->surname }}
                    </a>
                @else
                    <span class="semibold">
                        {{ $collection->outside_sender }}
                    </span>
                    <div class="margin-top-10">
                        <span class="received-from">uploaded by</span> 
                        <img class="ui avatar image" src="{{ asset($collection->uploader->profile_pic) }}">
                        <a href="#user" class="fullname-link">
                            {{$collection->uploader->firstname." ".$collection->uploader->middlename." ".$collection->uploader->surname }}
                        </a>
                    </div>
                @endif
            </div>
        </div>
        <div class="one wide column coll-item">
            <div id="{{ $collection->id }}" class="ui floating dropdown options-dropdown-coll">
                <i class="ellipsis vertical icon options-icon" document-id="{{ $collection->id }}"></i>
                <div class="menu">
                    <div class="item trash-coll" coll-title="{{ $collection->title }}" coll-id="{{ $collection->id }}">
                        <i class="trash alternate icon"></i>
                        Trash
                    </div>
                </div>
            </div>
        </div>
    </div>
@endforeach