@php
    function formatBytes($size, $precision = 2)
    {
        $base = log($size, 1024);
        $suffixes = array('', 'KB', 'MB', 'GB', 'TB');   

        return round(pow(1024, $base - floor($base)), $precision) .' '. $suffixes[floor($base)];
    }
@endphp
@foreach($docs as $doc)
    <tr>
        <td>
            <i class="{{ $icons[$doc->file_extension]}} red table-icon"></i>
        </td>
        <td>
            <span class="semibold">{{ $doc->file_name }}</span>
        </td>
        <td>
            {{ formatBytes($doc->file_size) }}
        </td>
        <td>
            <img class="ui avatar image" src="{{ asset($doc->collection->receiver->profile_pic) }}">
            {{ $doc->collection->receiver->firstname." ".$doc->collection->receiver->surname }}
        </td>
        <td>
            {{ Carbon\Carbon::parse($doc->created_at)->format('M d') }}
        </td>
        <td>
        </td>
    </tr>
@endforeach