@if($categ == 'outsiders')
	<div class="row" id="outsider{{ $obj->id }}">
		<div class="five wide column semibold" id="outsiderName{{ $obj->id }}">
			{{ $obj->name }}
		</div>
		<div class="six wide column" id="outsiderLocation{{ $obj->id }}">
			{{ $obj->location->name }}
		</div>
		<div class="four wide column">
			<img class="ui avatar image" src="{{ asset($obj->adder->profile_pic) }}">
			{{ $obj->adder->firstname.' '.$obj->adder->middlename[0].'. '.$obj->adder->surname}}
		</div>
		<div class="one wide column">
			<div class="ui floating dropdown icon options-dropdown">
                <i class="ellipsis vertical icon options-icon"></i>
                <div class="menu">
                    <div class="item edit" id="editOutsider{{ $obj->id }}" outsider-id="{{ $obj->id }}" outsider-name="{{ $obj->name }}" outsider-location="{{ $obj->location->id}}" outsider-location-name="{{ $obj->location->name }}" category="outsiders">
                        <i class="edit icon"></i>
                        Edit
                    </div>
                    <div class="item delete" outsider-name="{{ $obj->name }}" outsider-id="{{ $obj->id }}" category="outsiders">
                        <i class="trash alternate icon"></i>
                       Delete
                    </div>
                </div>
            </div>
		</div>
	</div>
@elseif($categ == 'locations')
	<div class="row" id="location{{ $obj->id }}">
		<div class="eight wide column semibold" id="locationName{{ $obj->id }}">
			{{ $obj->name }}
		</div>
		<div class="seven wide column">
			<img class="ui avatar image" src="{{ asset($obj->adder->profile_pic) }}">
			{{ $obj->adder->firstname.' '.$obj->adder->middlename[0].'. '.$obj->adder->surname}}
		</div>
		<div class="one wide column">
			<div class="ui floating dropdown icon options-dropdown">
                <i class="ellipsis vertical icon options-icon"></i>
                <div class="menu">
                    <div class="item edit" id="editLocation{{ $obj->id }}" location-id="{{ $obj->id }}" location-name="{{ $obj->name }}" category="locations">
                        <i class="edit icon"></i>
                        Edit
                    </div>
                    <div class="item delete" location-id="{{ $obj->id }}" location-name="{{ $obj->name }}" category="locations">
                        <i class="trash alternate icon"></i>
                       Delete
                    </div>
                </div>
            </div>
		</div>
	</div>
@elseif($categ == 'types')
	<div class="row" id="type{{ $obj->id }}">
		<div class="five wide column semibold" id="typeName{{ $obj->id }}">
			{{ $obj->name }}
		</div>
		<div class="six wide column">
			<img class="ui avatar image" src="{{ asset($obj->adder->profile_pic) }}">
			{{ $obj->adder->firstname.' '.$obj->adder->middlename[0].'. '.$obj->adder->surname}}
		</div>
		<div class="four wide column" id="typeCategory{{ $obj->id }}">
			{{ $category[$obj->category] }}
		</div>
		<div class="one wide column">
			<div class="ui floating dropdown icon options-dropdown">
                <i class="ellipsis vertical icon options-icon"></i>
                <div class="menu">
                    <div class="item edit" id="editType{{ $obj->id }}" type-id="{{ $obj->id }}" type-name="{{ $obj->name }}" type-category="{{ $obj->category }}" category="types">
                        <i class="edit icon"></i>
                        Edit
                    </div>
                    <div class="item delete" type-id="{{ $obj->id }}" type-name="{{ $obj->name }}" category="types">
                        <i class="trash alternate icon"></i>
                       Delete
                    </div>
                </div>
            </div>
		</div>
	</div>
@elseif($categ == 'points')
	<div class="row" id="point{{ $obj->id }}">
		<div class="eight wide column semibold" id="pointName{{ $obj->id }}">
			{{ $obj->name }}
		</div>
		<div class="three wide column" id="pointValue{{ $obj->id }}">
			{{ $obj->value }}
		</div>
		<div class="four wide column">
			<img class="ui avatar image" src="{{ asset($obj->adder->profile_pic) }}">
			{{ $obj->adder->firstname.' '.$obj->adder->middlename[0].'. '.$obj->adder->surname}}
		</div>
		<div class="one wide column">
			<div class="ui floating dropdown icon options-dropdown">
                <i class="ellipsis vertical icon options-icon"></i>
                <div class="menu">
                    <div class="item edit" id="editPoint{{ $obj->id }}" point-id="{{ $obj->id }}" point-name="{{ $obj->name }}" point-value="{{ $obj->value }}" category="points">
                        <i class="edit icon"></i>
                        Edit
                    </div>
                    <div class="item delete" point-name="{{ $obj->name }}" point-id="{{ $obj->id }}" category="points">
                        <i class="trash alternate icon"></i>
                       Delete
                    </div>
                </div>
            </div>
		</div>
	</div>
@elseif($categ == 'positions')
	<div class="row" id="position{{ $obj->id }}">
		<div class="five wide column semibold" id="positionName{{ $obj->id }}">
			{{ $obj->name }}
		</div>
		<div class="six wide column">
			<img class="ui avatar image" src="{{ asset($obj->adder->profile_pic) }}">
			{{ $obj->adder->firstname.' '.$obj->adder->middlename[0].'. '.$obj->adder->surname}}
		</div>
		<div class="four wide column" id="positionCategory{{ $obj->id }}">
			{{ $category[$obj->category] }}
		</div>
		<div class="one wide column">
			<div class="ui floating dropdown icon options-dropdown">
                <i class="ellipsis vertical icon options-icon"></i>
                <div class="menu">
                    <div class="item edit" id="editPosition{{ $obj->id }}" position-id="{{ $obj->id }}" position-name="{{ $obj->name }}" position-category="{{ $obj->category }}" category="positions">
                        <i class="edit icon"></i>
                        Edit
                    </div>
                    <div class="item delete" position-id="{{ $obj->id }}" position-name="{{ $obj->name }}" category="positions">
                        <i class="trash alternate icon"></i>
                       Delete
                    </div>
                </div>
            </div>
		</div>
	</div>
@endif