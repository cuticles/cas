@foreach($forwards as $forward)
	<div class="row">
		<div class="seven wide column">
			<a class="collection-link" href="/collection/{{ $forward->collection->id }}">
				<span class="semibold">{{ $forward->collection->title }}</span>
			</a>
		</div>
		<div class="five wide column">
			<img class="ui avatar image" src="@if($type == 'sent') {{ asset($forward->receiver->profile_pic) }} @else {{ asset($forward->sender->profile_pic) }} @endif">
			<span class="semibold">
				@if($type == 'sent')
					{{ $forward->receiver->firstname." ".$forward->receiver->middlename." ".$forward->receiver->surname }}
				@else
					{{ $forward->sender->firstname." ".$forward->sender->middlename." ".$forward->sender->surname }}
				@endif
			</span>
			@if($type == 'sent')
				@if(count($forward->collection->transmitsForwards) > 1)
					<span class="smaller-grey-margin"> and </span>
					<a class="ui label view-receivers" href="#receivers" coll-id="{{ $forward->collection->id }}" forward-id="{{ $forward->id }}">
						{{ count($forward->collection->transmitsForwards)-1 }} more
						<div class="ui mini inline loader" id="loader{{ $forward->id }}"></div>
					</a>
				@endif
			@endif
		</div>
		<div class="three wide column">
			{{ ucfirst($forward->collection->current_status) }}
		</div>
		<div class="one wide column">
			<div id="{{ $forward->id }}" class="ui floating dropdown icon">
                <i class="ellipsis vertical icon options-icon" forward-id="{{ $forward->id }}"></i>
                <div class="menu">
                    <div class="item delete-forward" coll-id="{{ $forward->collection->id }}">
                        <i class="trash alternate icon"></i>
                        Trash
                    </div>
                </div>
            </div>
		</div>
	</div>	
@endforeach
