@foreach ($credentials as $credential)
			<div class = "ui segment" id = "{{$credential->id}}">
				<div class = "ui column grid">
					<div class = "row first_row">
					    <div class = "three wide column"> 
					    	<div class = "ui basic large label" id = "category{{$credential->id}}">
					    		{{$credential->point->name}} 
					    	</div>
					    </div>
					    <div class = "four wide column" id = "date_updated"> 
					    	{{$credential->updated_at->diffForHumans()}}  
					    </div>
					    <div class = "six wide column" id = "updater_col"> 
					    	Updated by 
					    	<a href = "/personnelUtil/{{$credential->userUpdater['id']}}" id = "updater_label">
						    	<img class = "ui avatar image" src="/{{$credential->userUpdater->profile_pic}}"> 
						    	{{$credential->userUpdater->firstname.' '.$credential->userUpdater->surname}} 
						    </a>
					    </div>
					    <div class = "three wide column" id = "cred_btn_grp">
					    	@if ($credential->updater == Auth::user()->id)
								<a class = "fas fa-edit fa-lg edit_cred" id="credentialEdit{{ $credential->id }}" cred-id = "{{ $credential->id }}" aria-hidden = "true" title = "Edit Credential" cred-title="{{ $credential->title }}" cred-category-point="{{ $credential->point->id }}" cred-category ="{{ $credential->point->name }}" cred-description="{{ $credential->description }}" cred-update = "{{$credential->updated_at}}"> </a> 

								<a class = "fa fa-trash fa-lg delete_cred" cred-id = "{{$credential->id}}" aria-hidden = "true" title = "Delete Credential">   </a>
								
							@endif
								<a class = "fas fa-eye fa-lg view_cred" aria-hidden = "true" title = "View Credential" id="credential{{ $credential->id }}" cred-id = "{{ $credential->id }}" aria-hidden = "true" cred-title="{{ $credential->title }}" cred-description="{{ $credential->description }}" cred-updater-first = "{{ $credential->userUpdater->firstname }}"  cred-updater-last = "{{ $credential->userUpdater->surname }}" cred-date = "{{ $credential->updated_at->diffForHumans() }}" cred-profile-pic = "/{{$credential->userUpdater->profile_pic}}" cred-category="{{ $credential->point->name }}">   </a> 
							
					    </div>
				  	</div>  <!-- FIRST ROW -->

				 	<div class = "row" id = "second_row">
				  		<div class = "seven wide column cred_title_view" id = "title{{ $credential->id }}">
				  			{{$credential->title}}
				  		</div>

				  		<!-- Document Count -->
				  		<div class = "three wide column" id = "count_files">
				  			<span id="numFiles{{ $credential->id }}">{{count($credential->documents)}}</span> files
				  		</div>
				  	
				  	</div>  <!-- SECOND ROW -->

				</div> <!-- COLUMN GRID CLOSING TAG -->
			</div>
			@endforeach