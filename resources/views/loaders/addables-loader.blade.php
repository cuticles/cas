@if($categ == 'outsiders')
	@foreach($data as $outsider)
		<div class="row" id="outsider{{ $outsider->id }}">
			<div class="five wide column semibold" id="outsiderName{{ $outsider->id }}">
				{{ $outsider->name }}
			</div>
			<div class="six wide column" id="outsiderLocation{{ $outsider->id }}">
				{{ $outsider->location->name }}
			</div>
			<div class="four wide column">
				<img class="ui avatar image" src="{{ asset($outsider->adder->profile_pic) }}">
				{{ $outsider->adder->firstname.' '.$outsider->adder->middlename[0].'. '.$outsider->adder->surname}}
			</div>
			<div class="one wide column">
				<div class="ui floating dropdown icon options-dropdown">
                    <i class="ellipsis vertical icon options-icon"></i>
                    <div class="menu">
                        <div class="item edit" id="editOutsider{{ $outsider->id }}" outsider-id="{{ $outsider->id }}" outsider-name="{{ $outsider->name }}" outsider-location="{{ $outsider->location->id}}" outsider-location-name="{{ $outsider->location->name }}" category="outsiders">
                            <i class="edit icon"></i>
                            Edit
                        </div>
                        <div class="item delete" outsider-name="{{ $outsider->name }}" outsider-id="{{ $outsider->id }}" category="outsiders">
                            <i class="trash alternate icon"></i>
                           Delete
                        </div>
                    </div>
                </div>
			</div>
		</div>
	@endforeach
@elseif($categ == 'locations')
	@foreach($data as $location)
		<div class="row" id="location{{ $location->id }}">
			<div class="eight wide column semibold" id="locationName{{ $location->id }}">
				{{ $location->name }}
			</div>
			<div class="seven wide column">
				<img class="ui avatar image" src="{{ asset($location->adder->profile_pic) }}">
				{{ $location->adder->firstname.' '.$location->adder->middlename[0].'. '.$location->adder->surname}}
			</div>
			<div class="one wide column">
				<div class="ui floating dropdown icon options-dropdown">
                    <i class="ellipsis vertical icon options-icon"></i>
                    <div class="menu">
                        <div class="item edit" id="editLocation{{ $location->id }}" location-id="{{ $location->id }}" location-name="{{ $location->name }}" category="locations">
                            <i class="edit icon"></i>
                            Edit
                        </div>
                        <div class="item delete" location-id="{{ $location->id }}" location-name="{{ $location->name }}" category="locations">
                            <i class="trash alternate icon"></i>
                           Delete
                        </div>
                    </div>
                </div>
			</div>
		</div>
	@endforeach
@elseif($categ == 'types')
	@foreach($data as $type)
		<div class="row" id="type{{ $type->id }}">
			<div class="five wide column semibold" id="typeName{{ $type->id }}">
				{{ $type->name }}
			</div>
			<div class="six wide column">
				<img class="ui avatar image" src="{{ asset($type->adder->profile_pic) }}">
				{{ $type->adder->firstname.' '.$type->adder->middlename[0].'. '.$type->adder->surname}}
			</div>
			<div class="four wide column" id="typeCategory{{ $type->id }}">
				{{ $category[$type->category] }}
			</div>
			<div class="one wide column">
				<div class="ui floating dropdown icon options-dropdown">
                    <i class="ellipsis vertical icon options-icon"></i>
                    <div class="menu">
                        <div class="item edit" id="editType{{ $type->id }}" type-id="{{ $type->id }}" type-name="{{ $type->name }}" type-category="@if($type->is_collection_type == '1') collection @else tracking @endif" category="types">
                            <i class="edit icon"></i>
                            Edit
                        </div>
                        <div class="item delete" type-id="{{ $type->id }}" type-name="{{ $type->name }}" category="types">
                            <i class="trash alternate icon"></i>
                           Delete
                        </div>
                    </div>
                </div>
			</div>
		</div>
	@endforeach
@elseif($categ == 'points')
	@foreach($data as $point)
		<div class="row" id="point{{ $point->id }}">
			<div class="five wide column semibold" id="outsiderName{{ $point->id }}">
				{{ $point->name }}
			</div>
			<div class="six wide column" id="outsiderLocation{{ $point->id }}">
				{{ $point->value }}
			</div>
			<div class="four wide column">
				<img class="ui avatar image" src="{{ asset($point->adder->profile_pic) }}">
				{{ $point->adder->firstname.' '.$point->adder->middlename[0].'. '.$point->adder->surname}}
			</div>
			<div class="one wide column">
				<div class="ui floating dropdown icon options-dropdown">
                    <i class="ellipsis vertical icon options-icon"></i>
                    <div class="menu">
                        <div class="item edit" id="editPoint{{ $point->id }}" point-id="{{ $point->id }}" point-name="{{ $point->name }}" point-value="{{ $point->value }}" category="points">
                            <i class="edit icon"></i>
                            Edit
                        </div>
                        <div class="item delete" point-name="{{ $point->name }}" point-id="{{ $point->id }}" category="points">
                            <i class="trash alternate icon"></i>
                           Delete
                        </div>
                    </div>
                </div>
			</div>
		</div>
	@endforeach
@elseif($categ == 'positions')
	@foreach($data as $position)
		<div class="row" id="position{{ $position->id }}">
			<div class="five wide column semibold" id="typeName{{ $position->id }}">
				{{ $position->name }}
			</div>
			<div class="six wide column">
				<img class="ui avatar image" src="{{ asset($position->adder->profile_pic) }}">
				{{ $position->adder->firstname.' '.$position->adder->middlename[0].'. '.$position->adder->surname}}
			</div>
			<div class="four wide column" id="positionCategory{{ $position->id }}">
				{{ $category[$position->category] }}
			</div>
			<div class="one wide column">
				<div class="ui floating dropdown icon options-dropdown">
                    <i class="ellipsis vertical icon options-icon"></i>
                    <div class="menu">
                        <div class="item edit" id="editPosition{{ $position->id }}" position-id="{{ $position->id }}" position-name="{{ $position->name }}" position-category="{{ $position->category }}" category="positions">
                            <i class="edit icon"></i>
                            Edit
                        </div>
                        <div class="item delete" position-id="{{ $position->id }}" position-name="{{ $position->name }}" category="positions">
                            <i class="trash alternate icon"></i>
                           Delete
                        </div>
                    </div>
                </div>
			</div>
		</div>
	@endforeach
@endif