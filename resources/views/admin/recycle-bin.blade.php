@extends('layouts.admin')

@section('content')
<h1>Recycle Bin</h1>
    <div class="ui small modal" id="trashCollectionModal">
        <div class="header">
            Delete Collection
        </div>
        <div class="content">
            Are you sure you want to delete <span class="semibold" id="trashCollTitle"></span> permanently?
        </div>
        <div class="actions">
            <button class="ui black right labeled icon button" coll-id="" id="trashCollBtn">
                <i class="trash alternate outline icon"></i>
                Yes
            </button>
            <button class="ui deny button">No</button>
        </div>
    </div>
    <div class="ui small modal" id="restoreCollectionModal">
        <div class="header">
            Restore Collection
        </div>
        <div class="content">
            Are you sure you want to restore <span class="semibold" id="restoreCollTitle"></span> collection?
        </div>
        <div class="actions">
            <button class="ui black right labeled icon button restoreCollBtn" coll-id="" id="restoreCollBtn">
                <i class="trash alternate outline icon"></i>
                Yes
            </button>
            <button class="ui deny button">No</button>
        </div>
    </div>
    <div class="ui stackable vertically divided grid" id="collections-grid">
            @foreach($collections as $collection)
                <div class="row coll-item" id="collection{{$collection->id}}">
                    <div class="two wide column">
                        <img class="ui tiny image" src="{{ asset($icons[$collection->type->name]) }}" id="type{{ $collection->id }}">
                    </div>
                    <div class="seven wide column">
                        <div class="mg-bottom-12">
                            <span class="meta">
                                {{ $collection->created_at->format('F j, Y') }}
                            </span>
                        </div>
                        <a href="/collection/{{ $collection->id }}" class="header title-collection" id="title{{ $collection->id }}">
                            {{ $collection->title }}
                        </a>
                    </div>
                    <div class="six wide column">
                        <div>
                            <i class="paperclip icon"></i>
                            <span class="margin-r-10 semibold">{{ count($collection->documents) }}</span>
                            Attached documents
                        </div>
                    </div>
                    <div class="one wide column coll-item">
                        <div id="{{ $collection->id }}" class="ui floating dropdown options-dropdown-coll ">
                            <i class="ellipsis vertical icon options-icon" document-id="{{ $collection->id }}" ></i>
                            <div class="menu">
                                <div class="item restore-coll"  coll-id="{{ $collection->id }}" type="{{ $collection->type->name }}" coll-title="{{ $collection->title }}">
                                    <i class="fa-window-restore icon"></i>
                                    Restore
                                </div>
                                <div class="item trash-coll" coll-id="{{ $collection->id }}" coll-title="{{ $collection->title }}">
                                    <i class="trash alternate icon"></i>
                                    Delete
                                </div>
                            </div>
                        </div> 
                    </div>
                
                </div>
            @endforeach
    </div>

    <script type="text/javascript">

        $(document).ready(function(){
            $('.options-dropdown-coll').dropdown();
        });
        $('.trash-coll').each(function(){
            var id = $(this).attr('coll-id');
            var title = $(this).attr('coll-title');
            $(this).click(function(){
                $('#trashCollTitle').html(title);
                $('#trashCollBtn').attr('coll-id', id);
                $('#trashCollectionModal').modal('show');
            });
        });
        $('#trashCollBtn').click(function(){
                var id = $(this).attr('coll-id');
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: "/collection/trash/delete",
                    data: {
                        id: id,
                        type: 'sent'
                    },
                    success: function(coll){
                        $('#collection'+coll).remove();
                        $('#trashCollectionModal').modal('hide');
                    },
                    error: function(err){
                        console.log(err.responseText);
                    }
                });
            });
        $('.restore-coll').each(function(){
            var id = $(this).attr('coll-id');
            var title = $(this).attr('coll-title');
            $(this).click(function(){
                $('#restoreCollTitle').html(title);
                $('.restoreCollBtn').attr('coll-id', id);
                $('#restoreCollectionModal').modal('show');
            });
        });
        $('#restoreCollBtn').click(function(){
                var id = $(this).attr('coll-id');
                var type = $(this).attr('type');
                console.log(type);
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: "/collection/trash/restore",
                    data: {
                        id: id,
                        type:type,
                    },
                    success: function(coll){
                        $('#collection'+id).remove();
                        $('#restoreCollectionModal').modal('hide');
                    },
                    error: function(err){
                        console.log(err.responseText);
                    }
                });
            });
    </script>

@endsection
