@extends('layouts.admin')

@section('content')

<div>
	<h1>Notifications</h1>
	<div class="ui divided items">
		
	<div class="notif-cards-cont" id='notifs-grid'>
		@foreach ($notifications as $notification)
	   		<div class="ui segment @if( $notification->is_seen == 'false') notification @endif">
					<div class="content" id="{{$notification->id}}">
		   			@if($notification->type == '1')
		   				You received a <span class="semibold">collection</span>,  
		   					<a href="/collection/{{ $notification->object_id }}" class="semibold title-coll-notif myNotif" id="{{ $notification->id }}">
	                            {{ $notification->collection->title }}</a>, from <span class="semibold">
						 	<img class="ui avatar image" src="{{ asset($notification->sender->profile_pic) }}">
						 	{{ $notification->sender->firstname." ".$notification->sender->middlename." ".$notification->sender->surname }}
						 </span> of <span class="semibold">{{$notification->sender->location->name}}</span>.
					@elseif($notification->type == '2')
		   				<span class="semibold">
		   					<img class="ui avatar image" src="{{ asset($notification->sender->profile_pic) }}">
		   					{{ $notification->sender->firstname." ".$notification->sender->middlename." ".$notification->sender->surname }}</span> added <span class="semibold">document/s</span> to the collection, 
		   					<a href="/collection/{{ $notification->object_id }}" class="semibold title-coll-notif myNotif " id="{{ $notification->id }}">
	                            {{ $notification->collection->title }}</a>.
	                @elseif($notification->type == '3')
		   				@if($notification->object_id != $notification->to_user_id)
		   					<span class="semibold"> <img class="ui avatar image" src="{{ asset($notification->sender->profile_pic) }}">
		   					{{ $notification->sender->firstname." ".$notification->sender->middlename." ".$notification->sender->surname }}</span> added a credential, <a href="/personnelUtil/{{ $notification->to_user_id }}/Credentials" class="semibold title-coll-notif myNotif " id="{{ $notification->id }}">
		   						{{$notification->credential->title}}</a> to your credentials. 
		   				@else 
		   					<span class="semibold"> <img class="ui avatar image" src="{{ asset($notification->sender->profile_pic) }}">
		   					{{ $notification->sender->firstname." ".$notification->sender->middlename." ".$notification->sender->surname }}</span> updated your credential.
		   				@endif
		   				
					@elseif($notification->type == '4')
		   				<span class="semibold">
		   					<img class="ui avatar image" src="{{ asset($notification->sender->profile_pic) }}">
		   					{{ $notification->sender->firstname." ".$notification->sender->middlename." ".$notification->sender->surname }}</span> forwarded a <span class="semibold">collection</span>,  
		   					<a href="/collection/{{ $notification->object_id }}" class="semibold title-coll-notif myNotif " id="{{ $notification->id }}">
	                            {{ $notification->collection->title }}</a>.
	                
					@elseif($notification->type == '5')
		   					You are added as member in 
		   					<a href="/group/{{ $notification->object_id }}" class="semibold title-coll-notif myNotif " id="{{ $notification->id }}">
	                            {{$notification->group->name}} 
	                        </a>
						  by <img class="ui avatar image" src="{{ asset($notification->sender->profile_pic) }}">
						  <span class="semibold">{{ $notification->sender->firstname." ".$notification->sender->middlename." ".$notification->sender->surname }}</span>.
					@elseif($notification->type == '6')
		   					The collection
		   					<a href="/collection/{{ $notification->tracking->collection->id }}" class="semibold title-coll-notif myNotif" id="{{ $notification->id }}">
	                            {{$notification->tracking->collection->title}}
	                        </a> has been updated as <span class="semibold ">{{ $notification->tracking->status->name }}</span>
						  by <img class="ui avatar image" src="{{ asset($notification->sender->profile_pic) }}">
						  <span class="semibold">{{ $notification->sender->firstname." ".$notification->sender->middlename." ".$notification->sender->surname }}</span>.
					@elseif ($notification->type == '7')
	   					<span class="semibold">
	   						<img class="ui avatar image" src="{{ asset($notification->sender->profile_pic) }}">
	   						{{ $notification->sender->firstname." ".$notification->sender->middlename." ".$notification->sender->surname }} </span>
	   					 sent a <a class="semibold title-coll-notif  myNotif" id="{{$notification->id}}" href="/collection/{{ $notification->remark->collection->id }}">remark</a> for your collection, <a class="semibold title-coll-notif  myNotif" id="{{$notification->id}}" href="/collection/{{ $notification->remark->collection->id }}">{{ $notification->remark->collection->title }}</a>.
	   					 <div class="remark-content-notif">   <i>"{{ substr($notification->remark->content, 0, 100) }}..."</i></div>
	   				@elseif ($notification->type == '8')
	   					<span class="semibold title-coll-notif myNotif">
	   						<img class="ui avatar image" src="{{ asset($notification->sender->profile_pic) }}">
	   						{{ $notification->sender->firstname." ".$notification->sender->middlename." ".$notification->sender->surname }} </span> added on his backgrounds</div>.
	   				@elseif($notification->type == '9')
		   				<span class="semibold">
		   					<img class="ui avatar image" src="{{ asset($notification->sender->profile_pic) }}">
		   					{{ $notification->sender->firstname." ".$notification->sender->middlename." ".$notification->sender->surname }}</span> updated your <a href="/personnelUtil/{{ $notification->object_id }}" class="semibold title-coll-notif myNotif " id="{{ $notification->id }}" data-tooltip="See profile"> profile
	                            </a>.
		   			@elseif($notification->type == '10')
		   				<img class="ui avatar image" src="{{ asset($notification->sender->profile_pic) }}">
		   					{{ $notification->sender->firstname." ".$notification->sender->middlename." ".$notification->sender->surname }}</span> added your educational background in <a href="/personnelUtil/{{ $notification->receiver->id }}/Profile" class="semibold title-coll-notif myNotif" id="title{{ $notification->id }}"> {{$notification->background->type->name}} level.	</a>

		   			@endif
		   				<div class="time-diff-notif">
						 	<span> {{$notification->created_at->diffForHumans()}}</span>
						</div>
		   		</div>
			</div>
		@endforeach
			
		@if( $notifCount >=$numItems+1)
			<div class="seemore" numItems="{{ $numItems }}" count="{{$notifCount}}"><button>See more</button> </div>
		@endif
	</div>
<script type="text/javascript">
	$('.myNotif').click(function(){
        var id = $(this).attr('id');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url: "/notification/num",
            data: {
                id: id,
                
            },
            success: function(coll){
                console.log('yey'+ id);
            },
            error: function(err){
                console.log(err.responseText);
            }
        });
    });
	$('.seemore').click(function(){
		var numItems = $(this).attr('numItems');
		var num = $(this).attr('count');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url: "/notification/more",
            data:{
            	numItems: numItems
            },
            success: function(notifications){
            	$('#notifs-grid').empty();
                $('#notifs-grid').append(notifications);
                $('#notifs-grid').transition('pulse');
                console.log(num);
            },
            error: function(err){
                console.log(err.responseText);
            }
        });
    });

</script>
@endsection
 