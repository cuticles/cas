@extends('layouts.admin')

@section('content')
    @include('inc.modalforms')
    @include('inc.rename-form')
    <div class="ui container">
        <div id="viewDetailsModal" class="ui small modal">
            <div class="header">
                View Details
            </div>
            <div class="content" id="viewDetailsModalContent">
                <div class="ui active inverted dimmer">
                    <div class="ui text loader">Loading</div>
                </div>
            </div>
            <div class="actions">
                <button class="ui button cancel" id="viewDetailsClose">
                    Close
                </button>
            </div>
        </div>

        <div class="ui fullscreen modal" id="viewDocModal">
            <i class="close icon"></i>
            <div class="content">
                <embed src="" style="width:1200px; height:550px;" id="viewDocEmbed">
            </div>
        </div>

        <div class="ui stackable grid ">
            <div class="eight wide column send-rcv-btns">
                <button id="sendBtn" class="ui labeled icon red button test" data-tooltip="Send documents to anyone" data-position="bottom center">
                    <i class="paper plane outline icon"></i>
                    Send
                </button>
                <button id="receiveBtn" class="ui labeled icon red button test" data-tooltip="Receive documents from outside" data-position="bottom center">
                    <i class="cloud upload icon"></i>
                    Receive
                </button>
            </div>
            <div class="three wide column">
                <div class="ui fluid selection dropdown" id="searchby-dropdown">
                    <input type="hidden" name="type" value="file_name" id="search-by">
                    <i class="dropdown icon"></i>
                    <div class="default text">File Name</div>
                    <div class="menu">
                        <div class="item" data-value="file_name">File Name</div>
                        <div class="item" data-value="receiver">Receiver</div>
                        <div class="item" data-value="code">Code</div>
                    </div>
                </div>
            </div>
            <div class="five wide column">
                <div class="ui fluid left icon input">
                    <input type="text" id="search-box" placeholder="Search documents">
                    <i class="search icon"></i>
                </div>
            </div>
        </div>
        <!-- <div class="count-label-div tools-div">
            <div class="ui labeled button" tabindex="0">
                <div class="ui red button">
                    <i class="file alternate icon"></i> Total items
                </div>
                <a class="ui basic red left pointing label" id="totalItems">
                    {{ count($sentDocs)}}
                </a>
            </div>
        </div> -->
        <div class="ui stackable grid tools-div">
            <div class="five wide column">
                <label class="label-mg-right">Type</label>
                <div class="ui selection dropdown" id="type-dropdown">
                    <input type="hidden" value="all" name="type" id="file-type">
                    <i class="dropdown icon"></i>
                    <div class="default text">All</div>
                    <div class="menu">
                        <div class="item" data-value="all">All</div>
                        <div class="item" data-value="pdf">pdf</div>
                        <div class="item" data-value="docx">docx</div>
                        <div class="item" data-value="doc">doc</div>
                        <div class="item" data-value="rar">rar</div>
                        <div class="item" data-value="zip">zip</div>
                        <div class="item" data-value="pptx">pptx</div>
                        <div class="item" data-value="ppt">ppt</div>
                        <div class="item" data-value="txt">txt</div>
                    </div>
                </div>
            </div>
            <div class="nine wide column">
                <label class="label-mg-right">Sort by</label>
                <div class="ui selection dropdown" id="sort-dropdown">
                    <input type="hidden" value="created_at" name="type" id="sort-by">
                    <i class="dropdown icon"></i>
                    <div class="default text">Date added</div>
                    <div class="menu">
                        <div class="item" data-value="created_at">Date added</div>
                        <div class="item" data-value="file_name">File Name</div>
                        <div class="item" data-value="file_size">File Size</div>
                    </div>
                </div>

                <button class="circular ui icon button sort-btn" id="sort-button" sort-direction="desc">
                    <i class="arrow down icon" id="sort-icon"></i>
                </button>
            </div>
            <div class="two wide column">
                <div class="ui icon buttons">
                    <button class="ui button view-btn" view-type="grid"><i class="th icon"></i></button>
                    <button class="ui button view-btn" view-type="list"><i class="th list icon"></i></button>
                </div>
            </div>
        </div>

        <!-- <div class="ui secondary pointing menu">
            <a href="/sent" class="@if(Request::is('sent')) active @endif item red">
                <i class="file alternate icon"></i>
                Documents
            </a>
            <a href="/sent/collections" class="@if(Request::is('sent/collections')) active @endif item red">
                <i class="archive icon"></i>
                Collections
            </a>
        </div> -->
        @if(count($sentDocs) > 0)
        <div class="docs-container" id="docsContainer">
            @php
                function formatBytes($size, $precision = 2)
                {
                    $base = log($size, 1024);
                    $suffixes = array('', 'KB', 'MB', 'GB', 'TB');   

                    return round(pow(1024, $base - floor($base)), $precision) .' '. $suffixes[floor($base)];
                }
            @endphp
            <div class="ui four stackable cards" id="documents-grid">
                <div id="mask"></div>
                @foreach($sentDocs as $sentDoc)
                    <div class="ui fluid card">
                        <div class="content">
                            <div id="{{ $sentDoc->id }}" class="ui floating dropdown icon options-dropdown">
                                <i class="ellipsis vertical icon options-icon" document-id="{{ $sentDoc->id }}"></i>
                                <div class="menu">
                                    <div class="item rename-file" id="renameItem{{ $sentDoc->id }}" document-id="{{ $sentDoc->id }}" file-name="{{ $sentDoc->file_name }}">
                                        <i class="edit icon"></i>
                                        Rename
                                    </div>
                                    <div class="item view-document" path="/documents/pdf-document/{{$sentDoc->id}}">
                                        <i class="eye icon"></i>
                                       View Document
                                    </div>
                                    <div class="item view-details" document-id="{{ $sentDoc->id }}">
                                        <i class="history icon"></i>
                                        Details
                                    </div>
                                </div>
                            </div>
                            <div class="icon-container">
                                <i class="{{ $icons[$sentDoc->file_extension]}} extension-icon"></i>
                            </div>
                        </div>
                        <div class="extra content">
                            <div class="ui small header file-header" id="gridFileName{{ $sentDoc->id }}">
                                @if(strlen($sentDoc->file_name)>20)
                                    {{ substr($sentDoc->file_name, 0, 20) }}...
                                @else
                                    {{ $sentDoc->file_name }}
                                @endif
                            </div>
                            <div class="meta"> {{ formatBytes($sentDoc->file_size) }} | {{ $sentDoc->file_extension}} </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

        <div class="table-cont hidden" id="documentsTable">
            <div class="ui stackable vertically divided grid" id="documentsTableBody">
                @foreach($sentDocs as $doc)
                    <div class="row">
                        <div class="one wide column">
                            <i class="{{ $icons[$doc->file_extension]}} red table-icon"></i>
                        </div>
                        <div class="eight wide column">
                            <span class="semibold" id="listFileName{{ $doc->id }}">{{ $doc->file_name }}</span>
                        </div>
                        <div class="four wide column">
                            {{ formatBytes($doc->file_size) }}
                        </div>
                        <div class="two wide column">
                            {{ Carbon\Carbon::parse($doc->created_at)->format('M d') }}
                        </div>
                        <div class="one wide column">
                            <div id="row{{ $doc->id }}" class="ui floating dropdown icon options-dropdown">
                                <i class="ellipsis vertical icon options-icon" document-id="{{ $doc->id }}"></i>
                                <div class="menu">
                                    <div class="item rename-file" id="rename{{ $doc->id }}" document-id="{{ $doc->id }}" file-name="{{ $doc->file_name }}">
                                        <i class="edit icon"></i>
                                        Rename
                                    </div>
                                    <div class="item view-document" path="/documents/pdf-document/{{$doc->id}}">
                                        <i class="eye icon"></i>
                                       View Document
                                    </div>
                                    <div class="item view-details" document-id="{{ $doc->id }}">
                                        <i class="history icon"></i>
                                        Details
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        @else
            <div class="ui placeholder segment">
                <div class="ui icon header">
                    <i class="file outline icon"></i>
                    You don't have uploaded documents yet.
                </div>
            </div>
        @endif
        @if(count($sentDocs) > 0)
            <div class="pagination">
                <div class="ui stackable grid">
                    <div class="five wide column">
                        Show
                        <div class="ui selection dropdown receive-dropdown" id="showItems">
                            <input type="hidden" value="10" name="type" id="howManyItems">
                            <i class="dropdown icon"></i>
                            <div class="default text">3</div>
                            <div class="menu">
                                <div class="item" data-value="10">10 items</div>
                                <div class="item" data-value="20">20 items</div>
                                <div class="item" data-value="25">25 items</div>
                                <div class="item" data-value="50">50 items</div>
                            </div>
                        </div>
                    </div>
                    <div class="five wide column">
                        Page
                        <div class="ui selection dropdown receive-dropdown" id="paginate">
                            <input type="hidden" value="{{ '1:'.$partitions['1'] }}" name="type" id="page">
                            <i class="dropdown icon"></i>
                            <div class="default text" id="pageText"></div>
                            <div class="menu" id="pagesMenu">
                                @foreach($partitions as $index => $value)
                                    <div class="item" data-value="{{ $index.':'.$value }}">{{ $index }}</div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="six wide column">
                        <div class="showing">
                            Showing 
                            <span id="numOfItems">{{ count($sentDocs) }}</span> out of 
                            {{ $numItems }}
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
   
    <script type="text/javascript" src="{{ asset('js/tablesort.js') }}"></script>
    <script type="text/javascript">
        var fileIcons = {"pdf": "file pdf outline icon", "docx": "file word outline icon", "doc": "file word outline icon", "xls": "file excel outline icon", "xlsx": "file excel outline icon", "zip": "file archive outline icon", "rar": "file archive outline icon", "pptx": "file powerpoint outline icon", "ppt": "file powerpoint outline icon", "txt": "file alternate outline icon"};

        function humanFileSize(size) {
            var i = Math.floor( Math.log(size) / Math.log(1024) );
            return ( size / Math.pow(1024, i) ).toFixed(2) * 1 + ' ' + ['B', 'KB', 'MB', 'GB', 'TB'][i];
        }

        function initiateDynamicFunctions(){
            $('#viewDetailsTab .item').tab();

            //Dropdowns for documents
            $('.options-icon').each(function(){
                var id = "#"+$(this).attr('document-id');
                var docId = $(this).attr('document-id');
                $(id).dropdown({
                    transition: 'horizontal flip'
                });

                $('#row'+docId).dropdown();
            });

            //Viewing of a document when a dropdown item is clicked
             $('.view-document').click(function(){
                var path = $(this).attr('path');
                $('#viewDocEmbed').attr('src', path);
                $('#viewDocModal').modal('show');
            });

            //Renaming of a document
            $('.rename-file').each(function(){
                var docId = $(this).attr('document-id');
                var oldName = $(this).attr('file-name');
                var namesArr = oldName.split('.');
                var fileName = extractFileName(namesArr);
                var extension = namesArr[namesArr.length-1];

                $(this).click(function(){
                    $('#errorRename').empty();
                    $('#renameBtn').attr('document-id', docId);
                    $('#fileNameInput').val(fileName);
                    $('#fileNameInput').attr('old-name', fileName+'.'+extension);
                    $('#fileExtension').html('.'+extension);
                    $('#renameFileModal').modal('show');
                });
            });

            $('.view-details').each(function(){
                var id = $(this).attr('document-id');

                $(this).click(function(){
                    $('#viewDetailsModal').modal('show');

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url: "/doc/viewdetails",
                        data: {
                            id: id,
                            type: 'sent'
                        },
                        success: function(data){
                            $('#viewDetailsModalContent').empty();
                            $('#viewDetailsModalContent').append(data);
                            initiateDynamicFunctions();
                        },
                        error: function(err){
                            console.log(err.responseText);
                        }
                    });
                });
            });
        }

        $(document).ready(function(){
            $('#receiveBtn').click(function(){
                $('#receiveModal').modal('show');
            });
            
            initiateDynamicFunctions();
            $('#search-box').on('keyup', function(){
                var search = $('#search-box').val();
                var searchBy = $('#search-by').val();
                if($(this).val() == ''){
                    resetPages();
                }
                else{
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url: "/sent/search",
                        data: {
                            search: search,
                            searchBy:searchBy
                        },
                        success: function(docs){
                            $('#documents-grid').empty();
                            var documents = JSON.parse(docs);
                            refillAll(documents);
                        },
                        error: function(err){
                            console.log(err.responseText);
                        }
                    });
                } 
            });

            function resetPages(){
                var num_items = $('#howManyItems').val();

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: "/reset/pages/sent-docs",
                    data: {
                        num_items: num_items
                    },
                    success: function(data){
                        $('#pagesMenu').empty();
                        var pages = JSON.parse(data);
                        
                        for(var i in pages){
                            $('#pagesMenu').append(
                                $('<div>').addClass('item').attr('data-value', i+':'+pages[i]).html(i)
                            );
                            if(i == $('#page').val().split(':')[0])
                                $('#page').val(i+':'+pages[i]);
                        }
                        if($('#page').val().split(':')[0] > Object.keys(pages).length){
                            $('#page').val('1:'+pages['1']);
                            $('#pageText').html('1');
                        }
                        filter();
                    },
                    error: function(err){
                        console.log(err.responseText);
                    }
                });
            }

            //If the user changes the number of items to be shown per page
            $('#howManyItems').change(function(){
                resetPages();
            });

            $('#viewDetailsClose').click(function(){
                $('#viewDetailsModalContent').html(
                    $('<div>').addClass('ui active inverted dimmer').append(
                        $('<div>').addClass('ui text loader').html('Loading')
                    )
                );
            });
            
            $('#sendBtn').click(function(){
                $('#sendModal').modal('show');
            });

            $("#recipient-search").dropdown({
                clearable: true
            });

            $('.view-btn').each(function(){
                $(this).click(function(){
                    var viewType = $(this).attr('view-type');
                    if(viewType == 'list'){
                        $('#documentsTable').removeClass('hidden');
                        $('#docsContainer').addClass('hidden');
                    }
                    else{
                        $('#docsContainer').removeClass('hidden');
                        $('#documentsTable').addClass('hidden');
                    }
                });
            });

            $("#type-dropdown").dropdown();

            $("#sort-dropdown").dropdown();

            $("#searchby-dropdown").dropdown();

            $('#file-type').on('change', function(){
                filter();
            });

            $('#page').change(function(){
                filter();
            });

            $('#sort-by').on('change', function(){
                filter();
            });

            $('#sort-button').click(function(){
                if($('#sort-button').attr('sort-direction') == 'desc'){
                    $('#sort-icon').removeClass('down');
                    $('#sort-icon').addClass('up');
                    $('#sort-button').attr('sort-direction', 'asc');
                }
                else{
                    $('#sort-icon').removeClass('up');
                    $('#sort-icon').addClass('down');
                    $('#sort-button').attr('sort-direction', 'desc');
                }
                $('#sort-button').transition('jiggle');
                showMask();
                filter();
            });

            function showMask(){
                $('#mask').addClass('show');
            }

            function removeMask(){
                $('#mask').removeClass('show');
            }

            function refillTable(documents){
                $('#documentsTableBody').empty();

                for(var i = 0; i < documents.length; i++){
                    var doc = documents[i];
                    var doc_size = humanFileSize(doc.file_size);
                    $('#documentsTableBody').append(
                        $('<div>').addClass('row').append(
                            $('<div>').addClass('one wide column').append(
                                $('<i>').addClass(fileIcons[doc.file_extension]+" red table-icon")
                            )
                        ).append(
                            $('<div>').addClass('eight wide column').append(
                                $('<span>').addClass('semibold').attr('id', 'listFileName'+doc.id).append(
                                    doc.file_name
                                )
                            )
                        ).append(
                            $('<div>').addClass('four wide column').append(
                                doc_size
                            )
                        ).append(
                            $('<div>').addClass('two wide column').append(
                                doc.date_created
                            )
                        ).append(
                            $('<div>').addClass('one wide column').append(
                                $('<div>').addClass('ui floating dropdown icon options-dropdown').attr('id', 'row'+doc.id).append(
                                    $('<i>').addClass('ellipsis vertical icon options-icon').attr('document-id', doc.id)
                                ).append(
                                    $('<div>').addClass('menu').append(
                                        $('<div>').addClass('item rename-file').attr('id', 'renameList'+doc.id).attr('document-id', doc.id).attr('file-name', doc.file_name).append(
                                            $('<i>').addClass('edit icon')
                                        ).append('Rename')
                                    ).append(
                                        $('<div>').addClass('item view-details').attr('document-id', doc.id).append(
                                            $('<i>').addClass('history icon')
                                        ).append('Details')
                                    )
                                )
                            )
                        )
                    );
                }
            }

            function refillAll(documents){
                refillTable(documents);
                for(var i = 0; i < documents.length; i++){
                    var doc = documents[i];
                    var fileName = getFileNameSubstr(doc.file_name);
                    $('#documents-grid').append(
                        $('<div>').addClass('ui fluid card').append(
                            $('<div>').addClass('content').append(
                                $('<div>').addClass('ui floating dropdown icon options-dropdown').attr('id', doc.id).append(
                                    $('<i>').addClass("ellipsis vertical icon options-icon").attr('document-id', doc.id)
                                ).append(
                                    $('<div>').addClass('menu').append(
                                        $('<div>').addClass('item rename-file').attr('document-id', doc.id).attr('file-name', doc.file_name).attr('id', 'renameItem'+doc.id).append(
                                            $('<i>').addClass('edit icon')
                                        ).append('Rename')
                                    ).append(
                                        $('<div>').addClass('item view-document').attr('path', '/documents/pdf-document/'+doc.id).append(
                                            $('<i>').addClass('eye icon')
                                        ).append('View Document')
                                    ).append(
                                        $('<div>').addClass('item view-details').attr('document-id', doc.id).append(
                                            $('<i>').addClass('history icon')
                                        ).append('Details')
                                    )
                                )
                            ).append(
                                $('<div>').addClass('icon-container').append(
                                    $('<i>').addClass(fileIcons[doc.file_extension]+" extension-icon")
                                )
                            )
                        ).append(
                            $('<div>').addClass('extra content').append(
                                $('<div>').addClass('ui small header file-header').attr('id', 'gridFileName'+doc.id).append(
                                    fileName
                                )
                            ).append(
                                $('<div>').addClass('meta').append(
                                    humanFileSize(doc.file_size) + " | " + doc.file_extension
                                )
                            )
                        )
                    );
                }
                removeMask();
                initiateDynamicFunctions();
            }

            function filter(){
                var partition = $('#page').val().split(':');
                var fileType = $('#file-type').val();
                var sortBy = $('#sort-by').val();
                var sortDir = $('#sort-button').attr('sort-direction');
                var start = partition[1];
                var end = partition[2];

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: "/sentdocs/filter",
                    data: {
                        fileType: fileType,
                        sortBy: sortBy,
                        sortDir: sortDir,
                        start: start,
                        end: end,
                    },
                    beforeSend: function(){
                        showMask();
                    },
                    success: function(docs){
                        $('#documents-grid').empty();
                        var documents = JSON.parse(docs);
                        $('#totalItems').html(documents.length);
                        $('#numOfItems').html(documents.length);
                        refillAll(documents);
                    },
                    error: function(err){
                        console.log(err.responseText);
                    }
                });
            }
        });
    </script>
@endsection