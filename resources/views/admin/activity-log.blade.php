@extends('layouts.admin')

@section('content')

<div class="ui fullscreen modal" id="viewDocModal">
    <i class="close icon"></i>
    <div class="content">
        <embed src="" style="width:1200px; height:550px;" id="viewDocEmbed">
    </div>
</div>
<div>
	<h1>Activity Log</h1>
	<div class="ui divided items" id="logs-grid">
		   	@foreach($dates as $date => $logs)
		   		<div class="ui segments logs" id="{{$date}}">
					<div class="ui segment">
						{{Carbon\Carbon::parse($date)->format('F j, Y , l')}}
					</div>
				  	@foreach($logs as $log)
					    <div class="ui segment singleLog" id="{{$log->id}}">
			   				{{$log->content}}
							 <div class="meta">
								<span>{{Carbon\Carbon::parse($log->created_at)->format('g:ia')}}
							 	 {{$log->created_at->diffForHumans()}}</span>
							 </div>
				   		</div>
					@endforeach

				</div>
			@endforeach

	

</div>
<script type="text/javascript">
	$('.seemore').click(function(){
		var numItems = $(this).attr('numItems');
		var num = $(this).attr('count');
		var id = $('.logs').attr('id');
		var log =  $('.singleLog').attr('id');
	        $.ajaxSetup({
	            headers: {
	                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	            }
	        });
	        $.ajax({
	            type: "POST",
	            url: "/logs/more",
	            data:{
	            	numItems: numItems ,
	            },
	            success: function(logs){
        			$('#logs-grid').empty();
        			$('#logs-grid').append(logs);
					$('#logs-grid').transition('pulse');
					
	                console.log(num,numItems);
	                
	            },
	            error: function(err){
	                console.log(err.responseText);
	            }
	        });
    });

</script>
@endsection











