@extends('layouts.admin')

@section('content')
    <div class="ui container">
        <div id="renameFileModal" class="ui small modal">
            <div class="header">
                Rename Document
            </div>
            <div class="content">
                <div class="ui fluid right labeled left icon input">
                    <i class="pencil icon"></i>
                    <input type="text" name="rename" id="fileNameInput" old-name="">
                    <div class="ui label" id="fileExtension">
                    </div>
                </div>
                <div id="errorRename" class="error-msg"></div>
            </div>
            <div class="actions">
                <button class="ui red right labeled icon button" id="renameBtn" document-id="">
                    <i class="check icon"></i>
                    Rename
                </button>
            </div>
        </div>

        <div class="ui modal test" id="sendModal">
            <div class="header">
                Send Document
            </div>
            <div class="content">
                <form class="ui form" id="sendDocForm" action="/send/doc" enctype="multipart/form-data" method="POST">
                    {{ csrf_field() }}
                    <div class="field">
                        <label>Recipient</label>
                        <div id="recipient-search" class="ui fluid multiple search selection dropdown">
                            <input type="hidden" name="receivers">
                            <i class="dropdown icon"></i>
                            <div class="default text">Select recipient</div>
                            <div class="menu">
                                @foreach($users as $user)
                                    <div class="item" data-value="{{ $user->id }}">
                                        <img class="ui avatar image" src="{{ asset($user->profile_pic) }}">
                                        {{ $user->firstname." ".$user->middlename." ".$user->surname }}
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="field">
                        <label>Title</label>
                        <input type="text" name="title" placeholder="Enter title">
                    </div>
                    <div class="field">
                        <label>Type</label>
                        <select class="ui fluid dropdown" name="type">
                            <option disabled="" selected="">Select document type</option>
                            <option value="request">Request</option>
                            <option value="certificate">Certificate</option>
                            <option value="report">Report</option>
                        </select>
                    </div>
                    <div class="field">
                        <label>Description</label>
                        <textarea rows="2" form="sendDocForm" name="description" placeholder="Enter description"></textarea>
                    </div>
                    <div class="field">
                        <label>Upload files</label>
                        <div class="ui labeled left icon input">
                            <i class="paperclip icon"></i>
                            <input id="docs" type="file" name="documents[]" placeholder="Attach files" multiple="">
                        </div>
                    </div>
                    <div class="field">
                        <label>Tags</label>
                        <div class="ui right labeled left icon input">
                            <i class="tags icon"></i>
                            <input type="text" name="tags" placeholder="Enter tags">
                            <div class="ui tag label">
                                Add tags
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="actions">
                <button class="ui button deny">Cancel</button>
                <button type="submit" form="sendDocForm" class="ui approve red right labeled icon button">
                    <i class="paper plane outline icon"></i>
                    Send
                </button>
            </div>
        </div>
        <div class="ui grid ">
            <div class="eight wide column send-rcv-btns">
                <button id="sendBtn" class="ui labeled icon red button test" data-tooltip="Send documents to anyone" data-position="bottom center">
                    <i class="paper plane outline icon"></i>
                    Send
                </button>
                <button id="receiveBtn" class="ui labeled icon red button test" data-tooltip="Receive documents from outside" data-position="bottom center">
                    <i class="cloud upload icon"></i>
                    Receive
                </button>
            </div>
            <div class="three wide column">
                <div class="ui fluid selection dropdown" id="searchby-dropdown">
                    <input type="hidden" name="type" value="file_name" id="search-by">
                    <i class="dropdown icon"></i>
                    <div class="default text">File Name</div>
                    <div class="menu">
                        <div class="item" data-value="file_name">File Name</div>
                        <div class="item" data-value="collections.title">Collection Title</div>
                        <div class="item" data-value="receiver">Receiver</div>
                        <div class="item" data-value="code">Code</div>
                    </div>
                </div>
            </div>
            <div class="five wide column">
                <div class="ui fluid left icon input">
                    <input type="text" name="search-docs" placeholder="Search documents">
                    <i class="search icon"></i>
                </div>
            </div>
        </div>
        <!-- <div class="count-label-div tools-div">
            <div class="ui labeled button" tabindex="0">
                <div class="ui red button">
                    <i class="file alternate icon"></i> Total items
                </div>
                <a class="ui basic red left pointing label">
                    {{ count($sentDocs)}}
                </a>
            </div>
        </div> -->
        <div class="ui grid tools-div">
            <div class="fourteen wide column">
                <label class="label-mg-right">Type</label>
                <div class="ui selection dropdown" id="type-dropdown">
                    <input type="hidden" value="all" name="type" id="file-type">
                    <i class="dropdown icon"></i>
                    <div class="default text">All</div>
                    <div class="menu">
                        <div class="item" data-value="all">All</div>
                        <div class="item" data-value="pdf">pdf</div>
                        <div class="item" data-value="docx">docx</div>
                        <div class="item" data-value="doc">doc</div>
                        <div class="item" data-value="rar">rar</div>
                        <div class="item" data-value="zip">zip</div>
                        <div class="item" data-value="pptx">pptx</div>
                        <div class="item" data-value="ppt">ppt</div>
                        <div class="item" data-value="txt">txt</div>
                    </div>
                </div>

                <label class="label-mg-right label-mg-left">Sort by</label>
                <div class="ui selection dropdown" id="sort-dropdown">
                    <input type="hidden" value="created_at" name="type" id="sort-by">
                    <i class="dropdown icon"></i>
                    <div class="default text">Date added</div>
                    <div class="menu">
                        <div class="item" data-value="created_at">Date added</div>
                        <div class="item" data-value="file_name">File Name</div>
                        <div class="item" data-value="file_size">File Size</div>
                    </div>
                </div>

                <button class="circular ui icon button sort-btn" id="sort-button" sort-direction="desc">
                    <i class="arrow down icon" id="sort-icon"></i>
                </button>
            </div>
            <div class="two wide column">
                <div class="ui icon buttons">
                    <button class="ui button view-btn" view-type="grid"><i class="th icon"></i></button>
                    <button class="ui button view-btn" view-type="list"><i class="th list icon"></i></button>
                </div>
            </div>
        </div>

        <div class="ui secondary pointing menu">
            <a href="/sent" class="@if(Request::is('sent')) active @endif item red">
                <i class="file alternate icon"></i>
                Documents
            </a>
            <a href="/sent/collections" class="@if(Request::is('sent/collections')) active @endif item red">
                <i class="archive icon"></i>
                Collections
            </a>
        </div>

        <!-- <div class="docs-container" id="docsContainer">
            @php
                function formatBytes($size, $precision = 2)
                {
                    $base = log($size, 1024);
                    $suffixes = array('', 'KB', 'MB', 'GB', 'TB');   

                    return round(pow(1024, $base - floor($base)), $precision) .' '. $suffixes[floor($base)];
                }
            @endphp
            <div class="ui four stackable cards" id="documents-grid">
                <div id="mask"></div>
                @foreach($sentDocs as $sentDoc)
                    <div class="ui fluid card">
                        <div class="content">
                            <div id="{{ $sentDoc->id }}" class="ui floating dropdown icon options-dropdown">
                                <i class="ellipsis vertical icon options-icon" document-id="{{ $sentDoc->id }}"></i>
                                <div class="menu">
                                    <div class="item rename-file" id="rename{{ $sentDoc->id }}" document-id="{{ $sentDoc->id }}" file-name="{{ $sentDoc->file_name }}">
                                        <i class="edit icon"></i>
                                        Rename
                                    </div>
                                    <div class="item view-details" document-id="{{ $sentDoc->id }}">
                                        <i class="history icon"></i>
                                        Details
                                    </div>
                                </div>
                            </div>
                            <div class="icon-container">
                                <i class="{{ $icons[$sentDoc->file_extension]}} extension-icon"></i>
                            </div>
                        </div>
                        <div class="extra content">
                            <div class="ui small header file-header" id="gridFileName{{ $sentDoc->id }}">{{ $sentDoc->file_name}}</div>
                            <div class="meta"> {{ formatBytes($sentDoc->file_size) }} | {{ $sentDoc->file_extension}} </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

        <div class="table-cont hidden" id="documentsTable">
            <table class="ui striped very basic table">
                <thead>
                    <th class="one wide"></th>
                    <th class="six wide">File Name</th>
                    <th class="two wide">Size</th>
                    <th class="four wide">Receiver</th>
                    <th class="two wide">Date Created</th>
                    <th class="one wide"></th>
                </thead>
                <tbody id="documentsTableBody">
                    @foreach($sentDocs as $doc)
                        <tr>
                            <td>
                                <i class="{{ $icons[$doc->file_extension]}} red table-icon"></i>
                            </td>
                            <td>
                                <span class="semibold" id="listFileName{{ $doc->id }}">{{ $doc->file_name }}</span>
                            </td>
                            <td>
                                {{ formatBytes($doc->file_size) }}
                            </td>
                            <td>
                                <img class="ui avatar image" src="{{ asset($doc->collection->receiver->profile_pic) }}">
                                {{ $doc->collection->receiver->firstname." ".$doc->collection->receiver->surname }}
                            </td>
                            <td>
                                {{ Carbon\Carbon::parse($doc->created_at)->format('M d') }}
                            </td>
                            <td>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <script type="text/javascript" src="{{ asset('js/tablesort.js') }}"></script>
    <script type="text/javascript">
        var fileIcons = {"pdf": "file pdf outline icon", "docx": "file word outline icon", "xls": "file excel outline icon", "xlsx": "file excel outline icon", "zip": "file archive outline icon", "rar": "file archive outline icon", "pptx": "file powerpoint outline icon", "ppt": "file powerpoint outline icon", "txt": "file alternate outline icon"};

        function humanFileSize(size) {
            var i = Math.floor( Math.log(size) / Math.log(1024) );
            return ( size / Math.pow(1024, i) ).toFixed(2) * 1 + ' ' + ['B', 'KB', 'MB', 'GB', 'TB'][i];
        }

        function extractFileName(array){
            var fileName = '';
            for(var i = 0; i < (array.length - 1); i++){
                if(i === (array.length - 2))
                    fileName += array[i];
                else
                    fileName += array[i] + '.';
            }
            return fileName;
        }

        $(document).ready(function(){
            function initiateDynamicFunctions(){
                $('.options-icon').each(function(){
                    var id = "#"+$(this).attr('document-id');
                    $(id).dropdown({
                        transition: 'horizontal flip'
                    });
                });

                $('.rename-file').each(function(){
                    var docId = $(this).attr('document-id');
                    var oldName = $(this).attr('file-name');
                    var namesArr = oldName.split('.');
                    var fileName = extractFileName(namesArr);
                    var extension = namesArr[namesArr.length-1];

                    $(this).click(function(){
                        $('#errorRename').empty();
                        $('#renameBtn').attr('document-id', docId);
                        $('#fileNameInput').val(fileName);
                        $('#fileNameInput').attr('old-name', fileName+'.'+extension);
                        $('#fileExtension').html('.'+extension);
                        $('#renameFileModal').modal('show');
                    });
                });
            }

            initiateDynamicFunctions();
            
            $('#sendBtn').click(function(){
                $('#sendModal').modal({
                    inverted: true
                }).modal('show');
            });

            $("#recipient-search").dropdown({
                clearable: true
            });

            $('#renameBtn').click(function(){
                var docId = $('#renameBtn').attr('document-id');
                var extension = $('#fileExtension').html();
                var newName = $('#fileNameInput').val();
                var oldName = $('#fileNameInput').attr('old-name');

                if(oldName == (newName+extension)){
                    $('#errorRename').html('You should enter a new name!');
                    $('#errorRename').transition('bounce');
                }
                else if(newName == ''){
                    $('#errorRename').html('Please enter a new name for this file.');
                    $('#errorRename').transition('bounce');
                }
                else{
                    $('#errorRename').empty();
                    rename(docId, newName+extension);
                }
            });

            function rename(docId, newFileName){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: "/doc/rename",
                    data: {
                        id: docId,
                        newFileName: newFileName
                    },
                    success: function(name){
                        var renameItem = $('#rename'+docId);
                        renameItem.attr('file-name', name);
                        $('#gridFileName'+docId).html(name);
                        $('#listFileName'+docId).html(name);
                        $('#renameFileModal').modal('hide');
                        $('#gridFileName'+docId).transition('pulse');
                        $('#gridFileName'+docId).transition('pulse');
                    },
                    error: function(err){
                        console.log(err.responseText);
                    }
                });
            }

            $('.view-btn').each(function(){
                $(this).click(function(){
                    var viewType = $(this).attr('view-type');
                    if(viewType == 'list'){
                        $('#documentsTable').removeClass('hidden');
                        $('#docsContainer').addClass('hidden');
                    }
                    else{
                        $('#docsContainer').removeClass('hidden');
                        $('#documentsTable').addClass('hidden');
                    }
                });
            });

            $("#type-dropdown").dropdown();

            $("#sort-dropdown").dropdown();

            $("#searchby-dropdown").dropdown();

            $('#file-type').on('change', function(){
                showMask();
                filter();
            });

            $('#sort-by').on('change', function(){
                showMask();
                filter();
            });

            $('#sort-button').click(function(){
                if($('#sort-button').attr('sort-direction') == 'desc'){
                    $('#sort-icon').removeClass('down');
                    $('#sort-icon').addClass('up');
                    $('#sort-button').attr('sort-direction', 'asc');
                }
                else{
                    $('#sort-icon').removeClass('up');
                    $('#sort-icon').addClass('down');
                    $('#sort-button').attr('sort-direction', 'desc');
                }
                $('#sort-button').transition('jiggle');
                showMask();
                filter();
            });

            function showMask(){
                $('#mask').addClass('show');
            }

            function removeMask(){
                $('#mask').removeClass('show');
            }

            function refillTable(documents){
                $('#documentsTableBody').empty();

                for(var i = 0; i < documents.length; i++){
                    var doc = documents[i];
                    var doc_size = humanFileSize(doc.file_size);
                    $('#documentsTableBody').append(
                        $('<tr>').append(
                            $('<td>').append(
                                $('<i>').addClass(fileIcons[doc.file_extension]+" red table-icon")
                            )
                        ).append(
                            $('<td>').append(
                                $('<span>').addClass('semibold').attr('id', 'listFileName'+doc.id).append(
                                    doc.file_name
                                )
                            )
                        ).append(
                            $('<td>').append(
                                doc_size
                            )
                        ).append(
                            $('<td>').append(
                                $('<img>').addClass('ui avatar image').attr('src', doc.receiver_prof_pic)
                            ).append(
                                doc.receiver_name
                            )
                        ).append(
                            $('<td>').append(
                                doc.date_created
                            )
                        ).append(
                            $('<td>')
                        )
                    );
                }
            }

            function filter(){
                var fileType = $('#file-type').val();
                var sortBy = $('#sort-by').val();
                var sortDir = $('#sort-button').attr('sort-direction');

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: "/sentdocs/filter",
                    data: {
                        fileType: fileType,
                        sortBy: sortBy,
                        sortDir: sortDir,
                    },
                    success: function(docs){
                        $('#documents-grid').empty();
                        var documents = JSON.parse(docs);
                        refillTable(documents);
                        for(var i = 0; i < documents.length; i++){
                            var doc = documents[i];
                            $('#documents-grid').append(
                                $('<div>').addClass('ui fluid card').append(
                                    $('<div>').addClass('content').append(
                                        $('<div>').addClass('ui floating dropdown icon options-dropdown').attr('id', doc.id).append(
                                            $('<i>').addClass("ellipsis vertical icon options-icon").attr('document-id', doc.id)
                                        ).append(
                                            $('<div>').addClass('menu').append(
                                                $('<div>').addClass('item rename-file').attr('document-id', doc.id).attr('file-name', doc.file_name).attr('id', 'renameItem'+doc.id).append(
                                                    $('<i>').addClass('edit icon')
                                                ).append('Rename')
                                            ).append(
                                                $('<div>').addClass('item view-details').attr('document-id', doc.id).append(
                                                    $('<i>').addClass('history icon')
                                                ).append('Details')
                                            )
                                        )
                                    ).append(
                                        $('<div>').addClass('icon-container').append(
                                            $('<i>').addClass(fileIcons[doc.file_extension]+" extension-icon")
                                        )
                                    )
                                ).append(
                                    $('<div>').addClass('extra content').append(
                                        $('<div>').addClass('ui small header file-header').attr('id', 'gridFileName'+doc.id).append(
                                            doc.file_name
                                        )
                                    ).append(
                                        $('<div>').addClass('meta').append(
                                            doc.file_size + " | " + doc.file_extension
                                        )
                                    )
                                )
                            );
                        }
                        removeMask();
                        initiateDynamicFunctions();
                    },
                    error: function(err){
                        console.log(err.responseText);
                    }
                });
            }
        });
    </script> -->
@endsection