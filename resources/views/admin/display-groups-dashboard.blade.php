@extends('layouts.admin')

@section('content')
	<h3 class="ui header">Your Groups</h3>
	<div class="ui link four stackable cards">
		@foreach($groups as $group)
			<div class="card">
				<a class="image" href="/dashboard/{{ $group->id }}">
					<img src="{{ asset($group->cover) }}">
				</a>
				<div class="content">
					<a class="header" href="/group/{{ $group->id }}">
						{{ $group->name }}
					</a>
					<div class="meta">
						<i class="user icon"></i>
						{{ count($group->members) }} members
					</div>
				</div>
			</div>
		@endforeach
	</div>
@endsection