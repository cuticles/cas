@extends('layouts.admin')

@section('content')
    @include('inc.modalforms')
    <div class="ui small modal scrolling" id="receiversModal">
        <div class="header">
            Receivers
        </div>
        <div class="content" id="forwardReceivers">
            
        </div>
    </div>
    <div class="ui container">
        <div class="ui small modal" id="trashCollectionModal">
            <div class="header">
                Delete Collection
            </div>
            <div class="content">
                Are you sure you want to move <span class="semibold" id="trashCollTitle"></span> to your trash?
            </div>
            <div class="actions">
                <button class="ui black right labeled icon button" coll-id="" id="trashCollBtn">
                    <i class="trash alternate outline icon"></i>
                    Yes
                </button>
                <button class="ui deny button">No</button>
            </div>
        </div>

        <div class="ui modal" id="editCollectionModal">
            <div class="header">
                Edit: <span id="editCollectionTitle"></span>
            </div>
            <div class="content">
                <form class="ui form" id="editCollForm" action="/send/doc" enctype="multipart/form-data" method="POST">
                    {{ csrf_field() }}
                    <div class="field">
                        <label>Title</label>
                        <input type="text" name="title" id="editCollTitle" placeholder="Enter title" old-value="">
                        <div class="status-msg status-check" id="titleMsg" status="okay" changed="false"></div>
                    </div>
                    <div class="field">
                        <label>Type</label>
                        <div class="ui fluid selection dropdown receive-dropdown">
                            <input id="editCollType" type="hidden" name="type" old-value="">
                            <i class="dropdown icon"></i>
                            <div class="default text" id="collTypeText">Select collection type</div>
                            <div class="menu">
                                <div class="disabled item" data-value="" >Select collection type</div>
                                @foreach($types as $type)
                                    <div class="item" data-value="{{ $type->id }}">
                                        {{ ucfirst($type->name) }}
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="status-msg status-check" id="editTypeMsg" status="okay" changed="false"></div>
                    </div>
                    <div class="field">
                        <label>Description</label>
                        <textarea rows="2" form="sendDocForm" name="description" placeholder="Enter description" id="editCollDescription" old-value=""></textarea>
                        <div class="status-msg status-check" id="descriptionMsgEdit" status="okay" changed="false"></div>
                    </div>
                    <div class="field">
                        <label>Tags</label>
                        <div class="ui right labeled left icon input">
                            <i class="tags icon"></i>
                            <input type="text" name="tags" placeholder="Enter tags" id="editCollTags" old-value="">
                            <div class="ui tag label">
                                Add tags
                            </div>
                        </div>
                        <div class="status-msg status-check" id="tagsMsgEdit" status="okay" changed="false"></div>
                    </div>
                </form>
                <div class="error-msg" id="changesStatus"></div>
            </div>
            <div class="actions">
                <button class="ui button deny" id="cancelEditColl">Cancel</button>
                <button class="ui disabled red right labeled icon button" coll-id="" id="editCollBtn">
                    <i class="check icon"></i>
                    Save Changes
                </button>
            </div>
        </div>

        <div class="ui grid ">
            <div class="eight wide column send-rcv-btns">
                <button id="sendBtn" class="ui labeled icon red button test" data-tooltip="Send documents to anyone" data-position="bottom center">
                    <i class="paper plane outline icon"></i>
                    Send
                </button>
                <button id="receiveBtn" class="ui labeled icon red button test" data-tooltip="Receive documents from outside" data-position="bottom center">
                    <i class="cloud upload icon"></i>
                    Receive
                </button>
            </div>
            <div class="three wide column">
                <div class="ui fluid selection dropdown" id="searchby-dropdown">
                    <input type="hidden" name="type" value="title" id="search-by">
                    <i class="dropdown icon"></i>
                    <div class="default text">Title</div>
                    <div class="menu">
                        <div class="item" data-value="title">Title</div>
                        <div class="item" data-value="receiver">Receiver</div>
                        <div class="item" data-value="code">Code</div>
                        <div class="item" data-value="tags">Tags</div>
                    </div>
                </div>
            </div>
            <div class="five wide column">
                <form action="/sent/collections/search">
                    <div class="ui fluid left icon input">
                        <input type="text" id="search-box" placeholder="Search collections">
                        <i class="search icon"></i>
                        
                    </div>
                </form> 
            </div>
        </div>
        <!-- <div class="count-label-div tools-div">
            <div class="ui labeled button" tabindex="0">
                <div class="ui red button">
                    <i class="file alternate icon"></i> Total items
                </div>
                <a class="ui basic red left pointing label" id="totalItems">
                    {{ count($sentCollections)}}
                </a>
            </div>
        </div> -->
        <div class="ui grid tools-div">
            <div class="fourteen wide column">
                <label class="label-mg-right">Type</label>
                <div class="ui selection dropdown" id="type-dropdown">
                    <input type="hidden" value="all" name="coll-type" id="coll-type">
                    <i class="dropdown icon"></i>
                    <div class="default text">All</div>
                    <div class="menu">
                        <div class="item" data-value="all">All</div>
                        <div class="item" data-value="request">Request</div>
                        <div class="item" data-value="certificate">Certificate</div>
                        <div class="item" data-value="report">Report</div>
                    </div>
                </div>

                <label class="label-mg-right label-mg-left">Sort by</label>
                <div class="ui selection dropdown" id="sort-dropdown">
                    <input type="hidden" value="updated_at" name="type" id="sort-by">
                    <i class="dropdown icon"></i>
                    <div class="default text">Date Modified</div>
                    <div class="menu">
                        <div class="item" data-value="updated_at">Date Modified</div>
                        <div class="item" data-value="title">Title</div>
                        <div class="item" data-value="current_status">Status</div>
                    </div>
                </div>

                <button class="circular ui icon button sort-btn" id="sort-button" sort-direction="desc">
                    <i class="arrow down icon" id="sort-icon"></i>
                </button>
            </div>
            <div class="two wide column">
            </div>
        </div>

        @if(count($sentCollections) > 0)
        <div class="ui stackable vertically divided grid" id="collections-grid">
            @foreach($sentCollections as $collection)
                <div class="row coll-item">
                    <div class="two wide column">
                        <img class="ui tiny image" src="{{ asset($icons[$collection->type->name]) }}" id="type{{ $collection->id }}">
                    </div>
                    <div class="seven wide column">
                        <div class="mg-bottom-12">
                            <div class="ui basic horizontal label status-label">
                                {{ ucfirst($collection->status->name)}}
                            </div>
                            <span class="meta">
                                {{ $collection->created_at->diffForHumans() }}
                            </span>
                        </div>
                        <a href="/collection/{{ $collection->id }}" class="header title-collection" id="title{{ $collection->id }}">
                            {{ $collection->title }}
                        </a>
                    </div>
                    <div class="six wide column">
                        <div>
                            <i class="paperclip icon"></i>
                            <span class="margin-r-10 semibold">{{ count($collection->documents) }}</span>
                            Attached documents
                        </div>
                        <div class="margin-top-10 receiver-info">
                            <span class="sent-to">sent to</span>
                            @if($collection->to_outside == '1')
                                <span class="semibold">{{ $collection->outsider->name }}</span>
                            @else
                                <img class="ui avatar image" src="{{ asset($collection->receiver->profile_pic) }}">
                                <a href="#user" class="fullname-link">
                                    {{$collection->receiver->firstname." ".$collection->receiver->middlename." ".$collection->receiver->surname }}
                                </a>
                            @endif
                            @if(count($collection->transmits) > 1)
                                and 
                                <a class="ui horizontal label view-receivers" href="#receivers" coll-id="{{ $collection->id}}">
                                    {{ count($collection->transmits)-1 }} more
                                    <div class="ui mini inline loader" id="loader{{ $collection->id }}"></div>
                                </a>
                            @endif
                        </div>
                    </div>
                    <div class="one wide column coll-item">
                        <div id="{{ $collection->id }}" class="ui floating dropdown options-dropdown-coll">
                            <i class="ellipsis vertical icon options-icon" document-id="{{ $collection->id }}"></i>
                            <div class="menu">
                                <div class="item edit-coll" coll-title="{{ $collection->title }}" coll-description="{{ $collection->description }}" coll-type="{{ $collection->type->id }}" coll-type-name="{{ ucfirst($collection->type->name) }}" coll-tags="{{ $collection->tags }}" coll-id="{{ $collection->id }}" id="editItem{{ $collection->id }}">
                                    <i class="edit icon"></i>
                                    Edit
                                </div>
                                <div class="item trash-coll" coll-title="{{ $collection->title }}" coll-id="{{ $collection->id }}">
                                    <i class="trash alternate icon"></i>
                                    Trash
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        @else
            <div class="ui placeholder segment">
                <div class="ui icon header">
                    <i class="box icon"></i>
                    You don't have added collections yet.
                </div>
            </div>
        @endif

        @if(count($sentCollections) > 0)
            <div class="pagination" id="pagination_cont">
                <div class="ui stackable grid">
                    <div class="five wide column">
                        Show
                        <div class="ui selection dropdown receive-dropdown" id="showItems">
                            <input type="hidden" value="10" name="type" id="howManyItems">
                            <i class="dropdown icon"></i>
                            <div class="default text">3</div>
                            <div class="menu">
                                <div class="item" data-value="10">10 items</div>
                                <div class="item" data-value="20">20 items</div>
                                <div class="item" data-value="25">25 items</div>
                                <div class="item" data-value="50">50 items</div>
                            </div>
                        </div>
                    </div>
                    <div class="five wide column">
                        Page
                        <div class="ui selection dropdown receive-dropdown" id="paginate">
                            <input type="hidden" value="{{ '1:'.$partitions['1'] }}" name="type" id="page">
                            <i class="dropdown icon"></i>
                            <div class="default text" id="pageText"></div>
                            <div class="menu" id="pagesMenu">
                                @foreach($partitions as $index => $value)
                                    <div class="item" data-value="{{ $index.':'.$value }}">{{ $index }}</div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="six wide column">
                        <div class="showing">
                            Showing 
                            <span id="numOfItems">{{ count($sentCollections) }}</span> out of 
                            {{ $numItems }}
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
    <script type="text/javascript" src="{{ asset('js/validations/edit-coll-validations.js') }}"></script>
    <script type="text/javascript">
        var icons = {'report': 'images/analysis.png', 'certificate': 'images/diploma.png', 'request': 'images/application.png'};
        function capitalizeFirstLetter(string) {
            return string.charAt(0).toUpperCase() + string.slice(1);
        }
        $(document).ready(function(){
            $('#receiveBtn').click(function(){
                $('#receiveModal').modal('show');
            });

            //Initiate dynamic functions for dynamically added elements
            function initiateDynamicFunctions(){
                $('.options-dropdown-coll').dropdown();

                $('.edit-coll').each(function(){
                    var title = $(this).attr('coll-title');
                    var type = $(this).attr('coll-type');
                    var typeText = $(this).attr('coll-type-name');
                    var description = $(this).attr('coll-description');
                    var tags = $(this).attr('coll-tags');
                    var id = $(this).attr('coll-id');

                    $(this).click(function(){
                        $('#editCollectionTitle').html(title);
                        $('#editCollBtn').attr('coll-id', id);
                        $('#editCollTitle').val(title).attr('old-value', title);
                        $('#editCollDescription').html(description).attr('old-value', description);
                        $('#editCollTags').val(tags).attr('old-value', tags);
                        $('#editCollType').val(type).attr('old-value', type);
                        $('#collTypeText').html(typeText);

                        $('.status-check').each(function(){
                            $(this).empty().attr('status', 'okay').attr('changed', 'false');
                        });
                        $('#changesStatus').empty();
                        $('#editCollBtn').addClass('disabled');

                        $('#editCollectionModal').modal('show');
                    });
                });

                $('.trash-coll').each(function(){
                    var id = $(this).attr('coll-id');
                    var title = $(this).attr('coll-title');
                    $(this).click(function(){
                        $('#trashCollTitle').html(title);
                        $('#trashCollBtn').attr('coll-id', id);
                        $('#trashCollectionModal').modal('show');
                    });
                });

                $('.view-receivers').click(function(){
                    var id = $(this).attr('coll-id');
                    $('#loader'+id).addClass('active');
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url: "/view/receivers/sent-colls",
                        data: {
                            id: id
                        },
                        success: function(data){
                            $('#loader'+id).removeClass('active');
                            $('#forwardReceivers').html(data);
                            $('#receiversModal').modal('show');
                        },
                        error: function(err){
                            console.log(err.responseText);
                        }
                    });
                });
            }

            initiateDynamicFunctions();

            function resetPages(){
                var num_items = $('#howManyItems').val();

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: "/reset/pages/sent-colls",
                    data: {
                        num_items: num_items
                    },
                    success: function(data){
                        $('#pagesMenu').empty();
                        var result = JSON.parse(data);
                        var pages = result.partitions;
                        var count = result.count;
                        
                        if(count != 0){
                            for(var i in pages){
                                $('#pagesMenu').append(
                                    $('<div>').addClass('item').attr('data-value', i+':'+pages[i]).html(i)
                                );
                                if(i == $('#page').val().split(':')[0])
                                    $('#page').val(i+':'+pages[i]);
                            }
                            if($('#page').val().split(':')[0] > Object.keys(pages).length){
                                $('#page').val('1:'+pages['1']);
                                $('#pageText').html('1');
                            }
                        }
                        
                        else{
                            $('#pagesMenu').append(
                                $('<div>').addClass('item').attr('data-value', '1:0:0').html('1')
                            );

                            $('#page').val('1:0:0');

                            $('#pagination_cont').addClass('hidden');
                        }

                        filter();
                    },
                    error: function(err){
                        console.log(err.responseText);
                    }
                });
            }

            //If the user changes the number of items to be shown per page
            $('#howManyItems').change(function(){
                resetPages();
            });

            $('#sendBtn').click(function(){
                $('#sendModal').modal('show');
            });

            $("#recipient-search").dropdown({
                clearable: true
            });

            $('#trashCollBtn').click(function(){
                var id = $('#trashCollBtn').attr('coll-id');
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: "/collection/trash",
                    data: {
                        id: id,
                        type: 'sent'
                    },
                    success: function(coll){
                        $('#trashCollectionModal').modal('hide');
                        resetPages();
                    },
                    error: function(err){
                        console.log(err.responseText);
                    }
                });
            });

            $('#editCollBtn').click(function(){
                var id = $('#editCollBtn').attr('coll-id');
                var title = $('#editCollTitle').val();
                var type = $('#editCollType').val();
                var description = $('#editCollDescription').val();
                var tags = $('#editCollTags').val();

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: "/collection/edit",
                    data: {
                        id: id,
                        type: type,
                        title: title,
                        tags: tags,
                        description: description
                    },
                    success: function(collection){
                        var coll = JSON.parse(collection);
                        var editItem = $('#editItem'+id);
                        editItem.attr('coll-title', coll.title);
                        editItem.attr('coll-description', coll.description);
                        editItem.attr('coll-type', coll.type);
                        editItem.attr('coll-tags', coll.tags);

                        $('#title'+id).html(coll.title);
                        $('#type'+id).attr('src', coll.type_icon);
                        $('#editCollectionModal').modal('hide');
                        $('#collection'+id).transition('bounce');
                        initiateDynamicFunctions();
                    },
                    error: function(err){
                        console.log(err.responseText);
                    }
                });
            });

            $("#type-dropdown").dropdown();

            $("#sort-dropdown").dropdown();

            $('#page').change(function(){
                console.log('Bitch');
                filter();
            });

            $("#searchby-dropdown").dropdown();

            $('#coll-type').on('change', function(){
                showMask();
                filter();
            });

            $('#sort-by').on('change', function(){
                showMask();
                filter();
            });

            $('#sort-button').click(function(){
                if($('#sort-button').attr('sort-direction') == 'desc'){
                    $('#sort-icon').removeClass('down');
                    $('#sort-icon').addClass('up');
                    $('#sort-button').attr('sort-direction', 'asc');
                }
                else{
                    $('#sort-icon').removeClass('up');
                    $('#sort-icon').addClass('down');
                    $('#sort-button').attr('sort-direction', 'desc');
                }
                $('#sort-button').transition('jiggle');
                showMask();
                filter();
            });

            function showMask(){
                $('#mask').addClass('show');
            }

            function removeMask(){
                $('#mask').removeClass('show');
            }
            $('#search-box').on('keyup', function(){
                var search = $('#search-box').val();
                var searchBy = $('#search-by').val();

                if(search == ''){
                    resetPages();
                }
                else{
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url: "/sent/collections/search",
                        data: {
                            search: search,
                            searchBy:searchBy
                        },
                        success: function(collection){
                            $('#collections-grid').empty();
                            $('#collections-grid').append(collection);
                            $('#collections-grid').transition('pulse');
                            removeMask();

                        },
                        error: function(err){
                            console.log(err.responseText);
                            console.log(search);
                        }
                    });
                }
                
            });
           
            function filter(){
                var type = $('#coll-type').val();
                var sortBy = $('#sort-by').val();
                var sortDir = $('#sort-button').attr('sort-direction');
                var partition = $('#page').val().split(':');
                var start = partition[1];
                var end = partition[2];

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: "/sentcollections/filter",
                    data: {
                        type: type,
                        sortBy: sortBy,
                        sortDir: sortDir,
                        start: start,
                        end: end,
                    },
                    success: function(collections){
                        // console.log(collections);
                        $('#collections-grid').empty();
                        $('#collections-grid').append(collections);
                        $('#numOfItems').html($('#collections-grid').children().length);
                        $('#collections-grid').transition('pulse');
                        initiateDynamicFunctions();
                        removeMask();
                    },
                    error: function(err){
                        console.log(err.responseText);
                    }
                });
            }
        });
    </script>
@endsection