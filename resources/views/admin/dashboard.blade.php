@extends('layouts.admin')

@section('content')
@if($group != 'none')
	<style type="text/css">
		.group-cover{
			background-image: url('{{ asset($group->cover) }}');
			background-size: cover;
			height: 175px;
			width: 100%;
			padding: 2%;
			position: relative;
			z-index: -2;
		}
		.overlay{
			position: absolute;
	        top: 0;
	        left: 0;
	        width: 100%;
	        height: 100%;
	        z-index: -1;
	        background-image: linear-gradient(120deg, #B61519 0%, #f57175 100%);
	        opacity: .8;
		}
		.app-content{
			padding-left: 21%;
			padding-right: 0;
			padding-top: 0;
			margin-bottom: 1%;
		}
		.group-details{
			z-index: 3;
			color: #ffffff !important;
		}
		#collChart, #collChartStatus{
			margin: auto;
		}
		.search-cont{
			padding: 2%;
		}
		@media only screen and (min-device-width: 320px) and (max-device-width: 480px){
			.app-content{
				padding-left: 0;
			}
		}
		#rows-cont{
			margin-top: 2%;
		}
	</style>
	<div class="group-cover">
		<div class="overlay">
		</div>
		<div class="group-details">
			<h2 class="ui header group-details">
				{{ $group->name }}
			</h2>
			<div class="num-members">
				{{ count($group->members) }} Member(s)
			</div>
			<div class="supervisor-detail">
				Supervised by {{ $group->handler->firstname." ".$group->handler->middlename." ".$group->handler->surname}}
			</div>
		</div>
	</div>
	@if(Auth::user()->is_admin == 1)
	<div class="search-content-dashboard">
		<input type="hidden" name="id" id="group-id" value="{{ $group->id }}">
		<div class="ui stackable grid">
            <div class="four wide column">
                <div class="ui fluid selection dropdown" id="searchby-dropdown">
                    <input type="hidden" name="searchBy" value="collections" id="search-by">
                    <i class="dropdown icon"></i>
                    <div class="default text">Collections</div>
                    <div class="menu">
                        <div class="item" data-value="collections">Collections</div>
                        <div class="item" data-value="documents">Documents</div>
                    </div>
                </div>
            </div>
            <div class="six wide column">
				<div class="ui action fluid input">
					<input type="text" placeholder="Search items by title/file name" name="search" id="search-box">
					<button type="submit" form="search-items" class="ui button" id="searchBtn">Search</button>
				</div>
            </div>
        </div>
	</div>
	<div id="search-container" class="hidden search-cont">
		<button class="ui labeled icon red large button" id="back-btn">
            <i class="arrow left icon"></i>
            Back
        </button>
		<div class="ui stackable vertically divided grid" id="rows-cont">
			
		</div>
	</div>
	@endif
	<div id="stats-container">
		<div class="stats-group">
			<div class="ui three stackable cards">
				@foreach($group_stat as $type => $count)
					<div class="card stat-card">
						<h3 class="card-stat-header">
							<i class="{{ $stat_icons[$type] }} icon"></i>
							{{ ucfirst($type) }}
						</h3>
						<div class="content">
							<div class="ui statistic">
								<div class="value">
									{{ $count[0] }}
								</div>
								<div class="label stat-label">
									Sent
								</div>
							</div>
							<div></div>
							<div class="ui statistic">
								<div class="value">
									{{ $count[1] }}
								</div>
								<div class="label stat-label">
									Received
								</div>
							</div>
						</div>
					</div>
				@endforeach
			</div>
		</div>

		<div class="members-acts-cont">
			<div class="ui stackable grid">
				<div class="eight wide column">
					<div class="ui segments">
						<div class="ui segment">
							<h4>Breakdown of collections</h4>
						</div>
						<div class="ui segment">
							<div class="charts">
								<canvas width="300px" height="300px" id="collChart"></canvas>
							</div>
						</div>
					</div>
				</div>
				<div class="eight wide column">
					<div class="ui segments">
						<div class="ui segment">
							<h4>Breakdown of status of collections</h4>
						</div>
						<div class="ui segment">
							<div class="charts">
								<canvas width="300px" height="300px" id="collChartStatus"></canvas>
							</div>
						</div>
					</div>
				</div>
			</div>	
		</div>

		<div class="members-acts-cont">
			<div class="ui segments">
				<div class="ui segment">
					<h3>Members' Activities</h3>
				</div>
				<div class="ui segment">
					<table class="ui striped very basic table">
						<thead>
							<th class="seven wide">Name</th>
							<th class="three wide">Documents</th>
							<th class="three wide">Collections</th>
							<th class="three wide">Forwards</th>
						</thead>
						<tbody>
							@foreach($members_stats as $member)
								@php
									$doc_stats = explode(',', $member->documents_stat);
									$coll_stats = explode(',', $member->collections_stat);
									$forward_stats = explode(',', $member->forwards_stat);
								@endphp
								<tr>
									<td>
										<img class="ui avatar image" src="{{ asset($member->profile_pic) }}">
										<span class="semibold">{{ $member->firstname.' '.$member->middlename.' '.$member->surname }}</span>
									</td>
									<td>
										<div>Sent: {{ $doc_stats[0] }}</div>
										<div>Received: {{ $doc_stats[1] }}</div>
									</td>
									<td>
										<div>Sent: {{ $coll_stats[0] }}</div>
										<div>Received: {{ $coll_stats[1] }}</div>
									</td>
									<td>
										<div>Sent: {{ $forward_stats[0] }}</div>
										<div>Received: {{ $forward_stats[1] }}</div>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
@else
	You have no groups yet!
@endif
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.bundle.min.js"></script>
	<script type="text/javascript">
		var labels = <?php echo json_encode($labels) ?>;
		var data = <?php echo json_encode($data) ?>;

		var ctx = document.getElementById("collChart").getContext('2d');

		var myChart = new Chart(ctx, {
			type: 'doughnut',
			data: {
				labels: labels,
				datasets: [{
					label: 'Breakdown of collections',
					data: data,
					backgroundColor: [
						'rgba(255, 99, 132, 0.4)',
		                'rgba(54, 162, 235, 0.4)',
		                'rgba(255, 206, 86, 0.4)'
					],
					hoverBackgroundColor: [
						'rgba(255, 99, 132, 1)',
		                'rgba(54, 162, 235, 1)',
		                'rgba(255, 206, 86, 1)'
					]
				}]
			},
			options:{
				responsive: false
			}
		});

		var ctx2 = document.getElementById("collChartStatus").getContext('2d');
		var labels_status = <?php echo json_encode($status_labels) ?>;
		var data_status = <?php echo json_encode($status_data) ?>;

		var myChart = new Chart(ctx2, {
			type: 'pie',
			data: {
				labels: labels_status,
				datasets: [{
					label: 'Breakdown of collection status',
					data: data_status,
					backgroundColor: [
						'rgba(255, 99, 132, 0.4)',
		                'rgba(54, 162, 235, 0.4)',
		                'rgba(255, 206, 86, 0.4)'
					],
					hoverBackgroundColor: [
						'rgba(255, 99, 132, 1)',
		                'rgba(54, 162, 235, 1)',
		                'rgba(255, 206, 86, 1)'
					]
				}]
			},
			options:{
				responsive: false
			}
		});

		$('#searchby-dropdown').dropdown();

		$('#searchBtn').click(function(){
			$('#search-container').removeClass('hidden');
			$('#stats-container').addClass('hidden');
			fetchItems();
		});

		function fetchItems(){
			var search = $('#search-box').val();
			var searchBy = $('#search-by').val();
			var id = $('#group-id').val();

			$.ajaxSetup({
	            headers: {
	                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	            }
	        });
	        $.ajax({
	            type: "POST",
	            url: "/dashboard/search",
	            data: {
	                search: search,
	                searchBy:searchBy,
	                id: id
	            },
	            success: function(docs){
	                $('#rows-cont').html(docs);
	            },
	            error: function(err){
	                console.log(err.responseText);
	            }
	        });
		}

		$('#back-btn').click(function(){
			$('#search-container').addClass('hidden');
			$('#stats-container').removeClass('hidden');
		});
	</script>
@endsection