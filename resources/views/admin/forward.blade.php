@extends('layouts.admin')

@section('content')
	<div class="ui small modal" id="trashForwardModal">
		<div class="header">
            Trash Forward(s)
        </div>
        <div class="content">
            Are you sure you want to delete your forwarded collection?
        </div>
        <div class="actions">
        	<button class="ui black right labeled icon button" coll-id="" id="trashForwardBtn">
	            <i class="trash alternate outline icon"></i>
	            Yes
	        </button>
	        <button class="ui deny button">No</button>
        </div>
	</div>

	<div class="ui small modal scrolling" id="receiversModal">
        <div class="header">
            Receivers
        </div>
        <div class="content" id="forwardReceivers">
            
        </div>
    </div>
    <div class="forwards-tools-cont">
    	<div class="ui stackable grid">
    		<div class="seven wide column">
    			
    		</div>
            <div class="three wide column">
                <div class="ui fluid selection dropdown" id="searchby-dropdown">
                    <input type="hidden" name="type" value="title" id="search-by">
                    <i class="dropdown icon"></i>
                    <div class="default text">Title</div>
                    <div class="menu">
                        <div class="item" data-value="title">Title</div>
                        <div class="item" data-value="receiver">Receiver</div>
                        <div class="item" data-value="code">Code</div>
                        <div class="item" data-value="tags">Tags</div>
                    </div>
                </div>
            </div>
    		<div class="six wide column">
    			<div class="ui fluid left icon input">
                    <input type="text" id="search-box" placeholder="Search forwarded collection by title">
                    <i class="search icon"></i>
                </div>
    		</div>
    	</div>
    	<div class="ui stackable grid">
    		<div class="five wide column">
    			<label class="label-mg-right">Sort by</label>
                <div class="ui selection dropdown forwards-dropdown" id="sort-dropdown">
                    <input type="hidden" value="created_at" name="type" id="sort-by">
                    <i class="dropdown icon"></i>
                    <div class="default text">Date added</div>
                    <div class="menu">
                        <div class="item" data-value="created_at">Date added</div>
                        <div class="item" data-value="title">Title</div>
                    </div>
                </div>

                <button class="circular ui icon button sort-btn" id="sort-button" sort-direction="desc">
                    <i class="arrow down icon" id="sort-icon"></i>
                </button>
    		</div>

    		<div class="eleven wide column">
    			<label class="label-mg-right">Status</label>
    			<div class="ui selection dropdown forwards-dropdown" id="filterTypeDropdown">
                    <input type="hidden" value="all" name="type" id="coll-status">
                    <i class="dropdown icon"></i>
                    <div class="default text">All</div>
                    <div class="menu">
                        <div class="item" data-value="all">All</div>
                        @foreach($types as $type)
                    	<div class="item" data-value="{{ $type->id }}">
                            {{ ucfirst($type->name ) }}   
                        </div>
                        @endforeach
                    </div>
                </div>
    		</div>
    	</div>
    </div>

	<div class="ui secondary pointing menu">
        <a href="/myforwards" class="@if(Request::is('myforwards')) active @endif item red">
            <i class="cloud upload icon"></i>
            My Forwards
        </a>
        <a href="/forwards/tome" class="@if(Request::is('forwards/tome')) active @endif item red">
            <i class="cloud download icon"></i>
            Forwarded To Me
        </a>
    </div>

	<div class="table-cont">
		<div class="ui stackable middle aligned vertically divided grid" id="forwardsContainer">
			@foreach($forwards as $forward)
				<div class="row">
					<div class="seven wide column">
						<a class="collection-link" href="/collection/{{ $forward->collection->id }}">
							<span class="semibold">{{ $forward->collection->title }}</span>
						</a>
					</div>
					<div class="five wide column">
						<img class="ui avatar image" src="{{ asset($forward->receiver->profile_pic) }}">
						<span class="semibold">{{ $forward->receiver->firstname." ".$forward->receiver->surname }}</span>
						@if(count($forward->collection->transmitsForwards) > 1)
							<span class="smaller-grey-margin"> and </span>
							<a class="ui label view-receivers" href="#receivers" coll-id="{{ $forward->collection->id }}" forward-id="{{ $forward->id }}">
								{{ count($forward->collection->transmitsForwards)-1 }} more
								<div class="ui mini inline loader" id="loader{{ $forward->id }}"></div>
							</a> 
						@endif
					</div>
					<div class="three wide column">
						{{ ucfirst($forward->collection->current_status) }}
					</div>
					<div class="one wide column">
						<div id="{{ $forward->id }}" class="ui floating dropdown icon">
	                        <i class="ellipsis vertical icon options-icon" forward-id="{{ $forward->id }}"></i>
	                        <div class="menu">
	                            <div class="item delete-forward" coll-id="{{ $forward->collection->id }}">
	                                <i class="trash alternate icon"></i>
	                                Trash
	                            </div>
	                        </div>
	                    </div>
					</div>
				</div>	
			@endforeach
		</div>
	</div>

	@if(count($forwards) > 0)
        <div class="pagination">
            <div class="ui stackable grid">
                <div class="five wide column">
                    Show
                    <div class="ui selection dropdown receive-dropdown" id="showItems">
                        <input type="hidden" value="20" name="type" id="howManyItems">
                        <i class="dropdown icon"></i>
                        <div class="default text">20 items</div>
                        <div class="menu">
                        	<div class="item" data-value="1">1 item</div>
                            <div class="item" data-value="10">10 items</div>
                            <div class="item" data-value="20">20 items</div>
                            <div class="item" data-value="25">25 items</div>
                            <div class="item" data-value="50">50 items</div>
                        </div>
                    </div>
                </div>
                <div class="five wide column">
                    Page
                    <div class="ui selection dropdown receive-dropdown" id="paginate">
                        <input type="hidden" value="{{ '1:'.$partitions['1'] }}" name="type" id="page">
                        <i class="dropdown icon"></i>
                        <div class="default text" id="pageText">1</div>
                        <div class="menu" id="pagesMenu">
                            @foreach($partitions as $index => $value)
                                <div class="item" data-value="{{ $index.':'.$value }}">{{ $index }}</div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="six wide column">
                    <div class="showing">
                        Showing 
                        <span id="numOfItems">{{ count($forwards) }}</span> out of 
                        {{ $numItems }}
                    </div>
                </div>
            </div>
        </div>
    @endif

	<script type="text/javascript" src="{{ asset('js/tablesort.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			function initiateDynamicFunctions(){
				$('.receive-dropdown').dropdown();

                $('#search-box').on('keyup', function(){
                    var search = $('#search-box').val();
                    var searchBy = $('#search-by').val();

                    if(search == ''){
                        resetPages();
                    }
                    else{
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        $.ajax({
                            type: "POST",
                            url: "/forwards/search",
                            data: {
                                search: search,
                                searchBy:searchBy

                            },
                            success: function(collection){
                                $('#forwardsContainer').empty();
                                $('#forwardsContainer').append(collection);
                                $('#forwardsContainer').transition('pulse');
                                removeMask();

                            },
                            error: function(err){
                                console.log(err.responseText);
                                console.log(searchBy);
                            }
                        });
                    }
                    
                });
                $("#searchby-dropdown").dropdown();

				$('.options-icon').each(function(){
					var id = $(this).attr('forward-id');
					$('#'+id).dropdown();
				});

				$('.delete-forward').click(function(){
					var collId = $(this).attr('coll-id');
					$('#trashForwardBtn').attr('coll-id', collId);

					$('#trashForwardModal').modal('show');
				});

				$('.view-receivers').click(function(){
					var id = $(this).attr('coll-id');
					var forwardId = $(this).attr('forward-id');
					$('#loader'+forwardId).addClass('active');
					$.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url: "/forwards/receivers",
                        data: {
                            id: id
                        },
                        success: function(data){
                        	$('#loader'+forwardId).removeClass('active');
                            $('#forwardReceivers').html(data);
                            $('#receiversModal').modal('show');
                        },
                        error: function(err){
                            console.log(err.responseText);
                        }
                    });
				});
			}

			$('.forwards-dropdown').dropdown();

			function initiateTools(){
				$('#sort-by').on('change', function(){
	                organize();
	            });

	            $('#coll-status').change(function(){
	            	organize();
	            });

				$('#sort-button').click(function(){
	                if($('#sort-button').attr('sort-direction') == 'desc'){
	                    $('#sort-icon').removeClass('down');
	                    $('#sort-icon').addClass('up');
	                    $('#sort-button').attr('sort-direction', 'asc');
	                }
	                else{
	                    $('#sort-icon').removeClass('up');
	                    $('#sort-icon').addClass('down');
	                    $('#sort-button').attr('sort-direction', 'desc');
	                }
	                $('#sort-button').transition('jiggle');
	                organize();
	            });

	            //If the user changes the number of items to be shown per page
	            $('#howManyItems').change(function(){
	                resetPages();
	            });

	            $('#page').change(function(){
	                organize();
	            });
			}

			initiateTools();

			//Reset number of items shown in every page
			function resetPages(){
                var num_items = $('#howManyItems').val();

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: "/reset/pages/my-forwards",
                    data: {
                        num_items: num_items
                    },
                    success: function(data){
                        $('#pagesMenu').empty();
                        var pages = JSON.parse(data);
                        
                        for(var i in pages){
                            $('#pagesMenu').append(
                                $('<div>').addClass('item').attr('data-value', i+':'+pages[i]).html(i)
                            );
                            if(i == $('#page').val().split(':')[0])
                                $('#page').val(i+':'+pages[i]);
                        }
                        if($('#page').val().split(':')[0] > Object.keys(pages).length){
                            $('#page').val('1:'+pages['1']);
                            $('#pageText').html('1');
                        }
                        organize();
                    },
                    error: function(err){
                        console.log(err.responseText);
                    }
                });
            }

			function organize(){
                var partition = $('#page').val().split(':');
                var collStatus = $('#coll-status').val();
                var sortBy = $('#sort-by').val();
                var sortDir = $('#sort-button').attr('sort-direction');
                var start = partition[1];
                var end = partition[2];

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: "/forward/organize",
                    data:{
                        sortBy: sortBy,
                        sortDir: sortDir,
                        status: collStatus,
                        start: start,
                        end: end,
                    },
                    success: function(forwards){
                        $('#forwardsContainer').empty();
                        $('#forwardsContainer').html(forwards);
                        initiateDynamicFunctions();
                    },
                    error: function(err){
                        console.log(err.responseText);
                    }
                });
            }

			function trashForward(id){
				$.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: "/forwards/trash",
                    data: {
                        id: id,
                        type: 'sent'
                    },
                    success: function(data){
                    	$('#trashForwardModal').modal('hide');
                        $('#forwardsContainer').empty();
                        $('#forwardsContainer').append(data);
                    },
                    error: function(err){
                        console.log(err.responseText);
                    }
                });
			}

			$('#trashForwardBtn').click(function(){
				var collId = $(this).attr('coll-id');
				trashForward(collId);
			});
			initiateDynamicFunctions();
		});
	</script>
@endsection