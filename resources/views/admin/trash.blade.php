@extends('layouts.admin')

@section('content')
    @foreach($collections as $collection)
        <div class="item coll-item" id="collection{{ $collection->id }}">
            <div id="{{ $collection->id }}" class="ui floating dropdown options-dropdown-coll">
                <i class="ellipsis vertical icon options-icon" document-id="{{ $collection->id }}"></i>
                <div class="menu">
                    <!--  -->
                </div>
            </div>
            <div class="ui tiny image">
                <img src="{{ asset($icons[$collection->type]) }}" id="type{{ $collection->id }}">
            </div>
            <div class="content">
                <div class="mg-bottom-12">
                    <div class="ui basic horizontal label status-label">
                        {{ ucfirst($collection->current_status)}}
                    </div>
                    <span class="meta">
                        {{ $collection->created_at->diffForHumans() }}
                    </span>
                </div>
                <a href="/collection/{{ $collection->id }}" class="header title-collection" id="title{{ $collection->id }}">
                    {{ $collection->title }}
                </a>
                <div class="receiver-info">
                    <span class="sent-to">sent to</span> <img class="ui avatar image" src="{{ asset($collection->receiver->profile_pic) }}">
                    <a href="#user" class="fullname-link">
                        {{$collection->receiver->firstname." ".$collection->receiver->middlename." ".$collection->receiver->surname }}
                    </a>
                    @if(count($collection->transmits) > 1)
                        and <div class="ui horizontal label">{{ count($collection->transmits)-1 }} more</div>
                    @endif
                </div>
            </div>
        </div>
    @endforeach

@endsection