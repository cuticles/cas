@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <form method="POST" class="ui form" action="{{ route('register') }}">
                <h4 class="ui dividing header">{{ __('Register') }}</h4>
                @csrf
                <div class="row">
                    <div class="field">
                        <label for="name" >{{ __('First Name') }}</label>
                        <input id="firstname" type="text" class="names form-control{{ $errors->has('firstname') ? ' is-invalid' : '' }}" name="firstname" value="{{ old('firstname') }}" required autofocus placeholder="First Name">

                            @if ($errors->has('firstname'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('firstname') }}</strong>
                                </span>
                            @endif
                        </div>
                            
                        <div class="field">
                            <label for="name" >Middle Name</label>
                            <input id="middlename" type="text" class="names form-control{{ $errors->has('middlename') ? ' is-invalid' : '' }}" name="middlename" value="{{ old('middlename') }}" required autofocus placeholder="Middle Name">

                            @if ($errors->has('middlename'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('middlename') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="four wide field">
                            <label for="name" >Surname</label>
                            <input id="surname" type="text" class="names form-control{{ $errors->has('surname') ? ' is-invalid' : '' }}" name="surname" value="{{ old('surname') }}" required autofocus placeholder="Surname">

                            @if ($errors->has('surname'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('surname') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="field">
                        <label id="civil_status" for="civil_status">{{ __('CS') }}</label>
                        
                            <input id="civil_status" type="text" class="form-control{{ $errors->has('civil_status') ? ' is-invalid' : '' }}" name="civil_status" required placeholder="e.g. Single">

                            @if ($errors->has('civil_status'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('civil_status') }}</strong>
                                </span>
                            @endif
                        </div>
                </div>
                
                 
                <div class="row">
                    <div class="field">
                        <label for="username">{{ __('Username') }}</label>
                        
                            <input id="username" type="text" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" value="{{ old('username') }}" required autofocus placeholder="Username">

                            @if ($errors->has('username'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('username') }}</strong>
                                </span>
                            @endif
                    </div>
                        
                    <div class="field">
                        <label for="email" >{{ __('E-Mail Address') }}</label>

                       
                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required placeholder="E-Mail">

                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif  
                    </div>
                    <div class="field">
                        <label for="birthdate">{{ __('Birthdate') }}</label>
                        
                            <input id="birthdate" type="text" class="form-control{{ $errors->has('birthdate') ? ' is-invalid' : '' }}" name="birthdate" required placeholder="yyyy-mm-dd">

                            @if ($errors->has('birthdate'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('birthdate') }}</strong>
                                </span>
                            @endif
                        </div>

                    <div id='sex'  class="ui field">
                

                        <label>Sex</label>
                        <select class="ui dropdown" name='sex'>
                          <option value="1" >Male</option>
                          <option value="0">Female</option>
                        </select>
                    </div>
                </div>
   
                <div class="row">           

                
                    <div class="three wide field">
                        <label for="mobile_num" >{{ __('Mobile No.') }}</label>
                        <input id="mobile_num" type="text" class="form-control{{ $errors->has('mobile_num') ? ' is-invalid' : '' }}" name="mobile_num" value="{{ old('mobile_num') }}" required autofocus placeholder="Mobile Number">

                            @if ($errors->has('mobile_num'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('mobile_num') }}</strong>
                                </span>
                            @endif
                        </div>
                            
                        <div id="telephone_num" class="three wide field">
                            <label for="name" >Telephone No.</label>
                            <input type="text" class="form-control{{ $errors->has('telephone_num') ? ' is-invalid' : '' }}" name="telephone_num" value="{{ old('telephone_num') }}" required autofocus placeholder="Telephone Number">

                            @if ($errors->has('telephone_num'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('telephone_num') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div id="division" class="three wide field">
                            <label for="name" >Division</label>
                            <input type="text" class="form-control{{ $errors->has('division') ? ' is-invalid' : '' }}" name="division" value="{{ old('division') }}" required autofocus placeholder="Division e.g. DPSM">

                            @if ($errors->has('division'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('division') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div id="designation" class="three wide field">
                        <label  for="designation">{{ __('Designation') }}</label>
                        
                            <input type="text" class="form-control{{ $errors->has('designation') ? ' is-invalid' : '' }}" name="designation" required placeholder="e.g. Instructor">

                            @if ($errors->has('designation'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('designation') }}</strong>
                                </span>
                            @endif
                        </div>
                </div>
                



                <div class="row">
                <div class="twelve wide field">
                    <label for="address">{{ __('Address') }}</label>
                    
                        <input id="address" type="text" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" required placeholder="Street name, Barangay/Subdivision, Town/City">

                        @if ($errors->has('address'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('address') }}</strong>
                            </span>
                        @endif
                </div>
                </div>
                <div class="row">
                    <div class="field">
                        <label for="password">{{ __('Password') }}</label>

                
                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                     
                    </div>
                </div>
                <div class="row">
                    <div class="field">
                        <label for="password-confirm">{{ __('Confirm Password') }}</label>
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>  
                    </div>
                </div>
                <div class="row">
                    <div class="ui button" tabindex="0">
                            <button type="submit" class="btn btn danger">
                                {{ __('Register') }}
                            </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('.names').each(function(){
            $(this).on('change', function(){
                var fullname = $('#firstname').val() + " " + $('#middlename').val() + " " + $('#surname').val();
                $('#fullname').val(fullname);
                console.log(fullname);
            });
        });
    });    
</script>
@endsection
