<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Login</title>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Semantic UI -->
    <link rel="stylesheet" type="text/css" href="{{ asset('semantic/dist/semantic.min.css') }}">
    <script
      src="https://code.jquery.com/jquery-3.1.1.min.js"
      integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
      crossorigin="anonymous"></script>
    <script type="text/javascript" src="{{ asset('semantic/dist/semantic.min.js') }}"></script>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('fontawesome/css/all.css') }}" rel="stylesheet">
    <style type="text/css">
      body{
        /*background-color: #D41F24;*/
        padding-top: 3%;
      }
      .centered-cont{
        width: 30%;
        margin: auto;
        z-index: 1;
      }
      .login-card{
        background-color: #f0f0f0;
        width: 100%;
        border-radius: 15px;
        padding: 6%;
      }
      .login-header{
        text-align: center;
      }
      .top-header-login{
        font-family: "Primetime", sans-serif;
        font-size: 5rem;
        color: #ffd4d5;
        margin-bottom: 35px;
        margin-top: 15px;
      }
      .sub-header-login{
        font-family: "Montserrat Medium", sans-serif;
        font-size: 1.5rem;
        color: #ffd4d5;
        margin-bottom: 15px;
      }
      .gradient-bg{
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        z-index: -1;
        background-image: linear-gradient(120deg, #f57175 0%, #B61519 100%);
        opacity: .8;
      }
      .pic-bg{
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background-image: url('{{ asset('uploads/tomasfh.jpg') }}');
        background-size: cover;
        z-index: -2;
      }
    </style>
</head>
<body>
  <div class="gradient-bg"></div>
  <div class="pic-bg"></div>
	<div class="centered-cont">
    <div class="login-header">
      <img src="svg/CAS-DMS-PRS.svg" width="42%" height="42%">
        <div class="top-header-login">CAS</div>
        <div class="sub-header-login">Management System Login</div>
    </div>
		<div class="login-card">
			<form class="ui form" method="POST" action="{{ route('login') }}">    
        {{ csrf_field() }}
        <div class="field">
          <label for="username" >{{ __('Username') }}</label>
          <div class="ui left icon input">
              <input id="username" type="username" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" value="{{ old('username') }}" required autofocus>
              <i class="user icon"></i>
              @if ($errors->has('username'))
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('username') }}</strong>
                  </span>
              @endif
          </div>
        </div>

        <div class="field">
          <label for="password" >{{ __('Password') }}</label>
          <div class="ui left icon input">
              <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
              <i class="lock icon"></i>
              @if ($errors->has('password'))
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('password') }}</strong>
                  </span>
              @endif
          </div>
        </div>

        <div class="field">
          <button type="submit" class="fluid red ui button">
          <i class="key icon"></i>
            {{ __('Login') }}
          </button>
        </div>
        
      </form>
		</div>
	</div>
</body>
</html>