@extends('layouts.admin')

@section('content')
	<div id="createGroupModal" class="ui small modal">
		<div class="header" id="delModalHeader">
			Create Group
		</div>
		<div class="scrolling content" id="delModalContent">
			<form class="ui form" id="createGroupForm" action="/create/group" enctype="multipart/form-data" method="POST">
                {{ csrf_field() }}
                <input type="hidden" name="create_coll_type" value="receive">
                <div class="field">
                    <label>Name</label>
                    <input type="text" name="name" placeholder="Enter name of the group">
                </div>
                <div class="field">
                    <label>Description</label>
                    <textarea rows="2" form="createGroupForm" name="description" placeholder="Enter description"></textarea>
                </div>
                <div class="field">
                    <label>Upload cover photo</label>
                    <div class="ui labeled left icon input">
                        <i class="file image outline icon"></i>
                        <input id="docFiles" type="file" name="cover_photo" placeholder="Add cover photo">
                    </div>
                </div>
                <div class="field">
                    <label>Group members</label>
                    <div class="ui fluid multiple search selection dropdown receive-dropdown">
                        <input type="hidden" name="members">
                        <i class="dropdown icon"></i>
                        <div class="default text">Choose members</div>
                        <div class="menu">
                            @foreach($users as $user)
                                <div class="item" data-value="{{ $user->id }}">
                                    <img class="ui avatar image" src="{{ asset($user->profile_pic) }}">
                                    {{ $user->firstname." ".$user->middlename." ".$user->surname }}
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </form>
		</div>
		<div class="actions">
			<button type="submit" class="ui approve red right labeled icon button" form="createGroupForm">
                <i class="plus icon"></i>
                Add
            </button>
			<button class="ui button deny">Cancel</button>
		</div>
	</div>

	<button class="big ui red button" id="addnewgroup-btn">
		<i class="plus icon"></i>
		Create Group
	</button>
	<!-- <h3 class="ui header">Your Groups</h3> -->
	<div class="ui link four stackable cards group-cards-cont">
		@foreach($groups as $group)
			<div class="card">
				<a class="image" href="/group/{{ $group->id }}">
					<img src="{{ asset($group->cover) }}">
				</a>
				<div class="content">
					<a class="header" href="/group/{{ $group->id }}">
						@if(strlen($group->name) > 20)
                            {{ substr($group->name, 0, 20) }}...
                        @else
                            {{ $group->name }}
                        @endif
					</a>
					<div class="meta">
						<i class="user icon"></i>
						{{ count($group->members) }} members
					</div>
				</div>
			</div>
		@endforeach
	</div>
	<script type="text/javascript">
		$(document).ready(function(){
			$('#addnewgroup-btn').click(function(){
				$('#createGroupModal').modal('show');
			});
		});

		$('.dropdown').dropdown();
	</script>
@endsection