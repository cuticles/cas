@extends('layouts.admin')

@section('content')
@if($group == 'none')
	No groups yet. :(
@else
<style type="text/css">
	.group-cover{
		background-image: url('{{ asset($group->cover) }}');
		background-size: cover;
		height: 175px;
		width: 100%;
		padding: 2%;
		position: relative;
		z-index: 2;
	}
	.overlay{
		position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        z-index: 3;
        background-image: linear-gradient(120deg, #B61519 0%, #f57175 100%);
        opacity: .8;
	}
	.app-content{
		padding-left: 21%;
		padding-right: 0;
		padding-top: 0;
	}
	.group-details{
		position: relative;
		z-index: 4;
		color: #ffffff !important;
	}
	.group-container{
		padding: 1%;
	}
	.members-list-cont{
		margin-top: 3%;
		padding: 2%;
	}
	#options-group{
		position: absolute !important;
		top: 10%;
		right: 2%;
		z-index: 5 !important;
	}
</style>
<div class="ui modal" id="removeMemberModal">
	<i class="close icon"></i>
    <div class="header">
        Add Members
    </div>
    <div class="content">
    	Are you sure you want to remove <span class="semibold" id="removeMemberName"></span>?
    </div>
    <div class="actions">
        <button class="ui black right labeled icon button" membership-id="" id="removeMemberBtn">
            <i class="trash alternate outline icon"></i>
            Yes
        </button>
        <button class="ui deny button">No</button>
    </div>
</div>
<div class="ui modal test" id="addMembersModal">
    <div class="header">
        Add Members
    </div>
    <div class="content">
    	<form method="POST" action="/group/addmembers" id="addMembersForm">
    		{{ csrf_field() }}
    		<input type="hidden" name="group_id" value="{{ $group->id }}">
    		<div class="ui fluid multiple search selection dropdown" id="addableMembers">
	        	<input type="hidden" name="members" id="addMemberInput">
	        	<i class="dropdown icon"></i>
	        	<div class="default text">Select member(s)</div>
	        	<div class="menu">
	        		@foreach($add_members as $user)
	        			<div class="item" data-value="{{ $user->id }}">
	        				<img class="ui avatar image" src="{{ asset($user->profile_pic) }}">
	        				{{ $user->firstname." ".$user->middlename." ".$user->surname }}
	        			</div>
	        		@endforeach
	        	</div>
	        </div>
	        <div class="error-msg" id="addMembersMsg"></div>
    	</form>
    </div>
    <div class="actions">
        <button class="ui button deny">Cancel</button>
        <button type="submit" form="addMembersForm" id="saveMembersBtn" class="ui disabled approve red right labeled icon button">
            <i class="user plus icon"></i>
            Add Member
        </button>
    </div>
</div>

<div class="group-cover">
	<div class="overlay">
	</div>
	<div class="group-details">
		<h2 class="ui header group-details" id="group-name">
			{{ $group->name }}
		</h2>
		<div class="num-members">
			{{ count($group->members) }} Member(s)
		</div>
		<div class="supervisor-detail">
			Supervised by {{ $group->handler->firstname." ".$group->handler->middlename." ".$group->handler->surname}}
		</div>
	</div>
	<!-- <div class="ui right pointing dropdown icon circular red button" id="options-group">
		<i class="ellipsis vertical icon"></i>
		<div class="menu">
			<div class="item" id="view-details-item">
				<i class="eye icon"></i>
				View Details
			</div>
			<div class="item" id="edit-group">
				<i class="edit icon"></i>
				Edit
			</div>
		</div>
	</div> -->
</div>
<div class="group-container">
	<div class="upper-grid">
		<div class="ui grid">
			<div class="eleven wide column">
				@if(Auth::user()->id == $group->supervisor)
				<button id="addMembersBtn" class="ui labeled icon red button test" data-tooltip="Add member" data-position="bottom center">
                    <i class="user plus icon"></i>
                    Add
                </button>
                @endif
			</div>
			<div class="five wide column">
				<div class="ui fluid left icon input">
					<input type="hidden" name="group-id" id="groupIdValue" value="{{ $group->id}}">
                    <input type="text" name="search-member" placeholder="Search member by name" id="searchMember">
                    <i class="search icon"></i>
                </div>
			</div>
		</div>
	</div>
	<div class="members-list-cont">
		<div class="ui middle aligned vertically divided grid">
			<div class="row group-member-row semibold">
				<div class="six wide column">
					Name
				</div>
				<div class="four wide column">
					Designation
				</div>
				<div class="five wide column">
					Email
				</div>
				<div class="one wide column">
				</div>
			</div>
		</div>
		<div class="ui middle aligned vertically divided grid" id="membersList">
			@foreach($group->members as $member)
				<div class="row group-member-row" id="member{{$member->id}}">
					<div class="six wide column">
						<img class="ui avatar image" src="{{ asset($member->user->profile_pic) }}">
						<span class="semibold">{{ $member->user->firstname." ".$member->user->middlename." ".$member->user->surname }}</span>
					</div>
					<div class="four wide column">
						{{ ucfirst($member->user->location->name) }}
					</div>
					<div class="five wide column">
						{{ $member->user->email }}
					</div>
					<div class="one wide column">
						@if(Auth::user()->id == $group->supervisor)
						<button class="ui icon button remove-member" data-tooltip="Remove {{ $member->user->firstname.' '.$member->user->middlename.' '.$member->user->surname }}" member-name="{{ $member->user->firstname.' '.$member->user->middlename.' '.$member->user->surname }}" data-position="left center" membership-id="{{ $member->id }}">
							<i class="times circle outline icon remove-member-icon"></i>
						</button>
						@endif
					</div>
				</div>
			@endforeach
		</div>
	</div>
</div>
@endif
<script type="text/javascript">
	$(document).ready(function(){
		function initiateDynamicFunctions(){
			$('.remove-member').click(function(){
				var membershipId = $(this).attr('membership-id');
				var name = $(this).attr('member-name');
				$('#removeMemberBtn').attr('membership-id', membershipId);

				$('#removeMemberName').html(name);
				$('#removeMemberModal').modal({
					closable: true
				}).modal('show');
			});
		}

		$('.dropdown').dropdown();

		$('#addMemberInput').change(function(){
			var value = $(this).val();
			if(value == ''){
				$('#saveMembersBtn').addClass('disabled');
				$('#addMembersMsg').html("This is a required field! You need to send this to at least one user.");
				$('#addMembersMsg').transition('bounce');
			}
			else{
				$('#saveMembersBtn').removeClass('disabled');
				$('#addMembersMsg').empty();
			}
		});

		initiateDynamicFunctions();

		$('#searchMember').on('keyup', function(){
			var search = $(this).val();
			var  groupId = $('#groupIdValue').val();

			$.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "/group/searchmember",
                data: {
                    id: groupId,
                    search: search
                },
                success: function(data){
                    var members = JSON.parse(data);
                    $('#membersList').empty();
                    for(var i = 0; i < members.length; i++){
                    	var member = members[i];
                    	$('#membersList').append(
                    		$('<div>').addClass('row group-member-row').attr('id', 'member'+member.membership_id).append(
                    			$('<div>').addClass('six wide column').append(
                    				$('<img>').addClass('ui avatar image').attr('src', member.profile_pic_path)
                    			).append(
                    				$('<span>').addClass('semibold').html(member.name)
                    			)
                    		).append(
                    			$('<div>').addClass('four wide column').html(member.position_name)
                    		).append(
                    			$('<div>').addClass('five wide column').html(member.email)
                    		).append(
                    			$('<div>').addClass('one wide column').append(
                    				$('<button>').addClass('ui icon button remove-member').attr('data-tooltip', 'Remove '+member.name).attr('data-position', 'left center').attr('member-name', member.name).attr('membership-id', member.membership_id).append(
	                    				$('<i>').addClass('times circle outline icon remove-member-icon')
	                    			)
                    			)
                    		)
                    	);
                    }
                    initiateDynamicFunctions();
                },
                error: function(err){
                    console.log(err.responseText);
                }
            });
		});

		$('#removeMemberBtn').click(function(){
			var id = $(this).attr('membership-id');
			$.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "/group/removemember",
                data: {
                    id: id
                },
                success: function(data){
                    $('#member'+id).remove();
                    $('#removeMemberModal').modal('hide');
                },
                error: function(err){
                    console.log(err.responseText);
                }
            });
		});

		$('#addMembersBtn').click(function(){
			$('#addMembersModal').modal({
	            inverted: true
	        }).modal('show');
		});

		$('#addableMembers').dropdown();
	});
</script>
@endsection