
<!-- @section('credentials') -->
	
	
<!-- 	<h1> Add Credentials </h1> <br> -->

<div id="add_credentials_modal" class="ui modal">

    <i class="close icon"></i>
    <div class="header" style = "background: lightgray">
        Add Credentials
    </div>

    <form action="addCredentials" id = "addCredentials" method="post" enctype="multipart/form-data" class="ui form">
			@csrf
			<div class = "ui segments">
				<div class="ui segment" style="position: relative;">
				
					  	<div class="equal width fields">
					    	<div class="field">
					      		<label> Title </label>
					      		<input type="text" placeholder="Name of credential" name="cred_name" required="">
					    	</div>
					    	<div class="field">
					      		<label> Category </label>
					      		<input type="text" name="cred_category" required="">
					    	</div>
					  	</div>

					  	<div class="equal width fields">
					    	<div class="field">
					      		<labels> Description </labels>
					      		<input type="text" name="cred_desc">
					    	</div>
					    	<div class="field">
					      		<labesl> Upload Document </labesl>
					      		<input type="file" name="cred_supportDoc">

					    	</div>
					  	</div>

						<div class="field" style = "float:right; margin-right: 0">
								<button type="submit" class="ui red button positive" form="addCredentials">Add</button>
						</div>
				
					<br> <br>
				</div>
			</div>
	</form>

</div> <!-- ADD CREDENTIAL MODAL DISPLAYED -->

  <!--  <br>
    <div class="actions">
        <button class="ui red approve right labeled icon button" user-id="" id="saveBtn">
            Save
            <i class="checkmark icon"></i>
        </button>

    </div> -->


<!-- 	<div style = "float:right; margin-right: 0">
		<a href = "/managePersonnel">
			<button class="ui red button"> BACK </button>
		</a>
	</div> <br> <br>
	 <form action="/addCredentials" id="addCredential" method="post" enctype="multipart/form-data" class="ui form">
		@csrf
		<input type="hidden" name="user_id" value="{{$id}}">

			<div class = "ui segments">
				<div class="ui segment">
					<h2> Add Credentials </h2>
				</div>

				<div class="ui segment" style="position: relative;">
					<div>
					  	<div class="equal width fields">
					    	<div class="field">
					      		<label> Title </label>
					      		<input type="text" placeholder="Name of credential" name="cred_name" required="">
					    	</div>
					    	<div class="field">
					      		<label> Category </label>
					      		<select name = "cred_category">
					      			<option name = "cert"> Certification </option>
					      			<option name = "public_service"> Public Service	</option>
					      			<option name = "research"> Research </option>	
					      			<option name = "seminar"> Seminar </option>	      		
					      		</select>
					    	</div>
					  	</div>

					  	<div class="equal width fields">
					    	<div class="field">
					      		<label> Description </label>
					      		<input type="text" name="cred_desc">
					    	</div>
					    	<div class="field">
					      		<label> Upload Document </label>
					      		<input type="file" name="cred_supportDoc">

					    	</div>
					  	</div>

						<div class="field" style = "float:right; margin-right: 0">
								<button type="submit" class="ui red button" form="addCredential">Add</button>
						</div>
					</div>
					<br> <br>
				</div>
			</div>
 -->
	<br>
	
	<div>
	
	</div>
<!-- @endsection -->
