
@extends('layouts.admin')

@section('content')
	<!-- DELETE Background MODAL POP -->
	<div id="deleteBackgroundModal" class="ui modal">
	    <i class="close icon"></i>
		<div class="header ui grey secondary inverted segment">
			Delete Background
		</div>
		<div class = "content" id="delBackgroundCont">
		</div>
	    <div class="actions">
	    	<button class = "ui red right labeled icon button" id = "deleteBackgroundBtn" background-id = "">
	       		Yes
	        	<i class = "checkmark icon"></i>
	        </button>
	        <button class = "ui deny button">
	            No
	        </button>
	    </div>
	</div>

<div class = "noprint">
	<!-- EDIT BACKGROUND MODAL POP -->
	<div id="editBackgroundModal" class="ui small modal">
	    <i class="close icon"></i>

		<div class="header ui grey secondary inverted segment">
			Edit Background
		</div>
		<div class = "content">
			<div class="ui inverted dimmer" id="editBgLoader">
	            <div class="ui text loader">Editing background</div>
	        </div>
		   	<div class="ui form">
		   		<input type="hidden" name="user_id" id="userIdInput-edit" value="{{ $user->id }}">
		   		<div class="field">
		   			<label>School</label>
		   			<input type="text" class="bg-school" name="school" id="schoolInput-edit" placeholder="Enter name of school" old="" mode="edit">
		   			<div class="status-msg status-check-bg-edit status-check-inc-degree-edit" status="okay" id="schoolMsg-edit" changed="false"></div>
		   		</div>
		   		<div class="two fields">
		   			<div class="field">
	   					<label>Start Year</label>
	   					<input type="text" name="start_year" class="bg-add-input bg-year-start" id="yearStartInput-edit" placeholder="Enter start year of study" mode="edit" old="" onkeypress="return isNumber(event)">
	   					<div class="status-msg status-check-bg-edit status-check-inc-degree-edit" status="okay" id="yearStartMsg-edit" changed="false"></div>
		   			</div>
		   			<div class="field">
	   					<label>End Year</label>
	   					<input type="text" name="start_year" class="bg-add-input bg-year-end" id="yearEndInput-edit" placeholder="Enter end year of study" mode="edit" old="" onkeypress="return isNumber(event)">
	   					<div class="status-msg status-check-bg-edit status-check-inc-degree-edit" status="okay" id="yearEndMsg-edit" changed="false"></div>
		   			</div>
		   		</div>
		   		<div class="error-msg" id="wrong-interval-edit"></div>
		   		<div class="field">
		   			<label>Educational Level</label>
	   				<div class="ui fluid selection dropdown">
                        <input type="hidden" name="degree_level" class="bg-add-input bg-educ-level" id="educLevelInput-edit" mode="edit" old="">
                        <i class="dropdown icon"></i>
                        <div class="default text" id="defaultTextEducLevel-edit">Select educational level</div>
                        <div class="menu">
                            <div class="disabled item" data-value="">Select educational level</div>
                            @foreach($levels as $level)
                            	<div class="item" data-value="{{ $level->id }}">
                            		{{ ucfirst($level->name) }}
                            	</div>
                            @endforeach
                        </div>
                    </div>
                    <div class="status-msg status-check-bg-edit status-check-inc-degree-edit" status="okay" id="educLevelMsg-edit" changed="false"></div>
		   		</div>
		   		<div class="hidden" id="college-edit">
			   		<div class="ui stackable grid">
			   			<div class="six wide column">
			   				<div class="field">
			   					<label>Degree Level</label>
				   				<div class="ui fluid selection dropdown">
		                            <input type="hidden" class="bg-add-input bg-degree-level" name="degree_level" id="degreeLevelInput-edit" mode="edit" old="">
		                            <i class="dropdown icon"></i>
		                            <div class="default text" id="defaultTextDegLvl-edit">Select degree level</div>
		                            <div class="menu">
		                                <div class="disabled item" data-value="">Select degree level</div>
		                                <div class="item" data-value="1">Associative</div>
		                                <div class="item" data-value="2">Bachelor's</div>
		                                <div class="item" data-value="3">Master's</div>
		                                <div class="item" data-value="4">Doctoral</div>
		                            </div>
		                        </div>
		                        <div class="status-msg status-check-inc-degree-edit" status="okay" id="degreeLevelMsg-edit" changed="false"></div>
	                    	</div>
			   			</div>
			   			<div class="ten wide column">
			   				<div class="field">
			   					<label>Course</label>
				   				<div class="ui search selection dropdown fluid">
		                            <input type="hidden" class="bg-add-input bg-course" name="degree_level" id="courseInput-edit" mode="edit" old="">
		                            <i class="dropdown icon"></i>
		                            <div class="default text" id="defaultTextCourse-edit">Select course</div>
		                            <div class="menu">
		                            	<div class="disabled item" data-value="">Select course</div>
		                            	@foreach($courses as $course)
		                            		<div class="item" data-value="{{ $course }}">{{ $course }}</div>
		                            	@endforeach
		                            </div>
		                        </div>
		                        <div class="status-msg status-check-inc-degree-edit" status="okay" id="courseMsg-edit" changed="false"></div>
			   				</div>
			   			</div>
			   		</div>
		   		</div>
		   		<div class="error-msg" id="changesStatus"></div>
		   </div> 
		</div>
	    <div class="actions">
	    	 <button type="button" class="ui disabled red right labeled icon button" id="editBackgroundBtn" background-id="">
	            Edit
	            <i class="edit icon"></i>
	        </button>
	        <button class="ui deny button">
	            Cancel
	        </button>
	    </div>
	</div>

	<!-- ADD BACKGROUND MODAL POP -->
	<div id="addBackgroundModal" class="ui small modal">
	    <i class="close icon"></i>

	   <div class="header ui grey secondary inverted segment">
			Add Background
		</div>
		<div class = "content">
			<div class="ui inverted dimmer" id="addBgLoader">
	            <div class="ui text loader">Adding background</div>
	        </div>
		   	<div class="ui form">
		   		<input type="hidden" name="user_id" id="userIdInput" value="{{ $user->id }}">
		   		<div class="field">
		   			<label>Supervisor</label>
	   				<div class="ui fluid search selection dropdown">
                        <input type="hidden" class="bg-add-input" name="supervisor" id="supervisor">
                        <i class="dropdown icon"></i>
                        <div class="default text" id="defaultTextSupervisor">Select your supervisor</div>
                        <div class="menu">
                            <div class="disabled item" data-value="">Select your supervisor</div>
                            @foreach($admins as $admin)
                            	<div class="item" data-value="{{ $admin->id }}">
                            		<img src="{{ asset($admin->profile_pic) }}" class="ui avatar image">
                            		<span class="semibold">
                            			{{ $admin->firstname.' '.$admin->middlename.' '.$admin->surname}}
                            		</span>
                            	</div>
                            @endforeach
                        </div>
                    </div>
                    <div class="status-msg status-check-bg-add status-check-inc-degree-add" status="wrong" id="supervisorMsg"></div>
		   		</div>
		   		<div class="field">
		   			<label>School</label>
		   			<input type="text" name="school" class="bg-add-input bg-school" id="schoolInput" placeholder="Enter name of school" mode="add">
		   			<div class="status-msg status-check-bg-add status-check-inc-degree-add" status="wrong" id="schoolMsg-add"></div>
		   		</div>
		   		<div class="two fields">
		   			<div class="field">
	   					<label>Start Year</label>
	   					<input type="text" name="start_year" class="bg-add-input bg-year-start" id="yearStartInput" placeholder="Enter start year of study" mode="add" onkeypress="return isNumber(event)">
	   					<div class="status-msg status-check-bg-add status-check-inc-degree-add" id="yearStartMsg-add" status="wrong"></div>
		   			</div>
		   			<div class="field">
	   					<label>End Year (optional)</label>
	   					<input type="text" name="start_year" class="bg-add-input bg-year-end" id="yearEndInput" placeholder="Enter end year of study" mode="add" onkeypress="return isNumber(event)">
	   					<div class="status-msg status-check-bg-add status-check-inc-degree-add" id="yearEndMsg-add" status="okay"></div>
		   			</div>
		   		</div>
		   		<div class="error-msg" id="wrong-interval-add"></div>
		   		<div class="field">
		   			<label>Educational Level</label>
	   				<div class="ui fluid selection dropdown">
                        <input type="hidden" name="educational_level" class="bg-add-input bg-educ-level" id="educLevelInput" mode="add">
                        <i class="dropdown icon"></i>
                        <div class="default text" id="defaultTextEducLevel">Select educational level</div>
                        <div class="menu">
                            <div class="disabled item" data-value="">Select educational level</div>
                            @foreach($levels as $level)
                            	<div class="item" data-value="{{ $level->id }}">
                            		{{ ucfirst($level->name) }}
                            	</div>
                            @endforeach
                        </div>
                    </div>
                    <div class="status-msg status-check-bg-add status-check-inc-degree-add" id="educLevelMsg-add" status="wrong"></div>
		   		</div>
		   		<div class="hidden" id="college">
			   		<div class="ui stackable grid">
			   			<div class="six wide column">
			   				<div class="field">
			   					<label>Degree Level</label>
				   				<div class="ui fluid selection dropdown">
		                            <input type="hidden" class="bg-add-input bg-degree-level" name="degree_level" id="degreeLevelInput" mode="add">
		                            <i class="dropdown icon"></i>
		                            <div class="default text" id="defaultTextDegLevel">Select degree level</div>
		                            <div class="menu">
		                                <div class="disabled item" data-value="">Select degree level</div>
		                                <div class="item" data-value="1">Associative</div>
		                                <div class="item" data-value="2">Bachelor's</div>
		                                <div class="item" data-value="3">Master's</div>
		                                <div class="item" data-value="4">Doctoral</div>
		                            </div>
		                        </div>
		                        <div class="status-msg status-check-inc-degree-add" id="degreeLevelMsg-add" status="wrong"></div>
	                    	</div>
			   			</div>
			   			<div class="ten wide column">
			   				<div class="field">
			   					<label>Course</label>
				   				<div class="ui fluid selection dropdown">
		                            <input type="hidden" name="degree_level" class="bg-add-input bg-course" id="courseInput" mode="add">
		                            <i class="dropdown icon"></i>
		                            <div class="default text" id="defaultTextCourse">Select course</div>
		                            <div class="menu">
		                            	<div class="disabled item" data-value="">Select course</div>
		                            	@foreach($courses as $course)
		                            		<div class="item" data-value="{{ $course }}">{{ $course }}</div>
		                            	@endforeach
		                            </div>
		                        </div>
		                        <div class="status-msg status-check-inc-degree-add" id="courseMsg-add" status="wrong"></div>
			   				</div>
			   			</div>
			   		</div>
		   		</div>
		   </div> 
		</div>
	    <div class="actions">
	    	 <button type="button" class="ui disabled red right labeled icon button" id="addBackgroundBtn">
	            Add
	            <i class = "checkmark icon"></i>
	        </button>
	        <button class = "ui deny button">
	            Cancel
	        </button>
	    </div>
	</div>

	<input type="hidden" name="id" id="user-id" value="{{ $user['id'] }}">
	
	
	<div id = "frame" class = "ui raised padded container segment">
		<div class = "ui six column grid" id="userCard">
	  		<div class = "row">
	  			<div class = "one wide column"> </div>
	  			<div class="right aligned column image-upload">
				    <img class="ui large circular image" id = "profile_pic" src="{{ asset($user['profile_pic']) }}">
				</div>
		   		<div class = "left aligned four wide column" id = "user_container">
		   			<br>
			    	<h4> {{$user->firstname." ".$user->middlename." ".$user->surname}} </h4>
			    	@if ($user->division != NULL)
					     {{$user->location->name}}		
					@endif
     						
					<br>
					@if ($user->position_id != NULL)
		    			{{$user->position->name}}		
					@endif 					
				</div>
				<div class = "four wide column" id = "point_container">
					<div class = "ui statistic" id = "point_stat">
					  	<div class="value">
					    	{{ $credPoints }}
					  	</div>
					  	<div class="label">
					   		Credential Points
					  	</div>
					
					  	<button class = "ui button small lightgrey" id = "cred_breakdown"> View Breakdown </button>
					 
<!-- CREDENTIAL POINTS BREAKDOWN -->
					  	<div class = "ui modal" id = "pointsModal">
	  						<i class="close icon"></i>

							<div class="header ui grey secondary inverted segment">
								Credential Points Breakdown
							</div>
							<div class="content">
								<table class="ui sortable celled fixed red table" id = "pointsBreakdown">
									<thead>
									    <tr>
									      <th> Name </th>
									      <th> Category </th>
									      <th> Points </th>
									    </tr>
									</thead>
									<tbody>
										@foreach ($credentials as $credential)
									    <tr>
									      <td> {{ $credential->title }} </td>
									      <td> {{ $credential->point->name }}  </td>
									      <td> {{ $credential->point->value }}</td>
									    </tr>
										   @endforeach
									</tbody>
								</table>

								<span class = "semibold" id = "total_label"> Total: </span>
								<span class = "semibold"> {{ $credPoints }} </span>
							
							</div>
						</div>
<!-- CREDENTIAL POINTS BREAKDOWN -->
					</div>
				</div>
				@if (Auth::user()->id == $user->id)
				<div class = "four wide column" id = "resumeBtn">
					<button class = "ui button large lightgrey" id = "generatePDF" > Generate Resume </button>
				</div>
				@endif
			</div>

	  	</div>

	
		<br> <br>
		<div class="two ui buttons">
				
		   		<button class = "ui button large red" id = "profile_button" src = "/personnelUtil/Profile/{id}"> PROFILE </button>
		  
		   		<button class = "ui button large red" id = "cred_button" src = "/personnelUtil/Credentials/{id}"> CREDENTIALS </button>
		   	
		</div>

		<span id = "main">
		</span>

		<div id = "showProfile" class = "@if(Request::is('personnelUtil/'.$user['id'].'/Credentials')) hidden @endif">
	
		<br> 
			<div class = "ui segments">
				<div class="ui grey secondary inverted segment">
					<h3> Basic Information </h3> 
				</div>

				<div class="ui segment" id="segmentFirst">
					<div class="ui column grid">
						<div class = "row">
							<div class = "five wide column">
								<label class = "label_name"> Name </label> <br>
								<label class = "label_name"> Birthday </label> <br>
								<label class = "label_name"> Address </label> <br>
								<label class = "label_name"> Sex </label> <br>
								<label class = "label_name"> Civil Status </label>
							</div>
							<div class = "eight wide column">
								{{$user->firstname}} {{$user->middlename}} {{$user->surname}} <br>
								{{ \Carbon\Carbon::parse($user->birthdate)->toFormattedDateString()}} <br>
								{{$user->address}} <br>
								{{$user->sex}} <br>
								{{$user->civil_status}}
							</div>	
						</div>
					</div>
					
				</div>

				<div class="ui grey secondary inverted segment">
					<h3> Contact Information </h3> 
				</div>

				<div class="ui segment">
					<div class="ui column grid">
						<div class = "row">
							<div class = "five wide column">
								<label class = "label_name"> Mobile Number </label> <br>
								<label class = "label_name"> Telephone Number </label> <br>
								<label class = "label_name"> Email </label> 
							
							</div>
							<div class = "eight wide column">
								{{$user->mobile_num}} <br>
								{{$user->telephone_num}} <br>
								{{$user->email}}
							</div>	
						</div>
					</div>
				</div>

				<div class ="ui grey secondary inverted segment">
					<h3> Work </h3> 
				</div>

				<div class="ui segment">
					<div class="ui column grid">
						<div class = "row">
							<div class = "five wide column">
								<label class = "label_name"> Position </label> <br>
								<label class = "label_name"> Division </label> 
							</div>
							<div class = "eight wide column">
								@if ($user->position_id != NULL)
					    			{{$user->position->name}}		
								@endif  <br>
								@if ($user->division != NULL)
					    			{{$user->location->name}}		
								@endif  <br>
							</div>	
						</div>
					</div>
				</div>

			</div>

			<br>
			<div class="ui segments" id="backgroundSegments">
				<div class="ui segment"><!-- 
					<div class="ui top left attached red label">Empty</div> -->
					<div class="ui meduium header">Educational Background</div>
					<button class="ui right labeled icon red button add-bg-btn" id="addBackground">
						<i class="graduation cap icon"></i>
						Add Background
					</button>
				</div>
				@foreach($backgrounds as $background)
					<div class="ui segment" id="background{{ $background->id }}">
						<div class="ui stackable horizontally divided grid">
							<div class="nine wide column">
								<div class="ui basic label" id="bgTypeName{{ $background->id }}">
									{{ ucfirst($background->type->name) }}
								</div>
								<div class="marginalized big">
									Started school at <span class="semibold" id="bgSchool{{ $background->id }}"> {{ $background->school }}</span>
								</div>
								<div id="courseCont{{ $background->id }}">
										@if($background->type->name == 'tertiary')
											<div class="marginalized">
												<span id="bgDegLevel{{ $background->id }}">{{ $degLevels[$background->degree_level] }}</span> Degree in 
												<span class="semibold" id="bgCourse{{ $background->id }}">
													{{ $background->course }}
												</span>
											</div>
										@endif
								</div>
								<div class="small-grey-margin marginalized">
									<i class="graduation cap icon"></i>
									<span id="bgYearStart{{ $background->id }}">{{ $background->year_start}}</span> - 
									<span id="bgYearEnd{{ $background->id }}">
										@if($background->year_end == null)
											Present
										@else
											{{ $background->year_end}}
										@endif
									</span>
								</div>
							</div>
							<div class="seven wide column bg-cont-relative">
								<div class="ui floating dropdown background-drpdwn" id="dropdownBg{{ $background->id }}">
		                            <i class="ellipsis vertical icon options-icon" background-id="{{ $background->id }}"></i>
		                            <div class="menu">
		                                <div class="item edit-background" background-school="{{ $background->school }}" background-type-id="{{ $background->type_id }}" background-type-name="{{ ucfirst($background->type->name) }}" background-year-start="{{ $background->year_start }}" background-year-end="{{ $background->year_end }}" background-degree-level="{{ $background->degree_level }}" background-course="{{ $background->course }}" background-degree-level-name="@if($background->type->name == 'tertiary') {{ $degLevels[$background->degree_level] }} @endif" background-id="{{ $background->id }}" id="editBackground{{ $background->id }}">
		                                    <i class="edit icon"></i>
		                                    Edit
		                                </div>
		                                <div class="item delete-background" background-id="{{ $background->id }}">
		                                    <i class="trash alternate icon"></i>
		                                    Delete
		                                </div>
		                            </div>
		                        </div>
								<div class="marginalized">
									Managed by
									<img src="{{ asset($background->appeal->user->profile_pic) }}" class="ui avatar image">
									<span class="semibold">
										{{ $background->appeal->user->firstname.' '.$background->appeal->user->middlename.' '.$background->appeal->user->surname }}
									</span>
								</div>
								<div class="marginalized">
									<div class="ui basic label">
										@if($background->appeal->is_verified == '1')
											<i class="check icon"></i>
											Verified
										@else
											<i class="times icon"></i>
											Not yet verified
										@endif
									</div>
								</div>
							</div>
						</div>
					</div>
				@endforeach
			</div>

		</div> <!-- DISPLAY PERSONAL INFO -->


<!-- OPEN/VIEW CREDENTIAL DETAILS -->
<div id="view_credentials_modal" class="ui long modal">

	<i class="close icon"> </i>

	<div class = "header ui grey secondary inverted segment">
		View Credentials
	</div> 

	<div class = "scrolling content">
		<div id = "show_credentials">
			<div class = "ui grid">
	  			<div class = "row" id = "first">
	    			<div class = "six wide column" id = "categ_con">
	      				<div class = "ui basic large label" id = "categ_label">
					    	<span class = "categInput"> </span> 
					    </div>
					    <span id = "date_updated_view"> </span>  
	    			</div>
		    		
		    	</div>
		    	<div class = "row" id = "second">
		    		<div class = "twelve wide column">
		    			<span class = "titleInput cred_title_view"> </span>
		    		</div>
		    	</div>
		    	<div class = "row" id = "third">
		    		<div class = "ten wide column">
		    			<span> Updated by </span> 
	
			    			<img class = "pp ui avatar image" src="">
			    			<span class = "updater_firstname"> </span>
			    			<span class = "updater_surname"> </span>

		    		</div>
		    	</div>
		    	<div class = "row" id = "fourth">
		    		<div class = "column">
		    			<span class = "descriptionInput"> </span>
		    		</div>
	    		</div>
	    	
			</div>	

			<div class = "ui four stackable cards" id = "credDocsCont">
				<div class = "ui fluid card">
            		<div class = "content">
					 	<i class="massive plus-doc ui huge red plus icon"></i>
            		</div>
            		<div class = "docName extra content">
					
					<div class = "add_doc" id="" cred-id = ""> Add Document </div>

            		</div>
            	</div>
            	
			</div>
		</div>
		<!-- ADD DOCUMENT MODAL POP -->
		
		<div class = "content" id = show_add_doc>
			<button class = "ui red right button back_doc"> BACK </button> <br>

			<div class="ui segment">
				<form action="/addDocument/{{$user->id}}" id="addDocument" method="post" enctype="multipart/form-data" class="ui form">
				@csrf
				<input type="hidden" name="cred_id" id = "addDoc" value= ""/>
			
					<div class="field">
						<label> Upload File </label>
						<input type="file" name="doc_name" placeholder="">
					</div>
						
				</form>
			</div>	

			<div class="field" id = "addDocBtn">
				<button type="submit" class="ui red button" form="addDocument" > Add </button>
			</div>
		</div>

		<div class = "content" id = show_rename_doc>
			<button class = "ui red right button back_doc"> BACK </button> <br> <br>
			<span id = "rename-success"> </span>

			<div class="ui segment">
				<div id="renameDocument" class="ui form">
					<input type="hidden" name="doc-id" id = "renameDoc" value = ""/>
			
					<div class="field">
						<label> Enter new Document Name: </label>
						<input type="text" name="title_name" placeholder="" id="titleDocName">
					</div>

					<div class="field" id="renameDocBtn">
						<button type="submit" class="ui red button" form="renameDocument"> Save </button>
					</div>
				</div>			
			</div>
		</div>

		<div class = "content" id = show_delete_doc>
			<div class = "content">
	   			<h3> <span class = "forDeleteModal"> Are you sure you want to delete this document? </span> </h3> 
				<input type="hidden" name="doc-id" id = "deleteDoc" value = ""/>
	    		<div class="actions" id = "deleteDocGrp">
	        		<button class = "ui red approve right labeled icon button" id = "deleteDocBtn" doc-id="">
	            		Yes
	            		<i class = "checkmark icon"></i>
	        		</button>
	        		<button class = "ui black deny button">
	            		No
	        		</button>

	    		</div>
	    	</div>
		</div>

	</div> <!-- SCROLLING CONTENT DIV -->
</div> <!-- VIEW CREDENTIALS MODAL DISPLAYED -->

<!-- VIEW CREDENTIAL MODAL -->


<!-- ADD CREDENTIALS MODAL POP -->
<div id = "add_credentials_modal" class = "ui modal">

	<i class="close icon"></i>
	<div class="header ui grey secondary inverted segment">
		Add Credential
	</div>
	
	<div id = "form_cred_container">
		<form action="/addCredentials/{{$user->id}}" id = "addCredentials" method="post" enctype="multipart/form-data" class="ui form">
			@csrf
			
		  	<div class="equal width fields">
		    	<div class="field credField">
		      		<label> Title </label>
		      		<input type="text" placeholder="Name of credential" name="cred_name">
		    	</div>
		    	<!-- DROPDOWN -->
		    	<div class="field credField">
		      		<label> Category </label>
		      		<div class="ui fluid search selection dropdown">
		      			<input type = "hidden" name = "cred_category">
		      			<i class = "dropdown icon"> </i>
		      			<div class = "default text"> Select Category </div>
		      			<div class = "menu">
		      				@foreach ($points as $point)
								<div class = "item" data-value = "{{$point->id}}"> {{$point->name}} </div>
							@endforeach
						</div>
		      		</div>
		      		<!-- <input type="text" name="cred_category"> -->
		    	</div>
		  	</div>

		  	<div class="equal width fields">
		    	<div class="field credField">
		      		<label> Description </label>
		      		<textarea name = "cred_desc" rows = "2" cols = "50" maxlength = "250"> </textarea>
		    	</div>
		    	<div class="field credField">
		      		<label> Upload Document </label>
		      		<input type="file" name="support_docs[]" multiple="">
		    	</div>
		  	</div>

			<div class = "field credField">
				<button type="submit" class="ui red button" form="addCredentials">Add</button>
			</div>
	
			<br> <br>
		</form>
	</div>
	


</div> <!-- ADD CREDENTIAL MODAL DISPLAYED -->

<!-- DELETE CREDENTIALS MODAL POP -->
<div id="delete_cred_modal" class="ui modal">
    <i class="close icon"></i>

   <div class="header ui grey secondary inverted segment">
		Delete Credential
	</div>
	<div class = "content">
	   <h3> <span class = "forDeleteModal"> Are you sure you want to delete this credential? </span> </h3> 
	</div>
    <div class="actions" id = "delete_grp_btn">
    	 <button class = "ui red approve right labeled icon button" id = "deleteCredBtn" cred-id = "">
            Yes
            <i class = "checkmark icon"></i>
        </button>
        <button class = "ui black deny button">
            No
        </button>

    </div>
</div>

<!-- EDIT CREDENTIALS MODAL POP -->
<div id="edit_credentials_modal" class="ui modal">

	<i class="close icon"> </i>
	<div class="header ui grey secondary inverted segment">
		Edit Credential
	</div>
	<div class = "content">
		<div class="ui form" id = "editCredForm">
		  	<div class="equal width fields">
		    	<div class="field credField">
		      		<label> Title </label>
		      		<input type="text" class="titleInput" id = "editTitleInput" placeholder="Name of credential" name="cred_name">
		    	</div>
		    	<!-- DROPDOWN -->
		    	
		    	<div class="field credField">
		      		<label> Category </label>
		      		<div class="ui fluid search selection dropdown">
		      			<input type = "hidden" name = "cred_category_point" id = "editCategPoint">
		      			<i class = "dropdown icon"> </i>
		      			<div class = "default text" id="defaultTextCategPoint">
		      			</div>
		      			<div class = "menu">
		      				@foreach ($points as $point)
								<div class = "item" data-value = "{{$point->id}}"> {{$point->name}} </div>
							@endforeach
						</div>
		      		</div>
		    	</div>
		  	</div>

		  	<div class="equal width fields">
		    	<div class="field credField">
		      		<label> Description </label>
		      		<textarea name = "cred_description" class="descriptionInput" rows = "2" cols = "50" maxlength = "250" id = "editDescript"> </textarea>
		    	</div>
		  	</div>
		</div>
	</div>
	<div class="actions">
		<button type="submit" class="ui red button" form="editCredentials" id="editCredBtn" cred-id=""> Save </button>
	</div>
</div> <!-- EDIT CREDENTIAL MODAL DISPLAYED -->

<!-- DISPLAY CREDENTIALS -->
<div id = "credentials" class = "@if(Request::is('personnelUtil/'.$user['id'].'/Profile')) hidden @endif">		
   
   	@if ((Auth::user()->id == $user->id) || (Auth::user()->is_admin == 1))
       	<div id = "search_sort_grp">
       		<div class="ui stackable grid">
       			<div class="eleven wide column">
		            <div class="ui left icon input">
		                <input type="text" id="search-box" user="{{$user->id}}" placeholder="Search Credential">
		                <i class="search icon"></i>
		            </div>
		        </div>
		        <div class="five wide column">
		        	<label>Sort by</label>
					<div class="ui selection dropdown">
                    	<input type="hidden" value="updated_at" name="type" id="sort-by">
						<i class="dropdown icon"></i>
						<div class = "sortBy default text"> Date Modified </div>
						<div class = "menu">
					  		<div class = "item" data-value = "updated_at"> Date Modified</div>
					  		<div class = "item" data-value = "title">Title</div>
					  		<div class = "item" data-value = "category"> Category </div>
					  	</div>
					</div>
					<button class="circular ui icon button sort-btn" id="sort-button" sort-direction="desc">
	                    <i class="arrow down icon" id="sort-icon"></i>
	                </button>
				</div>
       		</div>
       	</div>
        
		@if (Auth::user()->is_admin == 1)
		<div class = "ui segment">
			<div class="ui two column centered grid">
					<span class = "add_cred" id = "add_cred"> Add new credential </span>
			</div>
		</div>
		@endif

		<div id="credentialSegments">
			@foreach ($credentials as $credential)
		
			<div class = "ui segment" id = "{{$credential->id}}">
				<div class = "ui column grid">
					<div class = "row first_row">
					    <div class = "three wide column"> 
					    	<div class = "ui basic large label" id = "category{{$credential->id}}">
					    		{{$credential->point->name}} 
					    	</div>
					    </div>
					    <div class = "four wide column" id = "date_updated"> 
					    	{{$credential->updated_at->diffForHumans()}}  
					    </div>
					    <div class = "six wide column" id = "updater_col"> 
					    	Updated by 
					    	<a href = "/personnelUtil/{{$credential->userUpdater['id']}}" id = "updater_label">
						    	<img class = "ui avatar image" src="/{{$credential->userUpdater->profile_pic}}"> 
						    	{{$credential->userUpdater->firstname.' '.$credential->userUpdater->surname}} 
						    </a>
					    </div>
					    <div class = "three wide column" id = "cred_btn_grp">
					    	@if ($credential->updater == Auth::user()->id)
								<a class = "fas fa-edit fa-lg edit_cred" id="credentialEdit{{ $credential->id }}" cred-id = "{{ $credential->id }}" aria-hidden = "true" title = "Edit Credential" cred-title="{{ $credential->title }}" cred-category-point="{{ $credential->point->id }}" cred-category ="{{ $credential->point->name }}" cred-description="{{ $credential->description }}" cred-update = "{{$credential->updated_at}}"> </a> 

								<a class = "fa fa-trash fa-lg delete_cred" cred-id = "{{$credential->id}}" aria-hidden = "true" title = "Delete Credential">   </a>
								
							@endif
								<a class = "fas fa-eye fa-lg view_cred" aria-hidden = "true" title = "View Credential" id="credentialView{{ $credential->id }}" cred-id = "{{ $credential->id }}" aria-hidden = "true" cred-title="{{ $credential->title }}" cred-description="{{ $credential->description }}" cred-updater-first = "{{ $credential->userUpdater->firstname }}"  cred-updater-last = "{{ $credential->userUpdater->surname }}" cred-date = "{{ $credential->updated_at->diffForHumans() }}" cred-profile-pic = "/{{$credential->userUpdater->profile_pic}}" cred-category="{{ $credential->point->name }}">   </a> 
							
					    </div>
				  	</div>  <!-- FIRST ROW -->

				 	<div class = "row" id = "second_row">
				  		<div class = "seven wide column cred_title_view" id = "title{{ $credential->id }}">
				  			{{$credential->title}}
				  		</div>

				  		<!-- Document Count -->
				  		<div class = "three wide column" id = "count_files">
				  			<span id="numFiles{{ $credential->id }}">{{count($credential->documents)}}</span> files
				  		</div>
				  	
				  	</div>  <!-- SECOND ROW -->

				</div> <!-- COLUMN GRID CLOSING TAG -->
			</div>
			@endforeach
		</div>

		@if(count($credentials) > 0)
            <div class="pagination">
                <div class="ui stackable grid">
                    <div class="five wide column">
                        Show
                        <div class="ui selection dropdown receive-dropdown" id="showItems">
                            <input type="hidden" value="10" name="type" id="howManyItems">
                            <i class="dropdown icon"></i>
                            <div class="default text">10 items</div>
                            <div class="menu">
                            	<div class="item" data-value="2">2 items</div>
                                <div class="item" data-value="10">10 items</div>
                                <div class="item" data-value="20">20 items</div>
                                <div class="item" data-value="25">25 items</div>
                                <div class="item" data-value="50">50 items</div>
                            </div>
                        </div>
                    </div>
                    <div class="five wide column">
                        Page
                        <div class="ui selection dropdown receive-dropdown" id="paginate">
                            <input type="hidden" value="{{ '1:'.$partitions['1'] }}" name="type" id="page">
                            <i class="dropdown icon"></i>
                            <div class="default text" id="pageText"></div>
                            <div class="menu" id="pagesMenu">
                                @foreach($partitions as $index => $value)
                                    <div class="item" data-value="{{ $index.':'.$value }}">{{ $index }}</div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="six wide column">
                        <div class="showing">
                            Showing 
                            <span id="numOfItems">{{ count($credentials) }}</span> out of 
                            {{ $numItems }}
                        </div>
                    </div>
                </div>
            </div>
        @endif
	@else
	<span id = "not_allowed_msg"> 
		You are not allowed to view this user's credentials. 
	</span>
	@endif
	<br>
</div>

</div> <!-- END DIV OF NOPRINT -->

<div id = "resume" class = "printme">
	<span style = "text-align: center;">
		<h2> {{$user->firstname." ".$user->middlename." ".$user->surname}} </h2> 
		<h4>
			@if ($user->position_id != NULL)
			    {{$user->position->name}}		
			@endif 
		</h4> 
	</span>

	<span style = "text-align: left">
	
		<h3> Basic Information </h3> 
		<div style = "margin-left: 40px;">
			<span class = "semibold"> Birthday </span>
			<span style = "margin-left: 58px;"> {{ \Carbon\Carbon::parse($user->birthdate)->toFormattedDateString()}}
			</span> <br>
			<span class = "semibold"> Address </span>
			<span style = "margin-left: 60px;">
				{{$user->address}}
			</span> <br>
			<span class = "semibold"> Sex </span>
			<span style = "margin-left: 88px;">
				{{ ucfirst($user->sex) }}
			</span> <br>
			<span class = "semibold"> Civil Status </span>
			<span style = "margin-left: 40px;">
				{{$user->civil_status}}
			</span>	<br> <br>
		</div>
						

		<h3> Education </h3>

		<div style = "margin-left: 40px;">
			@foreach ($backgrounds as $background)
				<span class = "semibold"> 
					{{ ucfirst($background->type->name) }} 
				</span>
				<div style = "margin-left: 25px;"> {{ $background->school }} </div>
				<div style = "margin-left: 25px;"> {{ $background->year_start }} to 
					@if ($background->year_end == NULL)
						Present
					@else
						{{ $background->year_end }}
					@endif 
				</div>
				<div style = "margin-bottom: 20px; margin-left: 25px;"> 
					@if ($background->type->name == "tertiary")
						{{$degLevels[$background->degree_level]}}
						Degree in 
						<span class = "semibold"> 
							{{$background->course}} 
						</span>
					@endif
				</div>
			@endforeach
		</div>

		<h3> Credentials </h3>

		<div style = "margin-left: 40px;">
			@foreach ($credentials as $credential) 
				<span class = "semibold"> 
					{{ $credential->point->name }} 
				</span>
				<div style = "margin-left: 25px; font-size: 1.1rem"> {{ $credential->title }} </div>
				<div style = "margin-left: 25px;"> {{ $credential->description }} </div>

			@endforeach
		</div>

		<h3> Contact </h3>

		<div style = "margin-left: 40px;">
			<div> {{$user->mobile_num}} </div> 
			<div> {{$user->telephone_num}} </div> 
			<div> {{$user->email}} </div>
		</div>
</div>



<script type="text/javascript" src="{{ asset('js/xepOnline.jqPlugin.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/validations/backgrounds-validation.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#sort-dropdown-credential').dropdown();
		$('#pointsBreakdown').tablesort()

		$('#cred_breakdown').click(function(){
			$('#pointsModal')
			  .modal('show')
			;
		});

		function resetAddBgForm(){
			$('.status-check-inc-degree-add').each(function(){
				$(this).empty().attr('status', 'wrong');
			});

			$('.bg-add-input').each(function(){
				$(this).val('');
			});

			$('#defaultTextSupervisor').html('Select your supervisor');
			$('#defaultTextEducLevel').html('Select educational level');
			$('#defaultTextDegLevel').html('Select degree level');
			$('#defaultTextCourse').html('Select course');
		}

		function initiateDynamicFunctions() {
			//edit credentials 
			$('.edit_cred').click(function(){
				var id = $(this).attr('cred-id');
				var category_point = $(this).attr('cred-category-point');
				var title = $(this).attr('cred-title');
				var description = $(this).attr('cred-description');
				var category = $(this).attr('cred-category');
				
				
				$('#defaultTextCategPoint').html(category);
				$('#editCategPoint').val(category_point);
				$('#editTitleInput').val(title);
				$('#editDescript').val(description);
				$('#editCredBtn').attr('cred-id', id);
				$('#edit_credentials_modal').modal('show');

				console.log("Before: " + category_point);	
				console.log(" Before: " + category);	
			});

			// delete credentials
			$('.delete_cred').click(function(){
				var id = $(this).attr('cred-id');
				$('#deleteCredBtn').attr('cred-id', id);
				$('#delete_cred_modal').modal('show');
				$('#delete_cred_modal').modal({
					closable: true,	
				});
			});

			$('#generatePDF').click(function(){
				return xepOnline.Formatter.Format('resume',
	            {pageWidth:'216mm', pageHeight:'279mm', render: 'newwin'});
			});

			$('.delete-background').click(function(){
				$('#delBackgroundCont').empty();
				var clone = $('#background'+$(this).attr('background-id')).clone();
				$('#deleteBackgroundBtn').attr('background-id', $(this).attr('background-id'));
				clone.find('#dropdownBg'+$(this).attr('background-id')).remove();
				$('#delBackgroundCont').append(clone).append(
					$('<span>').addClass('semibold').append("Are you sure you want to delete this educational background?")
				);

				$('#deleteBackgroundModal').modal('show');
			});

			$('.edit-background').click(function(){
				$('#schoolInput-edit').val($(this).attr('background-school')).attr('old', $(this).attr('background-school'));
				$('#educLevelInput-edit').val($(this).attr('background-type-id')).attr('old', $(this).attr('background-type-id'));
				$('#yearStartInput-edit').val($(this).attr('background-year-start')).attr('old', $(this).attr('background-year-start'));
				$('#yearEndInput-edit').val($(this).attr('background-year-end')).attr('old', $(this).attr('background-year-end'));
				$('#defaultTextEducLevel-edit').html($(this).attr('background-type-name'));
				$('#editBackgroundBtn').attr('background-id', $(this).attr('background-id'));

				if($(this).attr('background-type-name') == 'Tertiary'){
					$('#college-edit').removeClass('hidden');
					$('#defaultTextDegLvl-edit').html($(this).attr('background-degree-level-name'));
					$('#defaultTextCourse-edit').html($(this).attr('background-course'));
					$('#degreeLevelInput-edit').val($(this).attr('background-degree-level')).attr('old', $(this).attr('background-degree-level'));
					$('#courseInput-edit').val($(this).attr('background-course')).attr('old', $(this).attr('background-course'));
				}
				else{
					$('#college-edit').addClass('hidden');
				}

				$('.status-check-bg-edit').each(function(){
                        $(this).empty().attr('status', 'okay').attr('changed', 'false');
                    });

                $('#changesStatus').empty();
                $('#editBackgroundBtn').addClass('disabled');
                $('#wrong-interval-edit').empty();

				$('#editBackgroundModal').modal('show');
			});
			$("#search-box").on('keyup', function(){
                var searchValue = $('#search-box').val();
                var id = $(this).attr('user');

                if(searchValue == ''){
                    resetPages();
                }
                else{
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url: "/credentials/search",
                        data: {
                            searchValue: searchValue,
                            id: id
                        },
                        success: function(docs){
                            $('#credentialSegments').empty();
                            $('#credentialSegments').append(docs);
                            
                        },
                        error: function(err){
                        	console.log(id)
                            console.log(err.responseText);
                        }
                    });
                }
            });

			$('.rename_doc').click(function(){
				var doc_id = $(this).attr('doc-id');
				var name = $(this).attr('file_name');
				$('#titleDocName').val(name);
				$('#renameDoc').val(doc_id);
				$('#show_rename_doc').show();
				$('#show_credentials').hide();
			});

			$('.delete_doc').click(function(){
				var doc_id = $(this).attr('doc-id');
				$('#deleteDocBtn').attr('doc-id', doc_id);
				$('#show_delete_doc').show();
				$('#show_credentials').hide();
			});

			$("#deleteDocBtn").click(function() {
				var id = $(this).attr('doc-id');

				 $.ajaxSetup({
	                    headers: {
	                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	                    }
	                });
	                $.ajax({
	                    type: "POST",
	                    url: "/deleteDocument",
	                    data: {
	                        id: id
	                    },
	                    success: function(datum){
	                    	datum = JSON.parse(datum);
	                    	$('#numFiles'+datum.id).html(datum.numDocs);
	                    	$('#'+datum.id).transition('pulse').transition('pulse');
	                    },
	                    error: function(err){
	                        console.log(err.responseText);
	                    }
	                });

			});

			$('#renameDocBtn').click(function(){
				var id = $('#renameDoc').val();
				var title = $('#titleDocName').val();

				$.ajaxSetup({
	                headers: {
	                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	                }
	            });
	            $.ajax({
	                type: "POST",
	                url: "/renameDocument",
	                data: {
	                    id: id,
	                    title: title
	                },
	                success: function(doc){
	                	doc = JSON.parse(doc);
	                	$('#fileName'+id).html(doc.file_name);
	                	$('#renameDoc'+id).attr('file_name', doc.file_name);
	                	$('#rename-success').html('Document has been successfully renamed!');

	                },
	                error: function(err){
	                    console.log(err.responseText);
	                }
	            });
			});

			$('.ui.dropdown').dropdown();
		}
		//If the user changes the number of items to be shown per page
        $('#howManyItems').change(function(){
            resetPages();
        });
            
		function resetPages(){
            var num_items = $('#howManyItems').val();
            var user_id = $('#user-id').val();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "/reset/pages/credentials",
                data: {
                    num_items: num_items,
                    user_id: user_id
                },
                success: function(data){
                    $('#pagesMenu').empty();
                    var pages = JSON.parse(data);
                    
                    for(var i in pages){
                        $('#pagesMenu').append(
                            $('<div>').addClass('item').attr('data-value', i+':'+pages[i]).html(i)
                        );
                        if(i == $('#page').val().split(':')[0])
                            $('#page').val(i+':'+pages[i]);
                    }
                    if($('#page').val().split(':')[0] > Object.keys(pages).length){
                        $('#page').val('1:'+pages['1']);
                        $('#pageText').html('1');
                    }
                    organize();
                },
                error: function(err){
                    console.log(err.responseText);
                }
            });
        }

		function organize(){
			var partition = $('#page').val().split(':');
            var sortBy = $('#sort-by').val();
            var sortDir = $('#sort-button').attr('sort-direction');
            var start = partition[1];
            var end = partition[2];
            var user_id = $('#user-id').val();
			$.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "/credentials/organize",
                data: {
                	user_id: user_id,
                	start: start,
                	end: end,
                	sortBy: sortBy,
                	sortDir: sortDir,
                },
                success: function(data){
                	$('#credentialSegments').empty();
                	$('#credentialSegments').append(data);
                	initiateDynamicFunctions();

                },
                error: function(err){
                    console.log(err.responseText);
                }
            });
		}

		$('#page').on('change', function(){
            organize();
        });

		$('#sort-by').on('change', function(){
            organize();
        });

		//Pressing of sorting button if Asc or Desc
		$('#sort-button').click(function(){
            if($('#sort-button').attr('sort-direction') == 'desc'){
                $('#sort-icon').removeClass('down');
                $('#sort-icon').addClass('up');
                $('#sort-button').attr('sort-direction', 'asc');
            }
            else{
                $('#sort-icon').removeClass('up');
                $('#sort-icon').addClass('down');
                $('#sort-button').attr('sort-direction', 'desc');
            }
            $('#sort-button').transition('jiggle');
            organize();
        });

		$('#deleteBackgroundBtn').click(function(){
			var id = $(this).attr('background-id');
			$.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "/background/delete",
                data: {
                	id: id
                },
                success: function(data){
                	$('#background'+data).remove();
                	$('#deleteBackgroundModal').modal('hide');
                },
                error: function(err){
                    console.log(err.responseText);
                }
            });
		});

		function editBackground(details){
			$('#editBgLoader').addClass('active');
			$.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "/background/edit",
                data: details,
                success: function(bg){
                	$('#editBgLoader').removeClass('active');
                	bg = JSON.parse(bg);
                	var edit = $('#editBackground'+bg.id);
                	edit.attr('background-school', bg.school);
					edit.attr('background-type-id', bg.type_id);
					edit.attr('background-year-start', bg.year_start);
					edit.attr('background-year-end', bg.year_end);
					edit.attr('background-type-name', bg.type_name);
					edit.attr('background-course', bg.course);
					
                	$('#bgSchool'+bg.id).html(bg.school);
                	$('#bgTypeName'+bg.id).html(bg.type_name);
                	$('#bgYearStart'+bg.id).html(bg.year_start);
                	$('#bgYearEnd'+bg.id).html(bg.year_end);

                	if(bg.type_name == 'Tertiary'){
						edit.attr('background-degree-level-name', bg.degree_level_name);
						edit.attr('background-degree-level', bg.degree_level);
						edit.attr('background-course', bg.degree_course);

                		if($('#courseCont'+bg.id).children().length != 0){
                			$('#bgDegLevel'+bg.id).html(bg.degree_level_name);
                			$('#bgCourse'+bg.id).html(bg.course);
                		}
                		else{
                			$('#courseCont'+bg.id).append(
                				$('<div>').addClass('marginalized').append(
                					$('<span>').attr('id', 'bgDegLevel'+bg.id).append(
                						bg.degree_level_name
                					)
                				).append(
                					' Degree in '
                				).append(
                					$('<span>').addClass('semibold').attr('id', 'bgCourse'+bg.id).append(
                						bg.course
                					)
                				)
                			);
                		}
                	}
                	else if(bg.type_name == 'Primary' || bg.type_name == 'Secondary'){
                		$('#courseCont'+bg.id).empty();
                	}
                	
                	initiateDynamicFunctions();
                	$('#editBackgroundModal').modal('hide');
                },
                error: function(err){
                    console.log(err.responseText);
                }
            });
		}

		$('#editBackgroundBtn').click(function(){
			var id = $(this).attr('background-id');
			var school = $('#schoolInput-edit').val();
			var educLevel = $('#educLevelInput-edit').val();
			var degreeLevel = $('#degreeLevelInput-edit').val();
			var course = $('#courseInput-edit').val();
			var start = $('#yearStartInput-edit').val();
			var end = $('#yearEndInput-edit').val();

			var details = {};

			if($('#defaultTextEducLevel-edit').html().trim() == 'Tertiary'){
				details = {
					id: id,
					school: school,
					type_id: educLevel,
					degree_level: degreeLevel,
					course: course,
					year_start: start,
					year_end: end
				};
			}
			else{
				details = {
					id: id,
					school: school,
					type_id: educLevel,
					year_start: start,
					year_end: end
				};
			}

			editBackground(details);
		});

		$('#educLevelInput-edit').change(function(){
			if($('#defaultTextEducLevel-edit').html().trim() == 'Tertiary'){
				$('#college-edit').removeClass('hidden');
			}
			else{
				$('#college-edit').addClass('hidden');
			}
		});

		function addBackground(details){
			$('#addBgLoader').addClass('active');
			$.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "/background/add",
                data: details,
                success: function(data){
                	$('#backgroundSegments').append(data);
                	$('#addBgLoader').removeClass('active');
                	$('#addBackgroundModal').modal('hide');
                	initiateDynamicFunctions();
                	resetAddBgForm();
                },
                error: function(err){
                    console.log(err.responseText);
                }
            });
		}

		$('#addBackgroundBtn').click(function(){
			var user_id = $('#userIdInput').val();
			var school = $('#schoolInput').val();
			var educLevel = $('#educLevelInput').val();
			var degreeLevel = $('#degreeLevelInput').val();
			var course = $('#courseInput').val();
			var start = $('#yearStartInput').val();
			var end = $('#yearEndInput').val();
			var supervisor = $('#supervisor').val();

			var details = {};

			if($('#defaultTextEducLevel').html().trim() == 'Tertiary'){
				details = {
					user_id: user_id,
					school: school,
					type_id: educLevel,
					degree_level: degreeLevel,
					course: course,
					year_start: start,
					year_end: end,
					supervisor: supervisor
				};
			}
			else{
				details = {
					user_id: user_id,
					school: school,
					type_id: educLevel,
					year_start: start,
					year_end: end,
					supervisor: supervisor
				};
			}

			addBackground(details);
		});

		$('.doc-side-option').dropdown();

		$('#educLevelInput').change(function(){
			if($('#defaultTextEducLevel').html().trim() == 'Tertiary'){
				$('#college').removeClass('hidden');
			}
			else{
				$('#college').addClass('hidden');
			}
		});

		$('#addBackground').click(function(){
			$('#addBackgroundModal').modal('show');
		});

		initiateDynamicFunctions();

		// toggle between tabs
		icons = {"pdf" : "file pdf outline icon", "docx" : "file word outline icon", "xls" : "file excel outline icon", "xlsx" : "file excel outline icon", "zip" : "file archive outline icon", "rar" : "file archive outline icon", "pptx" : "file powerpoint outline icon", "ppt" : "file powerpoint outline icon", "txt" : "file alternate outline icon", "doc" : "file word outline icon"};


		var locCred = 'http://localhost:8000/personnelUtil/'+$('#user-id').val()+'/Credentials';
		var locProf = 'http://localhost:8000/personnelUtil/'+$('#user-id').val()+'/Profile';

		if (window.location.href == locCred){
			$("#cred_button").addClass("active");
			$("#profile_button").removeClass("active");
			$("#showProfile").hide();
			$("#credentials").show();
			console.log("credential");
		}
		else {
			$("#cred_button").removeClass("active");
			$("#profile_button").addClass("active");
			$("#showProfile").show();
			$("#credentials").hide();
			console.log("profile");
		}
		
		$("#cred_button").click(function(){
			$("#cred_button").addClass("active");
			$("#profile_button").removeClass("active");
			$("#showProfile").hide();
			$("#credentials").show();
			console.log("credential");
		});

		$("#profile_button").click(function(){
			$("#profile_button").addClass("active");
			$("#cred_button").removeClass("active");
			$("#showProfile").show();
			$("#credentials").hide();
			console.log("profile")
		});

		// add credentials
		$('.add_cred').click(function(){
			$('#add_credentials_modal').modal('show');
			$('#add_credentials_modal').modal({
				closable: true,
				onApprove: function() {
				console.log('click #submit credential');
				$.get("/CredentialsController/addCredentials");
				}

			});
		});

		$('#deleteCredBtn').click(function() {
			var id = $('#deleteCredBtn').attr('cred-id');

			$.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "/deleteCredential",
                data: {
                    id: id
                },
                success: function(id){
                  $('#'+id).remove();
                },
                error: function(err){
                    console.log(err.responseText);
                }
            });
		});

		$('#editCredBtn').click(function(){
			var id = $(this).attr('cred-id');
			var category_point = $('#editCategPoint').val()
			var title = $('#editTitleInput').val();
			var description = $('#editDescript').val();
			

			$.ajaxSetup({
	            headers: {
	                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	            }
	        });
	        $.ajax({
	            type: "POST",
	            url: "/editCredential",
	            data: {
	                id: id,
	                category_point: category_point,
	                title: title,
	                description: description
	            },
	            success: function(cred){
	            	var credential = $('#credentialEdit'+id);
	            	var cred = JSON.parse(cred);
	            	credential.attr('cred-title', cred.title);
	            	credential.attr('cred-description', cred.description);
	            	credential.attr('cred-category-point', cred.point_id);
	            	credential.attr('cred-category', cred.category_name);
	                $('#title'+id).html(cred.title);
	         		$('#category'+id).html(cred.category_name);


	         		var viewCred = $('#credentialView'+id);
	         		viewCred.attr("cred-category", cred.category_name);
					viewCred.attr("cred-title", cred.title);
					viewCred.attr("cred-description", cred.description);

	                $('#edit_credentials_modal').modal('hide');
	               


	                console.log("NAGAWA")
	            },
	            error: function(err){
	                console.log(err.responseText);
	            }
	        });
		});

		$('.dynamicDoc').each(function(){
			$(this).remove();

			$.ajaxSetup({
	            headers: {
	                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	            }
	        });
	        $.ajax({
	            type: "POST",
	            url: "/editCredential",
	            data: {
	                id: id,
	                category: category,
	                title: title,
	                description: description
	            },
	            success: function(cred){
	            	var credential = $('#credential'+id);
	            	var cred = JSON.parse(cred);
	            	credential.attr('cred-title', cred.title);
	            	credential.attr('cred-description', cred.description);
	            	credential.attr('cred-category', cred.category);
	                $('#title'+id).html(cred.title);
	                $('#category'+id).html(cred.category);
	                $("#edit_credentials_modal").modal('hide');
	            },
	            error: function(err){
	                console.log(err.responseText);
	            }
	        });
		});

		function retrieveDocs(id) {
			$('.dynamicDoc').each(function(){
				$(this).remove();
			});
			$.ajaxSetup({
	            headers: {
	                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	            }
	        });
	        $.ajax({
	            type: "POST",
	            url: "/retrieveDocs",
	            data: {
	                id: id,
	            },
	            success: function(docs){
	            	// console.log(docs);
	            	docs = JSON.parse(docs);
	            	console.log(docs.length);
	            	for(var i in docs){
	            		doc = docs[i];
	            		$('#credDocsCont').append(
	            			$('<div>').addClass('ui fluid card dynamicDoc').append(
	            				$('<div>').addClass('content').append(
	            					$('<i>').addClass(icons[doc.file_extension]+' massive red plus-doc')
	            				)
	            			).append(
	            				$('<div>').addClass('docName extra content').append(
	            					$('<div>').addClass('add_doc doc_title').attr('id', 'fileName'+doc.id).append(
	            						doc.file_name
	            					)
	            				)
	            			).append(
	            				$('<div>').addClass('ui floating right dropdown doc-side-option').append(
	            						$('<i>').addClass('grey ellipsis vertical icon doc-side-icon')
	            				).append(
	            						$('<div>').addClass('menu').append(
	            							$('<a>').addClass('rename_doc item').attr('doc-id', doc.id).attr('file_name', doc.file_name).attr('id','renameDoc'+doc.id).append(
	            								"Rename")
	            						).append(
	            							$('<a>').addClass('delete_doc item').attr('doc-id', doc.id).append(
	            								"Delete")
	            						)
	            				)		
	            			)
	            		);
	            		initiateDynamicFunctions();
	            	}
	            },
	            error: function(err){
	                console.log(err.responseText);
	            }
	        });
		}

		//view credentials
		$('.view_cred').click(function(){
			$("#view_credentials_modal").modal('show');
			$("#show_credentials").show();
			$("#show_add_doc").hide();
			$('#show_rename_doc').hide();
			$('#show_delete_doc').hide();

			var id = $(this).attr("cred-id");
			var category = $(this).attr("cred-category");
			var title = $(this).attr("cred-title");
			var description = $(this).attr("cred-description");
			var updater_firstname = $(this).attr("cred-updater-first");
			var updater_surname = $(this).attr("cred-updater-last");
			var date_updated = $(this).attr("cred-date");
			var profile_pic = $(this).attr("cred-profile-pic");

			$('.categInput').html(category);
			$('.titleInput').html(title);
			$('.descriptionInput').html(description);
			$('.updater_firstname').html(updater_firstname);
			$('.updater_surname').html(updater_surname);
			$('#date_updated_view').html(date_updated);
			$('.pp').attr("src", profile_pic);
		
			$('#addDoc').val(id);

			retrieveDocs(id);
			console.log(id);
			initiateDynamicFunctions()
		});

		$('.add_doc').click(function(){
			$('#show_add_doc').show();
			$('#show_credentials').hide();
			$('#show_rename_doc').hide();
			$('#show_delete_doc').hide();
			// console.log(id);
		});

		$(".back_doc").click(function(){
			$("#show_credentials").show();
			$("#show_add_doc").hide();
			$('#show_rename_doc').hide();
			
			console.log("view credential");
		});

		$("#addCredentials").form({
			fields: ({
				cred_name: {
					identifier: 'cred_name',
					rules: [
					{
						type: 'empty',
						prompt : 'Please enter credential title',
					}]
				},
				cred_category: {
					identifier: 'cred_category',
					rules: [
					{
						type: 'empty',
						prompt : 'Please enter credential category'
					}]
				},
				cred_desc: {
					identifier: 'cred_desc',
					rules: [
					{
						type: 'maxLength[250]'
					}]
				},
				support_docs: {
					identifier: 'support_docs[]',
					rules: [
					{
						type: 'empty',
						prompt : 'Please choose file(s)'
					},
				
					{
				        type   : 'regExp',
				        value  : '/^(.*.((doc|docx|xlsx|xls|pdf|ppt|pptx|rar)$))?[^.]*$/i',
				        prompt : ('Please upload a file in suggested format. Allowed files: (doc|docx|xlsx|xls|pdf|ppt|pptx|rar)')
				    }]
				 }

			}),
			inline : true,
	    	on : 'blur',
		});

		$("#editCredForm").form({
			fields: ({
				cred_name: {
					identifier: 'cred_name',
					rules: [
					{
						type: 'empty',
						prompt : 'Please enter credential title',
					}]
				},
				cred_category: {
					identifier: 'empty',
					rules: [
					{
						type: 'empty',
						prompt : 'Please enter credential category'
					}]
				},
				cred_descrption: {
					identifier: 'cred_desc',
					rules: [
					{
						type: 'maxLength[250]'
					}]
				},
			}),
			inline : true,
	    	on : 'blur',
		});

		$("#addDocument").form({
			fields : {
			  	addDocument: {
			  	identifier : 'doc_name',
			    rules      : [
			      {
					type   : 'empty',
					prompt : ('Please upload a file')
			      },
			      {
			        type   : 'regExp',
			        value  : '/^(.*.((doc|docx|xlsx|xls|pdf|ppt|pptx|rar)$))?[^.]*$/i',
			        prompt : ('Please upload a file in suggested format. Allowed files: (doc|docx|xlsx|xls|pdf|ppt|pptx|rar)')
			      }
			    ]
			  }
			},
		});
	});	
</script>

@endsection


