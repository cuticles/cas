
@extends('layouts.admin')

@section('content')

	<h1> Edit Profile </h1>

	@if(count($errors) > 0)
		<div class = "alert alert-danger">
			<ul>
				@foreach($errors->all() as $error)
					<li> {{$error}} </li>
				@endforeach
			</ul>
		</div>	
	@endif

	<form action="/editPersonnel/{{$user['id']}}" id="editPersonnel" method="post" enctype="multipart/form-data" class="ui form">
			@csrf
		<input type="hidden" name="source" value="add"/>

			<div class = "ui segments">
				<div class="ui grey secondary inverted segment">
					<h3> Personal Information </h3> 
				</div> 

				<div class="ui segment">
					  	<div class="fields">
					  		<!-- @if (Auth::user()->id == $user->id)
						    	<div class="eight wide field">
						      		<label> Username </label>
						      		<input type = "text" id = "username" placeholder = "Username" name="username" value = "{{$user->username}}">
						    	</div>
						    	<div class="six wide field">
						      		<label> New Password </label>
						      		<input type="password" name="password" value-hidden = "{{$user->password}}">
						    	</div>
						    	<div class="hide-show-pw hide-show-btn" >
	        						<span class = "ui button" id = "show_pw"> Show </span>
	     						</div>
						    	<div class="six wide field">
						      		<label> Confirm Password </label>
						      		<input type="password" name="confirmPassword">
						    	</div>
						    	<div class="hide-show-con-pw hide-show-btn" >
	        						<span class = "ui button" id = "show_confirm_pw"> Show </span>
	     						</div>
     						@endif -->
					  	</div>
					  	<div class="equal width fields">
					    	<div class="field">
					      		<label>First name</label>
					      		<input type="text" placeholder="First Name" name="firstname" value = "{{$user->firstname}}" >
					    	</div>
					    	<div class="field">
					      		<label>Middle name</label>
					      		<input type="text" placeholder="Middle Name" name="middlename" value = "{{$user->middlename}}" >
					    	</div>
					    	<div class="field">
					     		<label>Last name</label>
					      		<input type="text" placeholder="Last Name" name="surname" value = "{{$user->surname}}" >
					    	</div>
					  	</div>

					  	<div class="equal width fields">
					    	<div class="field">
					      		<label>Birthday</label>
					      		<input type="date" placeholder="YYYY-MM-DD" name="birthdate" value = "{{$user->birthdate}}" id = "birthday" max = "">
					    	</div>
					    	<div class="field">
					      		<label>Address</label>
					      		<input type="text" placeholder="Address" name="address" value = "{{$user->address}}">
					    	</div>
					    	<div class="field">
					      		<label>Sex</label>

					      		<div class="ui fluid selection dropdown">
					      			<input type = "hidden" name = "sex" value = "{{$user->sex}}">
					      			<i class = "dropdown icon"> </i>
					      			<div class = "default text">
					      				@if ($user->sex == NULL)
					      					Choose one:
					      				@else
					      					{{$user->sex}}
					      				@endif
					      			</div>
					      			<div class = "menu">
					      				<div class = "item" data-value = "male"> Male </div>
 										<div class = "item" data-value = "female"> Female </div>
 									</div>
					      		</div>
					      	
					    	</div>
					    	<div class="field">
					      		<label>Civil Status</label>

					      		<div class="ui fluid selection dropdown">
					      			<input type = "hidden" name = "civil_status" value = "{{$user->civil_status}}">
					      			<i class = "dropdown icon"> </i>
					      			<div class = "default text">
					      				@if ($user->civil_status == NULL)
					      					Choose one:
					      				@else
					      					{{$user->civil_status}}
					      				@endif
					      			</div>
					      			<div class = "menu">
					      				<div class = "item" data-value = "Single"> Single </div>
 										<div class = "item" data-value = "Married"> Married </div>
 										<div class = "item" data-value = "Divorced"> Divorced </div>
 										<div class = "item" data-value = "Widowed"> Widowed </div>
 									</div>
					      		</div>
					      	
					
					    	</div>

					  	</div>

					  	<div class="equal width fields">
					  		<div class="field">
								@if (Auth::user()->is_admin == 1)
									<label>Upload Image</label>
									<input type="file" name="myimage" placeholder="" value = "{{$user->profile_pic}}">
								@endif
							</div>
						</div>

				</div>
			</div> <!-- end of div ui segment -->

				<div class="ui segments">
					<div class = "ui grey secondary inverted segment">
						<h3> Contact Information </h3> 
					</div>
			
					<div class = "ui segment">
					  	<div class="equal width fields">
					    	<div class="field">
					      		<label>Mobile Number</label>
					      		<input type="text" placeholder="Mobile No." name="mobile_num" value = "{{$user->mobile_num}}">
					    	</div>
					    	<div class="field">
					    		<span id = "telnum"> </span>
					    		
					      		<label>Telephone Number</label>
					      		 <input type="tel" name="telephone_num" pattern="[0-9]{3}-[0-9]{2}-[0-9]{2}" placeholder="e.g xxx-xx-xx>" value = "{{$user->telephone_num}}">
					      		
					    	</div>
					    	<div class="field">
								<label>E-mail</label>
								<input type="email" name="email" value = "{{$user->email}}">
							</div>
					  	</div>
					</div>
				</div>

				<div class="ui segments">
					<div class = "ui grey secondary inverted segment">
						<h3> Work and Education </h3> 
					</div>  

					<div class = "ui segment">
						<div class="equal width fields">
					    	<div class="field">
					      		<label>Position</label>
					      		<div class="ui fluid search selection dropdown">
					      			<input type = "hidden" name = "designation" value = "{{$user->position_id}}">
					      			<i class = "dropdown icon"> </i>
					      			<div class = "default text"> 
					      				@if ($user->position_id == NULL)
					      					Choose one:
					      				@else
					      					{{$user->position->name}}
					      				@endif
					      			</div>
					      			<div class = "menu">
					      				@foreach ($positions as $position)
 										<div class = "item" data-value = "{{$position->id}}"> {{$position->name}} </div>
 										@endforeach
 									</div>
					      		</div>
					    	</div>
					    	<div class="field">
					      		<label>Division</label>
					      		<div class="ui fluid search selection dropdown">
					      			<input type = "hidden" name = "division" value = "{{$user->division}}">
					      			<i class = "dropdown icon"> </i>
					      			<div class = "default text"> 
					      				@if ($user->division == NULL)
					      					Choose one:
					      				@else
					      					{{$user->location->name}}
					      				@endif
					      			</div>
					      			<div class = "menu">
					      				@foreach ($locations as $location)
 										<div class = "item" data-value = "{{$location->id}}"> {{$location->name}} </div>
 										@endforeach
 									</div>
					      		</div>
					      		
					    	</div>
					  	</div>

					</div>
				</div>	<!-- end of div ui segment -->	
		</div>

			<div id = "saveEditBtn">
				<button type = "submit" class  ="ui red button" form = "editPersonnel"> Save </button>
			</div>



		</div>
 	</form>



<!-- EDIT PROFILE -->

<script type = "text/javascript">

	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!
	var yyyy = today.getFullYear();

	if (dd < 10) {
        dd = '0' + dd
    } 
    if (mm < 10) {
        mm = '0'+ mm
    } 

	today = yyyy+'-'+mm+'-'+dd;
	document.getElementById("birthday").setAttribute("max", today);

	$('.dropdown').dropdown();

	$(function(){
  		$('.hide-show-pw').show();
  		$('.hide-show-pw span').addClass('show')
  	
  		$('.hide-show-pw span').click(function(){
    		if( $(this).hasClass('show') ) {
		       $(this).text('Hide');
		       $('input[name="password"]').attr('type','text');
		       $(this).removeClass('show');
   			} 
   			else {
		       $(this).text('Show');
		       $('input[name="password"]').attr('type','password');
		       $(this).addClass('show');
    		}
  		});
	
		$('#show_pw').on('click', function(){
			$('.hide-show span').text('Show').addClass('show');
			$('.hide-show').parent().find('input[name="password"]').attr('type','password');
		}); 
	});

	$(function(){
  		$('.hide-show-con-pw').show();
  		$('.hide-show-con-pw span').addClass('show')
  	
  		$('.hide-show-con-pw span').click(function(){
    		if( $(this).hasClass('show') ) {
		       $(this).text('Hide');
		       $('input[name="confirmPassword"]').attr('type','text');
		       $(this).removeClass('show');
   			} 
   			else {
		       $(this).text('Show');
		       $('input[name="confirmPassword"]').attr('type','password');
		       $(this).addClass('show');
    		}
  		});
	
		$('#show_confirm_pw').on('click', function(){
			$('.hide-show span').text('Show').addClass('show');
			$('.hide-show').parent().find('input[name="confirmPassword"]').attr('type','password');
		}); 
	});


	$("#editPersonnel").form({
		fields: ({
			firstname: {
				identifier: 'firstname',
				rules: [
				{
					type: 'empty',
					prompt : 'Please enter the firstname'
				}]
			},
			middlename: {
				identifier: 'middlename',
				rules: [
				{
					type: 'empty',
					prompt : 'Please enter the middlename'
				}]
			},
			surname: {
				identifier: 'surname',
				rules: [
				{
					type: 'empty',
					prompt : 'Please enter the surname'
				}]
			},
			designation: {
				identifier: 'designation',
				rules: [
				{
					type: 'empty',
					prompt : 'Please enter the designation/position'
				}]
			},
			division: {
				identifier: 'division',
				rules: [
				{
					type: 'empty',
					prompt : 'Please enter the division'
				}]
			},
			email: {
				identifier: 'email',
				rules: [
				{
					type: 'empty',
					prompt : 'Please enter email address'
				},
				{
					type: 'email',
					prompt : 'The email address you entered is invalid'
				}]
			},
			birthdate: {
				identifier: 'birthdate',
				rules: [
				{
					type: 'empty',
					prompt : 'Please enter birthdate'
				}]
			},
			address: {
				identifier: 'address',
				rules: [
				{
					type: 'empty',
					prompt : 'Please enter address'
				}]
			},
			telephone_num: {
				identifier: 'telephone_num',
				rules: [
				{
					type: 'empty',
					prompt : 'Please enter telephone number'
				},
				{
					type: 'regExp',
					value: /\d{3}\-\d{2}-\d{2}$/,
					prompt : 'Invalid format or non-integer found (Format: xxx-xx-xx)'
				}]
			},
			mobile_num: {
				identifier: 'mobile_num',
				rules: [
				{
					type: 'empty',
					prompt : 'Please enter mobile number'
				},
				{
					type: 'regExp',
					value: /^(09|\+639)\d{9}$/,
					prompt : 'Invalid format (Format: +639xxxxxxxx or 09xxxxxxxx)'
				}]
			},
			

		}),
		inline : true,
    	on : 'blur',
	});
			

</script>

@endsection