@extends('layouts.admin')

@section('content')
	@if(Auth::user()->is_admin == 1)
		@if(count($appeals) > 0)
			@foreach ($appeals as $appeal)
				<div class="ui segment" id="background{{ $appeal->background->id }}">
					<div class="ui stackable horizontally divided grid">
						<div class="nine wide column">
							<div class="ui basic label" id="bgTypeName{{ $appeal->background->id }}">
								{{ ucfirst($appeal->background->type->name) }}
							</div>
							<div class="marginalized big">
								Started school at <span class="semibold" id="bgSchool{{ $appeal->background->id }}"> {{ $appeal->background->school }}</span>
							</div>
							<div id="courseCont{{ $appeal->background->id }}">
									@if($appeal->background->type->name == 'tertiary')
										<div class="marginalized">
											<span id="bgDegLevel{{ $appeal->background->id }}">{{ $degLevels[$appeal->background->degree_level] }}</span> Degree in 
											<span class="semibold" id="bgCourse{{ $appeal->background->id }}">
												{{ $appeal->background->course }}
											</span>
										</div>
									@endif
							</div>
							<div class="small-grey-margin marginalized">
								<i class="graduation cap icon"></i>
								<span id="bgYearStart{{ $appeal->background->id }}">{{ $appeal->background->year_start}}</span> - 
								<span id="bgYearEnd{{ $appeal->background->id }}">
									@if($appeal->background->year_end == null)
										Present
									@else
										{{ $appeal->background->year_end}}
									@endif
								</span>
							</div>
						</div>
						<div class="seven wide column bg-cont-relative">
							<div class = "ui grid">
								<div class = "nine wide column">
									<img class="ui avatar image circular" src="{{$appeal->background->user->profile_pic}}">
									<span class = "semibold">
										{{$appeal->background->user->firstname}}
										{{$appeal->background->user->middlename}}
										{{$appeal->background->user->surname}}
									</span>	
								</div>
								<div class = "seven wide column">
									@if($appeal->is_verified == '0')
					           			<button class = "ui button red action-btn" appeal-id="{{ $appeal->id }}" mode="verify" id="actionBtn{{ $appeal->id }}">
					           				<div class="ui tiny inverted inline loader" id="actionBtnLoader{{ $appeal->id }}"></div>
					           				Verify
					           			</button>
					           		@else
					           			<button class = "ui button action-btn" mode="undo" appeal-id="{{ $appeal->id }}" id="actionBtn{{ $appeal->id }}"> 
					           				<div class="ui tiny inline loader" id="actionBtnLoader{{ $appeal->id }}"></div>
					           				Undo
					           			</button>
					           		@endif
				           		</div>
				           	</div>
						</div>
					</div>
				</div>
			@endforeach
			<script type="text/javascript">
				$('.action-btn').click(function(){
					var id = $(this).attr('appeal-id');
					var mode = $(this).attr('mode');
					$('#actionBtnLoader'+id).addClass('active');
					$.ajaxSetup({
		                headers: {
		                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		                }
		            });
		            $.ajax({
		                type: "POST",
		                url: "/verify/appeal",
		                data: {
		                    id: id,
		                    mode: mode
		                },
		                success: function(datum){
		                	$('#actionBtnLoader'+id).removeClass('active');
		                	if(mode == 'verify'){
		                		$('#actionBtn'+id).removeClass('red').attr('mode', 'undo').empty().append(
		                			$('<div>').addClass('ui tiny inline loader').attr('id', 'actionBtnLoader'+id)
		                		).append(
		                			"Undo"
		                		)
		                	}
		                	else{
		                		$('#actionBtn'+id).addClass('red').attr('mode', 'verify').empty().append(
		                			$('<div>').addClass('ui tiny inverted inline loader').attr('id', 'actionBtnLoader'+id)
		                		).append(
		                			"Verify"
		                		)
		                	}
		                	

		                },
		                error: function(err){
		                    console.log(err.responseText);
		                }
		            });
				});
			</script>
		@else
			<div class="ui placeholder segment">
				<div class="ui icon header">
					<i class="hand paper outline icon"></i>
					You don't have any pending requests yet.
				</div>
			</div>
		@endif
	@else
		<div class="ui placeholder segment">
			<div class="ui icon header">
				<i class="hand paper outline icon"></i>
				You are not allowed to verify requests because you are not an admin.
			</div>
		</div>
	@endif
@endsection