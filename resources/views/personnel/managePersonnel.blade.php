@extends('layouts.admin')

@section('content')
@if(Auth::user()->is_admin == 1)
    <div class="ui top attached tabular menu">
        <a class="item active" data-tab="first"> Personnel List </a>
        <a class="item" data-tab="second"> Personnel Statistics</a>
    </div>

    <div class="ui bottom attached tab segment active" data-tab="first">
        <div class="ui inverted dimmer" id="personnelLoader">
            <div class="ui text loader">Fetching personnel</div>
        </div>
        <div class = "ui grid">
            <div class = "row">

                <div class="ten wide column" id = "personnel_title">
                    <label class="label-mg-right">Sort by</label>
                    <div class="ui selection dropdown" id="sort-dropdown">
                        <input type="hidden" value="created_at" name="type" id="sort-by">
                        <i class="dropdown icon"></i>
                        <div class="default text">Date added</div>
                        <div class="menu">
                            <div class="item" data-value="created_at">Date added</div>
                            <div class="item" data-value="name">Name</div>
                            <div class="item" data-value="position">Position</div>
                        </div>
                    </div>

                    <button class="circular ui icon button sort-btn" id="sort-button" sort-direction="desc">
                        <i class="arrow down icon" id="sort-icon"></i>
                    </button>
                </div>
                <br> <br> <br>

                <div class="six wide column" id = "search_person" >
                    <div class="ui fluid left icon input">
                        <input type="text" id="search-box" placeholder="Search Personnel">
                        <i class="search icon"></i>
                    </div>
                </div>
            </div>
        </div>

        <div id = "delete-modal" class = "ui modal">
            <i class="close icon"> </i>
            <div class="header ui grey secondary inverted segment">
                Delete Personnel
            </div>
            <div class = "content">
                <h3> <span class = "forDeleteModal"> Are you sure you want to delete this user? </span> </h3> 
            </div>

            <div class="actions">
                <button class = "ui black deny button">
                    No
                </button>
                <button class = "ui red approve right labeled icon button" user-id = "" id = "deleteBtn">
                    Yes
                    <i class = "checkmark icon"></i>
                </button>

            </div>
        </div> 

        <div class = "ui grid" id="users-grid"> 
  
                <div class = "row">
                    <div class = "five wide column">
                        <h3> Name </h3>
                    </div>
                     <div class = "four wide column">
                        <h3> Position </h3>
                    </div>
                     <div class = "three wide column">
                        <h3> Credential Points </h3>
                    </div>
                    <div class = "four wide column user-opt">
                        <h3> </h3>
                    </div>
                </div>
            @foreach ($users as $user)
                <div class = "row">
                    <div class = "five wide column" id = "{{$user->id}}">
                        <img class="ui avatar image circular" src="{{$user['profile_pic']}}">
                        <a href="/personnelUtil/{{$user->id}}" title = "View Profile" id = "fullname">
                            {{ $user->firstname }} {{ $user->surname }} 
                        </a>
                    </div>
                    <div class = "four wide column">
                        @if ($user->position_id != NULL)
                             {{ $user->position->name }}
                        @endif
                    </div>
                     <div class = "three wide column" id = "cred_points">
                        {{ $user->points }}
                    </div>
                    <div class = "four wide column user-opt">
                        @if (Auth::user()->is_admin == 1)
                            <a href = "/editPersonnelForm/{{$user->id}}" class = "fas fa-user-edit fa-lg" aria-hidden = "true" title = "Edit Profile"> </a> 
                            <a class = "item delete fa fa-trash fa-lg" user-id = "{{$user->id}}" aria-hidden = "true" title = "Delete">   </a>
                        @endif
                    </div>
                </div>
            @endforeach            
        </div>
        @if(count($users) > 0)
            <div class="pagination">
                <div class="ui stackable grid">
                    <div class="five wide column">
                        Show
                        <div class="ui selection dropdown receive-dropdown" id="showItems">
                            <input type="hidden" value="10" name="type" id="howManyItems">
                            <i class="dropdown icon"></i>
                            <div class="default text">10 items</div>
                            <div class="menu">
                                <div class="item" data-value="2">2 items</div>
                                <div class="item" data-value="10">10 items</div>
                                <div class="item" data-value="20">20 items</div>
                                <div class="item" data-value="25">25 items</div>
                                <div class="item" data-value="50">50 items</div>
                            </div>
                        </div>
                    </div>
                    <div class="five wide column">
                        Page
                        <div class="ui selection dropdown receive-dropdown" id="paginate">
                            <input type="hidden" value="{{ '1:'.$partitions['1'] }}" name="type" id="page">
                            <i class="dropdown icon"></i>
                            <div class="default text" id="pageText"></div>
                            <div class="menu" id="pagesMenu">
                                @foreach($partitions as $index => $value)
                                    <div class="item" data-value="{{ $index.':'.$value }}">{{ $index }}</div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="six wide column">
                        <div class="showing">
                            Showing 
                            <span id="numOfItems">{{ count($users) }}</span> out of 
                            {{ $numUsers }}
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>

    <div class="ui bottom attached tab segment" data-tab="second">
        <div class="ui stackable grid">
            <div class="eight wide column">
                <div class="ui segments">
                    <div class="ui segment">
                        <h4>Personnel Position Statistics</h4>
                    </div>
                    <div class="ui segment">
                        <div class="charts">
                            <canvas width="300px" height="300px" id="posChart"></canvas>
                        </div>
                    </div>
                </div>
            </div>

            <div class="eight wide column">
                <div class="ui segments">
                    <div class="ui segment">
                        <h4>Personnel Division Statistics</h4>
                    </div>
                    <div class="ui segment">
                        <div class="charts">
                            <canvas width="300px" height="300px" id="divChart"></canvas>
                        </div>
                    </div>
                </div>
            </div> <br> <br>  

        </div>
    </div>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.bundle.min.js"> </script>
    <script type="text/javascript">
        $('.menu .item')
        .tab()
        ;


        var labels = <?php echo json_encode($labels) ?>;
        var data = <?php echo json_encode($data) ?>;

        var ctx = document.getElementById("posChart").getContext('2d');

        var myChart = new Chart(ctx, {
            type: 'doughnut',
            data: {
                labels: labels,
                datasets: [{
                    label: 'Personnel Statistics',
                    data: data,
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.4)',
                        'rgba(54, 162, 235, 0.4)',
                        'rgba(255, 206, 86, 0.4)',
                        'rgba(10, 200, 20, 0.4)',
                        'rgba(255, 255, 86, 0.4)',
                        'rgba(10, 106, 5, 0.4)',
                        'rgba(150, 75, 0, 0.4)',
                        'rgba(75, 10, 86, 0.4)',
                        'rgba(20, 48, 180, 0.4)',
                        'rgba(255, 70, 20, 0.4)',
                        'rgba(100, 0, 0, 0.4)',
                        'rgba(175, 50, 23, 0.4)',
                        'rgba(0, 60, 100, 0.4)',
                        'rgba(10, 50, 120, 0.4)',
                        'rgba(0, 0, 0, 0.4)', 
                        'rgba(200, 255, 230, 0.4)', 
                        'rgba(20, 100, 20, 0.4)', 
                    ],
                    hoverBackgroundColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(10, 200, 20, 1)',
                        'rgba(255, 255, 86, 1)',
                        'rgba(10, 106, 5,1)',
                        'rgba(150, 75, 0, 1)',
                        'rgba(75, 10, 86, 1)',
                        'rgba(20, 48, 180, 1)',
                        'rgba(255, 70, 20, 1)', 
                        'rgba(100, 0, 0, 1)',
                        'rgba(175, 50, 23, 1)',
                        'rgba(0, 60, 100, 1)',
                        'rgba(10, 50, 120, 1)',
                        'rgba(0, 0, 0, 1)',  
                        'rgba(200, 255, 230, 1)', 
                        'rgba(20, 100, 20, 1)',  
                    ]
                }]
            },
            options:{
                legend: false,
                responsive: false,
                cutoutPercentage: 30
            }
        });

      

        var labels2 = <?php echo json_encode($locs) ?>;
        var data2 = <?php echo json_encode($user_count_loc) ?>;

        var ctx2 = document.getElementById("divChart").getContext('2d');
      

        var myChart = new Chart(ctx2, {
            type: 'pie',
            data: {
                labels: labels2,
                datasets: [{
                    label: 'Division Statistics',
                    data: data2,
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.4)',
                        'rgba(54, 162, 235, 0.4)',
                        'rgba(255, 206, 86, 0.4)',
                        'rgba(10, 200, 20, 0.4)',
                        'rgba(255, 255, 86, 0.4)',
                        'rgba(10, 106, 5, 0.4)',
                        'rgba(150, 75, 0, 0.4)',
                        'rgba(75, 10, 86, 0.4)',
                        'rgba(20, 48, 180, 0.4)',
                        'rgba(255, 70, 20, 0.4)', 
                    ],
                    hoverBackgroundColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(10, 200, 20, 1)',
                        'rgba(255, 255, 86, 1)',
                        'rgba(10, 106, 5,1)',
                        'rgba(150, 75, 0, 1)',
                        'rgba(75, 10, 86, 1)',
                        'rgba(20, 48, 180, 1)',
                        'rgba(255, 70, 20, 1)',   
                    ]
                }]
            },
            options:{
                legend: false,
                responsive: false
            }
        });
    </script>
    <script type="text/javascript"> 

        function initiateDynamicFunctions(){
            $(".delete").click(function(){
                var id = $(this).attr("user-id");
                $("#deleteBtn").attr("user-id", id);
                $("#delete-modal").modal('show');
                $("#delete-modal").modal({
                    closable: true,
                });
            });
        }

        $(document).ready(function() {
            function resetPages(){
                var num_items = $('#howManyItems').val();

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: "/reset/pages/users",
                    data: {
                        num_items: num_items
                    },
                    success: function(data){
                        $('#pagesMenu').empty();
                        var pages = JSON.parse(data);
                        
                        for(var i in pages){
                            $('#pagesMenu').append(
                                $('<div>').addClass('item').attr('data-value', i+':'+pages[i]).html(i)
                            );
                            if(i == $('#page').val().split(':')[0])
                                $('#page').val(i+':'+pages[i]);
                        }
                        if($('#page').val().split(':')[0] > Object.keys(pages).length){
                            $('#page').val('1:'+pages['1']);
                            $('#pageText').html('1');
                        }
                        organize();
                    },
                    error: function(err){
                        console.log(err.responseText);
                    }
                });
            }

            $('#howManyItems').change(function(){
                resetPages();
            });

            function organize(){
                var partition = $('#page').val().split(':');
                var sortBy = $('#sort-by').val();
                var sortDir = $('#sort-button').attr('sort-direction');
                var start = partition[1];
                var end = partition[2];
                $('#personnelLoader').addClass('active');

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: "/personnel/organize",
                    data: {
                        sortBy: sortBy,
                        sortDir: sortDir,
                        start: start,
                        end: end,
                    },
                    beforeSend: function(){
                        
                    },
                    success: function(users){
                        $('#users-grid').empty();
                        $('#users-grid').append(users);
                        $('#numOfItems').html($('#users-grid').children().length-1);
                        $('#personnelLoader').removeClass('active');
                    },
                    error: function(err){
                        console.log(err.responseText);
                    }
                });
            }

            $('#sort-button').click(function(){
                if($('#sort-button').attr('sort-direction') == 'desc'){
                    $('#sort-icon').removeClass('down');
                    $('#sort-icon').addClass('up');
                    $('#sort-button').attr('sort-direction', 'asc');
                }
                else{
                    $('#sort-icon').removeClass('up');
                    $('#sort-icon').addClass('down');
                    $('#sort-button').attr('sort-direction', 'desc');
                }
                $('#sort-button').transition('jiggle');
                organize();
            });

            $('#page').change(function(){
                organize();
            });

            $('#sort-by').on('change', function(){
                organize();
            });
            
            $('.ui.dropdown').dropdown();

            initiateDynamicFunctions();

    		function removeMask(){
                $('#mask').removeClass('show');
            }

            function resetUsers(){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: "/resetUsers",
                    success: function(docs){
                        $('#users-grid').empty();
                        $('#users-grid').append(user);
                        initiateDynamicFunctions();
                    },
                    error: function(err){
                        console.log(err.responseText);
                    }
                });
            }

    		$('#search-box').on('keyup', function(){
                    var search = $('#search-box').val();
                    var searchBy = $('#search-by').val();

                    if(search == ''){
                        resetUsers();
                    }
                    else{
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        $.ajax({
                            type: "POST",
                            url: "/users/search",
                            data: {
                                search: search,
                                searchBy:searchBy
                            },
                            success: function(user){
                                $('#users-grid').empty();
                                // var users = JSON.parse(user);
                                // refillTable(users);
                                $('#users-grid').append(user);
                                initiateDynamicFunctions();
                            },
                            error: function(err){
                                console.log(err.responseText);
                            }
                        });
                    }
                    
                });

    		$("#deleteBtn").click(function() {
    			var id = $("#deleteBtn").attr('user-id');

    			 $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: "/deletePersonnel",
                    data: {
                        id: id,
                       
                    },
                    success: function(id, total_users){
                      $('#'+id).remove();
                    
                      console.log("OK");
                    },
                    error: function(err){
                        console.log(err.responseText);
                    }
                });
    		});
    	});
    </script>
@else
    <div class="ui placeholder segment">
        <div class="ui icon header">
            <i class="hand paper outline icon"></i>
            You are not allowed to manage personnel because you are not an admin.
        </div>
    </div>
@endif
@endsection
