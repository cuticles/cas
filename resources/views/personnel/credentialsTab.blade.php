<!-- OPEN/VIEW CREDENTIAL DETAILS -->
<div id="view_credentials_modal" class="ui long modal">

	<i class="close icon"> </i>

	<div class = "header ui grey secondary inverted segment">
		View Credentials
	</div> 

	<div class = "scrolling content">
		<div id = "show_credentials">
			<div class = "ui grid">
	  			<div class = "row" id = "first">
	    			<div class = "six wide column" id = "categ_con">
	      				<div class = "ui basic large label" id = "categ_label">
					    	<span class = "categInput"> </span> 
					    </div>
					    <span id = "date_updated_view"> </span>  
	    			</div>
		    		
		    	</div>
		    	<div class = "row" id = "second">
		    		<div class = "twelve wide column">
		    			<span class = "titleInput cred_title_view"> </span>
		    		</div>
		    	</div>
		    	<div class = "row" id = "third">
		    		<div class = "ten wide column">
		    			<span> Updated by </span> 
	
			    			<img class = "pp ui avatar image" src="">
			    			<span class = "updater_firstname"> </span>
			    			<span class = "updater_surname"> </span>

		    		</div>
		    	</div>
		    	<div class = "row" id = "fourth">
		    		<div class = "column">
		    			<span class = "descriptionInput"> </span>
		    		</div>
	    		</div>
	    	
			</div>	

			<div class = "ui four stackable cards" id = "credDocsCont">
				<div class = "ui fluid card">
            		<div class = "content">
					 	<i class="massive plus-doc ui huge red plus icon"></i>
            		</div>
            		<div class = "docName extra content">
					
					<div class = "add_doc" id="" cred-id = ""> Add Document </div>

            		</div>
            	</div>
            	
			</div>
		</div>
		<!-- ADD DOCUMENT MODAL POP -->
		
		<div class = "content" id = show_add_doc>
			<button class = "ui red right button back_doc"> BACK </button> <br>

			<div class="ui segment">
				<form action="/addDocument/{{$user->id}}" id="addDocument" method="post" enctype="multipart/form-data" class="ui form">
				@csrf
				<input type="hidden" name="cred_id" id = "addDoc" value= ""/>
			
					<div class="field">
						<label> Upload File </label>
						<input type="file" name="doc_name" placeholder="">
					</div>
						
				</form>
			</div>	

			<div class="field" id = "addDocBtn">
					<button type="submit" class="ui red button" form="addDocument" > Add </button>
				</div>
		</div>

		<div class = "content" id = show_rename_doc>
			<button class = "ui red right button back_doc"> BACK </button> <br> <br>
			<span id = "rename-success"> </span>

			<div class="ui segment">
				<div id="renameDocument" class="ui form">
					<input type="hidden" name="doc-id" id = "renameDoc" value = ""/>
			
					<div class="field">
						<label> Enter new Document Name: </label>
						<input type="text" name="title_name" placeholder="" id="titleDocName">
					</div>

					<div class="field" id="renameDocBtn">
						<button type="submit" class="ui red button" form="renameDocument"> Save </button>
					</div>
				</div>			
			</div>
		</div>

		<div class = "content" id = show_delete_doc>

			<div class = "content">
	   			<h3> <span class = "forDeleteModal"> Are you sure you want to delete this document? </span> </h3> 
				<input type="hidden" name="doc-id" id = "deleteDoc" value = ""/>
	    		<div class="actions" id = "deleteDocGrp">
	        		<button class = "ui red approve right labeled icon button" id = "deleteDocBtn" doc-id="">
	            		Yes
	            		<i class = "checkmark icon"></i>
	        		</button>
	        		<button class = "ui deny button">
	            		No
	        		</button>

	    		</div>
	    	</div>
		</div>

	</div> <!-- SCROLLING CONTENT DIV -->
</div> <!-- VIEW CREDENTIALS MODAL DISPLAYED -->

<!-- VIEW CREDENTIAL MODAL -->


<!-- ADD CREDENTIALS MODAL POP -->
<div id = "add_credentials_modal" class = "ui modal">

	<i class="close icon"></i>
	<div class="header ui grey secondary inverted segment">
		Add Credential
	</div>
	
	<div id = "form_cred_container">
		<form action="/addCredentials/{{$user->id}}" id = "addCredentials" method="post" enctype="multipart/form-data" class="ui form">
			@csrf
			
		  	<div class="equal width fields">
		    	<div class="field credField">
		      		<label> Title </label>
		      		<input type="text" placeholder="Name of credential" name="cred_name">
		    	</div>
		    	<!-- DROPDOWN -->
		    	<div class="field credField">
		      		<label> Category </label>
		      		<div class="ui fluid search selection dropdown">
		      			<input type = "hidden" name = "cred_category">
		      			<i class = "dropdown icon"> </i>
		      			<div class = "default text"> Select Category </div>
		      			<div class = "menu">
		      				@foreach ($points as $point)
								<div class = "item" data-value = "{{$point->id}}"> {{$point->name}} </div>
							@endforeach
						</div>
		      		</div>
		      		<!-- <input type="text" name="cred_category"> -->
		    	</div>
		  	</div>

		  	<div class="equal width fields">
		    	<div class="field credField">
		      		<label> Description </label>
		      		<textarea name = "cred_desc" rows = "2" cols = "50" maxlength = "250"> </textarea>
		    	</div>
		    	<div class="field credField">
		      		<label> Upload Document </label>
		      		<input type="file" name="support_docs[]" multiple="">
		    	</div>
		  	</div>

			<div class = "field credField">
				<button type="submit" class="ui red button" form="addCredentials">Add</button>
			</div>
	
			<br> <br>
		</form>
	</div>
	


</div> <!-- ADD CREDENTIAL MODAL DISPLAYED -->

<!-- DELETE CREDENTIALS MODAL POP -->
<div id="delete_cred_modal" class="ui modal">
    <i class="close icon"></i>

   <div class="header ui grey secondary inverted segment">
		Delete Credential
	</div>
	<div class = "content">
	   <h3> <span class = "forDeleteModal"> Are you sure you want to delete this credential? </span> </h3> 
	</div>
    <div class="actions" id = "delete_grp_btn">
    	 <button class = "ui red approve right labeled icon button" id = "deleteCredBtn" cred-id = "">
            Yes
            <i class = "checkmark icon"></i>
        </button>
        <button class = "ui black deny button">
            No
        </button>

    </div>
</div>

<!-- EDIT CREDENTIALS MODAL POP -->
<div id="edit_credentials_modal" class="ui modal">

	<i class="close icon"> </i>
	<div class="header ui grey secondary inverted segment">
		Edit Credential
	</div>
	<div class = "content">
		<div class="ui form" id = "editCredForm">
		  	<div class="equal width fields">
		    	<div class="field credField">
		      		<label> Title </label>
		      		<input type="text" class="titleInput" id = "editTitleInput" placeholder="Name of credential" name="cred_name">
		    	</div>
		    	<!-- DROPDOWN -->
		    	
		    	<div class="field credField">
		      		<label> Category </label>
		      		<div class="ui fluid search selection dropdown">
		      			<input type = "hidden" name = "cred_category_point" id = "editCategPoint">
		      			<i class = "dropdown icon"> </i>
		      			<div class = "default text" id="defaultTextCategPoint">
		      			</div>
		      			<div class = "menu">
		      				@foreach ($points as $point)
								<div class = "item" data-value = "{{$point->id}}"> {{$point->name}} </div>
							@endforeach
						</div>
		      		</div>
		    	</div>
		  	</div>

		  	<div class="equal width fields">
		    	<div class="field credField">
		      		<label> Description </label>
		      		<textarea name = "cred_description" class="descriptionInput" rows = "2" cols = "50" maxlength = "250" id = "editDescript"> </textarea>
		    	</div>
		  	</div>
		</div>
	</div>
	<div class="actions">
		<button type="submit" class="ui red button" form="editCredentials" id="editCredBtn" cred-id=""> Save </button>
	</div>
</div> <!-- EDIT CREDENTIAL MODAL DISPLAYED -->


<!-- /////////////////////////////////////////////////////////////////////////////////// -->

<!-- DISPLAY CREDENTIALS -->
<div id = "credentials" class = "@if(Request::is('personnelUtil/'.$user['id'].'/Profile')) hidden @endif">		
   
   	@if ((Auth::user()->id == $user->id) || (Auth::user()->is_admin == 1))
       	<div id = "search_sort_grp">
       		<div class="ui stackable grid">
       			<div class="eleven wide column">
		            <div class="ui left icon input">
		                <input type="text" id="search-box" placeholder="Search Credential">
		                <i class="search icon"></i>
		            </div>
		        </div>
		        <div class="five wide column">
		        	<label>Sort by</label>
					<div class="ui selection dropdown">
                    	<input type="hidden" value="updated_at" name="type" id="sort-by">
						<i class="dropdown icon"></i>
						<div class = "sortBy default text">Date Modified</div>
						<div class = "menu">
					  		<div class = "item" data-value = "updated_at"> Date Modified</div>
					  		<div class = "item" data-value = "title">Title</div>
					  		<div class = "item" data-value = "category"> Category </div>
					  	</div>
					</div>
					<button class="circular ui icon button sort-btn" id="sort-button" sort-direction="desc">
	                    <i class="arrow down icon" id="sort-icon"></i>
	                </button>
				</div>
       		</div>
       	</div>
        
		@if (Auth::user()->is_admin == 1)
		<div class = "ui segment">
			<div class="ui two column centered grid">
					<span class = "add_cred" id = "add_cred"> Add new credential </span>
			</div>
		</div>
		@endif

		<div id="credentialSegments">
			@foreach ($credentials as $credential)
		
			<div class = "ui segment" id = "{{$credential->id}}">
				<div class = "ui column grid">
					<div class = "row first_row">
					    <div class = "three wide column"> 
					    	<div class = "ui basic large label" id = "category{{$credential->id}}">
					    		{{$credential->point->name}} 
					    	</div>
					    </div>
					    <div class = "four wide column" id = "date_updated"> 
					    	{{$credential->updated_at->diffForHumans()}}  
					    </div>
					    <div class = "six wide column" id = "updater_col"> 
					    	Updated by 
					    	<a href = "/personnelUtil/{{$credential->userUpdater['id']}}" id = "updater_label">
						    	<img class = "ui avatar image" src="/{{$credential->userUpdater->profile_pic}}"> 
						    	{{$credential->userUpdater->firstname.' '.$credential->userUpdater->surname}} 
						    </a>
					    </div>
					    <div class = "three wide column" id = "cred_btn_grp">
					    	@if ($credential->updater == Auth::user()->id)
								<a class = "fas fa-edit fa-lg edit_cred" id="credentialEdit{{ $credential->id }}" cred-id = "{{ $credential->id }}" aria-hidden = "true" title = "Edit Credential" cred-title="{{ $credential->title }}" cred-category-point="{{ $credential->point->id }}" cred-category ="{{ $credential->point->name }}" cred-description="{{ $credential->description }}" cred-update = "{{$credential->updated_at}}"> </a> 

								<a class = "fa fa-trash fa-lg delete_cred" cred-id = "{{$credential->id}}" aria-hidden = "true" title = "Delete Credential">   </a>
								
							@endif
								<a class = "fas fa-eye fa-lg view_cred" aria-hidden = "true" title = "View Credential" id="credential{{ $credential->id }}" cred-id = "{{ $credential->id }}" aria-hidden = "true" cred-title="{{ $credential->title }}" cred-description="{{ $credential->description }}" cred-updater-first = "{{ $credential->userUpdater->firstname }}"  cred-updater-last = "{{ $credential->userUpdater->surname }}" cred-date = "{{ $credential->updated_at->diffForHumans() }}" cred-profile-pic = "/{{$credential->userUpdater->profile_pic}}" cred-category="{{ $credential->point->name }}">   </a> 
							
					    </div>
				  	</div>  <!-- FIRST ROW -->

				 	<div class = "row" id = "second_row">
				  		<div class = "seven wide column cred_title_view" id = "title{{ $credential->id }}">
				  			{{$credential->title}}
				  		</div>

				  		<!-- Document Count -->
				  		<div class = "three wide column" id = "count_files">
				  			<span id="numFiles{{ $credential->id }}">{{count($credential->documents)}}</span> files
				  		</div>
				  	
				  	</div>  <!-- SECOND ROW -->

				</div> <!-- COLUMN GRID CLOSING TAG -->
			</div>
			@endforeach
		</div>

		@if(count($credentials) > 0)
            <div class="pagination">
                <div class="ui stackable grid">
                    <div class="five wide column">
                        Show
                        <div class="ui selection dropdown receive-dropdown" id="showItems">
                            <input type="hidden" value="10" name="type" id="howManyItems">
                            <i class="dropdown icon"></i>
                            <div class="default text">10 items</div>
                            <div class="menu">
                            	<div class="item" data-value="2">2 items</div>
                                <div class="item" data-value="10">10 items</div>
                                <div class="item" data-value="20">20 items</div>
                                <div class="item" data-value="25">25 items</div>
                                <div class="item" data-value="50">50 items</div>
                            </div>
                        </div>
                    </div>
                    <div class="five wide column">
                        Page
                        <div class="ui selection dropdown receive-dropdown" id="paginate">
                            <input type="hidden" value="{{ '1:'.$partitions['1'] }}" name="type" id="page">
                            <i class="dropdown icon"></i>
                            <div class="default text" id="pageText"></div>
                            <div class="menu" id="pagesMenu">
                                @foreach($partitions as $index => $value)
                                    <div class="item" data-value="{{ $index.':'.$value }}">{{ $index }}</div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="six wide column">
                        <div class="showing">
                            Showing 
                            <span id="numOfItems">{{ count($credentials) }}</span> out of 
                            {{ $numItems }}
                        </div>
                    </div>
                </div>
            </div>
        @endif
	@else
	<span id = "not_allowed_msg"> 
		You are not allowed to view this user's credentials. 
	</span>
	@endif
	<br>
</div>