<div class = "ui modal" id = "pointsModal">
	  <i class="close icon"></i>

	<div class="header ui grey secondary inverted segment">
		Credentials Point Breakdown
	</div>
	<div class="content">
		<table class="ui compact red table">
			<thead>
			    <tr>
			      <th> Name </th>
			      <th> Category </th>
			      <th> Points </th>
			    </tr>
			</thead>
			<tbody>
				@foreach ($credentials as $credential)
				    <tr>
				      <td> {{ $credential->title }}</td>
				      <td> {{ $credential->point->name }}</td>
				      <td> {{ $credential->point->value }}</td>
				    </tr>
				   @endforeach
				<tr>
					<td class = "semibold"> Total </td>
					<td> </td>
					<td> {{ $cred_points }} </td>>
				</tr>
			
			</tbody>
		</table>
	</div>
</div>

<script link = "text/javascript">
	$('#cred_breakdown').click(function(){
		$('#pointsModal')
		  .modal('show')
		;
	});

</script>




