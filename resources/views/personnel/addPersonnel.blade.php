
@extends('layouts.admin')

@section('content')
@if(Auth::user()->is_admin == 1)
<!-- <div class = "ui modal" id = "addModal"> -->	
	
	<h1> Add Personnel </h1> 

		<form action="/addPersonnel" id="addPersonnel" method="post" enctype="multipart/form-data" class="ui form">
			@csrf
		<input type="hidden" name="source" value="add"/>

			<div class = "ui segments">
				<div class="ui grey secondary inverted segment">
					<h3> Personal Information </h3> 
				</div>

				<div class="ui segment">
					<div>
					  	<div class="fields">
					    	<div class="eight wide field">
					      		<label> Username </label>
					      		<input type="text" placeholder="Username" name="username">
					    	</div>
					    	<div class="seven wide field">
					      		<label> Password </label>
					      		<input type="password" name="password">
					    	</div>
					    	<div class="hide-show">
        						<span class = "ui button"> Show </span>
     						</div>

					  	</div>

					  	<div class="equal width fields">
					    	<div class="field">
					      		<label>First name</label>
					      		<input type="text" placeholder="First Name" name="firstname">
					    	</div>
					    	<div class="field">
					      		<label>Middle name</label>
					      		<input type="text" placeholder="Middle Name" name="middlename">
					    	</div>
					    	<div class="field">
					     		<label>Last name</label>
					      		<input type="text" placeholder="Last Name" name="surname">
					    	</div>
					  	</div>

					  	<div class="equal width fields">
					    	<div class="field">
					      		<label>Designation</label>
					      		<div class="ui fluid search selection dropdown dp">
					      			<input type = "hidden" name = "designation">
					      			<i class = "dropdown icon"> </i>
					      			<div class = "default text"> Select Position </div>
					      			<div class = "menu">
					      				@foreach ($positions as $position)
 										<div class = "item" data-value = "{{$position->id}}"> {{$position->name}} </div>
 										@endforeach
 									</div>
					      		</div>
					    	</div>
					    	<div class="field">
					      		<label>Division</label>
					      		<div class="ui fluid search selection dropdown dp">
					      			<input type = "hidden" name = "division">
					      			<i class = "dropdown icon"> </i>
					      			<div class = "default text"> Select Division </div>
					      			<div class = "menu">
					      				@foreach ($locations as $location)
 										<div class = "item" data-value = "{{$location->id}}"> {{$location->name}} </div>
 										@endforeach
 									</div>
					      		</div>
					    	</div>
					  	</div>

					 	<div class="equal width fields">
							<div class="field">
								<label>E-mail</label>
								<input type="email" name="email">
							</div>

							<div class="field">
									<label>Upload Image</label>
									<input type="file" name="myimage" placeholder="">
							</div>
						</div>

						<div class="field addBtn">
								<button type="submit" class="ui red button" form="addPersonnel">Add</button>
						</div>
					</div>
					<br> <br>
				</div>
			</div>

			 <div class="ui error message"></div>
		</form>


<script type = "text/javascript"> //hide or show password

	$(function(){
  		$('.hide-show').show();
  		$('.hide-show span').addClass('show')
  	
  		$('.hide-show span').click(function(){
    		if( $(this).hasClass('show') ) {
		       $(this).text('Hide');
		       $('input[name="password"]').attr('type','text');
		       $(this).removeClass('show');
   			} 
   			else {
		       $(this).text('Show');
		       $('input[name="password"]').attr('type','password');
		       $(this).addClass('show');
    		}
  		});
	
		$('form button[type="submit"]').on('click', function(){
			$('.hide-show span').text('Show').addClass('show');
			$('.hide-show').parent().find('input[name="password"]').attr('type','password');
		}); 
	});

	$('.dropdown').dropdown();


	$("#addPersonnel").form({
		fields: ({
			username: {
				identifier: 'username',
				rules: [
				{
					type: 'empty',
					prompt : 'Please enter the username',
				}]
			},
			password: {
				identifier: 'password',
				rules: [
				{
					type: 'minLength[8]',
					prompt : 'Password must be at least {ruleValue} characters'
				}]
			},
			firstname: {
				identifier: 'firstname',
				rules: [
				{
					type: 'empty',
					prompt : 'Please enter the firstname'
				}]
			},
			middlename: {
				identifier: 'middlename',
				rules: [
				{
					type: 'empty',
					prompt : 'Please enter the middlename'
				}]
			},
			surname: {
				identifier: 'surname',
				rules: [
				{
					type: 'empty',
					prompt : 'Please enter the surname'
				}]
			},
			designation: {
				identifier: 'designation',
				rules: [
				{
					type: 'empty',
					prompt : 'Please enter the designation/position'
				}]
			},
			division: {
				identifier: 'division',
				rules: [
				{
					type: 'empty',
					prompt : 'Please enter the division'
				}]
			},
			email: {
				identifier: 'email',
				rules: [
				{
					type: 'empty',
					prompt: 'Please enter email address'
				},
				{
					type: 'email',
					prompt : 'The email address you entered is invalid'
				}]
			},
			myimage: {
				identifier: 'myimage',
				rules: [
				{
					type: 'regExp',
					value: /^.*\.(jpg|JPG|jpeg|JPEG|png|PNG)$/,
					prompt : 'Please choose a photo'
				}]
			},

		}),
		inline : true,
    	on : 'blur',
	});
</script>
@else
	<div class="ui placeholder segment">
		<div class="ui icon header">
			<i class="hand paper outline icon"></i>
			You are not allowed to add personnel because you are not an admin.
		</div>
	</div>
@endif

@endsection