@extends('layouts.admin')

@section('content')
	<div id="deleteModal" class="ui small modal">
		<div class="header" id="delModalHeader">
			Add Type
		</div>
		<div class="content" id="delModalContent">
			
		</div>
		<div class="actions">
			<button type="button" class="ui approve red right labeled icon button" id="deleteModalBtn" category="" object-id="">
                <i class="check icon"></i>
                Yes
            </button>
			<button class="ui button deny">No</button>
		</div>
	</div> <!-- Delete Modal -->

	<!-- Add Type Modal -->
	<div id="typesAddModal" class="ui small modal">
		<div class="header">
			Add Type
		</div>
		<div class="content">
			<div class="ui form">
				<div class="field">
                    <label>Name</label>
                    <input type="text" name="name" placeholder="Enter name" id="name-types">
                    <div class="status-msg status-check-types" id="nameTypesMsg" status="wrong">
                    </div>
                </div>
                <div class="field">
                	<label for="category">Category</label>
                	<div class="ui fluid selection dropdown update-dropdown">
                        <input type="hidden" name="category" value="" id="category-types">
                        <i class="dropdown icon"></i>
                        <div class="default text">Select category</div>
                        <div class="menu">
                        	<div class="disabled item" data-value="">Select category</div>
	                        <div class="item" data-value="collection">Collection</div>
	                        <div class="item" data-value="tracking">Tracking</div>
                        </div>
                    </div>
                    <div class="status-msg status-check-types" id="categoryTypesMsg" status="wrong">
                    </div>
                </div>
			</div>
		</div>
		<div class="actions">
			<button type="button" class="ui approve red right labeled icon button add-modal-btn" id="typesAddBtn" category="types">
                <i class="plus icon"></i>
                Add
            </button>
			<button class="ui button deny">Cancel</button>
		</div>
	</div> <!-- End Add Type Modal -->

	<!-- Edit Type Modal -->
	<div id="typesEditModal" class="ui small modal">
		<div class="header">
			Edit Type
		</div>
		<div class="content">
			<div class="ui form">
				<div class="field">
                    <label>Name</label>
                    <input type="text" name="name" placeholder="Enter name" id="name-edit-types">
                    <div class="status-msg status-check-types" id="nameOutsidersMsg" status="wrong">
                    </div>
                </div>
                <div class="field">
                	<label for="category">Category</label>
                	<div class="ui fluid selection dropdown update-dropdown">
                        <input type="hidden" name="category" value="" id="category-edit-types">
                        <i class="dropdown icon"></i>
                        <div class="default text" id="def-text-edit-types">Select category</div>
                        <div class="menu">
                        	<div class="disabled item" data-value="">Select category</div>
	                        <div class="item" data-value="collection">Collection</div>
	                        <div class="item" data-value="tracking">Tracking</div>
                        </div>
                    </div>
                    <div class="status-msg status-check-types" id="categoryTypesMsg" status="wrong">
                    </div>
                </div>
			</div>
		</div>
		<div class="actions">
			<button type="button" class="ui approve red right labeled icon button edit-modal-btn" id="typesEditBtn" type-id="" category="types">
                <i class="edit icon"></i>
                Edit
            </button>
			<button class="ui button deny">Cancel</button>
		</div>
	</div> <!-- End of Edit Type Modal -->

	<!-- Add Location Modal -->
	<div id="locationsAddModal" class="ui small modal">
		<div class="header">
			Add Location
		</div>
		<div class="content">
			<div class="ui form">
				<div class="field">
                    <label>Name</label>
                    <input type="text" name="name" placeholder="Enter name" id="name-locations">
                    <div class="status-msg status-check-locations" id="nameLocationsMsg" status="wrong">
                    </div>
                </div>
			</div>
		</div>
		<div class="actions">
			<button type="button" class="ui approve red right labeled icon button add-modal-btn" id="locationsAddBtn" category="locations">
                <i class="plus icon"></i>
                Add
            </button>
			<button class="ui button deny">Cancel</button>
		</div>
	</div> <!-- End Add Location Modal -->

	<!-- Edit Location Modal -->
	<div id="locationsEditModal" class="ui small modal">
		<div class="header">
			Edit Locations
		</div>
		<div class="content">
			<div class="ui form">
				<div class="field">
                    <label>Name</label>
                    <input type="text" name="name" placeholder="Enter name" id="name-edit-locations">
                    <div class="status-msg status-check-outsiders" id="nameOutsidersMsg" status="wrong">
                    </div>
                </div>
			</div>
		</div>
		<div class="actions">
			<button type="button" class="ui approve red right labeled icon button edit-modal-btn" id="locationsEditBtn" location-id="" category="locations">
                <i class="edit icon"></i>
                Edit
            </button>
			<button class="ui button deny">Cancel</button>
		</div>
	</div> <!-- End of edit location modal --> 

	<div id="outsidersAddModal" class="ui small modal">
		<div class="header">
			Add Outsider
		</div>
		<div class="content">
			<div class="ui form">
				<div class="field">
                    <label>Name</label>
                    <input type="text" name="name" placeholder="Enter name" id="name-outsiders">
                    <div class="status-msg status-check-outsiders" id="nameOutsidersMsg" status="wrong">
                    </div>
                </div>
                <div class="field">
                	<label>Location</label>
                    <div class="ui fluid selection dropdown update-dropdown">
                        <input type="hidden" name="location" value="" id="location-outsiders">
                        <i class="dropdown icon"></i>
                        <div class="default text">Select location</div>
                        <div class="menu">
                        	<div class="disabled item" data-value="">Select location</div>
	                        @foreach($locations as $location)
	                        	<div class="item" data-value="{{ $location->id }}">
                                    {{ $location->name }}
                                </div>
	                        @endforeach
                        </div>
                    </div>
                    <div class="status-msg status-check-outsiders" id="locationOutsidersMsg" status="wrong">
                    </div>
                </div>
			</div>
		</div>
		<div class="actions">
			<button type="button" class="ui approve red right labeled icon button add-modal-btn" id="outsidersAddBtn" category="outsiders">
                <i class="plus icon"></i>
                Add
            </button>
			<button class="ui button deny">Cancel</button>
		</div>
	</div>

	<!-- Edit Outsider Modal -->
	<div id="outsidersEditModal" class="ui small modal">
		<div class="header">
			Edit Outsiders
		</div>
		<div class="content">
			<div class="ui form">
				<div class="field">
                    <label>Name</label>
                    <input type="text" name="name" placeholder="Enter name" id="name-edit-outsiders">
                    <div class="status-msg status-check-outsiders" id="nameOutsidersMsg" status="wrong">
                    </div>
                </div>
                <div class="field">
                	<label>Location</label>
                    <div class="ui fluid selection dropdown update-dropdown">
                        <input type="hidden" name="location" value="" id="location-edit-outsiders">
                        <i class="dropdown icon"></i>
                        <div class="default text" id="def-text-edit-outsiders">Select location</div>
                        <div class="menu">
                        	<div class="disabled item" data-value="">Select location</div>
	                        @foreach($locations as $location)
	                        	<div class="item" data-value="{{ $location->id }}">
                                    {{ $location->name }}
                                </div>
	                        @endforeach
                        </div>
                    </div>
                    <div class="status-msg status-check-outsiders" id="locationOutsidersMsg" status="wrong">
                    </div>
                </div>
			</div>
		</div>
		<div class="actions">
			<button type="button" class="ui approve red right labeled icon button edit-modal-btn" id="outsidersEditBtn" outsider-id="" category="outsiders">
                <i class="edit icon"></i>
                Edit
            </button>
			<button class="ui button deny">Cancel</button>
		</div>
	</div> <!-- End of edit outsider modal -->


	<div class="ui secondary pointing menu" id="addablesTabs">
		<a class="active red item" data-tab="outsiders">
			<i class="users icon"></i>
			Outsiders
		</a>
		<a class="red item" data-tab="locations">
			<i class="map marker alternate icon"></i>
			Locations
		</a>
		<a class="red item" data-tab="types">
			<i class="bars icon"></i>
			Types
		</a>
	</div>
	<div class="ui bottom attached active tab" data-tab="outsiders">
		<div class="ui stackable grid">
			<div class="three wide column">
				<div class="ui fluid selection dropdown">
                    <input class="search-type" type="hidden" name="type" value="name" category="outsiders" id="search-type-outsiders">
                    <i class="dropdown icon"></i>
                    <div class="default text">Name</div>
                    <div class="menu">
                        <div class="item" data-value="name">Name</div>
                        <div class="item" data-value="adder">Adder</div>
                        <div class="item" data-value="location">Location</div>
                    </div>
                </div>
			</div>
			<div class="five wide column">
				<div class="ui fluid left icon input">
                    <input class="search" type="text" id="search-box-outsiders" placeholder="Search outsiders" category="outsiders">
                    <i class="search icon"></i>
                </div>
			</div>
		</div>
		<div class="ui stackable grid">
			<div class="left floated three wide column">
				<button id="addOutsiderBtn" class="ui labeled icon red button test add-btn" category="outsiders">
                    <i class="plus icon"></i>
                    Add Outsider
                </button>
			</div>
			<div class="right floated five wide column">
				<label class="label-mg-right">Sort by</label>
                <div class="ui selection dropdown" id="sort-dropdown">
                    <input class="sort-by" type="hidden" value="created_at" name="type" category="outsiders" id="sort-by-outsiders">
                    <i class="dropdown icon"></i>
                    <div class="default text">Date added</div>
                    <div class="menu">
                        <div class="item" data-value="created_at">Date added</div>
                        <div class="item" data-value="name">Name</div>
                        <div class="item" data-value="location">Location</div>
                    </div>
                </div>

                <button class="circular ui icon button sort-btn" id="sort-button-outsiders" sort-direction="desc" category="outsiders">
                    <i class="arrow down icon" id="sort-icon-outsiders"></i>
                </button>
			</div>
		</div>
		<div class="ui stackable grid">
			<div class="five wide column semibold">
				Name
			</div>
			<div class="six wide column semibold">
				Location
			</div>
			<div class="four wide column semibold">
				Adder
			</div>
			<div class="one wide column">
				
			</div>
		</div>
		<div class="ui vertically divided stackable grid" id="container-outsiders">
			@foreach($outsiders as $outsider)
				<div class="row" id="outsider{{ $outsider->id }}">
					<div class="five wide column semibold" id="outsiderName{{ $outsider->id }}">
						{{ $outsider->name }}
					</div>
					<div class="six wide column" id="outsiderLocation{{ $outsider->id }}">
						{{ $outsider->location->name }}
					</div>
					<div class="four wide column">
						<img class="ui avatar image" src="{{ asset($outsider->adder->profile_pic) }}">
						{{ $outsider->adder->firstname.' '.$outsider->adder->middlename[0].'. '.$outsider->adder->surname}}
					</div>
					<div class="one wide column">
						<div class="ui floating dropdown icon options-dropdown">
                            <i class="ellipsis vertical icon options-icon"></i>
                            <div class="menu">
                                <div class="item edit" id="editOutsider{{ $outsider->id }}" outsider-id="{{ $outsider->id }}" outsider-name="{{ $outsider->name }}" outsider-location="{{ $outsider->location->id}}" outsider-location-name="{{ $outsider->location->name }}" category="outsiders">
                                    <i class="edit icon"></i>
                                    Edit
                                </div>
                                <div class="item delete" outsider-name="{{ $outsider->name }}" outsider-id="{{ $outsider->id }}" category="outsiders">
                                    <i class="trash alternate icon"></i>
                                   Delete
                                </div>
                            </div>
                        </div>
					</div>
				</div>
			@endforeach
		</div>
		@if(count($outsiders) > 0)
            <div class="pagination">
                <div class="ui stackable grid">
                    <div class="five wide column">
                        Show
                        <div class="ui selection dropdown receive-dropdown" id="showItems">
                            <input type="hidden" value="10" class="howMany" category="outsiders" name="type" id="howManyItems-outsiders">
                            <i class="dropdown icon"></i>
                            <div class="default text">10</div>
                            <div class="menu">
                                <div class="item" data-value="10">10 items</div>
                                <div class="item" data-value="20">20 items</div>
                                <div class="item" data-value="25">25 items</div>
                                <div class="item" data-value="50">50 items</div>
                            </div>
                        </div>
                    </div>
                    <div class="five wide column">
                        Page
                        <div class="ui selection dropdown receive-dropdown" id="paginate">
                            <input type="hidden" class="page-change" category="outsiders" value="{{ '1:'.$partitions_outsiders['1'] }}" name="type" id="page-outsiders">
                            <i class="dropdown icon"></i>
                            <div class="default text" id="pageText-outsiders"></div>
                            <div class="menu" id="pagesMenu-outsiders">
                                @foreach($partitions_outsiders as $index => $value)
                                    <div class="item" data-value="{{ $index.':'.$value }}">{{ $index }}</div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="six wide column">
                        <div class="showing">
                            Showing 
                            <span id="numOfItems-outsiders">{{ count($outsiders) }}</span> out of 
                            {{ $numItems['outsiders'] }}
                        </div>
                    </div>
                </div>
            </div>
        @endif
	</div>
	<div class="ui bottom attached tab" data-tab="locations">
		<div class="ui stackable grid">
			<div class="three wide column">
				<div class="ui fluid selection dropdown">
                    <input class="search-type" type="hidden" name="type" value="name" category="locations" id="search-type-locations">
                    <i class="dropdown icon"></i>
                    <div class="default text">Name</div>
                    <div class="menu">
                        <div class="item" data-value="name">Name</div>
                        <div class="item" data-value="adder">Adder</div>
                    </div>
                </div>
			</div>
			<div class="five wide column">
				<div class="ui fluid left icon input">
                    <input class="search" type="text" id="search-box-locations" placeholder="Search locations" category="locations">
                    <i class="search icon"></i>
                </div>
			</div>
		</div>
		<div class="ui stackable grid">
			<div class="left floated three wide column">
				<button id="addLocationBtn" class="ui labeled icon red button test add-btn" category="locations">
                    <i class="plus icon"></i>
                    Add Location
                </button>
			</div>
			<div class="right floated five wide column">
				<label class="label-mg-right">Sort by</label>
                <div class="ui selection dropdown" id="sort-dropdown">
                    <input class="sort-by" type="hidden" value="created_at" name="type" category="locations" id="sort-by-locations">
                    <i class="dropdown icon"></i>
                    <div class="default text">Date added</div>
                    <div class="menu">
                        <div class="item" data-value="created_at">Date added</div>
                        <div class="item" data-value="name">Name</div>
                    </div>
                </div>

                <button class="circular ui icon button sort-btn" id="sort-button-locations" sort-direction="desc" category="locations">
                    <i class="arrow down icon" id="sort-icon-locations"></i>
                </button>
			</div>
		</div>
		<div class="ui stackable grid">
			<div class="eight wide column semibold">
				Name
			</div>
			<div class="seven wide column semibold">
				Adder
			</div>
			<div class="one wide column">
				
			</div>
		</div>

		<div class="ui vertically divided stackable grid" id="container-locations">
			@foreach($locations as $location)
				<div class="row" id="location{{ $location->id }}">
					<div class="eight wide column semibold" id="locationName{{ $location->id }}">
						{{ $location->name }}
					</div>
					<div class="seven wide column">
						<img class="ui avatar image" src="{{ asset($location->adder->profile_pic) }}">
						{{ $location->adder->firstname.' '.$location->adder->middlename[0].'. '.$location->adder->surname}}
					</div>
					<div class="one wide column">
						<div class="ui floating dropdown icon options-dropdown">
                            <i class="ellipsis vertical icon options-icon"></i>
                            <div class="menu">
                                <div class="item edit" id="editLocation{{ $location->id }}" location-id="{{ $location->id }}" location-name="{{ $location->name }}" category="locations">
                                    <i class="edit icon"></i>
                                    Edit
                                </div>
                                <div class="item delete" location-id="{{ $location->id }}" location-name="{{ $location->name }}" category="locations">
                                    <i class="trash alternate icon"></i>
                                   Delete
                                </div>
                            </div>
                        </div>
					</div>
				</div>
			@endforeach
		</div>

		@if(count($locations) > 0)
            <div class="pagination">
                <div class="ui stackable grid">
                    <div class="five wide column">
                        Show
                        <div class="ui selection dropdown receive-dropdown" id="showItems">
                            <input type="hidden" value="10" class="howMany" category="locations" name="type" id="howManyItems-locations">
                            <i class="dropdown icon"></i>
                            <div class="default text">10</div>
                            <div class="menu">
                            	<div class="item" data-value="5">5 items</div>
                                <div class="item" data-value="10">10 items</div>
                                <div class="item" data-value="20">20 items</div>
                                <div class="item" data-value="25">25 items</div>
                                <div class="item" data-value="50">50 items</div>
                            </div>
                        </div>
                    </div>
                    <div class="five wide column">
                        Page
                        <div class="ui selection dropdown receive-dropdown" id="paginate">
                            <input type="hidden" class="page-change" category="locations" value="{{ '1:'.$partitions_locations['1'] }}" name="type" id="page-locations">
                            <i class="dropdown icon"></i>
                            <div class="default text" id="pageText-locations"></div>
                            <div class="menu" id="pagesMenu-locations">
                                @foreach($partitions_locations as $index => $value)
                                    <div class="item" data-value="{{ $index.':'.$value }}">{{ $index }}</div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="six wide column">
                        <div class="showing">
                            Showing 
                            <span id="numOfItems-locations">{{ count($locations) }}</span> out of 
                            {{ $numItems['locations'] }}
                        </div>
                    </div>
                </div>
            </div>
        @endif
	</div>
	<div class="ui bottom attached tab" data-tab="types">
		<div class="ui stackable grid">
			<div class="three wide column">
				<div class="ui fluid selection dropdown">
                    <input class="search-type" type="hidden" name="type" value="name" category="types" id="search-type-types">
                    <i class="dropdown icon"></i>
                    <div class="default text">Name</div>
                    <div class="menu">
                        <div class="item" data-value="name">Name</div>
                        <div class="item" data-value="adder">Adder</div>
                    </div>
                </div>
			</div>
			<div class="five wide column">
				<div class="ui fluid left icon input">
                    <input class="search" type="text" id="search-box-types" placeholder="Search types" category="types">
                    <i class="search icon"></i>
                </div>
			</div>
		</div>
		<div class="ui stackable grid">
			<div class="left floated three wide column">
				<button id="addTypeBtn" class="ui labeled icon red button test add-btn" category="types">
                    <i class="plus icon"></i>
                    Add Type
                </button>
			</div>
			<div class="right floated five wide column">
				<label class="label-mg-right">Sort by</label>
                <div class="ui selection dropdown" id="sort-dropdown">
                    <input class="sort-by" type="hidden" value="created_at" name="type" category="types" id="sort-by-types">
                    <i class="dropdown icon"></i>
                    <div class="default text">Date added</div>
                    <div class="menu">
                        <div class="item" data-value="created_at">Date added</div>
                        <div class="item" data-value="name">Name</div>
                    </div>
                </div>

                <button class="circular ui icon button sort-btn" id="sort-button-types" sort-direction="desc" category="types">
                    <i class="arrow down icon" id="sort-icon-types"></i>
                </button>
			</div>
		</div>
		<div class="ui stackable grid">
			<div class="five wide column semibold">
				Name
			</div>
			<div class="six wide column semibold">
				Adder
			</div>
			<div class="four wide column semibold">
				Category
			</div>
			<div class="one wide column">
				
			</div>
		</div>
		<div class="ui vertically divided stackable grid" id="container-types">
			@foreach($types as $type)
				<div class="row" id="type{{ $type->id }}">
					<div class="five wide column semibold" id="typeName{{ $type->id }}">
						{{ $type->name }}
					</div>
					<div class="six wide column">
						<img class="ui avatar image" src="{{ asset($type->adder->profile_pic) }}">
						{{ $type->adder->firstname.' '.$type->adder->middlename[0].'. '.$type->adder->surname}}
					</div>
					<div class="four wide column" id="typeCategory{{ $type->id }}">
						@if($type->is_collection_type == '1')
							Collection
						@elseif($type->is_tracking_type == '1')
							Tracking
						@endif
					</div>
					<div class="one wide column">
						<div class="ui floating dropdown icon options-dropdown">
                            <i class="ellipsis vertical icon options-icon"></i>
                            <div class="menu">
                                <div class="item edit" id="editType{{ $type->id }}" type-id="{{ $type->id }}" type-name="{{ $type->name }}" type-category="@if($type->is_collection_type == '1') collection @else tracking @endif" category="types">
                                    <i class="edit icon"></i>
                                    Edit
                                </div>
                                <div class="item delete" type-id="{{ $type->id }}" type-name="{{ $type->name }}" category="types">
                                    <i class="trash alternate icon"></i>
                                   Delete
                                </div>
                            </div>
                        </div>
					</div>
				</div>
			@endforeach
		</div>

		@if(count($types) > 0)
            <div class="pagination">
                <div class="ui stackable grid">
                    <div class="five wide column">
                        Show
                        <div class="ui selection dropdown receive-dropdown" id="showItems">
                            <input type="hidden" value="10" class="howMany" category="types" name="type" id="howManyItems-types">
                            <i class="dropdown icon"></i>
                            <div class="default text">10</div>
                            <div class="menu">
                            	<div class="item" data-value="2">2 items</div>
                                <div class="item" data-value="10">10 items</div>
                                <div class="item" data-value="20">20 items</div>
                                <div class="item" data-value="25">25 items</div>
                                <div class="item" data-value="50">50 items</div>
                            </div>
                        </div>
                    </div>
                    <div class="five wide column">
                        Page
                        <div class="ui selection dropdown receive-dropdown" id="paginate">
                            <input type="hidden" class="page-change" category="types" value="{{ '1:'.$partitions_types['1'] }}" name="type" id="page-types">
                            <i class="dropdown icon"></i>
                            <div class="default text" id="pageText-types"></div>
                            <div class="menu" id="pagesMenu-types">
                                @foreach($partitions_types as $index => $value)
                                    <div class="item" data-value="{{ $index.':'.$value }}">{{ $index }}</div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="six wide column">
                        <div class="showing">
                            Showing 
                            <span id="numOfItems-types">{{ count($types) }}</span> out of 
                            {{ $numItems['types'] }}
                        </div>
                    </div>
                </div>
            </div>
        @endif
	</div>

	<script type="text/javascript">
		$('#addablesTabs .item').tab();

		function initiateDynamicFunctions(){
			$('.options-dropdown').dropdown();
			$('.dropdown').dropdown();

			//Delete any addable
			$('.delete').click(function(){
				var categ = $(this).attr('category');
				var id = '';
				if(categ == 'outsiders'){
					id = $(this).attr('outsider-id');
					var name = $(this).attr('outsider-name');
					$('#delModalHeader').html('Delete Outsider');
					$('#delModalContent').html('Are you sure you want to delete outsider, <span class="semibold">'+name+'</span>?');
					$('#deleteModalBtn').attr('category', 'outsiders');
					$('#deleteModalBtn').attr('object-id', id);
					$('#deleteModal').modal('show');
				}
				else if(categ == 'locations'){
					id = $(this).attr('location-id');
					var name = $(this).attr('location-name');
					$('#delModalHeader').html('Delete Location');
					$('#delModalContent').html('Are you sure you want to delete location, <span class="semibold">'+name+'</span>?');
					$('#deleteModalBtn').attr('category', 'locations');
					$('#deleteModalBtn').attr('object-id', id);
					$('#deleteModal').modal('show');
				}
				else if(categ == 'types'){
					id = $(this).attr('type-id');
					var name = $(this).attr('type-name');
					$('#delModalHeader').html('Delete Type');
					$('#delModalContent').html('Are you sure you want to delete type, <span class="semibold">'+name+'</span>?');
					$('#deleteModalBtn').attr('category', 'types');
					$('#deleteModalBtn').attr('object-id', id);
					$('#deleteModal').modal('show');
				}
			});

			//When edit item is clicked
			$('.edit').click(function(){
				var categ = $(this).attr('category');

				if(categ == 'outsiders'){
					var name = $(this).attr('outsider-name');
					var locName = $(this).attr('outsider-location-name');
					var locId = $(this).attr('outsider-location');
					var id = $(this).attr('outsider-id');

					$('#outsidersEditBtn').attr('outsider-id', id);
					$('#location-edit-outsiders').val(locId);
					$('#def-text-edit-outsiders').html(locName);
					$('#name-edit-outsiders').val(name);
					$('#'+categ+'EditModal').modal('show');
				}
				else if(categ == 'locations'){
					var name = $(this).attr('location-name');
					var id = $(this).attr('location-id');
					$('#locationsEditBtn').attr('location-id', id);
					$('#name-edit-locations').val(name);
					$('#'+categ+'EditModal').modal('show');
				}
				else if(categ == 'types'){
					var name = $(this).attr('type-name');
					var id = $(this).attr('type-id');
					var kind = $(this).attr('type-category');

					if(kind.trim() == 'collection'){
						$('#def-text-edit-types').html('Collection');
					}
					else{
						$('#def-text-edit-types').html('Tracking');
					}
					$('#name-edit-types').val(name);
					$('#category-edit-types').val(kind);
					$('#typesEditBtn').attr('type-id', id);
					$('#'+categ+'EditModal').modal('show');
				}
			});
		}

		initiateDynamicFunctions();

		$('.ui.radio.checkbox').checkbox();

		//Changing of how many items should be viewed in a page
		$('.howMany').change(function(){
			var categ = $(this).attr('category');
			resetPages(categ);
		});

		//Changing of pages
		$('.page-change').change(function(){
			var categ = $(this).attr('category');
			organize(categ);
		});

		//When delete modal button is clicked
		$('#deleteModalBtn').click(function(){
			var id = $(this).attr('object-id');
			var category = $(this).attr('category');
			$.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "/delete/addables",
                data: {
                    id: id,
                    category: category
                },
                beforeSend: function(){
                    
                },
                success: function(data){
                	$('#deleteModal').modal('hide');
                	if(category == 'outsiders'){
                		$('#outsider'+data).remove();
                	}
                	else if(category == 'locations'){
                		$('#location'+data).remove();
                	}
                	else if(category == 'types'){
                		$('#type'+data).remove();
                	}
                },
                error: function(err){
                    console.log(err.responseText);
                }
            });
		});

		function addOutsider(name, locId, category){
			$.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "/add/addables",
                data: {
                    name: name,
                    locId: locId,
                    category: category
                },
                beforeSend: function(){
                    
                },
                success: function(data){
                	$('#'+category+'AddModal').modal('hide');
                    $('#container-'+category).prepend(data);
                    initiateDynamicFunctions();
                },
                error: function(err){
                    console.log(err.responseText);
                }
            });
		}

		function editOutsider(id, name, locId, category){
			$.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "/edit/addables",
                data: {
                	id: id,
                    name: name,
                    locId: locId,
                    category: category
                },
                beforeSend: function(){
                    
                },
                success: function(data){
                	$('#'+category+'EditModal').modal('hide');
                	var out = JSON.parse(data);

                	$('#editOutsider'+out.id).attr('outsider-name', out.name);
					$('#editOutsider'+out.id).attr('outsider-location-name', out.location_name);
					$('#editOutsider'+out.id).attr('outsider-location', out.location_id);

                	$('#outsiderName'+out.id).html(out.name);
                	$('#outsiderLocation'+out.id).html(out.location_name);
                	initiateDynamicFunctions();
                },
                error: function(err){
                    console.log(err.responseText);
                }
            });
		}

		function addLocation(name, category){
			$.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "/add/addables",
                data: {
                    name: name,
                    category: category
                },
                beforeSend: function(){
                    
                },
                success: function(data){
                	$('#'+category+'AddModal').modal('hide');
                    $('#container-'+category).prepend(data);
                    initiateDynamicFunctions();
                },
                error: function(err){
                    console.log(err.responseText);
                }
            });
		}

		function editLocation(id, name, category){
			$.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "/edit/addables",
                data: {
                	id: id,
                    name: name,
                    category: category
                },
                beforeSend: function(){
                    
                },
                success: function(data){
                	$('#'+category+'EditModal').modal('hide');
                	var loc = JSON.parse(data);

                	$('#editLocation'+loc.id).attr('outsider-name', loc.name);
					$('#editLocation'+loc.id).attr('outsider-location-name', loc.location_name);
					$('#editLocation'+loc.id).attr('outsider-location', loc.location_id);

                	$('#locationName'+loc.id).html(loc.name);
                	initiateDynamicFunctions();
                },
                error: function(err){
                    console.log(err.responseText);
                }
            });
		}

		function addType(name, kind, category){
			$.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "/add/addables",
                data: {
                    name: name,
                    kind: kind,
                    category: category
                },
                beforeSend: function(){
                    
                },
                success: function(data){
                	$('#'+category+'AddModal').modal('hide');
                    $('#container-'+category).prepend(data);
                    initiateDynamicFunctions();
                },
                error: function(err){
                    console.log(err.responseText);
                }
            });
		}

		function editType(id, name, kind, category){
			$.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "/edit/addables",
                data: {
                	id: id,
                    name: name,
                    kind: kind,
                    category: category
                },
                beforeSend: function(){
                    
                },
                success: function(data){
                	$('#'+category+'EditModal').modal('hide');
                	var type = JSON.parse(data);

                	$('#editType'+type.id).attr('type-name', type.name);
                	if(type.is_collection_type == '1'){
                		$('#editType'+type.id).attr('type-category', 'collection');
                		$('#typeCategory'+type.id).html('Collection');
                	}
                	else{
                		$('#editType'+type.id).attr('type-category', 'tracking');
                		$('#typeCategory'+type.id).html('Tracking');
                	}
                	$('#typeName'+type.id).html(type.name);
                	$('#type'+type.id).transition('pulse');
                	$('#type'+type.id).transition('pulse');
                	initiateDynamicFunctions();
                },
                error: function(err){
                    console.log(err.responseText);
                }
            });
		}

		//When edit button in modal is clicked
		$('.edit-modal-btn').click(function(){
			var categ = $(this).attr('category');
			if( categ == 'outsiders'){
				var id = $('#outsidersEditBtn').attr('outsider-id');
				var name = $('#name-edit-outsiders').val();
				var locId = $('#location-edit-outsiders').val();
				editOutsider(id, name, locId, categ);
			}
			else if(categ == 'locations'){
				var name = $('#name-edit-locations').val();
				var id = $('#locationsEditBtn').attr('location-id'); 
				editLocation(id, name, categ);
			}
			else if(categ == 'types'){
				var name = $('#name-edit-types').val();
				var kind = $('#category-edit-types').val();
				var id = $('#typesEditBtn').attr('type-id');
				editType(id, name, kind, categ);
			}
		});

		//When add button in modal is clicked
		$('.add-modal-btn').click(function(){
			var categ = $(this).attr('category');
			if(categ == 'outsiders'){
				var name = $('#name-outsiders').val();
				var locId = $('#location-outsiders').val();
				addOutsider(name, locId, categ);
			}
			else if(categ == 'locations'){
				var name = $('#name-locations').val();
				addLocation(name, categ);
			}
			else if(categ == 'types'){
				var name = $('#name-types').val();
				var kind = $('#category-types').val();
				addType(name, kind, categ);
			}
		});

		//When add button is clicked, outsiders, locations, types
		$('.add-btn').click(function(){
			var categ = $(this).attr('category');
			$('#'+categ+'AddModal').modal('show');
		});

		//If sort-by is changed, changing of type
		$('.sort-by').change(function(){
			var categ = $(this).attr('category');
			organize(categ);
		});

		//If sort-button is clicked, changing of directions here
		$('.sort-btn').click(function(){
			var categ = $(this).attr('category');
            if($('#sort-button-'+categ).attr('sort-direction') == 'desc'){
                $('#sort-icon-'+categ).removeClass('down');
                $('#sort-icon-'+categ).addClass('up');
                $('#sort-button-'+categ).attr('sort-direction', 'asc');
            }
            else{
                $('#sort-icon-'+categ).removeClass('up');
                $('#sort-icon-'+categ).addClass('down');
                $('#sort-button-'+categ).attr('sort-direction', 'desc');
            }
            $('#sort-button-'+categ).transition('jiggle');
            organize(categ);
        });

        $(".search").on('keyup', function(){
            var categ = $(this).attr('category');
            var searchValue = '';
            var searchType = $('.search-type').attr('value');

            if(categ == 'outsiders'){
                searchValue = $('#search-box-outsiders').val();
            }
            else if (categ == 'locations'){
                searchValue = $('#search-box-locations').val();
            }
            else if (categ == 'points'){
                searchValue = $('#search-box-points').val();
            }
            else if (categ == 'positions'){
                searchValue = $('#search-box-positions').val();
            }
            else {
                searchValue = $('#search-box-types').val();
            }   
            if(searchValue ==''){
                resetPages(categ);
            } 
            else{
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: "/addables/search",
                    data: {
                        searchValue: searchValue,
                        searchType: searchType,
                        categ: categ
                    },
                    success: function(data){
                        $('#container-'+categ).empty();
                        refillConts(data, categ);
                        
                    },
                    error: function(err){ 
                        console.log(categ, searchType); 
                        console.log(err.responseText);

                    }
                });
            }
        });


		//Reseting of number of items shown per page
        function resetPages(category){
            var num_items = $('#howManyItems-'+category).val();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "/reset/pages/addables",
                data: {
                    num_items: num_items,
                    category: category,
                },
                success: function(data){
                    $('#pagesMenu-'+category).empty();
                    var pages = JSON.parse(data);
                    
                    for(var i in pages){
                        $('#pagesMenu-'+category).append(
                            $('<div>').addClass('item').attr('data-value', i+':'+pages[i]).html(i)
                        );
                        if(i == $('#page-'+category).val().split(':')[0])
                            $('#page-'+category).val(i+':'+pages[i]);
                    }
                    if($('#page-'+category).val().split(':')[0] > Object.keys(pages).length){
                        $('#page-'+category).val('1:'+pages['1']);
                        $('#pageText-'+category).html('1');
                    }
                    organize(category);
                },
                error: function(err){
                    console.log(err.responseText);
                }
            });
        }


		//Refilling of respective containers, the data parameter contains the objects to be filled along with the specific category of the container
        function refillConts(data, category){
        	var cont = $('#container-'+category);
        	cont.empty();
        	cont.append(data);
        	$('#numOfItems-'+category).html($('#container-'+category).children().length)
        	initiateDynamicFunctions();
        }

        function organize(category){
        	var sortBy = $('#sort-by-'+category).val();
        	var sortDir = $('#sort-button-'+category).attr('sort-direction');
        	var partition = $('#page-'+category).val().split(':');
            var start = partition[1];
            var end = partition[2];

        	$.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "/organize/addables",
                data: {
                    sortBy: sortBy,
                    sortDir: sortDir,
                    category: category,
                    start: start, 
                    end: end,
                },
                beforeSend: function(){
                    
                },
                success: function(data){
                    refillConts(data, category);
                },
                error: function(err){
                    console.log(err.responseText);
                }
            });
        }
	</script>
@endsection