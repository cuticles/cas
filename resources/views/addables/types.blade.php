@extends('layouts.admin')

@section('content')
@if(Auth::user()->is_admin == 1)
	<div id="deleteModal" class="ui small modal">
		<div class="header" id="delModalHeader">
			Add Type
		</div>
		<div class="content" id="delModalContent">
			
		</div>
		<div class="actions">
			<button type="button" class="ui approve red right labeled icon button" id="deleteModalBtn" category="" object-id="">
                <i class="check icon"></i>
                Yes
            </button>
			<button class="ui button deny">No</button>
		</div>
	</div> <!-- Delete Modal -->

	<!-- Add Type Modal -->
	<div id="typesAddModal" class="ui small modal">
		<div class="header">
			Add Type
		</div>
		<div class="content">
			<div class="ui form">
				<div class="field">
                    <label>Name</label>
                    <input type="text" class="types-name" name="name" placeholder="Enter name" id="name-types" mode="add">
                    <div class="status-msg status-check-types status-add" id="nameTypesMsg-add" status="wrong">
                    </div>
                </div>
                <div class="field">
                	<label for="category">Category</label>
                	<div class="ui fluid selection dropdown update-dropdown">
                        <input type="hidden" class="types-category" name="category" value="" id="category-types" mode="add">
                        <i class="dropdown icon"></i>
                        <div class="default text">Select category</div>
                        <div class="menu">
                        	<div class="disabled item" data-value="">Select category</div>
	                        <div class="item" data-value="1">Collection</div>
	                        <div class="item" data-value="2">Tracking</div>
	                        <div class="item" data-value="3">Status</div>
	                        <div class="item" data-value="4">Background</div>
                        </div>
                    </div>
                    <div class="status-msg status-check-types status-add" id="categoryTypesMsg-add" status="wrong">
                    </div>
                </div>
			</div>
		</div>
		<div class="actions">
			<button type="button" class="ui disabled approve red right labeled icon button add-modal-btn" id="typesAddBtn" category="types">
                <i class="plus icon"></i>
                Add
            </button>
			<button class="ui button deny">Cancel</button>
		</div>
	</div> <!-- End Add Type Modal -->

	<!-- Edit Type Modal -->
	<div id="typesEditModal" class="ui small modal">
		<div class="header">
			Edit Type
		</div>
		<div class="content">
			<div class="ui form">
				<div class="field">
                    <label>Name</label>
                    <input type="text" name="name" class="types-name" placeholder="Enter name" id="name-edit-types" mode="edit" old="">
                    <div class="status-msg status-check-types status-edit" id="nameTypesMsg-edit" status="okay" changed="false">
                    </div>
                </div>
                <div class="field">
                	<label for="category">Category</label>
                	<div class="ui fluid selection dropdown update-dropdown">
                        <input type="hidden" class="types-category" name="category" value="" id="category-edit-types" mode="edit" old="">
                        <i class="dropdown icon"></i>
                        <div class="default text" id="def-text-edit-types">Select category</div>
                        <div class="menu">
                        	<div class="disabled item" data-value="">Select category</div>
	                        <div class="item" data-value="1">Collection</div>
	                        <div class="item" data-value="2">Tracking</div>
	                        <div class="item" data-value="3">Status</div>
	                        <div class="item" data-value="4">Background</div>
                        </div>
                    </div>
                    <div class="status-msg status-check-types status-edit" id="categoryTypesMsg-edit" status="okay" changed="false">
                    </div>
                </div>
                <div class="error-msg" id="changesStatus"></div>
			</div>
		</div>
		<div class="actions">
			<button type="button" class="ui disabled approve red right labeled icon button edit-modal-btn" id="typesEditBtn" type-id="" category="types">
                <i class="edit icon"></i>
                Edit
            </button>
			<button class="ui button deny">Cancel</button>
		</div>
	</div> <!-- End of Edit Type Modal -->

	<div class="main-addables-content">
		<div class="ui stackable grid">
			<div class="three wide column">
				<div class="ui fluid selection dropdown">
                    <input class="search-type" type="hidden" name="type" value="name" category="types" id="search-type-types">
                    <i class="dropdown icon"></i>
                    <div class="default text">Name</div>
                    <div class="menu">
                        <div class="item" data-value="name">Name</div>
                        <div class="item" data-value="adder">Adder</div>
                    </div>
                </div>
			</div>
			<div class="five wide column">
				<div class="ui fluid left icon input">
                    <input class="search" type="text" id="search-box-types" placeholder="Search types" category="types">
                    <i class="search icon"></i>
                </div>
			</div>
		</div>
		<div class="ui stackable grid">
			<div class="left floated three wide column">
				<button id="addTypeBtn" class="ui labeled icon red button test add-btn" category="types">
                    <i class="plus icon"></i>
                    Add Type
                </button>
			</div>
			<div class="right floated five wide column">
				<label class="label-mg-right">Sort by</label>
                <div class="ui selection dropdown" id="sort-dropdown">
                    <input class="sort-by" type="hidden" value="created_at" name="type" category="types" id="sort-by-types">
                    <i class="dropdown icon"></i>
                    <div class="default text">Date added</div>
                    <div class="menu">
                        <div class="item" data-value="created_at">Date added</div>
                        <div class="item" data-value="name">Name</div>
                    </div>
                </div>

                <button class="circular ui icon button sort-btn" id="sort-button-types" sort-direction="desc" category="types">
                    <i class="arrow down icon" id="sort-icon-types"></i>
                </button>
			</div>
		</div>
		<div class="ui stackable grid">
			<div class="five wide column semibold">
				Name
			</div>
			<div class="six wide column semibold">
				Adder
			</div>
			<div class="four wide column semibold">
				Category
			</div>
			<div class="one wide column">
				
			</div>
		</div>
		<div class="ui vertically divided stackable grid" id="container-types">
			@foreach($types as $type)
				<div class="row" id="type{{ $type->id }}">
					<div class="five wide column semibold" id="typeName{{ $type->id }}">
						{{ $type->name }}
					</div>
					<div class="six wide column">
						<img class="ui avatar image" src="{{ asset($type->adder->profile_pic) }}">
						{{ $type->adder->firstname.' '.$type->adder->middlename[0].'. '.$type->adder->surname}}
					</div>
					<div class="four wide column" id="typeCategory{{ $type->id }}">
						{{ $category[$type->category] }}
					</div>
					<div class="one wide column">
						<div class="ui floating dropdown icon options-dropdown">
                            <i class="ellipsis vertical icon options-icon"></i>
                            <div class="menu">
                                <div class="item edit" id="editType{{ $type->id }}" type-id="{{ $type->id }}" type-name="{{ $type->name }}" type-category="{{ $type->category }}" category="types">
                                    <i class="edit icon"></i>
                                    Edit
                                </div>
                                <div class="item delete" type-id="{{ $type->id }}" type-name="{{ $type->name }}" category="types">
                                    <i class="trash alternate icon"></i>
                                   Delete
                                </div>
                            </div>
                        </div>
					</div>
				</div>
			@endforeach
		</div>

		@if(count($types) > 0)
            <div class="pagination">
                <div class="ui stackable grid">
                    <div class="five wide column">
                        Show
                        <div class="ui selection dropdown receive-dropdown" id="showItems">
                            <input type="hidden" value="10" class="howMany" category="types" name="type" id="howManyItems-types">
                            <i class="dropdown icon"></i>
                            <div class="default text">10</div>
                            <div class="menu">
                            	<div class="item" data-value="2">2 items</div>
                                <div class="item" data-value="10">10 items</div>
                                <div class="item" data-value="20">20 items</div>
                                <div class="item" data-value="25">25 items</div>
                                <div class="item" data-value="50">50 items</div>
                            </div>
                        </div>
                    </div>
                    <div class="five wide column">
                        Page
                        <div class="ui selection dropdown receive-dropdown" id="paginate">
                            <input type="hidden" class="page-change" category="types" value="{{ '1:'.$partitions_types['1'] }}" name="type" id="page-types">
                            <i class="dropdown icon"></i>
                            <div class="default text" id="pageText-types"></div>
                            <div class="menu" id="pagesMenu-types">
                                @foreach($partitions_types as $index => $value)
                                    <div class="item" data-value="{{ $index.':'.$value }}">{{ $index }}</div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="six wide column">
                        <div class="showing">
                            Showing 
                            <span id="numOfItems-types">{{ count($types) }}</span> out of 
                            {{ $numItems }}
                        </div>
                    </div>
                </div>
            </div>
        @endif
	</div>
    <script type="text/javascript" src="{{ asset('js/validations/types-validation.js') }}"></script>
	<script type="text/javascript">
		$('#addablesTabs .item').tab();
		var categ_list = {'1': 'Collection', '2': 'Tracking', '3': 'Status', '4': 'Background'};

		function initiateDynamicFunctions(){
			$('.options-dropdown').dropdown();
			$('.dropdown').dropdown();

			//Delete any addable
			$('.delete').click(function(){
				var categ = $(this).attr('category');
				var id = '';
				if(categ == 'types'){
					id = $(this).attr('type-id');
					var name = $(this).attr('type-name');
					$('#delModalHeader').html('Delete Type');
					$('#delModalContent').html('Are you sure you want to delete type, <span class="semibold">'+name+'</span>?');
					$('#deleteModalBtn').attr('category', 'types');
					$('#deleteModalBtn').attr('object-id', id);
					$('#deleteModal').modal('show');
				}
			});

			//When edit item is clicked
			$('.edit').click(function(){
				var categ = $(this).attr('category');
				if(categ == 'types'){
					var name = $(this).attr('type-name');
					var id = $(this).attr('type-id');
					var kind = $(this).attr('type-category');

					$('#def-text-edit-types').html(categ_list[''+kind]);
					$('#name-edit-types').val(name).attr('old', name);
					$('#category-edit-types').val(kind).attr('old', kind);
					$('#typesEditBtn').attr('type-id', id);
					$('#'+categ+'EditModal').modal('show');

                    $('.status-edit').each(function(){
                        $(this).empty().attr('status', 'okay').attr('changed', 'false');
                    });

                    $('#changesStatus').empty();
                    $('#typesEditBtn').addClass('disabled');
				}
			});
		}

		initiateDynamicFunctions();

		$('.ui.radio.checkbox').checkbox();

		//Changing of how many items should be viewed in a page
		$('.howMany').change(function(){
			var categ = $(this).attr('category');
			resetPages(categ);
		});

		//Changing of pages
		$('.page-change').change(function(){
			var categ = $(this).attr('category');
			organize(categ);
		});

		//When delete modal button is clicked
		$('#deleteModalBtn').click(function(){
			var id = $(this).attr('object-id');
			var category = $(this).attr('category');
			$.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "/delete/addables",
                data: {
                    id: id,
                    category: category
                },
                beforeSend: function(){
                    
                },
                success: function(data){
                	$('#deleteModal').modal('hide');
                	if(category == 'types'){
                		$('#type'+data).remove();
                	}
                },
                error: function(err){
                    console.log(err.responseText);
                }
            });
		});

		function addType(name, kind, category){
			$.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "/add/addables",
                data: {
                    name: name,
                    kind: kind,
                    category: category
                },
                beforeSend: function(){
                    
                },
                success: function(data){
                	$('#'+category+'AddModal').modal('hide');
                    $('#container-'+category).prepend(data);
                    initiateDynamicFunctions();
                },
                error: function(err){
                    console.log(err.responseText);
                }
            });
		}

		function editType(id, name, kind, category){
			$.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "/edit/addables",
                data: {
                	id: id,
                    name: name,
                    kind: kind,
                    category: category
                },
                beforeSend: function(){
                    
                },
                success: function(data){
                	$('#'+category+'EditModal').modal('hide');
                	var type = JSON.parse(data);

                	$('#editType'+type.id).attr('type-name', type.name);
                	$('#editType'+type.id).attr('type-category', type.category);
                	$('#typeCategory'+type.id).html(categ_list[''+type.category]);
                	$('#typeName'+type.id).html(type.name);
                	$('#type'+type.id).transition('pulse');
                	$('#type'+type.id).transition('pulse');
                	initiateDynamicFunctions();
                },
                error: function(err){
                    console.log(err.responseText);
                }
            });
		}

		//When edit button in modal is clicked
		$('.edit-modal-btn').click(function(){
			var categ = $(this).attr('category');
			if(categ == 'types'){
				var name = $('#name-edit-types').val();
				var kind = $('#category-edit-types').val();
				var id = $('#typesEditBtn').attr('type-id');
				editType(id, name, kind, categ);
			}
		});

		//When add button in modal is clicked
		$('.add-modal-btn').click(function(){
			var categ = $(this).attr('category');
			if(categ == 'types'){
				var name = $('#name-types').val();
				var kind = $('#category-types').val();
				addType(name, kind, categ);
			}
		});

		//When add button is clicked, outsiders, locations, types
		$('.add-btn').click(function(){
			var categ = $(this).attr('category');
			$('#'+categ+'AddModal').modal('show');
		});

		//If sort-by is changed, changing of type
		$('.sort-by').change(function(){
			var categ = $(this).attr('category');
			organize(categ);
		});

		//If sort-button is clicked, changing of directions here
		$('.sort-btn').click(function(){
			var categ = $(this).attr('category');
            if($('#sort-button-'+categ).attr('sort-direction') == 'desc'){
                $('#sort-icon-'+categ).removeClass('down');
                $('#sort-icon-'+categ).addClass('up');
                $('#sort-button-'+categ).attr('sort-direction', 'asc');
            }
            else{
                $('#sort-icon-'+categ).removeClass('up');
                $('#sort-icon-'+categ).addClass('down');
                $('#sort-button-'+categ).attr('sort-direction', 'desc');
            }
            $('#sort-button-'+categ).transition('jiggle');
            organize(categ);
        });

        $(".search").on('keyup', function(){
            var categ = $(this).attr('category');
            var searchValue = '';
            var searchType = $('.search-type').attr('value');

            if(categ == 'outsiders'){
                searchValue = $('#search-box-outsiders').val();
            }
            else if (categ == 'locations'){
                searchValue = $('#search-box-locations').val();
            }
            else if (categ == 'points'){
                searchValue = $('#search-box-points').val();
            }
            else if (categ == 'positions'){
                searchValue = $('#search-box-positions').val();
            }
            else if (categ == 'types'){
                searchValue = $('#search-box-types').val();
            } 

            if(searchValue ==''){
                resetPages(categ);
            } 
            else{
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: "/addables/search",
                    data: {
                        searchValue: searchValue,
                        searchType: searchType,
                        categ: categ
                    },
                    success: function(data){
                        $('#container-'+categ).empty();
                        refillConts(data, categ);
                        console.log(categ, searchType,searchValue); 
                    },
                    error: function(err){ 
                        
                        console.log(err.responseText);

                    }
                });
            }
        });


		//Reseting of number of items shown per page
        function resetPages(category){
            var num_items = $('#howManyItems-'+category).val();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "/reset/pages/addables",
                data: {
                    num_items: num_items,
                    category: category,
                },
                success: function(data){
                    $('#pagesMenu-'+category).empty();
                    var pages = JSON.parse(data);
                    
                    for(var i in pages){
                        $('#pagesMenu-'+category).append(
                            $('<div>').addClass('item').attr('data-value', i+':'+pages[i]).html(i)
                        );
                        if(i == $('#page-'+category).val().split(':')[0])
                            $('#page-'+category).val(i+':'+pages[i]);
                    }
                    if($('#page-'+category).val().split(':')[0] > Object.keys(pages).length){
                        $('#page-'+category).val('1:'+pages['1']);
                        $('#pageText-'+category).html('1');
                    }
                    organize(category);
                },
                error: function(err){
                    console.log(err.responseText);
                }
            });
        }


		//Refilling of respective containers, the data parameter contains the objects to be filled along with the specific category of the container
        function refillConts(data, category){
        	var cont = $('#container-'+category);
        	cont.empty();
        	cont.append(data);
        	$('#numOfItems-'+category).html($('#container-'+category).children().length)
        	initiateDynamicFunctions();
        }

        function organize(category){
        	var sortBy = $('#sort-by-'+category).val();
        	var sortDir = $('#sort-button-'+category).attr('sort-direction');
        	var partition = $('#page-'+category).val().split(':');
            var start = partition[1];
            var end = partition[2];

        	$.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "/organize/addables",
                data: {
                    sortBy: sortBy,
                    sortDir: sortDir,
                    category: category,
                    start: start, 
                    end: end,
                },
                beforeSend: function(){
                    
                },
                success: function(data){
                    refillConts(data, category);
                },
                error: function(err){
                    console.log(err.responseText);
                }
            });
        }
	</script>
@else
    <div class="ui placeholder segment">
        <div class="ui icon header">
            <i class="hand paper outline icon"></i>
            You are not allowed to manage types because you are not an admin.
        </div>
    </div>
@endif
@endsection