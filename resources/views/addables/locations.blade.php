@extends('layouts.admin')

@section('content')
@if(Auth::user()->is_admin == 1)
	<div id="deleteModal" class="ui small modal">
        <div class="ui inverted dimmer" id="delLocLoader">
            <div class="ui text loader">Deleting Location</div>
        </div>
		<div class="header" id="delModalHeader">
			Delete Type
		</div>
		<div class="content" id="delModalContent">
			
		</div>
		<div class="actions">
			<button type="button" class="ui red right labeled icon button" id="deleteModalBtn" category="" object-id="">
                <i class="check icon"></i>
                Yes
            </button>
			<button class="ui button deny">No</button>
		</div>
	</div> <!-- Delete Modal -->

	<!-- Add Location Modal -->
	<div id="locationsAddModal" class="ui small modal">
        <div class="ui inverted dimmer" id="addLocLoader">
            <div class="ui text loader">Adding Location</div>
        </div>
		<div class="header">
			Add Location
		</div>
		<div class="content">
			<div class="ui form">
				<div class="field">
                    <label>Name</label>
                    <input type="text" class="locations-name" name="name" placeholder="Enter name" id="name-locations" mode="add">
                    <div class="status-msg status-check-locations status-add" id="nameLocationsMsg-add" status="wrong">
                    </div>
                </div>
			</div>
		</div>
		<div class="actions">
			<button type="button" class="ui disabled red right labeled icon button add-modal-btn" id="locationsAddBtn" category="locations">
                <i class="plus icon"></i>
                Add
            </button>
			<button class="ui button deny">Cancel</button>
		</div>
	</div> <!-- End Add Location Modal -->

	<!-- Edit Location Modal -->
	<div id="locationsEditModal" class="ui small modal">
		<div class="header">
			Edit Locations
		</div>
		<div class="content">
			<div class="ui form">
				<div class="field">
                    <label>Name</label>
                    <input type="text" name="name" class="locations-name" placeholder="Enter name" id="name-edit-locations" mode="edit" old="">
                    <div class="status-msg status-check-outsiders status-edit" id="nameLocationsMsg-edit" status="okay" changed="false">
                    </div>
                </div>
                <div class="error-msg" id="changesStatus"></div>
			</div>
		</div>
		<div class="actions">
			<button type="button" class="ui disabled red right labeled icon button edit-modal-btn" id="locationsEditBtn" location-id="" category="locations">
                <i class="edit icon"></i>
                Edit
            </button>
			<button class="ui button deny">Cancel</button>
		</div>
	</div> <!-- End of edit location modal --> 

	<div class="addables-main-content">
		<div class="ui stackable grid">
			<div class="three wide column">
				<div class="ui fluid selection dropdown">
                    <input class="search-type" type="hidden" name="type" value="name" category="locations" id="search-type-locations">
                    <i class="dropdown icon"></i>
                    <div class="default text">Name</div>
                    <div class="menu">
                        <div class="item" data-value="name">Name</div>
                        <div class="item" data-value="adder">Adder</div>
                    </div>
                </div>
			</div>
			<div class="five wide column">
				<div class="ui fluid left icon input">
                    <input class="search" type="text" id="search-box-locations" placeholder="Search locations" category="locations">
                    <i class="search icon"></i>
                </div>
			</div>
		</div>
		<div class="ui stackable grid">
			<div class="left floated three wide column">
				<button id="addLocationBtn" class="ui labeled icon red button test add-btn" category="locations">
                    <i class="plus icon"></i>
                    Add Location
                </button>
			</div>
			<div class="right floated five wide column">
				<label class="label-mg-right">Sort by</label>
                <div class="ui selection dropdown" id="sort-dropdown">
                    <input class="sort-by" type="hidden" value="created_at" name="type" category="locations" id="sort-by-locations">
                    <i class="dropdown icon"></i>
                    <div class="default text">Date added</div>
                    <div class="menu">
                        <div class="item" data-value="created_at">Date added</div>
                        <div class="item" data-value="name">Name</div>
                    </div>
                </div>

                <button class="circular ui icon button sort-btn" id="sort-button-locations" sort-direction="desc" category="locations">
                    <i class="arrow down icon" id="sort-icon-locations"></i>
                </button>
			</div>
		</div>
		<div class="ui stackable grid">
			<div class="eight wide column semibold">
				Name
			</div>
			<div class="seven wide column semibold">
				Adder
			</div>
			<div class="one wide column">
				
			</div>
		</div>

		<div class="ui vertically divided stackable grid" id="container-locations">
			@foreach($locations as $location)
				<div class="row" id="location{{ $location->id }}">
					<div class="eight wide column semibold" id="locationName{{ $location->id }}">
						{{ $location->name }}
					</div>
					<div class="seven wide column">
						<img class="ui avatar image" src="{{ asset($location->adder->profile_pic) }}">
						{{ $location->adder->firstname.' '.$location->adder->middlename[0].'. '.$location->adder->surname}}
					</div>
					<div class="one wide column">
						<div class="ui floating dropdown icon options-dropdown">
                            <i class="ellipsis vertical icon options-icon"></i>
                            <div class="menu">
                                <div class="item edit" id="editLocation{{ $location->id }}" location-id="{{ $location->id }}" location-name="{{ $location->name }}" category="locations">
                                    <i class="edit icon"></i>
                                    Edit
                                </div>
                                <div class="item delete" location-id="{{ $location->id }}" location-name="{{ $location->name }}" category="locations">
                                    <i class="trash alternate icon"></i>
                                   Delete
                                </div>
                            </div>
                        </div>
					</div>
				</div>
			@endforeach
		</div>

		@if(count($locations) > 0)
            <div class="pagination">
                <div class="ui stackable grid">
                    <div class="five wide column">
                        Show
                        <div class="ui selection dropdown receive-dropdown" id="showItems">
                            <input type="hidden" value="10" class="howMany" category="locations" name="type" id="howManyItems-locations">
                            <i class="dropdown icon"></i>
                            <div class="default text">10</div>
                            <div class="menu">
                            	<div class="item" data-value="5">5 items</div>
                                <div class="item" data-value="10">10 items</div>
                                <div class="item" data-value="20">20 items</div>
                                <div class="item" data-value="25">25 items</div>
                                <div class="item" data-value="50">50 items</div>
                            </div>
                        </div>
                    </div>
                    <div class="five wide column">
                        Page
                        <div class="ui selection dropdown receive-dropdown" id="paginate">
                            <input type="hidden" class="page-change" category="locations" value="{{ '1:'.$partitions_locations['1'] }}" name="type" id="page-locations">
                            <i class="dropdown icon"></i>
                            <div class="default text" id="pageText-locations"></div>
                            <div class="menu" id="pagesMenu-locations">
                                @foreach($partitions_locations as $index => $value)
                                    <div class="item" data-value="{{ $index.':'.$value }}">{{ $index }}</div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="six wide column">
                        <div class="showing">
                            Showing 
                            <span id="numOfItems-locations">{{ count($locations) }}</span> out of 
                            {{ $numItems }}
                        </div>
                    </div>
                </div>
            </div>
        @endif
	</div>
    <script type="text/javascript" src="{{ asset('js/validations/locations-validation.js') }}"></script>
	<script type="text/javascript">
		$('#addablesTabs .item').tab();

		function initiateDynamicFunctions(){
			$('.options-dropdown').dropdown();
			$('.dropdown').dropdown();

			//Delete any addable
			$('.delete').click(function(){
				var categ = $(this).attr('category');
				var id = '';
				if(categ == 'locations'){
					id = $(this).attr('location-id');
					var name = $(this).attr('location-name');
					$('#delModalHeader').html('Delete Location');
					$('#delModalContent').html('Are you sure you want to delete location, <span class="semibold">'+name+'</span>?');
					$('#deleteModalBtn').attr('category', 'locations');
					$('#deleteModalBtn').attr('object-id', id);
					$('#deleteModal').modal('show');
				}
			});

			//When edit item is clicked
			$('.edit').click(function(){
				var categ = $(this).attr('category');

				if(categ == 'locations'){
					var name = $(this).attr('location-name');
					var id = $(this).attr('location-id');
					$('#locationsEditBtn').attr('location-id', id);
					$('#name-edit-locations').val(name).attr('old', name);
					$('#'+categ+'EditModal').modal('show');

                    $('.status-edit').each(function(){
                        $(this).empty().attr('status', 'okay').attr('changed', 'false');
                    });
                    $('#changesStatus').empty();
                    $('#locationsEditBtn').addClass('disabled');
				}
			});
		}

		initiateDynamicFunctions();

		$('.ui.radio.checkbox').checkbox();

		//Changing of how many items should be viewed in a page
		$('.howMany').change(function(){
			var categ = $(this).attr('category');
			resetPages(categ);
		});

		//Changing of pages
		$('.page-change').change(function(){
			var categ = $(this).attr('category');
			organize(categ);
		});

		//When delete modal button is clicked
		$('#deleteModalBtn').click(function(){
            $('#delLocLoader').addClass('active');
			var id = $(this).attr('object-id');
			var category = $(this).attr('category');
			$.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "/delete/addables",
                data: {
                    id: id,
                    category: category
                },
                beforeSend: function(){
                    
                },
                success: function(data){
                    $('#delLocLoader').removeClass('active');
                    resetPages(category);
                	$('#deleteModal').modal('hide');
                	/*if(category == 'locations'){
                		$('#location'+data).remove();
                	}*/
                },
                error: function(err){
                    console.log(err.responseText);
                }
            });
		});

		function addLocation(name, category){
            $('#addLocLoader').addClass('active');
			$.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "/add/addables",
                data: {
                    name: name,
                    category: category
                },
                beforeSend: function(){
                    
                },
                success: function(data){
                    resetPages(category);
                    $('#addLocLoader').removeClass('active');
                	$('#'+category+'AddModal').modal('hide');
                    /*$('#container-'+category).prepend(data);*/
                    initiateDynamicFunctions();
                },
                error: function(err){
                    console.log(err.responseText);
                }
            });
		}

		function editLocation(id, name, category){
			$.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "/edit/addables",
                data: {
                	id: id,
                    name: name,
                    category: category
                },
                beforeSend: function(){
                    
                },
                success: function(data){
                	$('#'+category+'EditModal').modal('hide');
                	var loc = JSON.parse(data);

                	$('#editLocation'+loc.id).attr('location-name', loc.name);

                	$('#locationName'+loc.id).html(loc.name);
                	initiateDynamicFunctions();
                },
                error: function(err){
                    console.log(err.responseText);
                }
            });
		}

		//When edit button in modal is clicked
		$('.edit-modal-btn').click(function(){
			var categ = $(this).attr('category');
			if(categ == 'locations'){
				var name = $('#name-edit-locations').val();
				var id = $('#locationsEditBtn').attr('location-id'); 
				editLocation(id, name, categ);
			}
		});

		//When add button in modal is clicked
		$('.add-modal-btn').click(function(){
			var categ = $(this).attr('category');
			if(categ == 'locations'){
				var name = $('#name-locations').val();
				addLocation(name, categ);
			}
		});

		//When add button is clicked, outsiders, locations, types
		$('.add-btn').click(function(){
			var categ = $(this).attr('category');
			$('#'+categ+'AddModal').modal('show');
		});

		//If sort-by is changed, changing of type
		$('.sort-by').change(function(){
			var categ = $(this).attr('category');
			organize(categ);
		});

		//If sort-button is clicked, changing of directions here
		$('.sort-btn').click(function(){
			var categ = $(this).attr('category');
            if($('#sort-button-'+categ).attr('sort-direction') == 'desc'){
                $('#sort-icon-'+categ).removeClass('down');
                $('#sort-icon-'+categ).addClass('up');
                $('#sort-button-'+categ).attr('sort-direction', 'asc');
            }
            else{
                $('#sort-icon-'+categ).removeClass('up');
                $('#sort-icon-'+categ).addClass('down');
                $('#sort-button-'+categ).attr('sort-direction', 'desc');
            }
            $('#sort-button-'+categ).transition('jiggle');
            organize(categ);
        });

        $(".search").on('keyup', function(){
            var categ = $(this).attr('category');
            var searchValue = '';
            var searchType = $('.search-type').attr('value');

            if(categ == 'outsiders'){
                searchValue = $('#search-box-outsiders').val();
            }
            else if (categ == 'locations'){
                searchValue = $('#search-box-locations').val();
            }
            else {
                searchValue = $('#search-box-types').val();
            }   
            if(searchValue ==''){
                resetPages(categ);
            } 
            else{
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: "/addables/search",
                    data: {
                        searchValue: searchValue,
                        searchType: searchType,
                        categ: categ
                    },
                    success: function(data){
                        $('#container-'+categ).empty();
                        refillConts(data, categ);
                        
                    },
                    error: function(err){ 
                        console.log(categ, searchType); 
                        console.log(err.responseText);

                    }
                });
            }
        });


		//Reseting of number of items shown per page
        function resetPages(category){
            var num_items = $('#howManyItems-'+category).val();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "/reset/pages/addables",
                data: {
                    num_items: num_items,
                    category: category,
                },
                success: function(data){
                    $('#pagesMenu-'+category).empty();
                    var pages = JSON.parse(data);
                    
                    for(var i in pages){
                        $('#pagesMenu-'+category).append(
                            $('<div>').addClass('item').attr('data-value', i+':'+pages[i]).html(i)
                        );
                        if(i == $('#page-'+category).val().split(':')[0])
                            $('#page-'+category).val(i+':'+pages[i]);
                    }
                    if($('#page-'+category).val().split(':')[0] > Object.keys(pages).length){
                        $('#page-'+category).val('1:'+pages['1']);
                        $('#pageText-'+category).html('1');
                    }
                    organize(category);
                },
                error: function(err){
                    console.log(err.responseText);
                }
            });
        }


		//Refilling of respective containers, the data parameter contains the objects to be filled along with the specific category of the container
        function refillConts(data, category){
        	var cont = $('#container-'+category);
        	cont.empty();
        	cont.append(data);
        	$('#numOfItems-'+category).html($('#container-'+category).children().length)
        	initiateDynamicFunctions();
        }

        function organize(category){
        	var sortBy = $('#sort-by-'+category).val();
        	var sortDir = $('#sort-button-'+category).attr('sort-direction');
        	var partition = $('#page-'+category).val().split(':');
            var start = partition[1];
            var end = partition[2];

        	$.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "/organize/addables",
                data: {
                    sortBy: sortBy,
                    sortDir: sortDir,
                    category: category,
                    start: start, 
                    end: end,
                },
                beforeSend: function(){
                    
                },
                success: function(data){
                    refillConts(data, category);
                },
                error: function(err){
                    console.log(err.responseText);
                }
            });
        }
	</script>
@else
    <div class="ui placeholder segment">
        <div class="ui icon header">
            <i class="hand paper outline icon"></i>
            You are not allowed to manage locations because you are not an admin.
        </div>
    </div>
@endif
@endsection