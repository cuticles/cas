@extends('layouts.admin')

@section('content')
@if(Auth::user()->is_admin == 1)
	<div id="deleteModal" class="ui small modal">
        <div class="header" id="delModalHeader">
            Delete Point
        </div>
        <div class="content" id="delModalContent">
            
        </div>
        <div class="actions">
            <button type="button" class="ui approve red right labeled icon button" id="deleteModalBtn" category="points" object-id="">
                <i class="check icon"></i>
                Yes
            </button>
            <button class="ui button deny">No</button>
        </div>
    </div> <!-- Delete Modal -->

    <div id="pointsAddModal" class="ui small modal">
        <div class="header">
            Add Credential Point
        </div>
        <div class="content">
            <div class="ui form">
                <div class="field">
                    <label>Name</label>
                    <input type="text" name="name" class="points-name" placeholder="Enter name" id="name-points" mode="add">
                    <div class="status-msg status-check-points status-add" id="namePointsMsg-add" status="wrong">
                    </div>
                </div>
                <div class="field">
                    <label>Value</label>
                    <input type="text" name="name" class="points-value" placeholder="Enter value" id="value-points" mode="add" onkeypress="return isNumber(event)">
                    <div class="status-msg status-check-points status-add" id="valuePointsMsg-add" status="wrong">
                    </div>
                </div>
            </div>
        </div>
        <div class="actions">
            <button type="button" class="ui disabled approve red right labeled icon button add-modal-btn" id="pointsAddBtn" category="points">
                <i class="plus icon"></i>
                Add
            </button>
            <button class="ui button deny">Cancel</button>
        </div>
    </div>

    <!-- Edit Outsider Modal -->
    <div id="pointsEditModal" class="ui small modal">
        <div class="header">
            Edit Credential Point
        </div>
        <div class="content">
            <div class="ui form">
                <div class="field">
                    <label>Name</label>
                    <input type="text" name="name" class="points-name" placeholder="Enter name" id="name-edit-points" mode="edit" old="">
                    <div class="status-msg status-check-points status-edit" id="namePointsMsg-edit" status="okay" changed="false">
                    </div>
                </div>
                <div class="field">
                    <label>Value</label>
                    <input type="text" name="name" class="points-value" placeholder="Enter value" id="value-edit-points" mode="edit" old="" onkeypress="return isNumber(event)">
                    <div class="status-msg status-check-points status-edit" id="valuePointsMsg-edit" status="okay" changed="false">
                    </div>
                </div>
                <div class="error-msg" id="changesStatus"></div>
            </div>
        </div>
        <div class="actions">
            <button type="button" class="ui disabled approve red right labeled icon button edit-modal-btn" id="pointsEditBtn" point-id="" category="points">
                <i class="edit icon"></i>
                Edit
            </button>
            <button class="ui button deny">Cancel</button>
        </div>
    </div> <!-- End of edit outsider modal -->

	<div class="addables-main-content">
		<div class="ui stackable grid">
			<div class="three wide column">
				<div class="ui fluid selection dropdown">
                    <input class="search-type" type="hidden" name="type" value="name" category="points" id="search-type-points">
                    <i class="dropdown icon"></i>
                    <div class="default text">Name</div>
                    <div class="menu">
                        <div class="item" data-value="name">Name</div>
                        <div class="item" data-value="adder">Adder</div>
                    </div>
                </div>
			</div>
			<div class="five wide column">
				<div class="ui fluid left icon input">
                    <input class="search" type="text" id="search-box-points" placeholder="Search points" category="points">
                    <i class="search icon"></i>
                </div>
			</div>
		</div>
		<div class="ui stackable grid">
			<div class="left floated four wide column">
				<button id="addPointBtn" class="ui labeled icon red button test add-btn" category="points">
                    <i class="plus icon"></i>
                    Add Credential Point
                </button>
			</div>
			<div class="right floated five wide column">
				<label class="label-mg-right">Sort by</label>
                <div class="ui selection dropdown" id="sort-dropdown">
                    <input class="sort-by" type="hidden" value="created_at" name="type" category="points" id="sort-by-points">
                    <i class="dropdown icon"></i>
                    <div class="default text">Date added</div>
                    <div class="menu">
                        <div class="item" data-value="created_at">Date added</div>
                        <div class="item" data-value="name">Name</div>
                    </div>
                </div>

                <button class="circular ui icon button sort-btn" id="sort-button-points" sort-direction="desc" category="points">
                    <i class="arrow down icon" id="sort-icon-points"></i>
                </button>
			</div>
		</div>
		<div class="ui stackable grid">
			<div class="eight wide column semibold">
				Name
			</div>
			<div class="three wide column semibold">
				Value
			</div>
			<div class="four wide column semibold">
				Adder
			</div>
			<div class="one wide column">
				
			</div>
		</div>
		<div class="ui vertically divided stackable grid" id="container-points">
			@foreach($points as $point)
				<div class="row" id="point{{ $point->id }}">
					<div class="eight wide column semibold" id="pointName{{ $point->id }}">
						{{ $point->name }}
					</div>
					<div class="three wide column" id="pointValue{{ $point->id }}">
						{{ $point->value }}
					</div>
					<div class="four wide column">
						<img class="ui avatar image" src="{{ asset($point->adder->profile_pic) }}">
						{{ $point->adder->firstname.' '.$point->adder->middlename[0].'. '.$point->adder->surname}}
					</div>
					<div class="one wide column">
						<div class="ui floating dropdown icon options-dropdown">
                            <i class="ellipsis vertical icon options-icon"></i>
                            <div class="menu">
                                <div class="item edit" id="editPoint{{ $point->id }}" point-id="{{ $point->id }}" point-name="{{ $point->name }}" point-value="{{ $point->value }}" category="points">
                                    <i class="edit icon"></i>
                                    Edit
                                </div>
                                <div class="item delete" point-name="{{ $point->name }}" point-id="{{ $point->id }}" category="points">
                                    <i class="trash alternate icon"></i>
                                   Delete
                                </div>
                            </div>
                        </div>
					</div>
				</div>
			@endforeach
		</div>
		@if(count($points) > 0)
            <div class="pagination">
                <div class="ui stackable grid">
                    <div class="five wide column">
                        Show
                        <div class="ui selection dropdown receive-dropdown" id="showItems">
                            <input type="hidden" value="10" class="howMany" category="points" name="type" id="howManyItems-points">
                            <i class="dropdown icon"></i>
                            <div class="default text">10</div>
                            <div class="menu">
                                <div class="item" data-value="10">10 items</div>
                                <div class="item" data-value="20">20 items</div>
                                <div class="item" data-value="25">25 items</div>
                                <div class="item" data-value="50">50 items</div>
                            </div>
                        </div>
                    </div>
                    <div class="five wide column">
                        Page
                        <div class="ui selection dropdown receive-dropdown" id="paginate">
                            <input type="hidden" class="page-change" category="points" value="{{ '1:'.$partitions_points['1'] }}" name="type" id="page-points">
                            <i class="dropdown icon"></i>
                            <div class="default text" id="pageText-points"></div>
                            <div class="menu" id="pagesMenu-points">
                                @foreach($partitions_points as $index => $value)
                                    <div class="item" data-value="{{ $index.':'.$value }}">{{ $index }}</div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="six wide column">
                        <div class="showing">
                            Showing 
                            <span id="numOfItems-points">{{ count($points) }}</span> out of 
                            {{ $numItems }}
                        </div>
                    </div>
                </div>
            </div>
        @endif
	</div>
    <script type="text/javascript" src="{{ asset('js/validations/points-validation.js') }}"></script>
	<script type="text/javascript">
        $('#addablesTabs .item').tab();

        function initiateDynamicFunctions(){
            $('.options-dropdown').dropdown();
            $('.dropdown').dropdown();

            //Delete any addable
            $('.delete').click(function(){
                var categ = $(this).attr('category');
                var id = '';
                if(categ == 'points'){
                    id = $(this).attr('point-id');
                    var name = $(this).attr('point-name');
                    $('#delModalHeader').html('Delete Outsider');
                    $('#delModalContent').html('Are you sure you want to delete credential point, <span class="semibold">'+name+'</span>?');
                    $('#deleteModalBtn').attr('category', 'points');
                    $('#deleteModalBtn').attr('object-id', id);
                    $('#deleteModal').modal('show');
                }
            });

            //When edit item is clicked
            $('.edit').click(function(){
                var categ = $(this).attr('category');

                if(categ == 'points'){
                    var name = $(this).attr('point-name');
                    var value = $(this).attr('point-value');
                    var id = $(this).attr('point-id');

                    $('#pointsEditBtn').attr('point-id', id);
                    $('#value-edit-points').val(value).attr('old', value);
                    $('#name-edit-points').val(name).attr('old', name);
                    $('#'+categ+'EditModal').modal('show');

                    $('.status-edit').each(function(){
                        $(this).empty().attr('status', 'okay').attr('changed', 'false');
                    });

                    $('#changesStatus').empty();
                    $('#pointsEditBtn').addClass('disabled');
                }
            });
        }

        initiateDynamicFunctions();

        $('.ui.radio.checkbox').checkbox();

        //Changing of how many items should be viewed in a page
        $('.howMany').change(function(){
            var categ = $(this).attr('category');
            resetPages(categ);
        });

        //Changing of pages
        $('.page-change').change(function(){
            var categ = $(this).attr('category');
            organize(categ);
        });

        //When delete modal button is clicked
        $('#deleteModalBtn').click(function(){
            var id = $(this).attr('object-id');
            var category = $(this).attr('category');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "/delete/addables",
                data: {
                    id: id,
                    category: category
                },
                beforeSend: function(){
                    
                },
                success: function(data){
                    $('#deleteModal').modal('hide');
                    if(category == 'points'){
                        $('#point'+data).remove();
                    }
                },
                error: function(err){
                    console.log(err.responseText);
                }
            });
        });

        function addPoint(name, value, category){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "/add/addables",
                data: {
                    name: name,
                    value: value,
                    category: category
                },
                beforeSend: function(){
                    
                },
                success: function(data){
                    $('#'+category+'AddModal').modal('hide');
                    $('#container-'+category).prepend(data);
                    initiateDynamicFunctions();
                },
                error: function(err){
                    console.log(err.responseText);
                }
            });
        }

        function editPoint(id, name, value, category){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "/edit/addables",
                data: {
                    id: id,
                    name: name,
                    value: value,
                    category: category
                },
                beforeSend: function(){
                    
                },
                success: function(data){
                    $('#'+category+'EditModal').modal('hide');
                    var out = JSON.parse(data);

                    $('#editPoint'+out.id).attr('point-name', out.name);
                    $('#editPoint'+out.id).attr('point-value', out.value);;

                    $('#pointName'+out.id).html(out.name);
                    $('#pointValue'+out.id).html(out.value);
                    initiateDynamicFunctions();
                },
                error: function(err){
                    console.log(err.responseText);
                }
            });
        }

        //When edit button in modal is clicked
        $('.edit-modal-btn').click(function(){
            var categ = $(this).attr('category');
            if( categ == 'points'){
                var id = $('#pointsEditBtn').attr('point-id');
                var name = $('#name-edit-points').val();
                var value = $('#value-edit-points').val();
                editPoint(id, name, value, categ);
            }
        });

        //When add button in modal is clicked
        $('.add-modal-btn').click(function(){
            var categ = $(this).attr('category');
            if(categ == 'points'){
                var name = $('#name-points').val();
                var value = $('#value-points').val();
                addPoint(name, value, categ);
            }
        });

        //When add button is clicked, outsiders, locations, types
        $('.add-btn').click(function(){
            var categ = $(this).attr('category');
            $('#'+categ+'AddModal').modal('show');
        });

        //If sort-by is changed, changing of type
        $('.sort-by').change(function(){
            var categ = $(this).attr('category');
            organize(categ);
        });

        //If sort-button is clicked, changing of directions here
        $('.sort-btn').click(function(){
            var categ = $(this).attr('category');
            if($('#sort-button-'+categ).attr('sort-direction') == 'desc'){
                $('#sort-icon-'+categ).removeClass('down');
                $('#sort-icon-'+categ).addClass('up');
                $('#sort-button-'+categ).attr('sort-direction', 'asc');
            }
            else{
                $('#sort-icon-'+categ).removeClass('up');
                $('#sort-icon-'+categ).addClass('down');
                $('#sort-button-'+categ).attr('sort-direction', 'desc');
            }
            $('#sort-button-'+categ).transition('jiggle');
            organize(categ);
        });

                $(".search").on('keyup', function(){
            var categ = $(this).attr('category');
            var searchValue = '';
            var searchType = $('.search-type').attr('value');

            if(categ == 'outsiders'){
                searchValue = $('#search-box-outsiders').val();
            }
            else if (categ == 'locations'){
                searchValue = $('#search-box-locations').val();
            }
            else if (categ == 'points'){
                searchValue = $('#search-box-points').val();
            }
            else if (categ == 'positions'){
                searchValue = $('#search-box-positions').val();
            }
            else {
                searchValue = $('#search-box-types').val();
            }   
            if(searchValue ==''){
                resetPages(categ);
            } 
            else{
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: "/addables/search",
                    data: {
                        searchValue: searchValue,
                        searchType: searchType,
                        categ: categ
                    },
                    success: function(data){
                        $('#container-'+categ).empty();
                        refillConts(data, categ);
                        
                    },
                    error: function(err){ 
                        console.log(categ, searchType); 
                        console.log(err.responseText);

                    }
                });
            }
        });

        //Reseting of number of items shown per page
        function resetPages(category){
            var num_items = $('#howManyItems-'+category).val();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "/reset/pages/addables",
                data: {
                    num_items: num_items,
                    category: category,
                },
                success: function(data){
                    $('#pagesMenu-'+category).empty();
                    var pages = JSON.parse(data);
                    
                    for(var i in pages){
                        $('#pagesMenu-'+category).append(
                            $('<div>').addClass('item').attr('data-value', i+':'+pages[i]).html(i)
                        );
                        if(i == $('#page-'+category).val().split(':')[0])
                            $('#page-'+category).val(i+':'+pages[i]);
                    }
                    if($('#page-'+category).val().split(':')[0] > Object.keys(pages).length){
                        $('#page-'+category).val('1:'+pages['1']);
                        $('#pageText-'+category).html('1');
                    }
                    organize(category);
                },
                error: function(err){
                    console.log(err.responseText);
                }
            });
        }


        //Refilling of respective containers, the data parameter contains the objects to be filled along with the specific category of the container
        function refillConts(data, category){
            var cont = $('#container-'+category);
            cont.empty();
            cont.append(data);
            $('#numOfItems-'+category).html($('#container-'+category).children().length)
            initiateDynamicFunctions();
        }

        function organize(category){
            var sortBy = $('#sort-by-'+category).val();
            var sortDir = $('#sort-button-'+category).attr('sort-direction');
            var partition = $('#page-'+category).val().split(':');
            var start = partition[1];
            var end = partition[2];

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "/organize/addables",
                data: {
                    sortBy: sortBy,
                    sortDir: sortDir,
                    category: category,
                    start: start, 
                    end: end,
                },
                beforeSend: function(){
                    
                },
                success: function(data){
                    refillConts(data, category);
                },
                error: function(err){
                    console.log(err.responseText);
                }
            });
        }
    </script>
@else
    <div class="ui placeholder segment">
        <div class="ui icon header">
            <i class="hand paper outline icon"></i>
            You are not allowed to manage credential points because you are not an admin.
        </div>
    </div>
@endif
@endsection