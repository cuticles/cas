@extends('layouts.admin')

@section('content')
@if(Auth::user()->is_admin == 1)
	<div id="deleteModal" class="ui small modal">
		<div class="header" id="delModalHeader">
			Delete Position
		</div>
		<div class="content" id="delModalContent">
			
		</div>
		<div class="actions">
			<button type="button" class="ui approve red right labeled icon button" id="deleteModalBtn" category="positions" object-id="">
                <i class="check icon"></i>
                Yes
            </button>
			<button class="ui button deny">No</button>
		</div>
	</div> <!-- Delete Modal -->

	<!-- Add Type Modal -->
	<div id="positionsAddModal" class="ui small modal">
		<div class="header">
			Add Position
		</div>
		<div class="content">
			<div class="ui form">
				<div class="field">
                    <label>Name</label>
                    <input type="text" name="name" class="positions-name" placeholder="Enter name" id="name-positions" mode="add">
                    <div class="status-msg status-check-positions status-add" id="namePositionsMsg-add" status="wrong">
                    </div>
                </div>
                <div class="field">
                	<label for="category">Category</label>
                	<div class="ui fluid selection dropdown update-dropdown">
                        <input type="hidden" name="category" class="positions-category" value="" id="category-positions" mode="add">
                        <i class="dropdown icon"></i>
                        <div class="default text">Select category</div>
                        <div class="menu">
                        	<div class="disabled item" data-value="">Select category</div>
	                        <div class="item" data-value="1">Faculty</div>
	                        <div class="item" data-value="2">Staff</div>
                        </div>
                    </div>
                    <div class="status-msg status-check-positions status-add" id="categoryPositionsMsg-add" status="wrong">
                    </div>
                </div>
			</div>
		</div>
		<div class="actions">
			<button type="button" class="ui disabled approve red right labeled icon button add-modal-btn" id="positionsAddBtn" category="positions">
                <i class="plus icon"></i>
                Add
            </button>
			<button class="ui button deny">Cancel</button>
		</div>
	</div> <!-- End Add Type Modal -->

	<!-- Edit Type Modal -->
	<div id="positionsEditModal" class="ui small modal">
		<div class="header">
			Edit Position
		</div>
		<div class="content">
			<div class="ui form">
				<div class="field">
                    <label>Name</label>
                    <input type="text" name="name" class="positions-name" placeholder="Enter name" id="name-edit-positions" mode="edit" old="">
                    <div class="status-msg status-check-positions status-edit" id="namePositionsMsg-edit" status="wrong" changed="false">
                    </div>
                </div>
                <div class="field">
                	<label for="category">Category</label>
                	<div class="ui fluid selection dropdown update-dropdown">
                        <input type="hidden" name="category" class="positions-category" value="" id="category-edit-positions" mode="edit" old="">
                        <i class="dropdown icon"></i>
                        <div class="default text" id="def-text-edit-positions">Select category</div>
                        <div class="menu">
                        	<div class="disabled item" data-value="">Select category</div>
	                        <div class="item" data-value="1">Faculty</div>
	                        <div class="item" data-value="2">Staff</div>
                        </div>
                    </div>
                    <div class="status-msg status-check-positions status-edit" id="categoryPositionsMsg-edit" status="wrong" changed="false">
                    </div>
                </div>
                <div class="error-msg" id="changesStatus"></div>
			</div>
		</div>
		<div class="actions">
			<button type="button" class="ui approve red right labeled icon button edit-modal-btn" id="positionsEditBtn" position-id="" category="positions">
                <i class="edit icon"></i>
                Edit
            </button>
			<button class="ui button deny">Cancel</button>
		</div>
	</div> <!-- End of Edit Type Modal -->

	<div class="main-addables-content">
		<div class="ui stackable grid">
			<div class="three wide column">
				<div class="ui fluid selection dropdown">
                    <input class="search-type" type="hidden" name="type" value="name" category="positions" id="search-type-positions">
                    <i class="dropdown icon"></i>
                    <div class="default text">Name</div>
                    <div class="menu">
                        <div class="item" data-value="name">Name</div>
                        <div class="item" data-value="adder">Adder</div>
                        <div class="item" data-value="location">Location</div>
                    </div>
                </div>
			</div>
			<div class="five wide column">
				<div class="ui fluid left icon input">
                    <input class="search" type="text" id="search-box-positions" placeholder="Search positions" category="positions">
                    <i class="search icon"></i>
                </div>
			</div>
		</div>
		<div class="ui stackable grid">
			<div class="left floated three wide column">
				<button id="addPositionBtn" class="ui labeled icon red button test add-btn" category="positions">
                    <i class="plus icon"></i>
                    Add Position
                </button>
			</div>
			<div class="right floated five wide column">
				<label class="label-mg-right">Sort by</label>
                <div class="ui selection dropdown" id="sort-dropdown">
                    <input class="sort-by" type="hidden" value="created_at" name="type" category="positions" id="sort-by-positions">
                    <i class="dropdown icon"></i>
                    <div class="default text">Date added</div>
                    <div class="menu">
                        <div class="item" data-value="created_at">Date added</div>
                        <div class="item" data-value="name">Name</div>
                    </div>
                </div>

                <button class="circular ui icon button sort-btn" id="sort-button-positions" sort-direction="desc" category="positions">
                    <i class="arrow down icon" id="sort-icon-positions"></i>
                </button>
			</div>
		</div>
		<div class="ui stackable grid">
			<div class="five wide column semibold">
				Name
			</div>
			<div class="six wide column semibold">
				Adder
			</div>
			<div class="four wide column semibold">
				Category
			</div>
			<div class="one wide column">
				
			</div>
		</div>
		<div class="ui vertically divided stackable grid" id="container-positions">
			@foreach($positions as $position)
				<div class="row" id="position{{ $position->id }}">
					<div class="five wide column semibold" id="positionName{{ $position->id }}">
						{{ $position->name }}
					</div>
					<div class="six wide column">
						<img class="ui avatar image" src="{{ asset($position->adder->profile_pic) }}">
						{{ $position->adder->firstname.' '.$position->adder->middlename[0].'. '.$position->adder->surname}}
					</div>
					<div class="four wide column" id="positionCategory{{ $position->id }}">
						{{ $category[$position->category] }}
					</div>
					<div class="one wide column">
						<div class="ui floating dropdown icon options-dropdown">
                            <i class="ellipsis vertical icon options-icon"></i>
                            <div class="menu">
                                <div class="item edit" id="editPosition{{ $position->id }}" position-id="{{ $position->id }}" position-name="{{ $position->name }}" position-category="{{ $position->category }}" category="positions">
                                    <i class="edit icon"></i>
                                    Edit
                                </div>
                                <div class="item delete" position-id="{{ $position->id }}" position-name="{{ $position->name }}" category="positions">
                                    <i class="trash alternate icon"></i>
                                   Delete
                                </div>
                            </div>
                        </div>
					</div>
				</div>
			@endforeach
		</div>

		@if(count($positions) > 0)
            <div class="pagination">
                <div class="ui stackable grid">
                    <div class="five wide column">
                        Show
                        <div class="ui selection dropdown receive-dropdown" id="showItems">
                            <input type="hidden" value="10" class="howMany" category="positions" name="type" id="howManyItems-positions">
                            <i class="dropdown icon"></i>
                            <div class="default text">10</div>
                            <div class="menu">
                            	<div class="item" data-value="2">2 items</div>
                                <div class="item" data-value="10">10 items</div>
                                <div class="item" data-value="20">20 items</div>
                                <div class="item" data-value="25">25 items</div>
                                <div class="item" data-value="50">50 items</div>
                            </div>
                        </div>
                    </div>
                    <div class="five wide column">
                        Page
                        <div class="ui selection dropdown receive-dropdown" id="paginate">
                            <input type="hidden" class="page-change" category="positions" value="{{ '1:'.$partitions_positions['1'] }}" name="type" id="page-positions">
                            <i class="dropdown icon"></i>
                            <div class="default text" id="pageText-positions"></div>
                            <div class="menu" id="pagesMenu-positions">
                                @foreach($partitions_positions as $index => $value)
                                    <div class="item" data-value="{{ $index.':'.$value }}">{{ $index }}</div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="six wide column">
                        <div class="showing">
                            Showing 
                            <span id="numOfItems-positions">{{ count($positions) }}</span> out of 
                            {{ $numItems }}
                        </div>
                    </div>
                </div>
            </div>
        @endif
	</div>
    <script type="text/javascript" src="{{ asset('js/validations/positions-validation.js') }}"></script>
    <script type="text/javascript">
		$('#addablesTabs .item').tab();
		var categ_list = {'1': 'Faculty', '2': 'Staff'};

		function initiateDynamicFunctions(){
			$('.options-dropdown').dropdown();
			$('.dropdown').dropdown();

			//Delete any addable
			$('.delete').click(function(){
				var categ = $(this).attr('category');
				var id = '';
				if(categ == 'positions'){
					id = $(this).attr('position-id');
					var name = $(this).attr('position-name');
					$('#delModalHeader').html('Delete Position');
					$('#delModalContent').html('Are you sure you want to delete position, <span class="semibold">'+name+'</span>?');
					$('#deleteModalBtn').attr('category', 'positions');
					$('#deleteModalBtn').attr('object-id', id);
					$('#deleteModal').modal('show');
				}
			});

			//When edit item is clicked
			$('.edit').click(function(){
				var categ = $(this).attr('category');
				if(categ == 'positions'){
					var name = $(this).attr('position-name');
					var id = $(this).attr('position-id');
					var kind = $(this).attr('position-category');

					$('#def-text-edit-positions').html(categ_list[''+kind]);
					$('#name-edit-positions').val(name).attr('old', name);
					$('#category-edit-positions').val(kind).attr('old', kind);
					$('#positionsEditBtn').attr('position-id', id);
					$('#'+categ+'EditModal').modal('show');

                    $('.status-edit').each(function(){
                        $(this).empty().attr('status', 'okay').attr('changed', 'false');
                    });

                    $('#changesStatus').empty();
                    $('#positionsEditBtn').addClass('disabled');
				}
			});
		}

		initiateDynamicFunctions();

		$('.ui.radio.checkbox').checkbox();

		//Changing of how many items should be viewed in a page
		$('.howMany').change(function(){
			var categ = $(this).attr('category');
			resetPages(categ);
		});

		//Changing of pages
		$('.page-change').change(function(){
			var categ = $(this).attr('category');
			organize(categ);
		});

		//When delete modal button is clicked
		$('#deleteModalBtn').click(function(){
			var id = $(this).attr('object-id');
			var category = $(this).attr('category');
			$.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "/delete/addables",
                data: {
                    id: id,
                    category: category
                },
                beforeSend: function(){
                    
                },
                success: function(data){
                	$('#deleteModal').modal('hide');
                	if(category == 'positions'){
                		$('#position'+data).remove();
                	}
                },
                error: function(err){
                    console.log(err.responseText);
                }
            });
		});

		function addPosition(name, kind, category){
			$.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "/add/addables",
                data: {
                    name: name,
                    kind: kind,
                    category: category
                },
                beforeSend: function(){
                    
                },
                success: function(data){
                	$('#'+category+'AddModal').modal('hide');
                    $('#container-'+category).prepend(data);
                    initiateDynamicFunctions();
                },
                error: function(err){
                    console.log(err.responseText);
                }
            });
		}

		function editPosition(id, name, kind, category){
			$.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "/edit/addables",
                data: {
                	id: id,
                    name: name,
                    kind: kind,
                    category: category
                },
                beforeSend: function(){
                    
                },
                success: function(data){
                	$('#'+category+'EditModal').modal('hide');
                	var position = JSON.parse(data);

                	$('#editPosition'+position.id).attr('position-name', position.name);
                	$('#editPosition'+position.id).attr('position-category', position.category);
                	$('#positionCategory'+position.id).html(categ_list[''+position.category]);
                	$('#positionName'+position.id).html(position.name);
                	$('#position'+position.id).transition('pulse');
                	$('#position'+position.id).transition('pulse');
                	initiateDynamicFunctions();
                },
                error: function(err){
                    console.log(err.responseText);
                }
            });
		}

		//When edit button in modal is clicked
		$('.edit-modal-btn').click(function(){
			var categ = $(this).attr('category');
			if(categ == 'positions'){
				var name = $('#name-edit-positions').val();
				var kind = $('#category-edit-positions').val();
				var id = $('#positionsEditBtn').attr('position-id');
				editPosition(id, name, kind, categ);
			}
		});

		//When add button in modal is clicked
		$('.add-modal-btn').click(function(){
			var categ = $(this).attr('category');
			if(categ == 'positions'){
				var name = $('#name-positions').val();
				var kind = $('#category-positions').val();
				addPosition(name, kind, categ);
			}
		});

		//When add button is clicked, outsiders, locations, types
		$('.add-btn').click(function(){
			var categ = $(this).attr('category');
			$('#'+categ+'AddModal').modal('show');
		});

		//If sort-by is changed, changing of type
		$('.sort-by').change(function(){
			var categ = $(this).attr('category');
			organize(categ);
		});

		//If sort-button is clicked, changing of directions here
		$('.sort-btn').click(function(){
			var categ = $(this).attr('category');
            if($('#sort-button-'+categ).attr('sort-direction') == 'desc'){
                $('#sort-icon-'+categ).removeClass('down');
                $('#sort-icon-'+categ).addClass('up');
                $('#sort-button-'+categ).attr('sort-direction', 'asc');
            }
            else{
                $('#sort-icon-'+categ).removeClass('up');
                $('#sort-icon-'+categ).addClass('down');
                $('#sort-button-'+categ).attr('sort-direction', 'desc');
            }
            $('#sort-button-'+categ).transition('jiggle');
            organize(categ);
        });

                $(".search").on('keyup', function(){
            var categ = $(this).attr('category');
            var searchValue = '';
            var searchType = $('.search-type').attr('value');

            if(categ == 'outsiders'){
                searchValue = $('#search-box-outsiders').val();
            }
            else if (categ == 'locations'){
                searchValue = $('#search-box-locations').val();
            }
            else if (categ == 'points'){
                searchValue = $('#search-box-points').val();
            }
            else if (categ == 'positions'){
                searchValue = $('#search-box-positions').val();
            }
            else {
                searchValue = $('#search-box-types').val();
            }   
            if(searchValue ==''){
                resetPages(categ);
            } 
            else{
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: "/addables/search",
                    data: {
                        searchValue: searchValue,
                        searchType: searchType,
                        categ: categ
                    },
                    success: function(data){
                        $('#container-'+categ).empty();
                        refillConts(data, categ);
                        
                    },
                    error: function(err){ 
                        console.log(categ, searchType); 
                        console.log(err.responseText);

                    }
                });
            }
        });


		//Reseting of number of items shown per page
        function resetPages(category){
            var num_items = $('#howManyItems-'+category).val();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "/reset/pages/addables",
                data: {
                    num_items: num_items,
                    category: category,
                },
                success: function(data){
                    $('#pagesMenu-'+category).empty();
                    var pages = JSON.parse(data);
                    
                    for(var i in pages){
                        $('#pagesMenu-'+category).append(
                            $('<div>').addClass('item').attr('data-value', i+':'+pages[i]).html(i)
                        );
                        if(i == $('#page-'+category).val().split(':')[0])
                            $('#page-'+category).val(i+':'+pages[i]);
                    }
                    if($('#page-'+category).val().split(':')[0] > Object.keys(pages).length){
                        $('#page-'+category).val('1:'+pages['1']);
                        $('#pageText-'+category).html('1');
                    }
                    organize(category);
                },
                error: function(err){
                    console.log(err.responseText);
                }
            });
        }


		//Refilling of respective containers, the data parameter contains the objects to be filled along with the specific category of the container
        function refillConts(data, category){
        	var cont = $('#container-'+category);
        	cont.empty();
        	cont.append(data);
        	$('#numOfItems-'+category).html($('#container-'+category).children().length)
        	initiateDynamicFunctions();
        	console.log('initiated');
        }

        function organize(category){
        	var sortBy = $('#sort-by-'+category).val();
        	var sortDir = $('#sort-button-'+category).attr('sort-direction');
        	var partition = $('#page-'+category).val().split(':');
            var start = partition[1];
            var end = partition[2];

        	$.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "/organize/addables",
                data: {
                    sortBy: sortBy,
                    sortDir: sortDir,
                    category: category,
                    start: start, 
                    end: end,
                },
                beforeSend: function(){
                    
                },
                success: function(data){
                    refillConts(data, category);
                },
                error: function(err){
                    console.log(err.responseText);
                }
            });
        }
	</script>
@else
    <div class="ui placeholder segment">
        <div class="ui icon header">
            <i class="hand paper outline icon"></i>
            You are not allowed to manage positions because you are not an admin.
        </div>
    </div>
@endif
@endsection