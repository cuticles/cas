@extends('layouts.admin')

@section('content')
@if(Auth::user()->is_admin == 1)
    <div id="deleteModal" class="ui small modal">
        <div class="header" id="delModalHeader">
            Add Type
        </div>
        <div class="content" id="delModalContent">
            
        </div>
        <div class="actions">
            <button type="button" class="ui approve red right labeled icon button" id="deleteModalBtn" category="" object-id="">
                <i class="check icon"></i>
                Yes
            </button>
            <button class="ui button deny">No</button>
        </div>
    </div> <!-- Delete Modal -->

    <div id="outsidersAddModal" class="ui small modal">
        <div class="header">
            Add Outsider
        </div>
        <div class="content">
            <div class="ui form">
                <div class="field">
                    <label>Name</label>
                    <input type="text" class="outsiders-name" name="name" placeholder="Enter name" id="name-outsiders" mode="add">
                    <div class="status-msg status-check-outsiders status-add" id="nameOutsidersMsg-add" status="wrong">
                    </div>
                </div>
                <div class="field">
                    <label>Location</label>
                    <div class="ui fluid selection dropdown update-dropdown">
                        <input type="hidden" class="outsiders-location" name="location" value="" id="location-outsiders" mode="add">
                        <i class="dropdown icon"></i>
                        <div class="default text">Select location</div>
                        <div class="menu">
                            <div class="disabled item" data-value="">Select location</div>
                            @foreach($locations as $location)
                                <div class="item" data-value="{{ $location->id }}">
                                    {{ $location->name }}
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="status-msg status-check-outsiders status-add" id="locationOutsidersMsg-add" status="wrong">
                    </div>
                </div>
            </div>
        </div>
        <div class="actions">
            <button type="button" class="ui disabled red right labeled icon button add-modal-btn" id="outsidersAddBtn" category="outsiders">
                <i class="plus icon"></i>
                Add
            </button>
            <button class="ui button deny">Cancel</button>
        </div>
    </div>

    <!-- Edit Outsider Modal -->
    <div id="outsidersEditModal" class="ui small modal">
        <div class="header">
            Edit Outsiders
        </div>
        <div class="content">
            <div class="ui form">
                <div class="field">
                    <label>Name</label>
                    <input type="text" name="name" class="outsiders-name" placeholder="Enter name" id="name-edit-outsiders" mode="edit" old="">
                    <div class="status-msg status-check-outsiders status-edit" id="nameOutsidersMsg-edit" status="okay" changed="false">
                    </div>
                </div>
                <div class="field">
                    <label>Location</label>
                    <div class="ui fluid selection dropdown update-dropdown">
                        <input type="hidden" class="outsiders-location" name="location" value="" id="location-edit-outsiders" mode="edit" old="">
                        <i class="dropdown icon"></i>
                        <div class="default text" id="def-text-edit-outsiders">Select location</div>
                        <div class="menu">
                            <div class="disabled item" data-value="">Select location</div>
                            @foreach($locations as $location)
                                <div class="item" data-value="{{ $location->id }}">
                                    {{ $location->name }}
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="status-msg status-check-outsiders status-edit" id="locationOutsidersMsg-edit" status="okay" changed="false">
                    </div>
                </div>
                <div class="error-msg" id="changesStatus"></div>
            </div>
        </div>
        <div class="actions">
            <button type="button" class="ui disabled red right labeled icon button edit-modal-btn" id="outsidersEditBtn" outsider-id="" category="outsiders">
                <i class="edit icon"></i>
                Edit
            </button>
            <button class="ui button deny">Cancel</button>
        </div>
    </div> <!-- End of edit outsider modal -->
    
    <div class="addables-main-content">
		<div class="ui stackable grid">
			<div class="three wide column">
				<div class="ui fluid selection dropdown">
                    <input class="search-type" type="hidden" name="type" value="name" category="outsiders" id="search-type-outsiders">
                    <i class="dropdown icon"></i>
                    <div class="default text">Name</div>
                    <div class="menu">
                        <div class="item" data-value="name">Name</div>
                        <div class="item" data-value="adder">Adder</div>
                        <div class="item" data-value="locations">Location</div>
                    </div>
                </div>
			</div>
			<div class="five wide column">
				<div class="ui fluid left icon input">
                    <input class="search" type="text" id="search-box-outsiders" placeholder="Search outsiders" category="outsiders">
                    <i class="search icon"></i>
                </div>
			</div>
		</div>
		<div class="ui stackable grid">
			<div class="left floated three wide column">
				<button id="addOutsiderBtn" class="ui labeled icon red button test add-btn" category="outsiders">
                    <i class="plus icon"></i>
                    Add Outsider
                </button>
			</div>
			<div class="right floated five wide column">
				<label class="label-mg-right">Sort by</label>
                <div class="ui selection dropdown" id="sort-dropdown">
                    <input class="sort-by" type="hidden" value="created_at" name="type" category="outsiders" id="sort-by-outsiders">
                    <i class="dropdown icon"></i>
                    <div class="default text">Date added</div>
                    <div class="menu">
                        <div class="item" data-value="created_at">Date added</div>
                        <div class="item" data-value="name">Name</div>
                        <div class="item" data-value="location">Location</div>
                    </div>
                </div>

                <button class="circular ui icon button sort-btn" id="sort-button-outsiders" sort-direction="desc" category="outsiders">
                    <i class="arrow down icon" id="sort-icon-outsiders"></i>
                </button>
			</div>
		</div>
		<div class="ui stackable grid">
			<div class="five wide column semibold">
				Name
			</div>
			<div class="six wide column semibold">
				Location
			</div>
			<div class="four wide column semibold">
				Adder
			</div>
			<div class="one wide column">
				
			</div>
		</div>
		<div class="ui vertically divided stackable grid" id="container-outsiders">
			@foreach($outsiders as $outsider)
				<div class="row" id="outsider{{ $outsider->id }}">
					<div class="five wide column semibold" id="outsiderName{{ $outsider->id }}">
						{{ $outsider->name }}
					</div>
					<div class="six wide column" id="outsiderLocation{{ $outsider->id }}">
						{{ $outsider->location->name }}
					</div>
					<div class="four wide column">
						<img class="ui avatar image" src="{{ asset($outsider->adder->profile_pic) }}">
						{{ $outsider->adder->firstname.' '.$outsider->adder->middlename[0].'. '.$outsider->adder->surname}}
					</div>
					<div class="one wide column">
						<div class="ui floating dropdown icon options-dropdown">
                            <i class="ellipsis vertical icon options-icon"></i>
                            <div class="menu">
                                <div class="item edit" id="editOutsider{{ $outsider->id }}" outsider-id="{{ $outsider->id }}" outsider-name="{{ $outsider->name }}" outsider-location="{{ $outsider->location->id}}" outsider-location-name="{{ $outsider->location->name }}" category="outsiders">
                                    <i class="edit icon"></i>
                                    Edit
                                </div>
                                <div class="item delete" outsider-name="{{ $outsider->name }}" outsider-id="{{ $outsider->id }}" category="outsiders">
                                    <i class="trash alternate icon"></i>
                                   Delete
                                </div>
                            </div>
                        </div>
					</div>
				</div>
			@endforeach
		</div>
		@if(count($outsiders) > 0)
            <div class="pagination">
                <div class="ui stackable grid">
                    <div class="five wide column">
                        Show
                        <div class="ui selection dropdown receive-dropdown" id="showItems">
                            <input type="hidden" value="10" class="howMany" category="outsiders" name="type" id="howManyItems-outsiders">
                            <i class="dropdown icon"></i>
                            <div class="default text">10</div>
                            <div class="menu">
                                <div class="item" data-value="10">10 items</div>
                                <div class="item" data-value="20">20 items</div>
                                <div class="item" data-value="25">25 items</div>
                                <div class="item" data-value="50">50 items</div>
                            </div>
                        </div>
                    </div>
                    <div class="five wide column">
                        Page
                        <div class="ui selection dropdown receive-dropdown" id="paginate">
                            <input type="hidden" class="page-change" category="outsiders" value="{{ '1:'.$partitions_outsiders['1'] }}" name="type" id="page-outsiders">
                            <i class="dropdown icon"></i>
                            <div class="default text" id="pageText-outsiders"></div>
                            <div class="menu" id="pagesMenu-outsiders">
                                @foreach($partitions_outsiders as $index => $value)
                                    <div class="item" data-value="{{ $index.':'.$value }}">{{ $index }}</div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="six wide column">
                        <div class="showing">
                            Showing 
                            <span id="numOfItems-outsiders">{{ count($outsiders) }}</span> out of 
                            {{ $numItems }}
                        </div>
                    </div>
                </div>
            </div>
        @endif
	</div>
    <script type="text/javascript" src="{{ asset('js/validations/outsiders-validation.js') }}"></script>
    <script type="text/javascript">
        $('#addablesTabs .item').tab();

        function initiateDynamicFunctions(){
            $('.options-dropdown').dropdown();
            $('.dropdown').dropdown();

            //Delete any addable
            $('.delete').click(function(){
                var categ = $(this).attr('category');
                var id = '';
                if(categ == 'outsiders'){
                    id = $(this).attr('outsider-id');
                    var name = $(this).attr('outsider-name');
                    $('#delModalHeader').html('Delete Outsider');
                    $('#delModalContent').html('Are you sure you want to delete outsider, <span class="semibold">'+name+'</span>?');
                    $('#deleteModalBtn').attr('category', 'outsiders');
                    $('#deleteModalBtn').attr('object-id', id);
                    $('#deleteModal').modal('show');
                }
            });

            //When edit item is clicked
            $('.edit').click(function(){
                var categ = $(this).attr('category');

                if(categ == 'outsiders'){
                    var name = $(this).attr('outsider-name');
                    var locName = $(this).attr('outsider-location-name');
                    var locId = $(this).attr('outsider-location');
                    var id = $(this).attr('outsider-id');

                    $('#outsidersEditBtn').attr('outsider-id', id);
                    $('#location-edit-outsiders').val(locId).attr('old', locId);
                    $('#def-text-edit-outsiders').html(locName);
                    $('#name-edit-outsiders').val(name).attr('old', name);
                    $('#'+categ+'EditModal').modal('show');

                    $('.status-edit').each(function(){
                        $(this).empty().attr('status', 'okay').attr('changed', 'false');
                    });
                    $('#changesStatus').empty();
                    $('#outsidersEditBtn').addClass('disabled');
                }
            });
        }

        initiateDynamicFunctions();

        $('.ui.radio.checkbox').checkbox();

        //Changing of how many items should be viewed in a page
        $('.howMany').change(function(){
            var categ = $(this).attr('category');
            resetPages(categ);
        });

        //Changing of pages
        $('.page-change').change(function(){
            var categ = $(this).attr('category');
            organize(categ);
        });

        //When delete modal button is clicked
        $('#deleteModalBtn').click(function(){
            var id = $(this).attr('object-id');
            var category = $(this).attr('category');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "/delete/addables",
                data: {
                    id: id,
                    category: category
                },
                beforeSend: function(){
                    
                },
                success: function(data){
                    $('#deleteModal').modal('hide');
                    /*if(category == 'outsiders'){
                        $('#outsider'+data).remove();
                    }
                    else if(category == 'locations'){
                        $('#location'+data).remove();
                    }
                    else if(category == 'types'){
                        $('#type'+data).remove();
                    }*/
                    resetPages(category);
                },
                error: function(err){
                    console.log(err.responseText);
                }
            });
        });

        function addOutsider(name, locId, category){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "/add/addables",
                data: {
                    name: name,
                    locId: locId,
                    category: category
                },
                beforeSend: function(){
                    
                },
                success: function(data){
                    $('#'+category+'AddModal').modal('hide');
                    resetPages(category);
                    initiateDynamicFunctions();
                },
                error: function(err){
                    console.log(err.responseText);
                }
            });
        }

        function editOutsider(id, name, locId, category){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "/edit/addables",
                data: {
                    id: id,
                    name: name,
                    locId: locId,
                    category: category
                },
                beforeSend: function(){
                    
                },
                success: function(data){
                    $('#'+category+'EditModal').modal('hide');
                    var out = JSON.parse(data);

                    $('#editOutsider'+out.id).attr('outsider-name', out.name);
                    $('#editOutsider'+out.id).attr('outsider-location-name', out.location_name);
                    $('#editOutsider'+out.id).attr('outsider-location', out.location_id);

                    $('#outsiderName'+out.id).html(out.name);
                    $('#outsiderLocation'+out.id).html(out.location_name);
                    initiateDynamicFunctions();
                },
                error: function(err){
                    console.log(err.responseText);
                }
            });
        }

        //When edit button in modal is clicked
        $('.edit-modal-btn').click(function(){
            var categ = $(this).attr('category');
            if( categ == 'outsiders'){
                var id = $('#outsidersEditBtn').attr('outsider-id');
                var name = $('#name-edit-outsiders').val();
                var locId = $('#location-edit-outsiders').val();
                editOutsider(id, name, locId, categ);
            }
        });

        //When add button in modal is clicked
        $('.add-modal-btn').click(function(){
            var categ = $(this).attr('category');
            if(categ == 'outsiders'){
                var name = $('#name-outsiders').val();
                var locId = $('#location-outsiders').val();
                addOutsider(name, locId, categ);
            }
        });

        //When add button is clicked, outsiders, locations, types
        $('.add-btn').click(function(){
            var categ = $(this).attr('category');
            $('#'+categ+'AddModal').modal('show');
        });

        //If sort-by is changed, changing of type
        $('.sort-by').change(function(){
            var categ = $(this).attr('category');
            organize(categ);
        });

        //If sort-button is clicked, changing of directions here
        $('.sort-btn').click(function(){
            var categ = $(this).attr('category');
            if($('#sort-button-'+categ).attr('sort-direction') == 'desc'){
                $('#sort-icon-'+categ).removeClass('down');
                $('#sort-icon-'+categ).addClass('up');
                $('#sort-button-'+categ).attr('sort-direction', 'asc');
            }
            else{
                $('#sort-icon-'+categ).removeClass('up');
                $('#sort-icon-'+categ).addClass('down');
                $('#sort-button-'+categ).attr('sort-direction', 'desc');
            }
            $('#sort-button-'+categ).transition('jiggle');
            organize(categ);
        });

        $(".search").on('keyup', function(){
            var categ = $(this).attr('category');
            var searchValue = '';
            var searchType = $('.search-type').attr('value');

            if(categ == 'outsiders'){
                searchValue = $('#search-box-outsiders').val();
            }
            else if (categ == 'locations'){
                searchValue = $('#search-box-locations').val();
            }
            else if (categ == 'points'){
                searchValue = $('#search-box-points').val();
            }
            else if (categ == 'positions'){
                searchValue = $('#search-box-positions').val();
            }
            else {
                searchValue = $('#search-box-types').val();
            }   
            if(searchValue ==''){
                resetPages(categ);
            } 
            else{
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: "/addables/search",
                    data: {
                        searchValue: searchValue,
                        searchType: searchType,
                        categ: categ
                    },
                    success: function(data){
                        $('#container-'+categ).empty();
                        refillConts(data, categ);
                        
                    },
                    error: function(err){ 
                        console.log(categ, searchType); 
                        console.log(err.responseText);

                    }
                });
            }
        });


        //Reseting of number of items shown per page
        function resetPages(category){
            var num_items = $('#howManyItems-'+category).val();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "/reset/pages/addables",
                data: {
                    num_items: num_items,
                    category: category,
                },
                success: function(data){
                    $('#pagesMenu-'+category).empty();
                    var pages = JSON.parse(data);
                    
                    for(var i in pages){
                        $('#pagesMenu-'+category).append(
                            $('<div>').addClass('item').attr('data-value', i+':'+pages[i]).html(i)
                        );
                        if(i == $('#page-'+category).val().split(':')[0])
                            $('#page-'+category).val(i+':'+pages[i]);
                    }
                    if($('#page-'+category).val().split(':')[0] > Object.keys(pages).length){
                        $('#page-'+category).val('1:'+pages['1']);
                        $('#pageText-'+category).html('1');
                    }
                    organize(category);
                },
                error: function(err){
                    console.log(err.responseText);
                }
            });
        }


        //Refilling of respective containers, the data parameter contains the objects to be filled along with the specific category of the container
        function refillConts(data, category){
            var cont = $('#container-'+category);
            cont.empty();
            cont.append(data);
            $('#numOfItems-'+category).html($('#container-'+category).children().length)
            initiateDynamicFunctions();
        }

        function organize(category){
            var sortBy = $('#sort-by-'+category).val();
            var sortDir = $('#sort-button-'+category).attr('sort-direction');
            var partition = $('#page-'+category).val().split(':');
            var start = partition[1];
            var end = partition[2];

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "/organize/addables",
                data: {
                    sortBy: sortBy,
                    sortDir: sortDir,
                    category: category,
                    start: start, 
                    end: end,
                },
                beforeSend: function(){
                    
                },
                success: function(data){
                    refillConts(data, category);
                },
                error: function(err){
                    console.log(err.responseText);
                }
            });
        }
    </script>
@else
    <div class="ui placeholder segment">
        <div class="ui icon header">
            <i class="hand paper outline icon"></i>
            You are not allowed to manage outsiders because you are not an admin.
        </div>
    </div>
@endif
@endsection