
def minimum_edit_distance(x, y):
	m = len(x)+1
	n = len(y)+1
	med=[[0]*n for i in range(m+1)]

	for a in range(0, m):
		med[a][0] = a
	for b in range(0, n):
		med[0][b] = b	
	for a in range(1, m):
		for b in range(1, n):
			value=1
			if(x[a-1]==y[b-1]):
				value=0
			med[a][b]=min(med[a-1][b]+1, med[a][b-1]+1, med[a-1][b-1]+value)
	return med[m-1][n-1]

def find_shortest_orthographic_sequence(words,start,end):
	sequence=[start]
	tmp=[]


	if start not in words:
		return sequence

	if minimum_edit_distance(start, end) == 1:
		sequence.append(end)
		return sequence

	else:
		for x in range(1, num_words):
			if words[x]!=start:
				m = minimum_edit_distance(start, words[x])
				if m==1:
					tmp.append(words[x])
					x+=1
				else:
					tmp2=[]
					n = minimum_edit_distance(words[x], end)
					if n==1:
						tmp2.append(x)
					x+=1
					
					for i in range(0, len(tmp2)):
						z = 0
						closest = []
						for r in tmp2:
							closest.append(min_edit_dist(r,end))

						z = closest.index(min(closest))	
						tmp.append(tmp2[z])

			if words[x]==end:
				tmp.append(end)
				break

	for y in range(0, len(tmp)):
		sequence.append(tmp[y])
		return sequence

if __name__ == '__main__':
	num_test_cases = int(raw_input())
	for i in range(num_test_cases):
		num_words = int(raw_input())
		words = [raw_input().strip() for x in range(num_words)]
		words = sorted(words)
		
		num_query = int(raw_input())
		for x in range(num_query):
			start,end = raw_input().strip().split()
			sequence = find_shortest_orthographic_sequence(words,start,end)
			print '%d %s' % (len(sequence),'-'.join(sequence))
