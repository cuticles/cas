function validateAddOutsider(mode){
    var okays = 0;

    if(mode == 'send'){
        if($('#sendOutsiderLocation').val() == 'others'){
            $('.status-check-include-new-loc-send').each(function(){
                if($(this).attr('status') == 'okay'){
                    okays += 1;
                }
            });
        }
        else{
            $('.status-check-outsider-name-send').each(function(){
                if($(this).attr('status') == 'okay'){
                    okays += 1;
                }
            });
        }
    }
    else{
        if($('#outsiderLocation').val() == 'others'){
            $('.status-check-include-new-loc-receive').each(function(){
                if($(this).attr('status') == 'okay'){
                    okays += 1;
                }
            });
        }
        else{
            $('.status-check-outsider-name-receive').each(function(){
                if($(this).attr('status') == 'okay'){
                    okays += 1;
                }
            });
        }
    }

    console.log(okays);

    if(mode == 'send'){
        if($('#sendOutsiderLocation').val() == 'others'){
            if(okays == 3)
                $('#sendAddOutsider').removeClass('disabled');
            else
                $('#sendAddOutsider').addClass('disabled');
        }
        else{
            if(okays == 2)
                $('#sendAddOutsider').removeClass('disabled');
            else
                $('#sendAddOutsider').addClass('disabled');
        }
    }
    else{
        if($('#outsiderLocation').val() == 'others'){
            if(okays == 3)
                $('#addOutsider').removeClass('disabled');
            else
                $('#addOutsider').addClass('disabled');
        }
        else{
            if(okays == 2)
                $('#addOutsider').removeClass('disabled');
            else
                $('#addOutsider').addClass('disabled');
        }
    }
}

//Assign value for each report messages
function assignValidationMessageOutsider(msgElement, message, status, mode){
    msgElement.html(message);
    msgElement.attr('status', status);

    if(status == 'okay'){
        msgElement.addClass('status-msg').removeClass('error-msg');
        msgElement.transition('pulse');
    }
    else{
        msgElement.removeClass('status-msg').addClass('error-msg');
        msgElement.transition('bounce');
    }

    validateAddOutsider(mode);
}

//Perform validation if name input is changed
$('.outsider-name').change(function(){
    var mode = $(this).attr('mode');
    var msgElement = $('#outsiderName-'+mode);
    var status = '';
    var message = '';

    if($(this).val() == ''){
        status = 'wrong';
        message = 'This is a required field. You need to enter a name (e.g. Maria Leonora Theresa).';
    }
    else{
        status = 'okay';
        message = "This is a valid name.";
    }
    
    assignValidationMessageOutsider(msgElement, message, status, mode);
});

//Perform validation if location input is changed
$('.outsider-location').change(function(){
    var mode = $(this).attr('mode');
    var msgElement = $('#outsiderLocation-'+mode);
    var status = '';
    var message = '';

    if($(this).val() == ''){
        status = 'wrong';
        message = 'This is a required field. You need to choose a location.';
    }
    else{
        status = 'okay';
        message = "This is a valid location.";
    }
    
    assignValidationMessageOutsider(msgElement, message, status, mode);
});

//Perform validation if location input is changed
$('.outsider-new-location').change(function(){
    var mode = $(this).attr('mode');
    var msgElement = $('#outsiderNewLoc-'+mode);
    var status = '';
    var message = '';

    if($(this).val() == ''){
        status = 'wrong';
        message = 'This is a required field. You need to enter name of new location (e.g. School of Technology).';
    }
    else{
        status = 'okay';
        message = "This is a valid location name.";
    }
    
    assignValidationMessageOutsider(msgElement, message, status, mode);
});










//Received Collection Form Validation!
function assignValidationMessagesReceived(msgElement, message, status){
    msgElement.html(message);
    msgElement.attr('status', status);

    if(status == 'okay'){
        msgElement.addClass('status-msg').removeClass('error-msg');
        msgElement.transition('pulse');
    }
    else{
        msgElement.removeClass('status-msg').addClass('error-msg');
        msgElement.transition('bounce');
    }

    validateReceiveForm();
}

$('#receiveRecipientTypeInput').change(function(){
    if($(this).val() == 'many'){
        $('#receiveGroupContainer').addClass('hidden');
        $('#receiveManyContainer').removeClass('hidden');
        $('#receiveMany').removeClass('disabled');
    }
    else if($(this).val() == 'group'){
        $('#receiveGroupContainer').removeClass('hidden');
        $('#receiveManyContainer').addClass('hidden');
    }
    else{
        $('#receiveGroupContainer').addClass('hidden');
        $('#receiveManyContainer').removeClass('hidden');
        $('#receiveMany').addClass('disabled');
        $('#receiveManyInput').val('');
    }

    checkSendToRecipientsReceive()
});

$('.receive-dropdown').dropdown();

$('#receiveBtn').click(function(){
    $('#receiveModal').modal('show');
});

function validateReceiveForm(){
    var count = 0;
    $('.status-check-receive').each(function(){
        if($(this).attr('status') == 'okay'){
            count += 1;
        }
    });

    if(count == 4){
        $('#receiveCollBtn').removeClass('disabled');
    }
    else{
        $('#receiveCollBtn').addClass('disabled');
    }
}

$('#outsiderInput').change(function(){
    var msgElement = $('#outsiderSelectMsgReceived'); 
    var message = '';
    var status = '';

    if($(this).val() == ''){
        status = 'wrong';
        message = 'This is a required field. You need to choose an outsider.';
    }
    else{
        status = 'okay';
        message = 'This is a valid outsider.';
    }

    assignValidationMessages(msgElement, message, status);
});

//Validation for Receive Collection Form
$('#receiveTitleInput').change(function(){
    var msgElement = $('#receiveTitlesMsg'); 
    var message = '';
    var status = '';

    if($(this).val() == ''){
        status = 'wrong';
        message = 'This is a required field. You need to have a title for this collection (e.g. Request For Something).';
    }
    else{
        status = 'okay';
        message = 'This looks like a good title.';
    }

    assignValidationMessagesReceived(msgElement, message, status);
});

$('#receiveTypeInput').change(function(){
    var msgElement = $('#receiveTypeMsg');
    var message = '';
    var status = '';

    if($(this).val() == ''){
        status = 'wrong';
        message = 'This is a required field. You need to select a collection type.';
    }
    else{
        status = 'okay';
        message = 'This type is valid.';
    }

    assignValidationMessagesReceived(msgElement, message, status);
});

$('#receiveManyInput').change(function(){
    checkSendToRecipientsReceive();
});

$('#receiveGroupInput').change(function(){
    checkSendToRecipientsReceive();
});

function checkSendToRecipientsReceive(){
    var type = $('#receiveRecipientTypeInput').val();
    var msgElement = $('#receiveRecipientsMsg');
    var message = '';
    var status = '';

    if(type == 'many'){
        if($('#receiveManyInput').val() == ''){
            message = "This is a required field. You should send this to at least one user.";
            status = 'wrong';
        }
        else{
            message = "You will send this collection to user(s)."
            status = 'okay';
        }
    }
    else if(type == 'group'){
        if($('#receiveGroupInput').val() == ''){
            message = "This is a required field. You should send this to a group.";
            status = 'wrong';
        }
        else{
            message = "You will send this collection to a group."
            status = 'okay';
        }
    }
    else if(type == 'all'){
        message = "You will send this collection to all users."
        status = 'okay';
    }

    assignValidationMessagesReceived(msgElement, message, status);
}












//Send Collection Form Validation!
function validateSendForm(){
    var count = 0;
    if($('#sendToInput').val() == 'outside'){
        $('.status-check-inc-outsider-send').each(function(){
            if($(this).attr('status') == 'okay')
                count += 1;
        });

        if(count == 3)
            $('#sendCollBtn').removeClass('disabled');
        else
            $('#sendCollBtn').addClass('disabled');
    }
    else{
        $('.status-check-send').each(function(){
            if($(this).attr('status') == 'okay')
                count += 1;
        });

        if(count == 3)
            $('#sendCollBtn').removeClass('disabled');
        else
            $('#sendCollBtn').addClass('disabled');
    }
}

$('#sendOutInput').change(function(){
    var msgElement = $('#outsiderSelectMsg'); 
    var message = '';
    var status = '';

    if($(this).val() == ''){
        status = 'wrong';
        message = 'This is a required field. You need to choose an outsider.';
    }
    else{
        status = 'okay';
        message = 'This is a valid outsider.';
    }

    assignValidationMessages(msgElement, message, status);
});

$('#recipientTypeInput').change(function(){
    if($(this).val() == 'many'){
        $('#groupContainer').addClass('hidden');
        $('#manyContainer').removeClass('hidden');
        $('#sendMany').removeClass('disabled');
    }
    else if($(this).val() == 'group'){
        $('#groupContainer').removeClass('hidden');
        $('#manyContainer').addClass('hidden');
    }
    else{
        $('#groupContainer').addClass('hidden');
        $('#manyContainer').removeClass('hidden');
        $('#sendMany').addClass('disabled');
        $('#manyInput').val('');
    }
    checkSendToRecipients();
});

$('#manyInput').change(function(){
    checkSendToRecipients();
});

$('#groupInput').change(function(){
    checkSendToRecipients();
});

function checkSendToRecipients(){
    var type = $('#recipientTypeInput').val();
    var msgElement = $('#recipientsMsg');
    var message = '';
    var status = '';

    if(type == 'many'){
        if($('#manyInput').val() == ''){
            message = "This is a required field. You should send this to at least one user.";
            status = 'wrong';
        }
        else{
            message = "You will send this collection to user(s)."
            status = 'okay';
        }
    }
    else if(type == 'group'){
        if($('#groupInput').val() == ''){
            message = "This is a required field. You should send this to a group.";
            status = 'wrong';
        }
        else{
            message = "You will send this collection to a group."
            status = 'okay';
        }
    }
    else{
        message = "You will send this collection to all users."
        status = 'okay';
    }

    assignValidationMessages(msgElement, message, status);
}

$('#titleInput').focusout(function(){
    var msgElement = $('#titlesMsg'); 
    var message = '';
    var status = '';

    if($(this).val() == ''){
        status = 'wrong';
        message = 'This is a required field. You need to have a title for this collection (e.g. Request For Something).';
    }
    else{
        status = 'okay';
        message = 'This looks like a good title.';
    }

    assignValidationMessages(msgElement, message, status);
});

$('#typeInput').change(function(){
    var msgElement = $('#typeMsg');
    var message = '';
    var status = '';

    if($(this).val() == ''){
        status = 'wrong';
        message = 'This is a required field. You need to select a collection type.';
    }
    else{
        status = 'okay';
        message = 'This type is valid.';
    }

    assignValidationMessages(msgElement, message, status);
});

function assignValidationMessages(msgElement, message, status){
    msgElement.html(message);
    msgElement.attr('status', status);

    if(status == 'okay'){
        msgElement.addClass('status-msg').removeClass('error-msg');
        msgElement.transition('pulse');
    }
    else{
        msgElement.removeClass('status-msg').addClass('error-msg');
        msgElement.transition('bounce');
    }

    validateSendForm();
}