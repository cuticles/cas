function validateEditCollForm(){
    var count = 0;
    var changes = 0;
    $('.status-check').each(function(){
        if($(this).attr('status') == 'okay')
            count += 1;
        if($(this).attr('changed') == 'true')
            changes += 1;
    });

    if(changes > 0){
        if(count == 4){
            $('#editCollBtn').removeClass('disabled');
        }
        else{
            $('#editCollBtn').addClass('disabled');
        }
        $('#changesStatus').empty();
    }
    else{
        $('#changesStatus').html('You need to have at least one change to edit this outsider.').transition('pulse');
        $('#editCollBtn').addClass('disabled');
    }
    console.log('Count: '+count+' Changes: '+changes);
}

//Check if inputs are changed
function checkForOldValues(input, msgElement){
    if(input.val() == input.attr('old-value')){
        msgElement.attr('changed', 'false');
    }
    else{
        msgElement.attr('changed', 'true');
    }
    validateEditCollForm();
}

$('#cancelEditColl').click(function(){
    $('.status-check').each(function(){
        $(this).empty().attr('status', 'wrong');
    });
});

$('#editCollTitle').focusout(function(){
    var msgElement = $('#titleMsg');
    var message = '';
    var status = '';

    if($(this).val() == ''){
        status = 'wrong';
        message = 'This is a required field. You need to fill this in to change the collection title.';
    }
    else{
        status = 'okay';
        message = 'This is a valid title.';
    }

    checkForOldValues($(this), msgElement);

    assignValidationMsgsEditColl(msgElement, message, status);
});

$('#editCollType').change(function(){
    var msgElement = $('#editTypeMsg');
    var message = '';
    var status = '';

    if($(this).val() == ''){
        status = 'wrong';
        message = 'This is a required field. You need to choose a collection type.';
    }
    else{
        status = 'okay';
        message = 'This is a valid type.';
    }

    checkForOldValues($(this), msgElement);

    assignValidationMsgsEditColl(msgElement, message, status);
});

$('#editCollDescription').change(function(){
    checkForOldValues($(this), $('#descriptionMsgEdit'));
});

$('#editCollTags').change(function(){
    checkForOldValues($(this), $('#tagsMsgEdit'));
});

function assignValidationMsgsEditColl(msgElement, message, status){
    msgElement.html(message);
    msgElement.attr('status', status);

    if(status == 'okay'){
        msgElement.addClass('status-msg').removeClass('error-msg');
        msgElement.transition('pulse');
    }
    else{
        msgElement.removeClass('status-msg').addClass('error-msg');
        msgElement.transition('bounce');
    }

    validateEditCollForm();
}