function validateAddOutsidersForm(mode){
	var okays = 0;
	var changes = 0;


	if(mode == 'edit'){
		$('.status-edit').each(function(){
	        if($(this).attr('status') == 'okay'){
	            okays += 1;
        	if($(this).attr('changed') == 'true')
        	 	changes += 1;
	        }
	    });
	}
	else{
		$('.status-add').each(function(){
			if($(this).attr('status') == 'okay'){
	            okays += 1;
	        }
		});
	}

    if(mode == 'edit'){
    	if(changes > 0){
	    	if(okays == 2)
	    		$('#pointsEditBtn').removeClass('disabled');
	    	else
	    		$('#pointsEditBtn').addClass('disabled');

	    	$('#changesStatus').empty();
	    }
	    else{
	    	$('#changesStatus').html('You need to have at least one change to edit this credential point.').transition('pulse');
	    	$('#pointsEditBtn').addClass('disabled');
	    }
    }
    else{
    	if(okays == 2)
    		$('#pointsAddBtn').removeClass('disabled');
    	else
    		$('#pointsAddBtn').addClass('disabled');
    }
    console.log(okays);
}

//Assign value for each report messages
function assignValidationMessage(msgElement, message, status, mode){
    msgElement.html(message);
    msgElement.attr('status', status);

    if(status == 'okay'){
        msgElement.addClass('status-msg').removeClass('error-msg');
        msgElement.transition('pulse');
    }
    else{
        msgElement.removeClass('status-msg').addClass('error-msg');
        msgElement.transition('bounce');
    }

    validateAddOutsidersForm(mode);
}

//Check if inputs are changed
function checkForOldValues(input, msgElement){
	if(input.val() == input.attr('old')){
		msgElement.attr('changed', 'false');
	}
	else{
		msgElement.attr('changed', 'true');
	}
}

//Perform validation if name input is changed
$('.points-name').change(function(){
	var mode = $(this).attr('mode');
	var msgElement = $('#namePointsMsg-'+mode);
	var status = '';
	var message = '';

	if($(this).val() == ''){
		status = 'wrong';
        message = 'This is a required field. You need to enter a name (e.g. Publication).';
	}
	else{
		status = 'okay';
		message = "This is a valid name.";
	}

	if(mode == 'edit')
		checkForOldValues($(this), msgElement);
	
	assignValidationMessage(msgElement, message, status, mode);
});

//Perform validation if name input is changed
$('.points-value').change(function(){
	var mode = $(this).attr('mode');
	var msgElement = $('#valuePointsMsg-'+mode);
	var status = '';
	var message = '';

	if($(this).val() == ''){
		status = 'wrong';
        message = 'This is a required field. You need to enter a number (e.g. 12, 13.75, 6.1).';
	}
	else{
		if(/^[0-9]+(\.[0-9]+)?$/.test($(this).val())){
			status = 'okay';
			message = "This is a valid value.";
		}
		else{
			status = 'wrong';
			message = "This is an invalid value. You should enter an exact number (12.3, 17.9, 6).";
		}
	}

	if(mode == 'edit')
		checkForOldValues($(this), msgElement);
	
	assignValidationMessage(msgElement, message, status, mode);
});

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)){
        return false;
    }
    return true;
}