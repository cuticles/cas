function validateAddOutsidersForm(mode){
	var okays = 0;
	var changes = 0;


	if(mode == 'edit'){
		$('.status-edit').each(function(){
	        if($(this).attr('status') == 'okay')
	            okays += 1;
        	if($(this).attr('changed') == 'true')
        	 	changes += 1;
	    });
	}
	else{
		$('.status-add').each(function(){
			if($(this).attr('status') == 'okay'){
	            okays += 1;
	        }
		});
	}

    if(mode == 'edit'){
    	if(changes > 0){
	    	if(okays == 2)
	    		$('#typesEditBtn').removeClass('disabled');
	    	else
	    		$('#typesEditBtn').addClass('disabled');

	    	$('#changesStatus').empty();
	    }
	    else{
	    	$('#changesStatus').html('You need to have at least one change to edit this type.').transition('pulse');
	    	$('#typesEditBtn').addClass('disabled');
	    }
    }
    else{
    	if(okays == 2)
    		$('#typesAddBtn').removeClass('disabled');
    	else
    		$('#typesAddBtn').addClass('disabled');
    }
    console.log(okays);
}

//Assign value for each report messages
function assignValidationMessage(msgElement, message, status, mode){
    msgElement.html(message);
    msgElement.attr('status', status);

    if(status == 'okay'){
        msgElement.addClass('status-msg').removeClass('error-msg');
        msgElement.transition('pulse');
    }
    else{
        msgElement.removeClass('status-msg').addClass('error-msg');
        msgElement.transition('bounce');
    }

    validateAddOutsidersForm(mode);
}

//Check if inputs are changed
function checkForOldValues(input, msgElement){
	if(input.val() == input.attr('old')){
		msgElement.attr('changed', 'false');
	}
	else{
		msgElement.attr('changed', 'true');
	}
}

//Perform validation if name input is changed
$('.types-name').change(function(){
	var mode = $(this).attr('mode');
	var msgElement = $('#nameTypesMsg-'+mode);
	var status = '';
	var message = '';

	if($(this).val() == ''){
		status = 'wrong';
        message = 'This is a required field. You need to enter a name for this type.';
	}
	else{
		status = 'okay';
		message = "This is a valid name.";
	}

	if(mode == 'edit')
		checkForOldValues($(this), msgElement);
	
	assignValidationMessage(msgElement, message, status, mode);
});

//Perform validation if name input is changed
$('.types-category').change(function(){
	var mode = $(this).attr('mode');
	var msgElement = $('#categoryTypesMsg-'+mode);
	var status = '';
	var message = '';

	if($(this).val() == ''){
		status = 'wrong';
        message = 'This is a required field. You need to choose a category.';
	}
	else{
		status = 'okay';
		message = "This is a valid category.";
	}

	if(mode == 'edit')
		checkForOldValues($(this), msgElement);
	
	assignValidationMessage(msgElement, message, status, mode);
});