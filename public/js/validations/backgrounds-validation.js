function validateBackgroundForms(mode){
	var okays = 0;
	var changes = 0;


	if(mode == 'add'){
		if($('#defaultTextEducLevel').html().trim() == 'Tertiary'){
			$('.status-check-inc-degree-add').each(function(){
		        if($(this).attr('status') == 'okay')
		            okays += 1;
		    });
		}
		else{
			$('.status-check-bg-add').each(function(){
		        if($(this).attr('status') == 'okay')
		            okays += 1;
		    });
		}
	}
	else{
		if($('#defaultTextEducLevel-edit').html().trim() == 'Tertiary'){
			$('.status-check-inc-degree-edit').each(function(){
		        if($(this).attr('status') == 'okay')
		            okays += 1;
		        if($(this).attr('changed') == 'true')
		        	changes += 1;
		    });
		}
		else{
			$('.status-check-bg-edit').each(function(){
		        if($(this).attr('status') == 'okay')
		            okays += 1;
		        if($(this).attr('changed') == 'true')
		        	changes += 1;
		    });
		}
	}

    if(mode == 'edit'){
    	if(changes > 0){
    		console.log('In edit mode: '+okays);
	    	if($('#defaultTextEducLevel-edit').html().trim() == 'Tertiary'){
				if(okays == 6)
					$('#editBackgroundBtn').removeClass('disabled');
				else
					$('#editBackgroundBtn').addClass('disabled');
			}
			else{
				if(okays == 4)
					$('#editBackgroundBtn').removeClass('disabled');
				else
					$('#editBackgroundBtn').addClass('disabled');
			}

	    	$('#changesStatus').empty();
	    }
	    else{
	    	$('#changesStatus').html('You need to have at least one change to edit this outsider.').transition('pulse');
	    	$('#editBackgroundBtn').addClass('disabled');
	    }
    }
    else{
    	if($('#defaultTextEducLevel').html().trim() == 'Tertiary'){
			if(okays == 7)
				$('#addBackgroundBtn').removeClass('disabled');
			else
				$('#addBackgroundBtn').addClass('disabled');
		}
		else{
			if(okays == 5)
				$('#addBackgroundBtn').removeClass('disabled');
			else
				$('#addBackgroundBtn').addClass('disabled');
		}
    }
    console.log(okays);
}

//Assign value for each report messages
function assignValidationMessage(msgElement, message, status, mode){
    msgElement.html(message);
    msgElement.attr('status', status);

    if(status == 'okay'){
        msgElement.addClass('status-msg').removeClass('error-msg');
        msgElement.transition('pulse');
    }
    else{
        msgElement.removeClass('status-msg').addClass('error-msg');
        msgElement.transition('bounce');
    }

    validateBackgroundForms(mode);
}

//Check if inputs are changed
function checkForOldValues(input, msgElement){
	if(input.val() == input.attr('old'))
		msgElement.attr('changed', 'false');
	else
		msgElement.attr('changed', 'true');
}

//Validate intervals for years
function validateYearInterval(mode){
	if(mode == 'add'){
		if(($('#yearEndInput').val().length > 0) && ($('#yearStartInput').val().length > 0)){
			var startVal = parseInt($('#yearStartInput').val());
			var endVal = parseInt($('#yearEndInput').val());

			if(endVal < startVal){
				$('#wrong-interval-add').html('Wrong year interval. End year should be greater than start year (e.g. 2012-2013).')
				.transition('bounce');
				$('#yearStartMsg-add').attr('status', 'wrong');
				$('#yearEndMsg-add').attr('status', 'wrong');
			}
			else if(endVal === startVal){
				$('#wrong-interval-add').html('Start and end years should never be equal.')
				.transition('bounce');
				$('#yearStartMsg-add').attr('status', 'wrong');
				$('#yearEndMsg-add').attr('status', 'wrong');
			}
			else{
        		$('#wrong-interval-add').empty();
        		$('#yearStartMsg-add').attr('status', 'okay');
			}
		}
		else if($('#yearEndInput').val().length <= 0){
    		$('#wrong-interval-add').empty();
    		if($('#yearStartInput').val().length > 0){
    			$('#yearStartMsg-add').attr('status', 'okay');
    		}
		}
	}
	else{
		if(($('#yearEndInput-edit').val().length > 0) && ($('#yearStartInput-edit').val().length > 0)){
			var startVal = parseInt($('#yearStartInput-edit').val());
			var endVal = parseInt($('#yearEndInput-edit').val());

			if(endVal < startVal){
				$('#wrong-interval-edit').html('Wrong year interval. End year should be greater than start year (e.g. 2012-2013).')
				.transition('bounce');
				$('#yearStartMsg-edit').attr('status', 'wrong');
				$('#yearEndMsg-edit').attr('status', 'wrong');
			}
			else if(endVal === startVal){
				$('#wrong-interval-edit').html('Start and end years should never be equal.')
				.transition('bounce');
				$('#yearStartMsg-edit').attr('status', 'wrong');
				$('#yearEndMsg-edit').attr('status', 'wrong');
			}
			else{
        		$('#wrong-interval-edit').empty();
        		$('#yearStartMsg-edit').attr('status', 'okay');
			}
		}
		else if($('#yearEndInput-edit').val().length <= 0){
    		$('#wrong-interval-edit').empty();
    		if($('#yearStartInput-edit').val().length > 0){
    			$('#yearStartMsg-edit').attr('status', 'okay');
    		}
		}
	}
}

//Perform validation if school input is changed
$('#supervisor').change(function(){
	var mode = 'add';
	var msgElement = $('#supervisorMsg');
	var status = '';
	var message = '';

	if($(this).val() == ''){
		status = 'wrong';
        message = 'This is a required field. You need to enter a supervisor.';
	}
	else{
		status = 'okay';
		message = "This is a valid supervisor.";
	}
	
	assignValidationMessage(msgElement, message, status, mode);
});

//Perform validation if school input is changed
$('.bg-school').change(function(){
	var mode = $(this).attr('mode');
	var msgElement = $('#schoolMsg-'+mode);
	var status = '';
	var message = '';

	if($(this).val() == ''){
		status = 'wrong';
        message = 'This is a required field. You need to enter a school\'s name (e.g. St. John Academy).';
	}
	else{
		status = 'okay';
		message = "This is a valid school name.";
	}

	if(mode == 'edit')
		checkForOldValues($(this), msgElement);
	
	assignValidationMessage(msgElement, message, status, mode);
});

//Perform validation if name input is changed
$('.bg-year-start').change(function(){
	var mode = $(this).attr('mode');
	var msgElement = $('#yearStartMsg-'+mode);
	var status = '';
	var message = '';

	if($(this).val() == ''){
		status = 'wrong';
        message = 'This is a required field. You need to enter a year.';
	}
	else if($(this).val() == 0 || $(this).val() > 2019){
		status = 'wrong';
        message = 'Invalid year. Please enter a year greater than 0 or not greater than the current year.';
	}
	else{
		status = 'okay';
		message = "This is a valid year.";
	}

	//Validating intervals
	validateYearInterval(mode);

	if(mode == 'edit')
		checkForOldValues($(this), msgElement);
	
	assignValidationMessage(msgElement, message, status, mode);
});

$('.bg-year-end').change(function(){
	var mode = $(this).attr('mode');
	var msgElement = $('#yearEndMsg-'+mode);
	var status = '';
	var message = '';

	if($(this).val() == ''){
		status = 'okay';
        message = '';
	}
	else if($(this).val() == 0 || $(this).val() > 2019){
		status = 'wrong';
        message = 'Invalid year. Please enter a year greater than 0 or not greater than the current year.';
	}
	else{
		status = 'okay';
		message = "This is a valid year.";
	}

	//Validating intervals
	validateYearInterval(mode);

	if(mode == 'edit')
		checkForOldValues($(this), msgElement);
	
	assignValidationMessage(msgElement, message, status, mode);
});

$('.bg-educ-level').change(function(){
	var mode = $(this).attr('mode');
	var msgElement = $('#educLevelMsg-'+mode);
	var status = '';
	var message = '';

	if($(this).val() == ''){
		status = 'wrong';
        message = 'This is a required field. You need to choose an educational level.';
	}
	else{
		status = 'okay';
		message = "This is a valid educational level.";

		//Validating intervals
		validateYearInterval(mode);
	}

	if(mode == 'edit')
		checkForOldValues($(this), msgElement);
	
	assignValidationMessage(msgElement, message, status, mode);
});

$('.bg-degree-level').change(function(){
	var mode = $(this).attr('mode');
	var msgElement = $('#degreeLevelMsg-'+mode);
	var status = '';
	var message = '';

	if($(this).val() == ''){
		status = 'wrong';
        message = 'This is a required field. You need to choose a degree level.';
	}
	else{
		status = 'okay';
		message = "This is a valid degree level.";
	}

	if(mode == 'edit')
		checkForOldValues($(this), msgElement);
	
	assignValidationMessage(msgElement, message, status, mode);
});

$('.bg-course').change(function(){
	var mode = $(this).attr('mode');
	var msgElement = $('#courseMsg-'+mode);
	var status = '';
	var message = '';

	if($(this).val() == ''){
		status = 'wrong';
        message = 'This is a required field. You need to choose a degree level.';
	}
	else{
		status = 'okay';
		message = "This is a valid degree level.";
	}

	if(mode == 'edit')
		checkForOldValues($(this), msgElement);
	
	assignValidationMessage(msgElement, message, status, mode);
});

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (/*charCode != 46 && */charCode > 31 && (charCode < 48 || charCode > 57)){
        return false;
    }
    return true;
}