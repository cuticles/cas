function validateAddOutsidersForm(mode){
	var okays = 0;
	var changes = 0;


	if(mode == 'edit'){
		$('.status-edit').each(function(){
	        if($(this).attr('status') == 'okay'){
	            okays += 1;
        	if($(this).attr('changed') == 'true')
        	 	changes += 1;
	        }
	    });
	}
	else{
		$('.status-add').each(function(){
			if($(this).attr('status') == 'okay'){
	            okays += 1;
	        }
		});
	}

    if(mode == 'edit'){
    	if(changes > 0){
	    	if(okays == 2)
	    		$('#positionsEditBtn').removeClass('disabled');
	    	else
	    		$('#positionsEditBtn').addClass('disabled');

	    	$('#changesStatus').empty();
	    }
	    else{
	    	$('#changesStatus').html('You need to have at least one change to edit this position.').transition('pulse');
	    	$('#positionsEditBtn').addClass('disabled');
	    }
    }
    else{
    	if(okays == 2)
    		$('#positionsAddBtn').removeClass('disabled');
    	else
    		$('#positionsAddBtn').addClass('disabled');
    }
    console.log(okays);
}

//Assign value for each report messages
function assignValidationMessage(msgElement, message, status, mode){
    msgElement.html(message);
    msgElement.attr('status', status);

    if(status == 'okay'){
        msgElement.addClass('status-msg').removeClass('error-msg');
        msgElement.transition('pulse');
    }
    else{
        msgElement.removeClass('status-msg').addClass('error-msg');
        msgElement.transition('bounce');
    }

    validateAddOutsidersForm(mode);
}

//Check if inputs are changed
function checkForOldValues(input, msgElement){
	if(input.val() == input.attr('old')){
		msgElement.attr('changed', 'false');
	}
	else{
		msgElement.attr('changed', 'true');
	}
}

//Perform validation if name input is changed
$('.positions-name').change(function(){
	var mode = $(this).attr('mode');
	var msgElement = $('#namePositionsMsg-'+mode);
	var status = '';
	var message = '';

	if($(this).val() == ''){
		status = 'wrong';
        message = 'This is a required field. You need to enter a name (e.g. Senior Executive).';
	}
	else{
		status = 'okay';
		message = "This is a valid name.";
	}

	if(mode == 'edit')
		checkForOldValues($(this), msgElement);
	
	assignValidationMessage(msgElement, message, status, mode);
});

//Perform validation if name input is changed
$('.positions-category').change(function(){
	var mode = $(this).attr('mode');
	var msgElement = $('#categoryPositionsMsg-'+mode);
	var status = '';
	var message = '';

	if($(this).val() == ''){
		status = 'wrong';
        message = 'This is a required field. You need to choose a category.';
	}
	else{
		status = 'okay';
		message = "This is a valid category.";
	}

	if(mode == 'edit')
		checkForOldValues($(this), msgElement);
	
	assignValidationMessage(msgElement, message, status, mode);
});