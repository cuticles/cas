function validateAddOutsider(mode){
    var okays = 0;

    if($('#outsiderLocation').val() == 'others'){
        $('.status-check-include-new-loc').each(function(){
            if($(this).attr('status') == 'okay'){
                okays += 1;
            }
        });
    }
    else{
        $('.status-check-outsider-add').each(function(){
            if($(this).attr('status') == 'okay'){
                okays += 1;
            }
        });
    }

    if($('#outsiderLocation').val() == 'others'){
        if(okays == 3)
            $('#addOutsider').removeClass('disabled');
        else
            $('#addOutsider').addClass('disabled');
    }
    else{
        if(okays == 2)
            $('#addOutsider').removeClass('disabled');
        else
            $('#addOutsider').addClass('disabled');
    }
}

//Assign value for each report messages
function assignValidationMessageOutsider(msgElement, message, status){
    msgElement.html(message);
    msgElement.attr('status', status);

    if(status == 'okay'){
        msgElement.addClass('status-msg').removeClass('error-msg');
        msgElement.transition('pulse');
    }
    else{
        msgElement.removeClass('status-msg').addClass('error-msg');
        msgElement.transition('bounce');
    }

    validateAddOutsider();
}

//Perform validation if name input is changed
$('#outsiderName').change(function(){
    var msgElement = $('#outsiderNameMsg');
    var status = '';
    var message = '';

    if($(this).val() == ''){
        status = 'wrong';
        message = 'This is a required field. You need to enter a name (e.g. Maria Leonora Theresa).';
    }
    else{
        status = 'okay';
        message = "This is a valid name.";
    }
    
    assignValidationMessageOutsider(msgElement, message, status);
});

//Perform validation if location input is changed
$('#outsiderLocation').change(function(){
    var msgElement = $('#outsiderLocationMsg');
    var status = '';
    var message = '';

    if($(this).val() == ''){
        status = 'wrong';
        message = 'This is a required field. You need to choose a location.';
    }
    else{
        status = 'okay';
        message = "This is a valid location.";
    }
    
    assignValidationMessageOutsider(msgElement, message, status);
});

//Perform validation if location input is changed
$('#newLocation').change(function(){
    var msgElement = $('#outsiderNewLocMsg');
    var status = '';
    var message = '';

    if($(this).val() == ''){
        status = 'wrong';
        message = 'This is a required field. You need to enter name of new location (e.g. School of Technology).';
    }
    else{
        status = 'okay';
        message = "This is a valid location name.";
    }
    
    assignValidationMessageOutsider(msgElement, message, status);
});











function validateUpdateStatusForm(){
	var okays = 0;
	var changes = 0;


	if($('#updateMethod').val() == 'outside'){
		$('.status-inc-outsiders').each(function(){
	        if($(this).attr('status') == 'okay')
	        	okays += 1;
	    });
	}
	else{
		$('.status-check-update').each(function(){
			if($(this).attr('status') == 'okay')
	            okays += 1;
		});
	}

    if($('#updateMethod').val() == 'outside'){
		if(okays == 4)
			$('#updateStatusModalBtn').removeClass('disabled');
		else
			$('#updateStatusModalBtn').addClass('disabled');
	}
	else{
		if(okays == 3)
			$('#updateStatusModalBtn').removeClass('disabled');
		else
			$('#updateStatusModalBtn').addClass('disabled');
	}
}

//Assign value for each report messages
function assignValidationMessage(msgElement, message, status){
    msgElement.html(message);
    msgElement.attr('status', status);

    if(status == 'okay'){
        msgElement.addClass('status-msg').removeClass('error-msg');
        msgElement.transition('pulse');
    }
    else{
        msgElement.removeClass('status-msg').addClass('error-msg');
        msgElement.transition('bounce');
    }

    validateUpdateStatusForm();
}

function disableSomeFields(kind){
	if(kind == 'status-change'){
		if($('#defaultTextNewStatus').html().trim() == 'Received'){
			$('#attachmentField').addClass('disabled');
			$('#attachmentInput').removeAttr('required');
		}
		else{
			$('#attachmentField').removeClass('disabled');
			$('#attachmentInput').attr('required', 'true');
		}
	}
	else if(kind == 'update-method'){
		if($('#updateMethod').val() == 'me'){
			$('#actingPersonnel').addClass('disabled');
		}
		else{
			$('#actingPersonnel').removeClass('disabled');
		}
	}
}

//Perform validation if name input is changed
$('#newStatusInput').change(function(){
	var msgElement = $('#statusMsg');
	var status = '';
	var message = '';

	if($(this).val() == ''){
		status = 'wrong';
        message = 'This is a required field. You need to choose a new status.';
	}
	else{
		status = 'okay';
		message = '';
	}

	disableSomeFields('status-change');
	
	assignValidationMessage(msgElement, message, status);
});

//Perform validation if name input is changed
$('#updateMethod').change(function(){
	var msgElement = $('#updateMethodMsg');
	var status = '';
	var message = '';

	if($(this).val() == ''){
		status = 'wrong';
        message = 'This is a required field. You need to choose on how you want to update this collection.';
	}
	else{
		status = 'okay';
		message = '';
	}

	disableSomeFields('update-method');

	assignValidationMessage(msgElement, message, status);
});

$('#outsiderInput').change(function(){
	var msgElement = $('#updateOutsiderMsg');
	var status = '';
	var message = '';

	if($(this).val() == ''){
		status = 'wrong';
        message = 'This is a required field. You need to choose an outsider.';
	}
	else{
		status = 'okay';
		message = '';
	}

	assignValidationMessage(msgElement, message, status);
});

$('#locationInput').change(function(){
	var msgElement = $('#updateLocationMsg');
	var status = '';
	var message = '';

	if($(this).val() == ''){
		status = 'wrong';
        message = 'This is a required field. You need to choose a location.';
	}
	else{
		status = 'okay';
		message = '';
	}

	assignValidationMessage(msgElement, message, status);
});