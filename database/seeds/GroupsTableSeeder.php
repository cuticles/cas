<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Group;

class GroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Group::create([
            'supervisor' => User::where('username', 'joserizal')->first()->id,
            'name' => 'CAS Dean\'s Office',
            'description' => 'Located at CAS basement',
            'cover' => 'images/stillschooloffice.jpg',
        ]);

        Group::create([
            'supervisor' => User::where('username', 'joserizal')->first()->id,
            'name' => 'DPSM Faculty',
            'description' => 'Located at CAS basement',
            'cover' => 'images/stillschooloffice.jpg',
        ]);
    }
}
