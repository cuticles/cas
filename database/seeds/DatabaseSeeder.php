<?php
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Outsider;
use App\Type;
use App\Location;
use App\Position;
use App\Point;

use Faker\Factory as Faker;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cred_points = ['Research' => 10.9, 'Public Service' => 6.5, 'Academics' => 15.7];
        $levels = ['primary', 'secondary', 'tertiary'];
        $tracking_types = ['attachment', 'update', 'forward', 'initial'];
        $staffs = ['Administrative Officer I', 'Administrative Officer II', 'Administrative Officer III', 'Administrative Officer IV', 'Administrative Officer V', 'Student Records Officer I', 'Student Records Officer II', 'Administrative Assistant', 'Administrative Aid I', 'Administrative Aid II', 'Administrative Aid III', 'Administrative Aid IV', 'Administrative Aid V', 'Administrative Aid VI', 'Laboratory Technician', 'University Research Associate'];
        $faculties = ['Professor', 'Associate Professor', 'Assistant Professor', 'Instructor I', 'Instructor II', 'Instructor III', 'Instructor IV', 'Instructor V', 'Lecturer'];
        $collection_types = ['request', 'report', 'certificate'];
        $document_status_types = ['pending', 'approved', 'endorsed', 'recommended', 'disapproved', 'noted', 'received'];
        $locations = ['CAS Dean\'s Office', 'Office of the College Secretary', 'Administration Building', 'Department of Chemistry', 'Division of Physical Sciences and Mathematics', 'Division of Humanities', 'Division of Social Sciences', 'School of Technology', 'College of Fisheries and Ocean Sciences', 'Coca-Cola Building', 'College of Arts and Sciences'];

        foreach($staffs as $staff){
            Position::create([
                'name' => $staff,
                'category' => '2',
                'user_id' => 21
            ]);
        }

        foreach($faculties as $faculty){
            Position::create([
                'name' => $faculty,
                'category' => '1',
                'user_id' => 21
            ]);
        }

        foreach($cred_points as $name => $score){
            Point::create([
                'name' => $name,
                'value' => $score,
                'user_id' => 21
            ]);
        }

        foreach($levels as $level){
            Type::create([
                'name' => $level,
                'category' => '4',
                'user_id' => 21
            ]);
        }

        foreach($document_status_types as $type){
            Type::create([
                'name' => $type,
                'category' => '3',
                'user_id' => 21

            ]);
        }

        foreach($tracking_types as $type){
            Type::create([
                'name' => $type,
                'category' => '2',
                'user_id' => 1
            ]);
        } 

        foreach($collection_types as $type){
            Type::create([
                'name' => $type,
                'category' => '1',
                'user_id' => 1
            ]);
        }

        foreach($locations as $location){
            Location::create([
                'name' => $location,
                'user_id' => 1
            ]);
        }

        Outsider::create([
            'name' => 'Coca-Cola Company',
            'location_id' => Location::where('name', 'Coca-Cola Building')->first()->id,
            'user_id' => 1
        ]);

        Outsider::create([
            'name' => 'College of Arts and Sciences',
            'location_id' => Location::where('name', 'College of Arts and Sciences')->first()->id,
            'user_id' => 1
        ]);

        $faker = Faker::create('en_PH');
        foreach (range(1, 50) as $index) {
            User::create([
                'firstname' => $faker->randomElement(['Blake', 'Jane','Jose','Paul','Mary', 'Perfecto','Juan','Julio','Arnold','Jessica', 'Carlos', 'Dianna','Bruce','Jet','Jackie','Kevin','Sarah', 'Melissa', 'Exaltacion','Luzviminda', 'Epifanio','Euphemia','Jason']),
                'surname' => $faker->randomElement(['Lively', 'Doe','Rizal','Abad','Santos','Cruz','Inocencio','Flavio','Santillan','Romualdez','Zubiri','Garcia', 'Dizon','Daza','Diaz', 'Chan', 'Lee', 'Statham','Jenner','Jobs','Streisand']),
                'middlename' => $faker->randomElement(['Silvestre', 'Mead','Protacio','Stallone','Schwarzenegger','Gray','Momoa','Romulo','Ayala','Zobel','Noble','Nulada', 'Magtanggol', 'Pamulaklakin']),
                'sex' => $faker->randomElement(['Male','Female']),
                'birthdate' => $faker->date($format = 'Y-m-d', $max = 'now'),
                'username' => strtolower($faker->unique()->firstname),
                'address' => $faker->address,
                'position_id' => Position::where('name', $faker->randomElement($staffs))->first()->id,
                'email' => $faker->email,
                'is_admin' =>'0',
                'division' => Location::where('name', $faker->randomElement($locations))->first()->id,
                'profile_pic' => $faker->randomElement(['images/blake.jpeg', 'images/ate-gurl.png','images/jose-rizal.jpg','images/girl.png','images/wonder-woman.png','images/boy.png','images/man.png']),
                'password' => Hash::make('secret'),
            ]);
        }

        $this->call(UsersTableSeeder::class);
        $this->call(GroupsTableSeeder::class);
        $this->call(MembershipsTableSeeder::class);
    }    
}
