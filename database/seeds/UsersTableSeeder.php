<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Location;
use App\Position;
use App\Collection;
use App\Transmit;
use App\Document;
use App\Tracking;
use App\Type;
use Illuminate\Support\Str;

use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'firstname' => 'Jose' ,
            'surname' => 'Alonzo',
            'middlename' => 'Rizal',
            'sex' => 'Male',
            'birthdate' => '1998-11-17',
            'username' => 'joserizal',
            'address' =>'Barotac Nuevo, Iloilo',
            'mobile_num' => '09123456789',
            'telephone_num' => '315-6789',
            'civil_status' => 'Married',
            'position_id' => Position::where('name', 'University Research Associate')->first()->id,
            'email' => 'joserizal@gmail.com',
            'division' => Location::where('name', 'CAS Dean\'s Office')->first()->id,
            'is_admin' => '1',
            'profile_pic' => 'images/jose-rizal.jpg',
            'password' => Hash::make('secret'),
        ]);

        User::create([
            'firstname' => 'John' ,
            'surname' => 'Doe',
            'middlename' => 'Alter',
            'sex' => 'Male',
            'birthdate' => '1998-12-25',
            'username' => 'johndoe',
            'address' =>'San Jose, Antique',
            'mobile_num' => '09123456789',
            'telephone_num' => '315-6789',
            'civil_status' => 'Single',
            'position_id' => Position::where('name', 'University Research Associate')->first()->id,
            'email' => 'johndoe@gmail.com',
            'division' => Location::where('name', 'CAS Dean\'s Office')->first()->id,
            'is_admin' => '1',
            'profile_pic' => 'images/man.png',
            'password' => Hash::make('secret'),
        ]);

        User::create([
            'firstname' => 'Blake',
            'middlename' => 'Masipag',
            'surname' => 'Lively',
            'sex' => 'Female',
            'birthdate' => '1987-11-17',
            'username' => 'blakelively',
            'address' =>'Barotac Nuevo, Iloilo',
            'position_id' => Position::where('name', 'Professor')->first()->id,
            'email' => 'blakelively@up.edu.ph',
            'division' => Location::where('name', 'Administration Building')->first()->id,
            'is_admin' => '1',
            'profile_pic' => 'images/blake.jpeg',
            'password' => Hash::make('secret'),
        ]);

        $collection_types = ['request', 'report', 'certificate'];
        $user = User::where('username', 'joserizal')->first();
        $receiver = User::first();
        $faker = Faker::create('en_PH');
        foreach (range(1, 100) as $index){
            $coll = new Collection;
            $coll->code = Str::random(10);
            $coll->title = $faker->unique()->catchPhrase;
            $coll->description = $faker->unique()->paragraph;
            $coll->status_id = Type::where('name', 'pending')->first()->id;
            $coll->type_id = Type::where('name', $faker->randomElement($collection_types))->first()->id;
            $coll->tags = 'document,collection,cas';
            $coll->save();

            $track = new Tracking;
            $track->user_updater = $user->id;
            $track->collection_id = $coll->id;
            $track->type_id = Type::where('name', 'initial')->first()->id;
            $track->status_id = Type::where('name', 'pending')->first()->id;
            $track->location_id = $receiver->location->id;
            $track->save();

            $ext = $faker->randomElement(['pdf', 'doc', 'docx', 'pptx', 'xls', 'xlsx', 'zip', 'rar', 'ppt', 'txt']);
            $randCode = Str::random(5);
            $filename = $faker->word.'-'.$randCode.'.'.$ext;
            $doc = new Document;
            $doc->collection_id = $coll->id;
            $doc->tracking_id = $track->id;
            $doc->file_name = $filename;
            $doc->file_size = 670567.0;
            $doc->file_path = '/uploads/'.$filename;
            $doc->file_extension = $ext;
            $doc->status_id = Type::where('name', 'pending')->first()->id;
            $doc->save();

            $trans = new Transmit;
            $trans->from_user_id = $user->id;
            $trans->to_user_id = $receiver->id;
            $trans->collection_id = $coll->id;
            $trans->save();
        }
    }
}
