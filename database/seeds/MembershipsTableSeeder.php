<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Membership;

class MembershipsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('memberships')->insert([
            'group_id' => 1,
            'user_id' => User::where('username', 'joserizal')->first()->id,
        ]);

        DB::table('memberships')->insert([
            'group_id' => 2,
            'user_id' => User::where('username', 'joserizal')->first()->id,
        ]);
    }
}
