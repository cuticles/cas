<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransmitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transmits', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('collection_id');
            $table->integer('from_user_id')->nullable();
            $table->integer('to_user_id')->nullable();
            $table->integer('outsider_id')->nullable();
            $table->enum('is_forward', ['0', '1'])->default('0');
            $table->integer('uploader')->nullable();
            $table->enum('from_outside', ['0', '1'])->default('0');
            $table->enum('to_outside', ['0', '1'])->default('0');
            $table->enum('trashed_by_sender', ['0', '1'])->default('0');
            $table->enum('trashed_by_receiver', ['0', '1'])->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transmits');
    }
}
