<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrackingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trackings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_updater');
            $table->integer('collection_id');
            $table->integer('type_id');
            $table->integer('location_id')->nullable();
            $table->integer('outsider_id')->nullable();
            $table->enum('from_outside', ['0', '1'])->default('0');
            $table->enum('to_outside', ['0', '1'])->default('0');
            $table->integer('status_id');
            $table->integer('is_step')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trackings');
    }
}
