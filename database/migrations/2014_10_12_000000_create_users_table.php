<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username', 32);
            $table->enum('sex', ['male', 'female'])->nullable();
            $table->date('birthdate')->nullable();
            $table->string('address')->nullable();
            $table->string('firstname');
            $table->string('middlename');
            $table->string('surname');
            $table->string('mobile_num')->nullable();
            $table->string('telephone_num')->nullable();
            $table->string('email')->unique();
            $table->string('password');
            $table->float('salary')->nullable();
            $table->string('civil_status')->nullable();
            $table->integer('division')->nullable();
            $table->integer('position_id')->nullable();
            $table->string('profile_pic')->nullable();
            $table->integer('is_admin');
            $table->enum('is_deleted', ['0', '1'])->default('0');
            $table->timestamp('contractAlert')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
