<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('collection_id')->nullable();
            $table->integer('credential_id')->nullable();
            $table->integer('tracking_id')->nullable();
            $table->string('file_name');
            $table->float('file_size');
            $table->enum('file_extension', ['pdf', 'doc', 'docx', 'pptx', 'xls', 'xlsx', 'zip', 'rar', 'ppt', 'txt']);
            $table->string('file_path');
            $table->integer('status_id')->nullable();
            $table->enum('is_credential_doc', ['0', '1'])->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documents');
    }
}
