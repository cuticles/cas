<?php

use Faker\Generator as Faker;
use Illuminate\Support\Facades\Hash;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    return [
		'firstname' => $faker->randomElement(['Blake', 'Jane','Jose','Paul']),
        'surname' => $faker->randomElement(['Lively', 'Doe','Rizal']),
        'middlename' => $faker->randomElement(['Silvestre', 'Mead','Protacio']),
        'sex' => $faker->randomElement(['male','female']),
        'birthdate' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'username' => lower($faker->unique()->firstname),
        'address' => $faker->address,
        'designation' => $faker->randomElement(['Staff','Instructor','Professor']),
        'email' => $faker->email,
        'is_admin' =>'0',
        'profile_pic' => $faker->randomElement(['images/blake.jpeg', 'images/ate-gurl.png','images/jose-rizal.jpg','images/girl.png','images/wonder-woman.png','images/boy.png','images/man.png']),
        'password' => Hash::make('secret'),
    ];
});
